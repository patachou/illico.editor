# Illico Editor

## Illico, Kesaco ?

Illico est

* un **couteau suisse de la qualification de données**
* destiné aux experts métiers (et non uniquement à des informaticiens)


## Vous voulez découvrir Illico ?

Vous trouverez la dernière version stable

 * sur le **site officiel** : <https://illico.ti-nuage.fr>
 * fournie **clé en main** : ne nécessite aucune installation particulière
    * dézippez/décompressez puis ouvrez *index.html* dans votre navigateur favori


## Que dit-on sur Illico ?

Illico a été présenté sur *Linuxfr.org* et *Framalibre*.

### Linuxfr.org

* les dépêches :

   * **2017** : https://linuxfr.org/news/oui-illico
   * **2018** : https://linuxfr.org/news/illico-editor-retrospective-2017-2018
   * **2021** : https://linuxfr.org/news/illico-editor-nouveautes-depuis-2019
   * **2025** : https://linuxfr.org/news/illico-editor-nouveautes-depuis-2021

* la discussion : http://linuxfr.org/forums/general-general/posts/analyse-de-donnees

### Framalibre

* la fiche de présentation : https://framalibre.org/content/illico-editor


- - -

# Participer

## Vous voulez contribuer ?

1. sur le code source : HTML, JS, CSS
2. sur la documentation ou les tutoriels


## sur le code source : HTML, JS, CSS

Les modifications HTML ou JS peuvent être testée directement.

Seules les modifications du CSS nécessitent de re-générer des fichiers (cf. point suivant).

### dépendances

*aucune*

### générer

Génération de fichiers CSS nécessaires pour certaines fonctionnalités d'export (historique, ouvrir dans un nouvel onglet).

```
$ cd illico.editor
$ cd sh
$ bash ./menu.sh
# choix n°3
```


## sur la documentation ou les tutoriels

La documentation et les tutoriels sont générés via Sphinx (format reStructuredText) dans les formats :

  * HTML
  * PDF

### installation de l'environnement Sphinx

  * sphinx <http://sphinx-doc.org/>

```
# dépendance spécifique Debian/Ubuntu
$ sudo apt install python3-venv
# environnement virtuel Python
$ python3 -m venv .venv
$ source .venv/bin/activate
(.venv) $ python3 -m pip install pip -U
(.venv) $ python3 -m pip install sphinx -U
```

### dépendances Unix pour actualiser les compteurs

La section **architecture** de la documentation présente des statistiques sur le code.

  * nombre de ligne de code, de commentaires, etc.
  * nombre de fonctions, etc.

environnement Unix

  * column

```
$ sudo apt install util-linux
```

### générer au format HTML la documentation et les tutoriels

#### installer le thème graphique Piccolo

  * piccolo-theme (thème graphique HTML)

```
(.venv) $ python3 -m pip install piccolo-theme -U
```

#### "compiler" la documentation

Seulement la documentation

```
$ (.venv) cd illico.editor
$ (.venv) cd doc
$ (.venv) make html
```

Seulement les tutoriels

```
$ (.venv) cd illico.editor
$ (.venv) cd tuto
$ (.venv) make html
```

Il convient parfois de supprimer le répertoire /build avant de générer

```
$ (.venv) make clean
$ (.venv) make html
```

En une action, la combinaison de suppression (x2) + génération documentation et tutoriels :

```
$ (.venv) cd illico.editor
$ (.venv) cd sh
$ (.venv) bash ./menu.sh
# choix n°4
```

En plus de générer au formation HTML, le choix n°4 actualise également les compteurs (nombre de lignes de code, nombre de fonctions, etc.).


## sur la documentation ou les tutoriels, au format PDF

Documentation et tutoriels sont également fournis au format PDF.

### installation de l'environnement Sphinx

cf. plus haut

### dépendances environnement LaTeX

  * latexmk
  * LaTeX : texlive texlive-lang-french texlive-latex-extra

```
$ sudo apt install latexmk texlive texlive-lang-french texlive-latex-extra
```

### générer au format PDF la documentation les tutoriels

Seulement la documentation

```
$ (.venv) cd illico.editor
$ (.venv) cd doc
$ (.venv) make latexpdf
```

Seulement les tutoriels

```
$ (.venv) cd illico.editor
$ (.venv) cd tuto
$ (.venv) make latexpdf
```

Il convient parfois de supprimer le répertoire /build

```
$ (.venv) make clean
$ (.venv) make latexpdf
```


## pour packager l'archive compressée à déployer sur le site Web officiel (livraison d'une nouvelle version)

Le site Web officiel regroupe à la fois l'application, la documentation, le tutoriel.

Ainsi, livrer une nouvelle version --- i.e. générer une nouvelle image du site Web officiel --- se résume à l'enchaînement des actions suivantes :

* nettoyer des générations précédentes
* actualiser des compteurs
* générer la documentation et les tutoriels dans les formats HTML et PDF
* compresser l'ensemble

### installation de l'environnement Sphinx

cf. plus haut

#### installer le thème graphique Piccolo

cf. plus haut

### installer les dépendances

  * Unix : column (cf. plus haut)
  * LaTeX (cf. plus haut) 

### dépendances Unix pour compresser

  * zip

```
$ sudo apt install zip
```

### générer une image complète à déployer sur le site Web officiel

```
$ (.venv) cd sh
$ (.venv) bash ./menu.sh
# choix n°5
```

- - -

