Versions 2025
==============

09 mars : 25.03-2
~~~~~~~~~~~~~~~~~~

**Corrections de bug**

bugs majeurs

(merci @Harknisouf)

  * TEMPS -> calculer une durée (jours) entre 2 dates : correction pour l'identification des jours ouvrés (lundi à vendredi)


bug mineur

  * EXPORT CSV -> horodatage du fichier (temps local, bug induit par la correction UTC version 25.03-1)


02 mars : 25.03-1
~~~~~~~~~~~~~~~~~~

**Corrections de bug**

bugs majeurs

(merci @Harknisouf)

  * CRÉER UN JEU DE DONNÉES -> créer un calendrier : neutralisation de l'effet "fuseau horaire" dans la conversion de la date en jour (lundi, mardi...) (calculs sur date UTC)
  * MATRICE -> trier par ordre chronologique : (bug pas constaté) (application même correctif calculs sur date UTC) pour l'option "les valeurs vides sont égales à la date du jour"
  * TEMPS -> donner le jour de la semaine : neutralisation de l'effet "fuseau horaire" dans la conversion de la date en jour (lundi, mardi...) (calculs sur date UTC)
  * TEMPS -> compter chacun des jours de la semaine : idem
  * TEMPS -> obtenir le numéro du jour dans l'année : idem


bugs mineurs

  * CRÉER UN JEU DE DONNÉES -> créer une liste avec les noms de plusieurs fichiers : affiche un message d'erreur si aucun fichier n'a été sélectionné
  * CRÉER UN JEU DE DONNÉES -> créer des identifiants (liste numérique) : affiche un message d'erreur si la description (pas, premier, limite/max) n'est pas correctement décrite


**Rubrique**

  * déplacement de INTERVALLES plus haut dans le catalogue (juste après CALCULS)


22 février : 25.02-4
~~~~~~~~~~~~~~~~~~~~~

**Nouveauté**

  * VALEURS EN LISTE : NUMÉROTER

     * numéroter les valeurs


**Nouvelle rubrique et renommage**

  * LIGNES : NUMÉROTER regroupe

     * numéroter -> numéroter chaque ligne
     * numéroter chaque série -> numéroter chaque série de lignes
     * numéroter dans chaque série -> numéroter les lignes de chaque série


**Technique**

  * renommage des identifiants internes *lignes_1 à 3* -> *lig_numero_1 à 3*


15 février : 25.02-3
~~~~~~~~~~~~~~~~~~~~~

Prise en charge des préconisations et contributions de Xavier (cf. gitlab)

**Correction**

  * résolution d'un effet de bord en lien avec les titres des écrans des paramètres, affiche désormais complètement le haut de l'écran des paramètres


**Interface graphique**

  * modification du bandeau supérieur : bouton *accueil* désormais à droite, bouton *réduire l'affichage* désormais tout à gauche


**Tutoriel**

  * adaptation, corrections et reformulations pour une meilleure compréhension


09 février : 25.02-2
~~~~~~~~~~~~~~~~~~~~~

(merci @Xavier)

**Correction**

  * dans le volet latéral, les clics dans la partie blanche/vide ou dans les titres de regroupement ne provoquent plus de page blanche sur la partie centrale


**Ergonomie**

  * changement du curseur sur le volet latéral afin d'indiquer qu'il s'agit d'un catalogue et non d'une information figée
  * ajout d'un titre au-dessus de l'écran des paramètres de chaque transformation


07 février : 25.02-1
~~~~~~~~~~~~~~~~~~~~~

**Nouveauté**

  * AGRÉGATS -> mesurer la longueur des valeurs d'une colonne


**Renommage**

  * ENRICHISSEMENT -> convertir d'un système de numération à un autre => changer le système de numération


**Documentation**

  * amélioration de la présentation des exemples pour les transformations de valeurs en liste
  * suppression de la page de décompte des versions 2013-2022


**Tutoriel**

  * adaptation de l'introduction, corrections et reformulations pour une meilleure compréhension (merci @Xavier)


24 janvier : 25.01-2
~~~~~~~~~~~~~~~~~~~~~

**Documentation**

  * aide sur les expressions rationnelles : exemple pour découper une liste de valeurs


**Hébergement**

  * le site Web officiel est désormais hébergé sur `https://illico.ti-nuage.fr <https://illico.ti-nuage.fr>`__

Un immense merci à l'équipe de TuxFamily pour l'hébergement du site depuis 2014.


03 janvier : 25.01-1
~~~~~~~~~~~~~~~~~~~~~

**Nouveautés**

  * VALEURS EN LISTE : ENRICHISSEMENT -> lister les permutations des valeurs d'une liste
  * VALEURS EN LISTE : ENRICHISSEMENT -> mélanger les valeurs de la liste
  * TEMPS -> obtenir le numéro du jour dans l'année
  * ENRICHISSEMENT -> convertir d'un système de numération à un autre


**Tutoriel**

  * créer autant de lignes que d'évènements (produit cartésien) : approche alternative


Bonne et heureuse année 2025 !

