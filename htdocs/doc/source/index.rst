.. Illico Editor documentation master file, created by
   sphinx-quickstart on Sun Dec 16 14:06:37 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Table des matières
===================


Introduction
-------------


.. toctree::
   :maxdepth: 1

   intro
   concepts


Documentation utilisateur
--------------------------

.. toctree::
   :maxdepth: 1
   :numbered: 2

   fct/principes
   fct/charger
   fct/evaluer
   fct/journal
   fct/val
   fct/remplacer_si
   fct/val_liste
   fct/val_liste_numero
   fct/val_liste_compar
   fct/val_liste_agregat
   fct/val_liste_structure
   fct/val_liste_filtres
   fct/val_liste_enrichissement
   fct/trad_liste
   fct/en_tete
   fct/col
   fct/col_ordre
   fct/col_compar
   fct/lig
   fct/lig_numero
   fct/agregat
   fct/temps
   fct/temps_interv
   fct/calcul
   fct/interv
   fct/struct
   fct/emphase
   fct/filtre
   fct/riche
   fct/matrice
   fct/pref
   dev/compatibilite


Complément indépendant d'Illico
--------------------------------

.. toctree::
   :maxdepth: 1

   memo/regex


Documentation développeur
--------------------------

.. toctree::
   :maxdepth: 1

   dev/archi
   dev/code_principal

..
   interne uniquement ?

   dev/cycle


Versions précédentes
---------------------

.. toctree::
   :maxdepth: 1

   versions/2025
   versions/2024
   versions/2023
