Mémo sur les expressions rationnelles
######################################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

À quoi servent que les expressions rationnelles ?
==================================================

Les expressions rationnelles -- ou *regular expression (regex)* -- sont des motifs de correspondance.
Elles permettent de tester une correspondance ou de sélectionner une partie d'un texte pour le modifier. 
Elles sont si efficaces qu'on les trouve jusque dans LibreOffice et OpenOffice (ce qui vous donne un bon
terrain de jeu).

Un exemple astucieux
--------------------

Vous avez récupéré un texte sur internet, mais il a été copié avec le code html. Comment l'enlever ? Une 
toute petite regex fait l'affaire : ``<[^>]+>`` C'est obscur n'est-ce pas ? On dirait des insultes dans
un dialogue de BD. Cette regex trouve le code html. Celui-ci est toujours encadré par ``<`` et ``>``, ce 
qui est "incompréhensible" c'est ``[^>]+``. Les crochets indiquent un ensemble de caractères, le signe 
plus dit qu'on cherche ce type de caractère au moins une fois. Entre les crochets on a simplement écrit 
qu'on veut tout sauf ``^`` le caractère ``>``. Autrement dit, cette regex se comprend 

*un ``<`` suivi d'au moins un caractère différent de ``>`` suivi d'un ``>``*

Vous voyez qu'elles sont puissantes et concises. Le revers c'est d'être difficiles à comprendre pour un 
humain. Toutefois, un peu de pratique suffit pour "parler" un langage regex suffisant.

On peut bâtir des regex compliquées, genre charabia, effrayantes au début, pour identifier avec certitude 
des choses un peu complexes (comme les adresses courriel), vous trouverez beaucoup d'exemples sur internet. 
Le site https://regexr.com/ en regorge et vous permet de mettre au point votre propre regex. Habituez-
vous à les utiliser dans LibreOffice ou OpenOffice en imaginant des motifs simples.

Des courts exemples simples
---------------------------

* Trouver les espaces répétés ``\s\s+`` (s comme space)
* Trouver les espaces devant des virgules ``\s+,``
* Trouver les points non suivis d'une espace sans tomber sur les points de suspension ``\.[^\s\.]`` (le caractère ``.`` est échappé, voir plus bas)
* Chercher les signes qui doivent être précédés d'espaces insécables ``\s*[;:!?"%€]`` (``\s*`` dit un ou zéro espace)
* Trouver les mots ``\s[A-Za-z]+\s+`` (des caractères minuscules ou majuscules de A à Z)
* Trouver les nombres ``[0-9]+``

..


.. source : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/RegExp


+-----------------------------+---------------------------------------------------------------+
| motif                       | signification                                                 |
+=============================+===============================================================+
| ``.``                       | n'importe quel caractère (sauf ``\n`` et ``\r``)              |
+-----------------------------+---------------------------------------------------------------+
| ``a?b*c+`` ou               | a répété 0 ou 1 fois, b 0...n fois, c au moins 1 fois         |
| ``a{0,1}b{0,}c{1,}``        |                                                               |
+-----------------------------+---------------------------------------------------------------+
| ``a??b*?c+?d{1,3}?``        | avec ``?``, les plus courtes correspondances seront préférées |
+-----------------------------+---------------------------------------------------------------+
| ``^a.*b$``                  | commence par a et se termine par b (ab, alkjb...)             |
+-----------------------------+---------------------------------------------------------------+
|  ``(.*)``                   | le contenu qui correspond au motif entre parenthèses          |
|                             | peut être réutilisé avec ``\1`` dans la recherche             |
|                             | et avec ``$1`` dans les résultats : on parle de **capture**   |
+-----------------------------+---------------------------------------------------------------+
|  ``a(?=b)``                 | a, uniquement s'il est suivi par b                            |
+-----------------------------+---------------------------------------------------------------+
|  ``a(?!b)``                 | a, uniquement s'il n'est pas suivi par b                      |
+-----------------------------+---------------------------------------------------------------+
| ``eleve(\d).+poste\1``      | valide "eleve3 au poste3" mais pas "eleve5 au poste3"         |
+-----------------------------+---------------------------------------------------------------+
| ``[abc][^def]``             | une lettre parmi a,b,c et une lettre différente de d,e,f      |
+-----------------------------+---------------------------------------------------------------+
| ``(aa|bb)(?!cc)``           | aa ou bb, suivi de tout sauf cc                               |
+-----------------------------+---------------------------------------------------------------+
| ``\bAGE\b``                 | le mot "AGE" (limite de mot) ;                                |
|                             | ANTI-AGE sera reconnu, BAGAGE et ADAGE non                    |
+-----------------------------+---------------------------------------------------------------+
| ``\d`` ou ``[0-9]``         | n'importe quel chiffre                                        |
+-----------------------------+---------------------------------------------------------------+
| ``\w`` ou ``[A-Za-z0-9_]``  | n'importe quel caractère (alphanumérique)                     |
+-----------------------------+---------------------------------------------------------------+
| ``\s`` ou ``[\r \n \f \t]`` | n'importe quel "espace" (espace, tabulation...)               |
+-----------------------------+---------------------------------------------------------------+
| ``\D \W \S \B``             | ensembles complémentaires à ``\d \w \s \b``                   |
+-----------------------------+---------------------------------------------------------------+
| ``\? \* \. \\ \^ \$ \( \)`` | les caractères ``? * . \ ^ $ ( )``                            |
|                             |                                                               |
|                             | le caractère ``\`` force/annule l'interprétation              |
|                             | du caractère qui suit juste après                             |
+-----------------------------+---------------------------------------------------------------+
| ``^(.*):(?!\1)$``           | couple de valeurs différentes avant et après ``:``            |
+-----------------------------+---------------------------------------------------------------+
| ``.*(\(.*?\)).*``           | la chaîne entre parenthèses, place la sélection dans ``$1``   |
+-----------------------------+---------------------------------------------------------------+
| ``.*(\[.*?\]).*``           | la chaîne entre crochets, place la sélection dans ``$1``      |
+-----------------------------+---------------------------------------------------------------+
| ``^([A-Z \-']*) (.*)$``     | dans une liste de "NOM Prénom",                               |
|                             | ``$1`` sélectionne les noms, ``$2`` les prénoms               |
|                             | (fonctionne pour des noms et prénoms composés)                |
+-----------------------------+---------------------------------------------------------------+
| ``(\d+)/(\d+)/(\d{4})``     | découpe une date au format ``DD/MM/AAAA``                     |
|                             |                                                               |
|                             | ``$3-$2-$1`` convertira au format ``AAAA-MM-DD``              |
+-----------------------------+---------------------------------------------------------------+
| ([^|]+)\|([^|]+)\|?([^|]+)? | découpe une donnée au format ``xx|yy`` ou ``xx|yy|zz`` et     |
|                             | place la sélection dans ``$1``, ``$2`` et optionnel ``$3`` ;  |
|                             |                                                               |
|                             | ``$1 + $2 = $3`` convertira en ``xx + yy`` et ``x + y = z``   |
+-----------------------------+---------------------------------------------------------------+
| ``\u00a0``                  | espace insécable                                              |
+-----------------------------+---------------------------------------------------------------+
