Transformations d'intervalle
#############################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

compléter un intervalle
========================

Pour deux colonnes décrivant les bornes de début et fin des intervalles, complète ces intervalles avec toutes les valeurs intermédiaires comprises entre les bornes.

.. important:: Les bornes des intervalles sont des nombres positifs ou négatifs, entiers ou décimaux.

.. important:: Le pas doit être un **nombre positif**.

.. important:: Les résultats sont tronqués à 2 chiffres après la virgule.

**paramètres**

  * colonnes de début et fin
  * pas : par défaut vaut 1
  * séparateur

.. note:: L'en-tête de la colonne résultat rappellera les intitulés des colonnes décrivant les bornes.


**exemple**

  * pas = 2
  * séparateur = ``,``

+-------+------+---------------------------+
| début | fin  | [ début ; fin ] - complet |
+=======+======+===========================+
| 2013  | 2018 | 2013,2015,2017            |
+-------+------+---------------------------+
| 2011  | 2011 | 2011                      |
+-------+------+---------------------------+
|       |      |                           |
+-------+------+---------------------------+
| 2009  |      | 2009                      |
+-------+------+---------------------------+


rechercher une valeur dans un intervalle
=========================================

Pour deux colonnes décrivant les bornes de début et fin des intervalles, indique si la valeur d'une autre colonne est située avant, après ou dans cet intervalle.

.. important:: La valeur recherchée et les bornes des intervalles sont des **nombres positifs ou négatifs** (entiers ou décimaux).

.. note:: L'en-tête de la colonne résultat rappellera les intitulés de la valeur recherchée et des colonnes décrivant les bornes.


**exemple**

+-------+------+---------+---------------------------+
| début | fin  | valeur  | [ début ; fin ] ? valeur  |
+=======+======+=========+===========================+
| 2013  | 2018 | 2015    | incluse                   |
+-------+------+---------+---------------------------+
| 2011  | 2011 | 2011    | incluse                   |
+-------+------+---------+---------------------------+
| 2007  | 2011 | 2015    | supérieure                |
+-------+------+---------+---------------------------+
| 2006  | 2011 | 2003    | inférieure                |
+-------+------+---------+---------------------------+
|       | 2015 |         | interv. non-borné         |
+-------+------+---------+---------------------------+
| 2009  | 2007 | 2008    | interv. incorrect         |
+-------+------+---------+---------------------------+


combiner deux intervalles
==========================

Pour quatre colonnes décrivant les bornes de début et de fin de deux intervalles, calcule

  * soit la fusion : intervalle englobant les deux [min, max]
  * soit l'union : intervalle englobant les deux si intersection
  * soit l'intersection : plus petit intervalle commun

.. important:: Les bornes des intervalles sont des **nombres positifs ou négatifs** (entiers ou décimaux).

.. note:: L'en-tête de la colonne résultat rappellera les intitulés des colonnes décrivant les deux intervalles ainsi que l'opération : intersection ou union.

.. tip:: En cas d'intervalles non-bornés ou disjoints (union/intersection), la colonne résultat indiquera l'anomalie.

**exemple**

La comparaison étant insensible aux espaces, l'exemple illustre ici une comparaison de périodes où les dates sont au format ``SSAA MM JJ``.

option : plus petit intervalle commun (intersection)

+------------+------------+------------+------------+--------------------------------------------+
|  début 1   |  fin 1     | début 2    |   fin 2    | [ début 1 ; fin 1 ] ?i [ début 2 ; fin 2 ] |
+============+============+============+============+============================================+
| 2019 01 01 | 2019 03 01 | 2019 02 01 | 2019 03 01 |   2019 02 01 à 2019 03 01                  |
+------------+------------+------------+------------+--------------------------------------------+
| 2019 01 01 | 2019 03 01 | 2019 02 01 |            | interv. 2 non-borné                        |
+------------+------------+------------+------------+--------------------------------------------+
| 2019 01 01 |            | 2019 02 01 |            | interv. 1 et 2 non-bornés                  |
+------------+------------+------------+------------+--------------------------------------------+
| 2019 01 01 | 2019 03 01 | 2018 02 01 | 2018 03 01 | interv. disjoints                          |
+------------+------------+------------+------------+--------------------------------------------+


centrer et réduire
===================

Ajoute une nouvelle colonne en début de tableau avec le résultat de la *standardisation* --- *centrer et réduire* --- des valeurs d'une colonne.

**paramètres**

  * centrer et réduire dans intervalle : [0, 1] ou [-1, 1]

.. important:: Les valeurs sont des **nombres positifs ou négatifs** (entiers ou décimaux).

.. important:: Pour une valeur contenant un nombre et une unité (cm, kg, etc.),
               la partie numérique est prise en compte pour l'opération **si l'unité est placée à la fin**.


comparer valeurs et seuils
===========================

Ajoute une nouvelle colonne en début de tableau avec le résultat de la comparaison entre les valeurs d'une colonne et une suite de seuils.

.. important:: Les bornes des intervalles sont des **nombres positifs ou négatifs** (entiers ou décimaux).

**paramètres**

  * opérateur de comparaison

      * supérieur/inférieur à
      * strictement supérieur/inférieur à

  * liste de seuils (1 par ligne)

**exemple**

  * liste de seuils : 50, 80, 100

+--------+---------------+--------------+
|   v    | v > {seuils}  | v < {seuils} |
+========+===============+==============+
|  40    |               |    50        |
+--------+---------------+--------------+
|  60    | 50            |    80        |
+--------+---------------+--------------+
|  90    | 80            |    100       |
+--------+---------------+--------------+
| 110    |  100          |              |
+--------+---------------+--------------+
