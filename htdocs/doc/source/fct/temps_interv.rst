Transformations d'intervalle de temps
######################################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

compléter un intervalle de date
================================

Pour deux colonnes décrivant les dates de début et fin d'un intervalle, complète ces intervalles avec toutes les valeurs intermédiaires comprises entre les bornes.

.. note:: Lorsqu'une des dates est vide, le résultat indique *donnée manquante* et précise la colonne en défaut.

.. note:: Lorsque deux dates sont renseignées mais qu'une des dates est mal formatée, le résultat indique *format incorrect* et précise la colonne en défaut.

.. note:: Lorsque l'intervalle est inversé ou trop court --- du jour J pour le même jour, dernier jour de l'intervalle exclu --- le résultat l'indique pour la ligne concernée.

**paramètres**

format de la date

  * ``jj mm ssaa``
  * ``mm jj ssaa``
  * ``ssaa mm jj``

séparateur

  * ``.``
  * ``-``
  * ``/``
  * (espace)

.. tip:: exemple de combinaison pour obtenir ``ssaa/mm/jj``

          * ``ssaa mm jj``
          * ``/``

le dernier jour de l'intervalle

  * est exclu
  * est inclus

.. note:: L'en-tête de la colonne résultat rappellera les intitulés des colonnes décrivant la période (les bornes).


recherche une date dans un intervalle de date
==============================================

Pour deux colonnes décrivant les dates de début et fin d'un intervalle, confirme/infirme si la date lue dans une troisième colonne est contenue dans l'intervalle.

.. note:: Lorsqu'une des dates est vide, le résultat indique *donnée manquante* et précise la colonne en défaut.

.. note:: Lorsque deux dates sont renseignées mais qu'une des dates est mal formatée, le résultat indique *format incorrect* et précise la colonne en défaut.

.. note:: Lorsque l'intervalle est inversé ou trop court --- du jour J pour le même jour, dernier jour de l'intervalle exclu --- le résultat l'indique pour la ligne concernée.

**paramètres**

format de la date

  * ``jj mm ssaa``
  * ``mm jj ssaa``
  * ``ssaa mm jj``

séparateur

  * ``.``
  * ``-``
  * ``/``
  * (espace)

.. tip:: exemple de combinaison pour obtenir ``ssaa/mm/jj``

          * ``ssaa mm jj``
          * ``/``

le dernier jour de l'intervalle

  * est exclu
  * est inclus

.. note:: L'en-tête de la colonne résultat rappellera les intitulés des colonnes décrivant la période (les bornes).


combiner deux périodes
=======================

Pour quatre colonnes décrivant les bornes de début et de fin de deux périodes, calcule

  * soit la fusion : période englobant les deux [min, max]
  * soit l'union : période englobant les deux seulement si intersection
  * soit l'intersection : plus petite période commune

.. note:: L'en-tête de la colonne résultat rappellera les intitulés des colonnes décrivant les deux périodes ainsi que l'opération : intersection ou union.

.. note:: Lorsque deux dates sont renseignées mais qu'une des dates est mal formatée, le résultat indique *format incorrect* et précise la colonne en défaut.

.. note:: Lorsque l'intervalle est inversé ou trop court --- du jour J pour le même jour, dernier jour de l'intervalle exclu --- le résultat l'indique pour la ligne concernée.

.. tip:: En cas de périodes non-bornées ou disjointes (union/intersection), la colonne résultat indiquera l'anomalie.

**exemple**

option : plus petit intervalle commun (intersection)

+------------+------------+------------+------------+--------------------------------------------+
|  début 1   |  fin 1     | début 2    |   fin 2    | [ début 1 ; fin 1 ] ?i [ début 2 ; fin 2 ] |
+============+============+============+============+============================================+
| 2019-01-01 | 2019-03-01 | 2019-02-01 | 2019-03-01 |   2019-02-01 à 2019-03-01                  |
+------------+------------+------------+------------+--------------------------------------------+
| 2019-01-01 | 2019-03-01 | 2019-02-01 |            | période 2 non-bornée                       |
+------------+------------+------------+------------+--------------------------------------------+
| 2019-01-01 |            | 2019-02-01 |            | périodes 1 et 2 non-bornées                |
+------------+------------+------------+------------+--------------------------------------------+
| 2019-01-01 | 2019-03-01 | 2018-02-01 | 2018-03-01 | périodes disjointes                        |
+------------+------------+------------+------------+--------------------------------------------+


comparer les dates et une liste de seuils
==========================================

Ajoute une nouvelle colonne en début de tableau avec le résultat de la comparaison entre les dates d'une colonne et une suite de seuils.

.. note:: Lorsque une date est mal formatée, le résultat indique *format incorrect* et précise la valeur rejetée.

.. important:: Les bornes des intervalles sont des **dates**.

**paramètres**

  * format des dates (colonne comparée et seuils)
  * opérateur de comparaison

      * supérieur/inférieur à
      * strictement supérieur/inférieur à

  * liste de dates seuils (1 par ligne)


détecter des collisions de périodes
====================================

Ajoute une nouvelle colonne en début de tableau avec les valeurs de retour de la colonne vr 
lorsque les périodes définies par [c1, c2] (date de fin incluse) ou [c1, c2[ (date de fin exclue) 
entrent en conflit d'une ligne à l'autre, en regard soit dans la totalité des données, soit en regard d'une colonne servant d'identifiant ou d'identification d'une série

.. important:: Lorsque une date est mal formatée, la transformation ne s'exécute pas et un message d'erreur précise les données en anomalie.

**paramètres**

  * format des dates (colonnes décrivant les périodes)

le dernier jour de l'intervalle

  * est exclu
  * est inclus

règle d'application

  * sans condition (tout le jeu de données)
  * pour chaque série
  * pour chaque identifiant

si règle

  * valeur identifiante (si la règle d'application est liée à une condition) (colonne)

valeur à copier en première colonne

  * valeur de retour (colonne)
  * séparateur des valeurs de retour

.. note:: L'en-tête de la colonne résultat rappellera

         * la colonne utilisée dans la règle d'application (si condition)
         * *tout* (sans condition), *série* ou *identifiant*
         * intervalle fermé ``[début, fin]`` (fin incluse) ou ouvert ``[début, fin[`` (fin exclue)

**exemple : si collision, pour chaque série X retourner code**

+------------+------------+------------+------------+------------+
|  début 1   |  fin 1     |     X      |    code    | **retour** |
+============+============+============+============+============+
| 2019-01-01 | 2019-03-01 |    AA      |    XYZ     |   **245**  |
+------------+------------+------------+------------+------------+
| 2019-02-01 | 2019-04-01 |    AA      |    245     |   **XYZ**  |
+------------+------------+------------+------------+------------+
| 2019-04-01 | 2019-06-01 |    BB      |    679     |            |
+------------+------------+------------+------------+------------+
| 2019-01-01 | 2019-02-01 |    AA      |    BGC     |            |
+------------+------------+------------+------------+------------+
| 2018-01-01 | 2019-05-01 |    BB      |    JRC     |            |
+------------+------------+------------+------------+------------+


**exemple : si collision, pour chaque identifiant X retourner code (date de fin exclue)**

+------------+------------+------------+------------+------------------+
|  début 1   |  fin 1     |     X      |    code    |    **retour**    |
+============+============+============+============+==================+
| 2019-01-01 | 2019-03-01 |    AA      |    XYZ     |   **245 / BGC**  |
+------------+------------+------------+------------+------------------+
| 2019-02-01 | 2019-04-01 |    AA      |    245     |      **XYZ**     |
+------------+------------+------------+------------+------------------+
| 2019-04-01 | 2019-06-01 |    BB      |    679     |      **JRC**     |
+------------+------------+------------+------------+------------------+
| 2019-01-01 | 2019-02-01 |    AA      |    BGC     |      **XYZ**     |
+------------+------------+------------+------------+------------------+
| 2018-01-01 | 2019-05-01 |    BB      |    JRC     |      **679**     |
+------------+------------+------------+------------+------------------+
