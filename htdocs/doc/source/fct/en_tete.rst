Transformations de l'en-tête
#############################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

renommer l'en-tête
===================

Permet de renommer jusqu'à 6 en-têtes de colonne en une seule opération.


extraire l'en-tête
===================

Restitue l'en-tête sous la forme d'un tableau horizontal ou vertical.

**paramètres**

  * tableau horizontal/vertical


ajouter un préfixe
===================

Ajoute un préfixe aux en-têtes des colonnes sélectionnées

**paramètre**

  * préfixe à ajouter


remplace un préfixe
====================

remplace le préfixe des en-têtes des colonnes sélectionnées par un autre préfixe

**paramètres**

  * préfixe à remplacer
  * nouveau préfixe

enlever un préfixe
===================

Enlève le préfixe aux en-têtes des colonnes sélectionnées

**paramètre**

  * préfixe à enlever

enlever le délimiteur
======================

Enlève le délimiteur aux en-têtes des colonnes sélectionnées

**paramètre**

  * délimiteur à enlever

enlever les accents et les cédilles de l'en-tête
=================================================

Remplace tous les caractères accentués par leur équivalent sans accent.

::

    ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöø
    ÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž
