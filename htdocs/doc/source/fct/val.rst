Transformations de valeur
##########################

.. contents:: Contenu
   :local:   
   :backlinks: top 

-------

préfixer et/ou suffixer
========================

Ajoute un préfixe, suffixe ou les deux,
sur les données, en fonction de leur caractère inconsistant --- nul --- ou non.

**paramètres**

  * préfixe
  * suffixe
  * appliquer aux valeurs (choix unique)

     * non-nulles uniquement
     * nulles uniquement
     * toutes


CAPITALES
==========

Transforme tous les caractères en capitale d'imprimerie, y compris les caractères accentués.


minuscules
===========

Transforme tous les caractères en caractères minuscules, y compris les caractères accentués.


Majuscules
===========

Transforme tous les caractères en caractères minuscules. 
Selon l'option sélectionnée, transforme en majuscule le premier caractère du premier mot ou de tous les mots.

**paramètres**

  * à chaque mot : oui/non

.. note:: Cette transformation s'applique sur les caractères alphabétiques, y compris si la chaîne de texte commence par d'autres caractères (espaces, etc.).


compter les occurrences
========================

.. note:: Fonction d'analyse.

Pour une valeur donnée, compte le nombre de lignes où elle apparaît et la proportion d'apparition.


compléter par un caractère
===========================

Complète toutes les valeurs par un caractère, pour obtenir la longueur totale désirée, en remplissant à gauche ou à droite

.. note:: Les valeurs vides sont conservées à l'identique.


**paramètres**

  * caractère
  * longueur totale
  * direction de remplissage : gauche/droite


tronquer n caractères
======================

Tronque toutes les valeurs de n caractères, en partant de gauche ou de droite.

**paramètres**

  * nombre de caractères (à tronquer)
  * en partant de : gauche/droite


conserver n caractères
=======================

Conserve les n caractères de gauche ou de droite. Supprime le reste.

**paramètres**

  * nombre de caractères (à conserver)
  * en partant de : gauche/droite


traduire ou supprimer des valeurs
==================================

Remplace par une nouvelle valeur ou supprime toutes les occurrences de la valeur recherchée.

**paramètres**

  * valeur exacte (recherchée)
  * sera remplacée par (laissez vide pour supprimer)
