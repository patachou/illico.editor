Transformations de colonne
###########################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

supprimer ou conserver
=======================

Supprime ou conserve plusieurs colonnes en une seule opération.

**paramètre**

  * supprimer/conserver (les colonnes sélectionnées)


supprimer les colonnes vides
=============================

Supprime toutes les colonnes ne contenant que des espaces, tabulations et autres caractères d'espacement (saut de ligne, espaces insécables, retour-chariot...).


vider seulement les valeurs
============================

Réinitialise à vide le contenu de plusieurs colonnes en une seule opération.


ajouter une colonne
====================

Ajoute une nouvelle colonne (initialisée avec une valeur donnée) avant/après une colonne existante.

**paramètres**

  * avant/après (une colonne)
  * en-tête
  * valeur d'initialisation


copier une colonne
===================

Copie n'importe quelle colonne avant ou après celle-là.

**paramètres**

  * avant/après (une colonne)
  * en-tête


copier plusieurs colonnes
==========================

Copie les colonnes sélectionnées en début ou fin du tableau de données

**paramètre**

  * au début/à la fin


concaténer/fusionner
=====================

Ajoute une colonne en début du tableau avec le résultat de la concaténation des valeurs des colonnes et des séparateurs.

**paramètres**

  * en-tête (de la colonne résultat)
  * préfixe
  * suffixe
  * colonne 1 + séparateur 1-2 + colonne 2

     * \+ séparateur 2-3 + colonne 3
     * \+ séparateur 3-4 + colonne 4
     * ...


concaténer en boucle
=====================

Ajoute une colonne en début du tableau avec le résultat d'une concaténation de colonnes en intercalant un séparateur disponible parmi une liste circulaire de séparateurs.

**paramètres**

  * colonnes
  * liste circulaire de séparateurs (1 par ligne)

Les colonnes sont concaténées dans l'ordre de la sélection, en intercalant le premier séparateur de la liste, puis le deuxième, puis le troisième, etc.

Si la liste de séparateurs est épuisée, le séparateur sera de nouveau le premier puis le deuxième, etc.

.. warning:: Dans la liste des séparateurs, une ligne vide sera interprétée comme un séparateur de longueur 0 : les valeurs concaténées seront alors collées.


pivoter (construire une liste)
===============================

Ajoute une colonne en début du tableau contenant la liste de toutes les valeurs rencontrées pour une même valeur de la colonne pivot.

.. note:: Le nouvel en-tête rappelle le séparateur et entre ``{``, ``}`` le nom de la colonne dont sont issues les valeurs.

.. warning:: Une colonne n'est utilisable qu'une seule fois.

.. tip:: Pour créer plusieurs listes à partir d'une même colonne, copiez la colonne et sélectionnez chaque copie.

**paramètres**

  * pivot (colonne)
  * créer une liste de valeurs depuis (colonne)

     * séparateur des valeurs de la liste résultat

**original**

+----------+------------+-----------------+
| années   | individu   | action          |
+==========+============+=================+
| 2009     |   ABC      | formation       |
+----------+------------+-----------------+
| 2012     |   ABC      | formation       |
+----------+------------+-----------------+
| 2013     |   DEF      | formation       |
+----------+------------+-----------------+
| 2013     |   DEF      | certification   |
+----------+------------+-----------------+


**résultat**

  * pivot : colonne *action*
  * créer une liste de valeurs depuis : colonne *années*

    * délimitée par : ``,``

  * créer une liste de valeurs depuis : colonne *individus*

    * délimitée par : ``+``

+----------+------------+-----------------+------------------+-----------------+
| années   | individu   | action          | {années} ,       | {individus} +   |
+==========+============+=================+==================+=================+
| 2009     | ABC        | formation       | 2009,2012,2013   | ABC + ABC + DEF |
+----------+------------+-----------------+------------------+-----------------+
| 2012     | ABC        | formation       | 2009,2012,2013   | ABC + ABC + DEF |
+----------+------------+-----------------+------------------+-----------------+
| 2013     | DEF        | formation       | 2009,2012,2013   | ABC + ABC + DEF |
+----------+------------+-----------------+------------------+-----------------+
| 2013     | DEF        | certification   | 2013             |      DEF        |
+----------+------------+-----------------+------------------+-----------------+

.. tip:: Cette forme intermédiaire peut ensuite être filtrée pour obtenir une synthèse.

+-----------------+------------------+-----------------+
| action          | {années} ,       | {individus} +   |
+=================+==================+=================+
| formation       | 2009,2012,2013   | ABC + ABC + DEF |
+-----------------+------------------+-----------------+
| certification   | 2013             |      DEF        |
+-----------------+------------------+-----------------+

.. note:: cf. tutoriel : section *traiter les doublons*

.. note:: cf. documentation : section *LIGNES -> compacter les lignes*


transposer (groupes de colonnes)
=================================

Bascule des groupes de colonnes en autant de lignes que de groupes.

**paramètres**

  * premier groupe de colonnes
  * première colonne du deuxième groupe (la colonne la plus à gauche)
  * première colonne du dernier groupe (la colonne la plus à gauche)

.. note:: La définition des groupes est **souple** : le chevauchement, la définition de la droite à la gauche du tableau et les colonnes non-voisines sont autorisées.

.. important:: Les nouvelles colonnes reprennent les intitulés des en-têtes du premier groupe (préfixe **pivot #**).


**exemple 1**

+----+----+---+----+----+
| a1 | b1 | x | a2 | b2 |
+====+====+===+====+====+
|  1 |  2 | x | 3  | 4  |
+----+----+---+----+----+

On souhaite avoir une ligne pour le couple de valeur **a1, b1** et une nouvelle ligne avec le couple **a2, b2**.

  * sélection 1er groupe : a1, b1
  * début 2ème groupe : a2
  * début dernier groupe : a2


**résultat**

+------------+------------+-------+-------+---+-------+-------+
| pivot # a1 | pivot # b1 | a1    | b1    | x | a2    | b2    |
+============+============+=======+=======+===+=======+=======+
|   **1**    |    **2**   | ``1`` | ``2`` | x | 3     |   4   |
+------------+------------+-------+-------+---+-------+-------+
|   **3**    |    **4**   |  1    |  2    | x | ``3`` | ``4`` |
+------------+------------+-------+-------+---+-------+-------+

**exemple 2**

+----+----+---+----+----+
| a  | b  | c | d  | e  |
+====+====+===+====+====+
| 1  | 2  | 3 | 4  | 5  |
+----+----+---+----+----+

On souhaite avoir une ligne pour le couple de valeur **c, e**, une nouvelle ligne pour le couple **b, d**, une nouvelle ligne pour le couple **a, c**.

  * sélection 1er groupe : c, e
  * début 2ème groupe : b
  * début dernier groupe : a

+------------+------------+-------+-------+-------+-------+-------+
| pivot # c  | pivot # e  | a     | b     | c     | d     | e     |
+============+============+=======+=======+=======+=======+=======+
|   **3**    |   **5**    | 1     | 2     | ``3`` | 4     | ``5`` |
+------------+------------+-------+-------+-------+-------+-------+
|   **2**    |   **4**    | 1     | ``2`` | 3     | ``4`` | 5     |
+------------+------------+-------+-------+-------+-------+-------+
|   **1**    |   **3**    | ``1`` | 2     | ``3`` | 4     | 5     |
+------------+------------+-------+-------+-------+-------+-------+


projeter des valeurs sur d'autres colonnes
============================================

Pour la sélection de colonnes, projette, selon l'option

  * soit les valeurs précédemment lues sur la ligne vers les colonnes voisines vides
  * soit la dernière valeur de la ligne vers la dernière colonne de la sélection (à droite)


**paramètres**

  * sélection multiple de colonnes
  * action (choix unique)

     * projeter à droite,
     * remplir les "trous"


**original**

+-----+-----+-----+-----+
|  A  |  B  |  C  |  D  |
+=====+=====+=====+=====+
|  1  |  2  |  3  |     |
+-----+-----+-----+-----+
|  4  |  5  |     |     |
+-----+-----+-----+-----+
|  6  |     |  7  |     |
+-----+-----+-----+-----+
|  8  |     |     |     |
+-----+-----+-----+-----+
|     |     |     |  9  |
+-----+-----+-----+-----+

**exemple 1**

::

    sélection : colonnes
         - A
         - B
         - C
         - D

    action : projeter à droite

**résultat**

+-----+-----+-----+-------+
|  A  |  B  |  C  |   D   |
+=====+=====+=====+=======+
|  1  |  2  |  3  | **3** |
+-----+-----+-----+-------+
|  4  |  5  |     | **5** |
+-----+-----+-----+-------+
|  6  |     |  7  | **7** |
+-----+-----+-----+-------+
|  8  |     |     | **8** |
+-----+-----+-----+-------+
|     |     |     |   9   |
+-----+-----+-----+-------+

**exemple 2**

::

    sélection : colonnes
         - A
         - B
         - C
         - D

    action : remplir les "trous"

**résultat**

+-----+-------+-------+-------+
|  A  |  B    |  C    |   D   |
+=====+=======+=======+=======+
|  1  |  2    |  3    | **3** |
+-----+-------+-------+-------+
|  4  |  5    | **5** | **5** |
+-----+-------+-------+-------+
|  6  | **6** |  7    | **7** |
+-----+-------+-------+-------+
|  8  | **8** | **8** | **8** |
+-----+-------+-------+-------+
|     |       |       |   9   |
+-----+-------+-------+-------+


recréer une filiation (relation parent-enfant)
===============================================

Ajoute une nouvelle colonne contenant la filiation (noeud parent, grand-parent) pour chaque noeud enfant.

.. important:: Pour reconstituer une filiation de génération en génération, seul l'un des noeuds parents est pris en compte pour remonter de génération en génération.

**paramètres**

  * colonne décrivant les parents
  * colonne décrivant les enfants
  * séparateur des valeurs de la liste des ancêtres
  * filiation déterminée par :

     * le premier parent connu
     * le dernier parent connu
