Journal de bord
################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

Historisation des actions
==========================

Accessible dans le bandeau supérieur.

  | |bandeau supérieur|

Génère automatiquement la liste des actions exécutées --- *autres que les fonctions d'analyse*.

  * chaque ligne comporte

     * un numéro
     * le nom de l'action
     * les paramètres de la transformation
     * le message de résultat
     * éventuellement le ou les noms des fichiers CSV exportés au niveau de cette action

  * un liseré orange apparaît toutes les dix lignes
  * les actions...

    * ... de chargement des données sont indiquées en orange et en gras
    * ... annulées sont indiquées en rouge et suffixées de la mention **ANNULÉ**

.. important:: Seule la dernière action est annulable.

.. tip:: Le journal de bord se copie-colle dans un traitement de texte ou un tableur --- *Word, Excel, OpenOffice*.

.. |bandeau supérieur| image:: /_static/images/0/1.png
                                 :height: 22pt
