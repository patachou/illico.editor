Transformations de structure
#############################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

créer des identifiants
=======================

Remplace dans la colonne sélectionnée toutes les valeurs par un identifiant unique. Plusieurs occurrences de la même valeur auront le même identifiant.
Les identifiants négatifs et décimaux sont autorisés. L'incrémentation est fixée à ``+1``.

Produit la table de correspondance entre les valeurs et les identifiants.

.. note:: Il s'agit de la deuxième forme normale de la méthode Merise.


**paramètre**

  * premier numéro (par défaut vaut 1)


enlever les accents et les cédilles
====================================

Remplace tous les caractères accentués par leur équivalent sans accent.

::

    ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöø
    ÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž


remplacer les caractères non-alphanumériques
=============================================

Remplace les caractères non-alphanumériques par un caractère.

::

    ! "#%\'()*+,-./:;<=>?@[\]^_`{|}~§«°»–—’“„…&


::

    + espace insécable


**paramètre**

  * remplacer ces caractères par (caractère de substitution)


trouver les valeurs courtes/longues
====================================

.. note:: Fonction d'analyse.

Identifie les valeurs dont la longueur est supérieure ou inférieure à un seuil donné.

**paramètre**

  * seuil min/max


connaître le nombre de caractères
==================================

Ajoute une colonne en début de tableau avec le nombre de caractère des valeurs de la colonne sélectionnée.

**paramètres**

  * en-tête


supprimer le délimiteur
========================

Supprime le caractère délimiteur s'il se trouve à la fois en début et fin de la donnée.

**paramètres**

  * délimiteur (caractère)


enlever les espaces en début/fin
=================================

Supprime tous les espaces en début et fin de la donnée.


compacter les espaces consécutifs
==================================

Réduit les espaces consécutifs à un seul espace.


enlever les articles
=====================

Supprime tous les articles.

::

    d', l', un, une, du, de, des, le, la, les

.. note:: Les apostrophes dactylographiques (droites) et typographiques (courbes) sont prises en compte.
