Transformations d'emphase
##########################

.. contents:: Contenu
   :local:   
   :backlinks: top 

-------

principes
==========

La mise en forme s'effectue en injectant des balises HTML dans les données.

En copiant-collant ces données dans un traitement de texte ou un tableur, les balises HTML sont interprétées et restituent la mise en forme désirée.


activer l'interpréter les balises HTML
=======================================

Dans certains cas, les données contiennent du code HTML qui doit être conservé tel quel, sans être interprété.

L'activation/désactivation de l'interprétation des balises HTML s'effectue dans le menu **configurer -> export : tableau** (liste déroulante à gauche).


transformer
============

  * gras
  * italique
  * barré
  * augmenter/diminuer la taille
  * couleur [1]_
  * normal [2]_

.. [1] si le navigateur ne propose pas de *color picker*, le format hexadécimal RVB/RGB ou le nom de la couleur sont acceptés.

.. [2] suppression de toute mise en forme

.. tip:: par exemple pour utiliser plusieurs couleurs dans la même colonne de donnée, il faut dans un premier temps colorier une colonne, puis colorier l'autre et concaténer les deux.
