Réordonner les colonnes
########################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

réordonner
===========

Modifie l'ordre des colonnes du tableau.


permuter les colonnes
======================

Permute les colonnes sélectionnées.

**paramètres**

  * permuter (choix unique)

     * 1ère et 2ème, 3ème et 4ème, etc.
     * 1ère et dernière, 2ème et avant-dernière, etc.
