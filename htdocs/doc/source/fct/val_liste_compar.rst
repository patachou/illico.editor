Transformations de liste (comparaison)
#######################################

.. contents:: Contenu
   :local:
   :backlinks: top


tester la présence d'une valeur
================================

Ajoute une colonne en début de tableau et indique pour chaque ligne, si la valeur dans la colonne témoin existe dans la liste de valeurs.

**paramètres**

  * colonne de valeur témoin
  * liste de valeurs (colonne)

     * séparateur

**exemple**

+------------------+----------------+---------------------------+
| liste de valeurs | colonne témoin | résultat                  |
+==================+================+===========================+
| 2,4,3,2          |      3         | oui                       |
+------------------+----------------+---------------------------+
| 2,4,3,2          |      5         | non                       |
+------------------+----------------+---------------------------+
| 2,4,3,2          |                | valeur vide               |
+------------------+----------------+---------------------------+
|                  |      7         | liste vide                |
+------------------+----------------+---------------------------+
|                  |                | valeur vide et liste vide |
+------------------+----------------+---------------------------+


calculer le ratio de similarité entre 2 listes
===============================================

.. note:: *Le ratio de similarité* est déterminé par le nombre de valeurs communes entre 2 listes.

.. important:: La notion de *similarité*

               - ne tient pas compte des répétitions de valeurs,
               - est insensible à l'ordre des valeurs.

Ajoute une colonne au début du tableau avec le résultat du calcul de similarité entre les listes de données de deux colonnes.

Une option permet de spécifier si le résultat est *décimal* ou sous forme de *fraction*.

**paramètres**

  * séparateur (colonne 1)
  * séparateur (colonne 2)
  * résultat présenté sous la forme

     * nombre décimal
     * fraction

.. note:: La **fraction** représente

          - au numérateur, le nombre de valeurs communes entre les deux listes
          - au dénominateur, le nombre de valeurs des deux listes réunies


comparer les nombres d'éléments de 2 listes
============================================

Ajoute une colonne en début du tableau avec le résultat de la comparaison du nombre d'éléments dans les listes de deux colonnes.

**paramètres**

  * séparateur de liste (colonne 1)
  * séparateur de liste (colonne 2)

**exemple**

+------------+------------+--------------+
| colonne 1  | colonne 2  | résultat     |
+============+============+==============+
| 2010, 2011 |   a / b    |       =      |
+------------+------------+--------------+
| 2010       |   a / b    |       <      |
+------------+------------+--------------+
|            |     a      |       <      |
+------------+------------+--------------+
| 2010, 2011 |     a      |       >      |
+------------+------------+--------------+
|   2010     |            |       >      |
+------------+------------+--------------+
|            |            | listes vides |
+------------+------------+--------------+
