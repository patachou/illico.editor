Calculs arithmétiques
######################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

calculer une opération sur 1 colonne
=====================================

Ajoute une colonne en début de tableau avec le résultat d'une opération sur l'ensemble des valeurs d'une même colonne.

Les opérations disponibles sont (au choix) :

  * minimum
  * maximum
  * somme
  * moyenne (arrondi à deux chiffres après la virgule)

Pour ces opérations, les valeurs vides sont (au choix) :

  * ignorées
  * traduites par zéro

Pour chaque ligne, le résultat inscrit est (au choix) :

  * le total (le même pour toutes les lignes)
  * la valeur *cumulée* (peut être différent d'une ligne à l'autre)

     * en partant de la première ligne
     * en partant de la dernière ligne

Le résultat lui-même peut être (au choix) :

  * global : le calcul prend en compte toutes les lignes
  * local : le calcul s'applique sur des lots de lignes déterminés par les valeurs d'une autre colonne (au choix) :

     * par un identifiant commun : indépendamment de l'ordre des lignes
     * par des séries : une série est un ensemble de lignes consécutives avec le même identifiant


calculer une opération sur 2 colonnes
======================================

Ajoute une colonne en début de tableau avec le résultat d'une opération entre les valeurs de deux colonnes.

Les opérations disponibles sont :

  * addition,
  * soustraction,
  * multiplication,
  * division,
  * division entière,
  * modulo,
  * taux d'évolution en pourcentage.

.. important:: Pour une valeur contenant un nombre et une unité (cm, kg, etc.),
               la partie numérique est prise en compte pour l'opération **si l'unité est placée à la fin**.

.. note:: Les opérations renverront un résultat vide si l'une des deux valeurs est vide.

**paramètres**

  * en-tête (nouvelle colonne)


calculer une opération avec 1 constante
========================================

Ajoute une colonne en début de tableau avec le résultat d'une opération entre une constante et les valeurs d'une colonne.

Les opérations disponibles sont :

  * addition,
  * soustraction de la constante (pour chaque valeurs de la colonne, on soustrait la constante),
  * soustraction des valeurs de la colonne (à la constante, on soustrait à chaque ligne, la valeur lue dans la colonne),
  * multiplication,
  * division des valeurs de la colonne par la constante,
  * division de la constante, par les valeurs de la constante.


calculer la somme ou moyenne sur x colonnes
============================================

Ajoute une colonne en début de tableau avec le résultat de la somme ou de la moyenne calculée avec les valeurs des colonnes sélectionnées.

Les opérations disponibles sont (au choix) :

  * somme
  * moyenne (arrondi à deux chiffres après la virgule)

Pour ces opérations, les valeurs vides sont (au choix) :

  * ignorées
  * traduites par zéro

