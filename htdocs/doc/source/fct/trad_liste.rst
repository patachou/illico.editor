Traduire les listes
####################

.. contents:: Contenu
   :local:   
   :backlinks: top 

-------

traduire les listes en lignes
==============================

Crée une ligne de données pour chaque valeur de la liste.

.. note:: Normalise les données en appliquant la première forme normale selon Merise.

**paramètres**

  * séparateur


**original**

+----------------+------------+
| période        | activité   |
+================+============+
| 2009,2010,2012 | formation  |
+----------------+------------+

**résultat**

  * séparateur : ``,``

+---------+-----------+
| période | activité  |
+=========+===========+
|  2009   | formation |
+---------+-----------+
|  2010   | formation |
+---------+-----------+
|  2012   | formation |
+---------+-----------+


traduire les listes en colonnes
================================

Crée une colonne de données pour chaque élément de la liste.

Au choix, conserve ou supprime la colonne contenant la liste originale.

.. note:: Les nouvelles colonnes seront porteront le nom de la colonne originale et un suffixe avec le numéro d'ordre de la valeur dans la liste originale.

**paramètres**

  * direction (action unique)

     * de la gauche vers la droite
     * de la droite vers la gauche

  * séparateur
  * action (choix unique)

     * remplacer la colonne
     * conserver la colonne


**original**

+-----------+-------+
| notes     | élève |
+===========+=======+
| A,A,B,A,C |   1   |
+-----------+-------+
| C,B,B     |   2   |
+-----------+-------+


**résultat 1**

  * de la gauche vers la droite
  * séparateur ``,``
  * remplacer la colonne

+-----------+---------+---------+---------+---------+---------+-------+
| notes     | notes_1 | notes_2 | notes_3 | notes_4 | notes_5 | élève |
+===========+=========+=========+=========+=========+=========+=======+
| A,A,B,A,C | A       | A       | B       | A       | C       |   1   |
+-----------+---------+---------+---------+---------+---------+-------+
| C,B,B     | C       | B       | B       |         |         |   2   |
+-----------+---------+---------+---------+---------+---------+-------+


**résultat 2**

  * de la droite vers la gauche
  * séparateur ``,``
  * conserver la colonne

+-----------+---------+---------+---------+---------+---------+-------+
| notes     | notes_5 | notes_4 | notes_3 | notes_2 | notes_1 | élève |
+===========+=========+=========+=========+=========+=========+=======+
| A,A,B,A,C | A       | A       | B       | A       | C       |   1   |
+-----------+---------+---------+---------+---------+---------+-------+
| C,B,B     |         |         | C       | B       | B       |   2   |
+-----------+---------+---------+---------+---------+---------+-------+


traduire les listes en une matrice
===================================

Crée une colonne de données pour chaque valeur de la liste.
Le nombre d'occurrences rencontrées dans la liste est indiquée aux intersections ligne/colonne.

Pour les associations clé/valeur, la clé devient une colonne et la valeur est reportée aux intersections.

**paramètres**

  * séparateur

**paramètres optionnels**

  * association clé/valeur

    * opérateur d'association
    * séparateur de la liste résultat


  * interprétation ``a => b => c``

    * clé ``a`` + valeur ``b => c``
    * équivalent à ``a => b`` + ``a => c`` (développement)
    * équivalent à ``a => b`` + ``b => c`` (chaîne)
    * équivalent à ``a => b`` + ``b => c`` + ``a => c`` (transitivité)


**original 1**

+-----------+
| notes     |
+===========+
| A,A,B,A,C |
+-----------+

**résultat 1**

  * séparateur ``,``

+-----------+---+---+---+
|   notes   | A | B | C |
+===========+===+===+===+
| A,A,B,A,C | 3 | 1 | 1 |
+-----------+---+---+---+

**original 2**

+-----------------------------+
| années / note               |
+=============================+
| 2009=F,2009=C,2012=B,2013=A |
+-----------------------------+
| 2013                        |
+-----------------------------+

**résultat 2**

  * séparateur ``,``
  * opérateur association ``=``
  * séparateur de la liste résultat ``+``
  * ``a => b => c`` signifie ``a => b`` + ``a => c``

+-----------------------------+------+------+------+
| années / note               | 2009 | 2012 | 2013 |
+=============================+======+======+======+
| 2009=F,2009=C,2012=B,2013=A | F+C  | B    | A    |
+-----------------------------+------+------+------+
| 2009=A=B                    | A+B  |      |      |
+-----------------------------+------+------+------+
| 2013                        |      |      |      |
+-----------------------------+------+------+------+

.. warning:: L'ordre des nouvelles colonnes est arbitraire.


**original 3**

+-----------+
| notes     |
+===========+
| A>E,A>B>C |
+-----------+

**résultat 3**

  * séparateur ``,``
  * opérateur association ``>``
  * séparateur de la liste résultat ``+``

*option* ``a => b => c`` signifie clé ``a`` et valeur ``b => c``

+-----------+-------+
| notes     | A     |
+===========+=======+
| A>E,A>B>C | E+B>C |
+-----------+-------+

*option* ``a => b => c`` signifie ``a => b`` + ``a => c``

+-----------+-------+
| notes     | A     |
+===========+=======+
| A>E,A>B>C | E+B+C |
+-----------+-------+

*option* ``a => b => c`` signifie ``a => b`` + ``b => c``

+-----------+-----+---+
| notes     | A   | B |
+===========+=====+===+
| A>E,A>B>C | E+B | C |
+-----------+-----+---+

*option* ``a => b => c`` signifie ``a => b`` + ``b => c`` + ``a => c``

+-----------+-------+---+
| notes     | A     | B |
+===========+=======+===+
| A>E,A>B>C | E+B+C | C |
+-----------+-------+---+

.. warning:: L'ordre des nouvelles colonnes est arbitraire.
