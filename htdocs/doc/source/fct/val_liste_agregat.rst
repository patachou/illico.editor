Transformations de liste (agrégats)
####################################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

Fonctions d'analyse et combinaisons de listes de valeurs.

-------


répartition/distribution
=========================

.. note:: Fonction d'analyse.

Pour une colonne donnée, affiche la répartition des valeurs en liste.
Les listes vides peuvent être interprétées comme des listes contenant aucune valeur ou comme des listes contenant une seule valeur, la valeur vide.

**paramètres**

  * séparateur
  * ignorer les listes vides : oui/non


compter le nombre d'éléments
=============================

Ajoute une colonne en début de tableau et indique pour chaque ligne le nombre de valeurs dans les listes de la colonne sélectionnée.

Une valeur répétée peut au choix être comptabilisées autant de fois que d'occurrence ou une seule fois.

**paramètres**

  * séparateur
  * ignorer les répétitions oui/non

.. tip:: Sans séparateur, cette transformation calcule la longueur de la liste/expression, à l'instar de la fonction *AGRÉGATS -> mesurer la longueur des valeurs*.


compacter les listes
=====================

Ajoute une colonne en début de tableau avec le résultat de la règle de conservation des valeurs répétées.

**paramètres**

  * séparateur
  * réduire les répétitions
 
     * à une occurrence, pour chaque série
     * globalement, dans toute la liste

        * conserver

           * premières occurrences
           * dernières occurrences
     

**exemples**

+-------------------------+--------------------------------------------------------------+-------------+
| original                | paramètres                                                   | résultat    |
+=========================+==============================================================+=============+
| 1,1,2,2,3,3,4,4         | séparateur  : ,                                              | 1,2,3,4     |
|                         |                                                              |             |
|                         | réduire les répétitions : (toutes options)                   |             |
+-------------------------+--------------------------------------------------------------+-------------+
| 1,1,2,2,3,3,4,4,2,2,1,1 | séparateur  : ,                                              | 1,2,3,4,2,1 |
|                         |                                                              |             |
|                         | réduire les répétitions : à une occurrence pour chaque série |             |
+-------------------------+--------------------------------------------------------------+-------------+
| 1,1,2,2,3,3,4,4,2,2,1,1 | séparateur  : ,                                              | 1,2,3,4     |
|                         |                                                              |             |
|                         | réduire les répétitions : globalement                        |             |
|                         |                                                              |             |
|                         | conserver   : premières occurrences                          |             |
+-------------------------+--------------------------------------------------------------+-------------+
| 1,1,2,2,3,3,4,4,2,2,1,1 | séparateur  : ,                                              | 3,4,2,1     |
|                         |                                                              |             |
|                         | réduire les répétitions : globalement                        |             |
|                         |                                                              |             |
|                         | conserver   : dernières occurrences                          |             |
+-------------------------+--------------------------------------------------------------+-------------+

.. tip:: L'en-tête de la colonne résultat rappelle

         - la colonne de référence
         - la règle de conservation
         - la portée globale ou locale


agréger les valeurs de la liste
================================

Ajoute une colonne en début de tableau avec le résultat de l'opération ensembliste sur la liste de valeurs.

**paramètres**

  * séparateur
  * action (choix unique)

     * minimum
     * maximum
     * moyenne
     * somme

.. note:: Les valeurs "vides" -- *nulles* -- ne sont pas prises en compte.

.. important:: Pour une valeur contenant un nombre et une unité (cm, kg, etc.),
               la partie numérique est prise en compte pour l'opération **si l'unité est placée à la fin**.


connaître le format
====================

.. note:: Fonction d'analyse.

Analyse la forme des listes de valeurs. Indique

   * le nombre de listes vides,
   * le nombre de listes non-vides,

      * dont le nombre de listes constituées d'un seul élément,

   * le nombre moyen d'éléments (listes avec élément),
   * le nombre moyen d'éléments (global),
   * le nombre min-max d'éléments (> 1),
   * le nombre de listes avec des éléments répétés,
   * le nombre de listes triées.
