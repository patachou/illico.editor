Transformations et calculs de temps
####################################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

x/60 -> y/100
==============

Ajoute une colonne en début de tableau en reportant la conversion d'un temps ``hh:mm`` ou ``mm:ss`` en une fraction de 100 (base 100).

**exemple**

  `1:30` sera converti en `1:50`

.. tip:: une fois les temps convertis en base 100, les fonctions d'analyse et de calculs peuvent s'appliquer sur ces données sans modification du séparateur ``:``.


x/100 -> y/60
==============

Ajoute une colonne en début de tableau en reportant la conversion d'un temps ``hh:mm`` ou ``mm:ss`` en une fraction de 60 (base 60).

**exemple**

  `1:50` sera converti en `1:30`


cumuler 2 durées
=================

Ajoute une nouvelle colonne en début de tableau en reportant la somme de durées définies dans 2 colonnes et dans le même format.

.. note:: ne nécessite pas de conversion de format ``base 60 -> base 100``.

**paramètres**

format de temps de la colonne

  * ``hh:mm:ss``
  * ``hh:mm``
  * ``mm:ss``

.. note:: une durée non-renseignée est équivalente à la durée ``00:00:00``.

.. important:: le format ``mm:ss`` ne convertit pas 60 minutes en 1 heure afin de rester cohérent au format des autres durées de la colonne résultat.


calculer une durée (temps) entre 2 instants
============================================

Ajoute une nouvelle colonne en début de tableau en reportant le temps écoulé entre les instants exprimés dans le même format (2 colonnes).

.. note:: ne nécessite pas de conversion de format ``base 60 -> base 100``.

**paramètres**

format

  * ``hh:mm:ss``
  * ``hh:mm``
  * ``mm:ss``

.. note:: lorsqu'un des 2 instants est vide, la transformation renvoie *donnée manquante* pour la ligne de données correspondante.


**cas particulier : instant de fin plus petit que l'instant de début**


Lorsque l'instant de fin est plus petit que l'instant de début -- par exemple cas d'un calcul entre des instants sur 2 jours consécutifs -- alors :

  * le résultat est inférieur à 24h pour les formats ``hh:mm:ss`` et ``hh:mm``,
  * le résultat est inférieur à 60 min pour le format ``mm:ss``.

Pour exemple :

+----------+----------+--------------------+
|  A       |  B       | temps écoulé [A,B] |
+==========+==========+====================+
| 21:40:45 | 01:17:21 | 03:36:36           |
+----------+----------+--------------------+


calculer une durée (jours) entre 2 dates
=========================================

Ajoute une nouvelle colonne en début de tableau en reportant le nombre de jours écoulés entre deux dates exprimées dans le même format (2 colonnes).

.. note:: Lorsqu'une des dates est vide, le résultat indique *donnée manquante* et précise la colonne en défaut.

.. note:: Lorsque deux dates sont renseignées mais qu'une des dates est mal formatée, le résultat indique *format incorrect* et précise la colonne en défaut.

.. note:: Lorsque l'intervalle est inversé ou trop court --- du jour J pour le même jour, dernier jour de l'intervalle exclu --- le résultat l'indique pour la ligne concernée.

**paramètres**

format de la date

  * ``jj mm ssaa``
  * ``mm jj ssaa``
  * ``ssaa mm jj``

séparateur

  * ``.``
  * ``-``
  * ``/``
  * (espace)

.. tip:: exemple de combinaison pour obtenir ``ssaa/mm/jj``

          * ``ssaa mm jj``
          * ``/``

le dernier jour de l'intervalle

  * est exclu
  * est inclus

calculer le nombre de jours

  * calendaires (lundi à dimanche)
  * ouvrés (lundi à vendredi)

.. important:: La notion de *jour férié* --- à exclure potentiellement --- n'est pas gérée.


décaler des dates avec 1 constante
===================================

Ajoute une nouvelle colonne en début de tableau en décalant les dates par le nombre de jour, de semaine, de mois ou d'année indiqué.

.. note:: Lorsqu'une des dates est vide, le résultat indique *donnée manquante* pour la ligne en défaut.

**paramètres**

format de la date

  * ``jj mm ssaa``
  * ``mm jj ssaa``
  * ``ssaa mm jj``

séparateur

  * ``.``
  * ``-``
  * ``/``
  * (espace)

.. tip:: exemple de combinaison pour obtenir ``ssaa/mm/jj``

          * ``ssaa mm jj``
          * ``/``

décaler de *x* périodes avec *x* spécifié en terme de (choix unique)

  * jours
  * semaines
  * mois
  * années


décaler des dates selon 1 autre colonne
========================================

Ajoute une nouvelle colonne en début de tableau en décalant les dates par le nombre de jour, de semaine, de mois ou d'année indiqué par une autre colonne.

.. note:: Lorsqu'une des dates est vide, le résultat indique *donnée manquante* pour la ligne en défaut.

.. note:: Lorsque la valeur de décalage est vide, le résultat indique *décalage manquant* pour la ligne en défaut.

**paramètres**

format de la date

  * ``jj mm ssaa``
  * ``mm jj ssaa``
  * ``ssaa mm jj``

séparateur

  * ``.``
  * ``-``
  * ``/``
  * (espace)

.. tip:: exemple de combinaison pour obtenir ``ssaa/mm/jj``

          * ``ssaa mm jj``
          * ``/``

décaler de *x* périodes avec *x* spécifié en terme de (choix unique)

  * jours
  * semaines
  * mois
  * années


donner le jour de la semaine
=============================

Ajoute une nouvelle colonne en début de tableau avec le jour de la semaine de la date lue.

.. note:: Lorsqu'une des dates est vide, le résultat indique *donnée manquante* pour la ligne en défaut.

**paramètres**

format de la date

  * ``jj mm ssaa``
  * ``mm jj ssaa``
  * ``ssaa mm jj``

séparateur

  * ``.``
  * ``-``
  * ``/``
  * (espace)

.. tip:: exemple de combinaison pour obtenir ``ssaa/mm/jj``

          * ``ssaa mm jj``
          * ``/``


compter chacun des jours de la semaine
=======================================

Ajoute une nouvelle colonne en début de tableau avec la fréquence des jours de la semaine --- lundi, mardi, mercredi... --- entre deux dates exprimées dans le même format (2 colonnes).

.. note:: Lorsqu'une des dates est vide, le résultat indique *donnée manquante* et précise la colonne en défaut.

.. note:: Lorsque deux dates sont renseignées mais qu'une des dates est mal formatée, le résultat indique *format incorrect* et précise la colonne en défaut.

.. note:: Lorsque l'intervalle est inversé ou trop court --- du jour J pour le même jour, dernier jour de l'intervalle exclu --- le résultat l'indique pour la ligne concernée.

Le résultat est une liste : ``lundi:1,mardi:2,mercredi:2...``.

**paramètres**

format de la date

  * ``jj mm ssaa``
  * ``mm jj ssaa``
  * ``ssaa mm jj``

séparateur

  * ``.``
  * ``-``
  * ``/``
  * (espace)

.. tip:: exemple de combinaison pour obtenir ``ssaa/mm/jj``

          * ``ssaa mm jj``
          * ``/``

le dernier jour de l'intervalle

  * est exclu
  * est inclus

.. tip:: La fonctionnalité ``traduire les listes en une matrice`` permet de traduire le résultat en 7 colonnes, une pour chaque jour de la semaine.



obtenir le numéro du jour dans l'année
=======================================

Ajoute une nouvelle colonne en début de tableau avec le numéro du jour dans l'année.

.. note:: Lorsqu'une des dates est vide, le résultat indique *donnée manquante* et précise la colonne en défaut.

.. note:: Lorsque deux dates sont renseignées mais qu'une des dates est mal formatée, le résultat indique *format incorrect* et précise la colonne en défaut.

.. note:: Lorsque l'intervalle est inversé ou trop court --- du jour J pour le même jour, dernier jour de l'intervalle exclu --- le résultat l'indique pour la ligne concernée.

**paramètres**

format de la date

  * ``jj mm ssaa``
  * ``mm jj ssaa``
  * ``ssaa mm jj``

séparateur

  * ``.``
  * ``-``
  * ``/``
  * (espace)

.. tip:: exemple de combinaison pour obtenir ``ssaa/mm/jj``

          * ``ssaa mm jj``
          * ``/``
