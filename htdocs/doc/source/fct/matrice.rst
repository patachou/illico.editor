Transformations de matrice
###########################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

calculer la transposée
=======================

Transpose le jeu de données :

  - les lignes deviennent les colonnes et inversement
  - la ligne d'en-tête devient la première colonne
  - la première colonne devient la ligne d'en-tête


**exemple**

+---+---+---+
| A | B | C |
+===+===+===+
| 1 | 2 | 3 |
+---+---+---+


**résultat**

+---+---+
| A | 1 |
+===+===+
| B | 2 |
+---+---+
| C | 3 |
+---+---+


inverser l'ordre des lignes
============================

Inverse l'ordre des lignes du jeu de données : la première ligne devient la dernière, la dernière devient la première, etc.


trier par ordre alphabétique
=============================

**paramètres**

  * ordre des lettres

      * A...Z...a...z...É...é
      * A...É...Z...a...é...z

  * placer les valeurs vides

      * au début
      * à la fin

.. tip:: Pour trier sur plusieurs colonnes --- par exemple A, B et C --- commencez par trier sur la colonne C, puis B, puis A.


trier par ordre numérique
==========================

**paramètre**

  * les valeurs vides sont

      * les plus petites (seront placées au début du tableau)
      * les plus grandes (seront placées à la fin du tableau)
      * égales à zéro

.. tip:: Pour trier sur plusieurs colonnes --- par exemple A, B et C --- commencez par trier sur la colonne C, puis B, puis A.


trier par ordre chronologique
==============================

**paramètres**

format de la date

  * ``jj mm ssaa``
  * ``mm jj ssaa``
  * ``ssaa mm jj``

séparateur

  * ``.``
  * ``-``
  * ``/``
  * (espace)

.. tip:: exemple de combinaison pour obtenir ``ssaa/mm/jj``

          * ``ssaa mm jj``
          * ``/``

* les valeurs vides sont

    * dans le passé lointain
    * dans un futur lointain
    * égales à la date du jour
    * égales à une date précise (à saisir)

.. tip:: Pour trier sur plusieurs colonnes --- par exemple A, B et C --- commencez par trier sur la colonne C, puis B, puis A.
