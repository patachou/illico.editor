Créer un jeu de données
########################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

À propos des chargements de données
====================================

Le choix du chargement s'effectue via le menu **charger** (liste déroulante à gauche) :

  * importer un fichier texte ou CSV
  * créer une liste avec les noms de plusieurs fichiers
  * créer une liste à partir d'un tableau
  * créer des identifiants (suite numérique)
  * créer un calendrier


Importer un fichier texte ou CSV
=================================

Le chargement classique de données s'effectue en important un fichier CSV.


Encodage
---------

Chaque application possède une manière particulière de ``coder`` les caractères.
Certains encodages permettent de prendre en charge d'autre alphabets : alphabets japonais, cyrillique...

Les formats reconnus sont :

  * sauts de (fin de) lignes Linux ou Windows,
  * encodages UTF-8 et ISO-8859-1.

Si vous rencontrez des signes cabalistiques à la place des accents, vous
avez sûrement spécifié un encodage différent de celui du fichier.

Dans ces cas, modifiez le sélecteur de l'encodage par le format idoine puis rechargez votre fichier.

.. tip:: Si cela est possible, mettez les accents dans les en-têtes. Ce témoin visuel permettra de détecter très rapidement l'encodage du fichier.

.. note:: Certains encodages possèdent un caractère ``BOM``. Ce caractère invisible est alors le tout premier caractère du fichier pour permettre aux applications de détecter automatiquement l'encodage.

          Par exemple, si le fichier est encodé en UTF-8 avec BOM, Illico utilisera l'encodage désigné par ce caractère BOM y compris si l'encodage sélectionné dans la liste indique ISO-8859-1.


Détection du séparateur
------------------------

Lorsque le fichier est chargé pour la première fois, le caractère utilisé comme séparateur de colonne est *deviné* à partir de la première ligne
de données qui contient l'en-tête.

Ce séparateur peut être forcé au besoin.

.. note:: Le séparateur de colonne est le séparateur qui simule le passage d'une colonne à une autre. N'importe quel caractère peut servir de séparateur.

Les caractères suivants sont reconnus :

  * ``|``
  * ``;``
  * ``,``
  * ``.``
  * ``:``
  * ``/``
  * ``\``
  * (tabulation)
  * (espace)


.. warning:: Illico ne détectera pas correctement les colonnes lorsque des valeurs contiennent le séparateur de colonne --- *y compris si la valeur est protégée par des guillemets*.


Détection du nombre de colonne
-------------------------------

Certains jeux de données n'ont pas le même nombre de colonnes pour toutes les lignes, typiquement des colonnes de commentaires tout à droite du tableau et sans en-tête.

Selon les cas, il peut être préférable de tronquer en se basant sur l'en-tête déclaré plutôt qu'adapter.

Avec cette option, le nombre de colonne est au choix

  * soit déterminé par l'en-tête des données (tronque)
  * soit déduit des lignes de données (adapte)


Données sur plusieurs lignes
-----------------------------

Par défaut, un saut de ligne permet de passer d'un enregistrement au suivant.
Or, certains fichiers de données contiennent des textes sur plusieurs lignes.


Un mécanisme est prévu à pour éviter une interprétation erronnée des lignes.

**délimiteur**

Pour une donnée sur plusieurs lignes, le délimiteur désigne un caractère indiquant le début et la fin de la donnée.
Les caractères délimiteurs pris en charge sont :

Les caractères suivants peuvent par exemple servir de délimiteur :

  * ``"`` (guillemet)
  * ``'`` (apostrophe)

.. tip:: Le symbole monétaire indéfini (AltGr-$) peut également être utilisé.


**données sources**

::

    colonne 1              | colonne 2
    -----------------------+-----------------
    "voici un texte
    sur plusieurs lignes"  |  blabla


**exemple sans indiquer de caractère délimiteur**

Les lignes sont lues *mécaniquement* et sont interprétées à tort comme 2 lignes de données.


::

    # | colonne 1              | colonne 2
    --+------------------------+-----------------
    1 | "voici un texte        |
    2 | sur plusieurs lignes"  |  blabla


**exemple en indiquant le caractère délimiteur**

Les données sur plusieurs lignes sont correctement détectées

::

    # | colonne 1              | colonne 2
    --+-----------------------+-----------------
    1 | "voici un texte
      | sur plusieurs lignes"  |  blabla



**traduire en liste**

Dans certains cas, il peut être intéressant de supprimer ou de remplacer à la volée les sauts de ligne par un autre caractère.

Sur l'exemple précédent, il s'agit par exemple d'obtenir

::

    # | colonne 1                              | colonne 2
    --+----------------------------------------+-----------------
    1 | "voici un texte sur plusieurs lignes"  |  blabla


Les caractères de remplacement disponibles sont :

  * ``,``
  * ``;``
  * ``-``
  * ``/``
  * ``.``
  * ``:``
  * ``#``
  * ``_``
  * (espace)
  * (saut de ligne ``\n``)
  * (saut de ligne HTML br)


Ainsi le texte suivant, en précisant de remplacer les sauts de ligne par ``,``

::

    colonne 1 | colonne 2
    ----------+------------
    "2010
    2011
    2013
    2014"     | blabla

sera traduit par

::

    colonne 1             | colonne 2
    ----------------------+------------
    "2010,2011,2013,2014" | blabla

On se retrouve alors dans une gestion de données en liste.

.. note:: Le choix ``saut de ligne HTML br`` simule un saut de ligne *exportable* en activant conjointement *l'interprétation des caractères HTML* dans *préférences->configuration*.

.. important:: Les choix ``saut de ligne HTML br`` et ``saut de ligne \n`` sont compatibles avec

          - les classeurs sous OpenOffice et LibreOffice
          - Word

          Remarque : Excel fusionne des cellules pour respecter la forme globale du tableau.


Ignorer les n premières lignes
------------------------------

Parfois, certains logiciels exportent un fichier CSV avec une description dans les premières lignes : la date, la requête de l'utilisateur ou d'autres informations qui décrivent les données.

Cette option permet d'ignorer les premières lignes et de lire uniquement les données situées juste après.

Une fois le nombre de ligne à ignorer est renseigné, le bouton *recharger* permet de prendre en compte cet ajustement.


Créer une liste avec les noms de plusieurs fichiers
====================================================

Crée un nouveau jeu de données avec en première colonne les noms des fichiers et en deuxième colonne le champ remarque.

Il est possible de construire de manière itérative son jeu de données en ajoutant *à la liste existante* c'est-à-dire le jeu de données courant (2 colonnes).

**paramètres**

  * ajouter à la liste existante : oui/non
  * remarque

.. important:: un jeu de données construit par ajout successif doit toujours avoir le même nombre de colonnes.


Créer une liste à partir d'un texte/tableau
============================================

Crée un nouveau jeu de données par copier-coller d'un texte ou d'un tableau issu d'une page Web, d'un tableur comme Excel ou LibreOffice par exemple.


Ignorer les n premières lignes
-------------------------------

Cf. section *importer un fichier texte ou CSV*


Détection du séparateur
------------------------

Cf. section *importer un fichier texte ou CSV*


Données sur plusieurs lignes
-----------------------------

Cf. section *importer un fichier texte ou CSV*


Ignorer les n premières lignes
------------------------------

Cf. section *importer un fichier texte ou CSV*


Créer des identifiants (suite numérique)
=========================================

Crée un nouveau jeu de données en initialisant les données par une suite numérique.

::

    valeur de départ + (pas x numéro de la ligne) < ou = limite

**paramètres**

  * valeur de départ
  * pas (incrément)
  * limite/dernière valeur
  * séparateur de colonne

.. important:: Ce chargement ne permet pas de créer une liste contenant une seule donnée.


Créer un calendrier
====================

Crée un nouveau jeu de données en initialisant les données par un calendrier.

**paramètres**

  * date de début
  * date de fin
  * liste des jours de la semaine à retenir pour le calendrier
  * séparateur de colonne

  .. note:: Pour chaque date, la seconde colonne rappelle le jour de la semaine.
