Transformations de valeur en liste
###################################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

Cette catégorie concerne les transformations de données présentées sous forme de liste de valeurs.

Le séparateur est un ensemble de caractères qui permet de distinguer deux valeurs.

+--------------------------------+------------+--------------------------+
|        listes de données       | séparateur | frappe                   |
+================================+============+==========================+
| 2009,2010,2012                 |   ,        | ``virgule``              |
+--------------------------------+------------+--------------------------+
| tel : 01 02 03, fax : 54 31 54 |   ,        | ``virgule espace``       |
+--------------------------------+------------+--------------------------+
| 5135 / 57187 / ? / AG5431      |   /        |  ``espace slash espace`` |
+--------------------------------+------------+--------------------------+
|  prévisionnel ou confirmé      |   ou       |  ``espace "ou" espace``  |
+--------------------------------+------------+--------------------------+
|       PK-2043-5405             |      \-    |  ``tiret``               |
+--------------------------------+------------+--------------------------+

------------------------------


enlever une des valeurs
========================

Supprime dans les listes toutes les occurrences d'une valeur donnée.

**paramètres**

  * séparateur
  * valeurs à effacer

**exemples**

+----------+----------------+----------+
| original | paramètres     | résultat |
+==========+================+==========+
| 2,4,3,2  | séparateur : , |          |
|          |                |          |
|          | valeur     : 2 | 4,3      |
+----------+----------------+----------+
| 2,,4,,3  | séparateur : , | 2,4,3    |
|          |                |          |
|          | valeur     :   |          |
+----------+----------------+----------+


remplacer une valeur
=====================

Remplace toutes les occurrences d'une valeur par une autre valeur.

**paramètres**

  * séparateur
  * valeur (à remplacer)
  * sera remplacée par (valeur)

**exemple**

  * séparateur : ``,``
  * remplacer  : ``2``
  * par        : ``5``

+----------+----------+
| original | résultat |
+==========+==========+
| 2,4,3,2  | 5,4,3,5  |
+----------+----------+


remplacer les valeurs vides
============================

Remplace toutes valeurs vides par une autre valeur.

**paramètres**

  * séparateur
  * seront remplacées par (valeur)

**exemple**

  * séparateur    : ``,``
  * remplacer par : ``7``

+----------+----------+
| original | résultat |
+==========+==========+
| 2,4,,    | 2,4,7,7  |
+----------+----------+


trier les valeurs
==================

Trie alphabétiquement --- ascendant ou descendant --- ou numériquement --- croissant ou décroissant --- les valeurs d'une liste.

Les valeurs répétées peuvent être réduites à une seule occurrence.

**paramètres**

  * séparateur
  * tri alphabétique/numérique
  * ordre ascendant/descendant
  * compacter les occurrences multiples oui/non

**exemple**

+----------+---------------------------+-------------------------+-------------------+
| original | paramètres                | résultat sans compacter | résultat compacté |
+==========+===========================+=========================+===================+
| 2,4,13,2 | séparateur : ,            |                         |                   |
|          |                           |                         |                   |
|          | tri        : numérique    |                         |                   |
|          |                           |                         |                   |
|          | ordre      : croissant    | 2,2,4,13                | 2,4,13            |
+----------+---------------------------+-------------------------+-------------------+
| 2,4,13,2 | séparateur : ,            |                         |                   |
|          |                           |                         |                   |
|          | tri        : alphabétique |                         |                   |
|          |                           |                         |                   |
|          | ordre      : ascendant    | 13,2,2,4                | 13,2,4            |
+----------+---------------------------+-------------------------+-------------------+


enlever n éléments
===================

Enlève les n premières ou dernières valeurs de chaque liste.

**paramètres**

  * séparateur
  * nb valeurs à supprimer
  * à gauche/droite


conserver n éléments
=====================

Conserve les n premières ou dernières valeurs de chaque liste.

**paramètres**

  * séparateur
  * nb valeurs à conserver
  * à gauche/droite
