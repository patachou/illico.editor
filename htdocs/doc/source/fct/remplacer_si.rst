Transformations conditionnelles (remplacer si)
###############################################

.. contents:: Contenu
   :local:   
   :backlinks: top 

-------


tester un motif
================

.. note:: Fonction d'analyse.

Renvoie toutes les correspondances avec un motif --- ou les non-correspondances --- et précise le nombre de répétitions pour chaque valeur trouvée.


**paramètres**

  * motif recherché (expression rationnelle)
  * sensible à la casse : oui/non


définir des valeurs conditionnelles (regex)
============================================

Permet de modifier des données en fonction de la correspondance --- ou l'absence de correspondance --- entre la valeur d'une (autre) colonne et un motif donné.

**paramètres**

  * motif recherché (expression rationnelle)

     * sensible à la casse : oui/non

  * si motif trouvé : valeur à inscrire + colonne
  * si motif absent : valeur à inscrire + colonne

.. important:: Les expressions rationnelles constituent un langage concis --- un peu déroutant au début --- simple et terriblement efficace.
   Prenez le temps d'en étudier la syntaxe (cf. `regex <../memo/regex.html>`__).

.. tip:: Dans Illico, le bandeau supérieur contient une aide sur les expressions rationnelles.


définir des valeurs conditionnelles (numérique)
================================================

Permet de modifier des données en fonction d'un test d'égalité numérique entre deux colonnes.

**paramètres**

  * test d'égalité numérique

     * égal, non égal à
     * supérieur/inférieur ou égal à
     * strictement supérieur/inférieur à

  * valeur si vrai + colonne
  * valeur si la condition n'est pas vérifiée + colonne


.. important:: Pour une valeur commençant par des chiffres et se terminant
    par des lettres, seule la partie numérique est prise en compte pour la
    comparaison.
    
    exemples :
      -  33.3%, 33,3%, 33.3 % seront équivalents à 33.3
      
      -  2011-2012 est équivalent à 2011


mixer les valeurs de 2 colonnes
================================

Ajoute une colonne en début de tableau en mixant intelligemment pour chaque ligne, les valeurs de 2 colonnes en fonction de règles de priorités.

**paramètres**

  * (colonne) originale

     * action (choix unique)

        * \-- (pas d'action particulière)
        * conserver les valeurs vides
        * conserver les valeurs non-vides

  * mettre à jour avec (colonne)

    * action (choix unique)

        * \-- (pas d'action particulière)
        * ignorer les valeurs vides
        * ignorer les valeurs non-vides


remplacer un motif (regex) par une valeur
==========================================

Remplace toutes les valeurs correspondantes au motif par une nouvelle valeur.

**paramètres**

  * motif à remplacer (expression rationnelle)
  * par
  * sensible à la casse : oui/non
