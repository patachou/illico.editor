Principes
##########

.. contents:: Contenu
   :local:
   :backlinks: top

-------

Les transformations
====================

Les transformations disponibles dans Illico sont de deux types :

  * les traitements de données,
  * les fonctions d'analyse.


Les traitements de données
---------------------------

Les traitements de données modifient généralement les données et affichent systématiquement un message de retour avec dans la plupart des cas le nombre de lignes ou de valeurs modifiées.

Le dernier traitement exécuté peut être annulé pour revenir à la situation antérieure.

Chaque traitement exécuté est consigné dans un **journal de bord** et peut être consulté à tout moment.

Cet historique permet ainsi de générer sans effort une procédure pas-à-pas avec

  * le nom du traitement,
  * la liste des paramètres utilisés,
  * le message résultat du traitement,
  * et si le traitement a été annulé.

.. note:: L'historique s'exporte par copier-coller.


Les fonctions d'analyse
------------------------

Contrairement aux traitements, les fonctions d'analyse ne modifient pas les données.

Ces fonctions permettent d'obtenir rapidement des tableaux de synthèses sur le format ou sur la répartition des données avant de réaliser un traitement.

Elles ne sont pas consignées dans le journal de bord.

.. tip:: Certains traitements permettent *une simulation* agissant ainsi comme une fonction d'analyse, sans effet ni sur l'historique ni sur les données.


Charger des données
====================

Les modes de chargement suivants sont disponibles

  * importer un fichier texte ou CSV
  * créer une liste à partir d'un texte/tableau (par copier-coller)
  * créer une liste avec les noms de plusieurs fichiers
  * créer des identifiants (suite numérique)
  * créer un calendrier

.. note:: Les options des chargements de données sont expliquées au chapitre suivant.


Exploiter les données modifiées
================================

Il est possible à tout moment de consulter les données brutes depuis les fonctionnalités du bandeau.

  | |bandeau supérieur|

dans un fichier CSV
--------------------

* **export**

.. note:: L'export CSV est encodé en UTF-8 (international) avec le *BOM unicode*.

          Ce caractère en tout début des données permet aux logiciels Excel/OpenOffice et aux applications de reconnaître automatiquement l'encodage UTF-8.

.. tip:: Les noms des fichiers CSV exportés sont consignés dans le journal de bord.


dans un nouvel onglet
----------------------

  * **aperçu CSV** : ouvre dans un onglet à l'identique du fichier CSV
  * **tableau** : adapté à un copier-coller dans un tableur (Excel, LibreOffice...)
  * **texte** : les colonnes sont alignées en utilisant une police *monospace*

.. tip:: L'onglet est nommé avec un horodatage, ce qui permet de comparer plusieurs états.


Exploiter les données agrégées (analyse)
=========================================

Les résultats des fonctions d'analyse sont disponibles dans au moins un des deux formats *HTML* et *CSV*.

.. important:: L'export HTML permet d'exporter avec ou sans l'interprétation des balises HTML, balises utilisées pour la mise en forme telle la couleur, l'emphase...

.. warning:: Sous Excel/OpenOffice, copier-coller un export HTML réduira automatiquement les espaces doubles, triples, etc. à un seul espace.


Jeu de données volumineux
==========================

Il existe plusieurs limites

  1. l'import accepte des fichiers entre 100 Mo et 200 Mo [#]_
  2. l'aperçu/export HTML (mise en forme) est limité à plusieurs dizaines de Mo [#]_
  3. l'export CSV est *en général* limité à 500 Mo [#]_

.. [#] exemples : 10 Mo ~ 40 col x 30 000 lignes, 100 Mo ~ 12 col. x 1 million de lignes

.. [#] plusieurs centaines de milliers de lignes

.. [#] parfois plus selon les navigateurs


Les traitements par défaut
===========================

Pour simplifier certains traitements, certaines données sont tronquées ou interprétées pour le calcul.

Les données sources ne sont pas modifiées.


Les valeurs numériques
-----------------------

Lorsqu'une donnée décrit une valeur avec son unité --- 10 %, 15 kg, 2h30 --- seule la partie à gauche (avant l'unité) est prise en compte pour le calcul.

La partie décimale peut être identifiée indifféremment par ``,``, ``.``, ``:``.


Les valeurs vides
------------------

Les valeurs vides -- *nulles* -- sont ignorées et ne provoquent pas d'erreur dans les calculs.


Les décimales
--------------

  * les calculs sur les données sont effectués avec la précision de la donnée source,
  * la précision des résultats des traitements, lorsqu'ils sont réinjectés dans le tableau de données est la précision de la donnée source,
  * la précision des résultats des fonctions d'analyse est tronquée à 2 décimales.

.. tip:: Pour éviter les décimales, lorsque c'est possible, une bonne pratique consiste à convertir au préalable la donnée dans l'unité la plus petite.


Les séparateurs de milliers
----------------------------

Le séparateur de milliers peut être identifié indifféremment par un espace ou un espace insécable. Ces espaces sont neutres dans la détection du nombre.


Les dates
----------

.. tip:: En général, pour simplifier les tris, le tri alphabétique correspond au tri chronologique quand la date au format ``SSAA-MM-JJ``, ``SSAA.MM.JJ``, ``SSAA/MM/JJ`` ou ``SSAA MM JJ``.

.. tip:: Dans *Illico*, les comparaisons sur des intervalles sont insensibles aux espaces. Ainsi, des intervalles décrivant des périodes/bornes/dates au format ``SSAA MM JJ`` sont valides car la valeur est lue comme un nombre entier ``SSAAMMJJ``.

.. |bandeau supérieur| image:: /_static/images/0/1.png

