Transformations de liste (enrichissement)
##########################################

.. contents:: Contenu
   :local:
   :backlinks: top


recoder des clés par des valeurs (ou l'inverse)
================================================

Ajoute une colonne en début de tableau avec le résultat de l'opération souhaitée.

Pour les valeurs en liste, vérifie l'existence de chaque valeur dans une table de correspondance et au choix, recode ou filtre/exclut ces valeurs.

**paramètres**

  * listes à recoder

     * sélection (1 colonne)
     * séparateur des valeurs (des listes de la colonne traitée)

  * table de correspondance

     * données de la table de recodage
     * séparateur de colonne
     * colonne servant de clé (valeur recherchée)
     * colonne servant de valeur (valeur recodée)

  * règle de fusion (choix unique)

     * conserver les valeurs sans correspondance
     * conserver uniquement les correspondances, sans recodage
     * conserver uniquement les correspondances, recodées
     * conserver toutes les valeurs et recoder si correspondance

**exemple**

Cette liste décrit les numéros de pièces requises pour construire un modèle

+--------+-----------------+
| modèle | pièces requises |
+========+=================+
| M-2020 | 21,42,22        |
+--------+-----------------+

Dans cet exemple, la nouvelle nomenclature a modifié le numéro de deux pièces (42 et 43) ; seule la pièce 42 nous concerne ici.

La table de correspondance est directement copiée-collée

.. code-block::

   ancien codage;nouveau codage
   42;45
   43;46

Les paramètres du recodage sont

  * listes à recoder

     * colonne : pièces requises
     * séparateur : ","

  * table de correspondance

     * séparateur : ";"
     * valeurs recherchées : ancien codage (ici, la première colonne de la table de correspondance)
     * seront remplacées par : nouveau codage (ici, la seconde colonne)

  * règle de fusion

     * conserver toutes les valeurs et recoder si correspondance

**résultat**

+--------+-----------------+
| modèle | pièces requises |
+========+=================+
| M-2020 | 21,45,22        |
+--------+-----------------+


combiner deux listes
=====================

Ajoute une colonne en début de tableau avec le résultat de l'opération souhaitée.

Associe chaque valeur de la première liste avec celle de même ordre de la seconde liste.

.. note:: Une liste vide sera interprétée comme une liste d'autant de valeurs vides que nécessaires.

**paramètres**

  * séparateur (colonne 1)
  * séparateur (colonne 2)
  * séparateur (nouvelle colonne)

     * opérateur d'association

**exemple**

La colonne de résultat est une liste d'association séparée par ``(espace)//(espace)``.

Chaque association est un couple de clé/valeur écrit sous la forme ``clé=valeur``, c'est-à-dire avec l'opérateur d'association ``=``.

+---------------+------------------+-------------------------------------------+
| colonne 1     | colonne 2        | résultat                                  |
+===============+==================+===========================================+
| 3,15,3,17,3   | 2012/2013/2014   | 3=2012 // 15=2013 // 3=2014 // 17= // 3=  |
+---------------+------------------+-------------------------------------------+
| 3,15,3,17,3   |                  | 3= // 15= // 3= // 17= // 3=              |
+---------------+------------------+-------------------------------------------+
|               | 2012/2013/2014   | =2012 // =2013 // =2014                   |
+---------------+------------------+-------------------------------------------+


déduire un sous-ensemble à partir de 2 listes
==============================================

Ajoute une colonne au début du tableau avec le résultat de l'opération ensembliste entre deux colonnes décrivant des listes de données.

Une option permet de gérer les répétitions de valeurs communes dans une même liste ou suite à une opération de réunion ou d'intersection.

.. important:: Dans la liste résultat, l'ordre des valeurs n'est pas forcément l'ordre de départ.

.. important:: Une liste vide sera interprétée comme une liste avec une seule valeur, la valeur vide.

**paramètres**

  * séparateur (colonne 1)
  * séparateur (colonne 2)
  * conserver tous les éléments (choix unique)

     * de A et de B (réunion)
     * communs à A et à B (intersection)
     * de A, n'appartenant pas à B (différence)
     * de B, n'appartenant pas à A (différence)
     * de A ou de B, sauf ceux communs à A et B (différence symétrique)

  * action pour les valeurs répétées (choix unique)

     * ne conserver qu'une seule occurrence
     * conserver le plus grand nombre d'occurrences entre A et B
     * conserver le plus petit nombre d'occurrences entre A et B
     * conserver toutes les occurrences de A et de B
     * conserver le nombre d'occurrences de A
     * conserver le nombre d'occurrences de B

  * séparateur (résultat)


lister les permutations des valeurs d'une liste
================================================

Ajoute une colonne au début du tableau avec la liste de toutes les permutations des valeurs des listes de la colonne sélectionnée.

**paramètres**

  * séparateur des valeurs (des listes de la colonne sélectionnée)
  * séparateur des permutations (pour la liste résultat des permutations)


mélanger les valeurs de la liste
=================================

Ajoute une colonne au début du tableau avec la liste des valeurs mélangées (mélange de Fisher–Yates)

**paramètres**

  * séparateur des valeurs
