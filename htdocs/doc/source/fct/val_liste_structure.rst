Transformations de liste (structure)
#####################################

.. contents:: Contenu
   :local:
   :backlinks: top


supprimer leur délimiteur
==========================

Supprime le caractère délimiteur pour toutes les valeurs des listes.

.. note:: Le délimiteur est le caractère situé *à la fois* en début et en fin de la donnée.

**paramètres**

  * séparateur
  * délimiteur (caractère)

**exemple**

  * séparateur : ``,``
  * opérateur : ``'``

+----------+----------+
| original | résultat |
+==========+==========+
| '2',3,'4 | 2,3,'4   |
+----------+----------+


enlever les espaces en début/fin (liste)
=========================================

Pour chaque valeur de la liste, supprime tous les espaces en début et/ou à la fin de la valeur.

**paramètres**

  * délimiteur

**exemple**

  * séparateur : ``,``

+------------+----------+
| original   | résultat |
+============+==========+
| 2, 4 , 3,2 | 2,4,3,2  |
+------------+----------+


modifier le séparateur (liste)
===============================

Changer de séparateur reste une opération délicate.
En effet, si vos données contiennent déjà le caractère que vous voulez utiliser comme séparateur, certaines applications qui recevront les données peuvent ne pas charger correctement les données.

Permet de vérifier si le changement de séparateur des valeurs (dans les listes de valeurs) perturbera ou non.

.. note:: Cette action est historisée.

.. tip:: Selon vos contraintes et les logiciels utilisés, vous pouvez également être amené à encapsuler les données avec un **caractère d'échappement** --- par exemple ``"``, ``'``, ``\``.


modifier les associations (clé/valeur)
=======================================

Réduit les associations aux seules clés --- ou aux seules valeurs --- ou inverse la relation d'association clé/valeur en valeur/clé.

**paramètres**

  * séparateur
  * opérateur d'association
  * action (choix unique)

     * conserver les clés uniquement
     * conserver les valeurs uniquement
     * inverser le sens des associations

.. note:: Inverser le sens des associations n'a pas d'effet sur les couples ne contenant que la clé.


**exemple**

  * séparateur : ``(espace)//(espace)``
  * opérateur : ``=``

+-----------------------+----------------------+----------------------------+
| original              | action               | résultat                   |
+=======================+======================+============================+
| a=b=c // **a=b** // c | clés uniquement      | a=b=c // **a** // c        |
+-----------------------+----------------------+----------------------------+
| a=b=c // **a=b** // c | valeurs uniquement   | a=b=c // **b** // c        |
+-----------------------+----------------------+----------------------------+
| a=b=c // **a=b** // c | inverser             | a=b=c // **b=a** // c      |
+-----------------------+----------------------+----------------------------+


inverser l'ordre des éléments dans les listes
==============================================

Inverse l'ordre des éléments dans les listes et/ou l'ordre des éléments dans les listes imbriquées


  * séparateur
  * action (choix unique)

     * inverser l'ordre des valeurs des listes
     * inverser l'ordre des valeurs des listes imbriquées seulement

       * séparateur interne des listes imbriquées

     * inverser l'ordre des listes imbriquées et des valeurs dans ces listes

       * séparateur interne des listes imbriquées


**exemple**

  * séparateur         : ``/``
  * séparateur interne : ``,``

+---------------+-----------------------------------------------------+----------------------------+
| original      | action (inverser l'ordre...)                        | résultat                   |
+===============+=====================================================+============================+
| 1,2,3/a,b,c,d | des valeurs des listes                              | a,b,c,d/1,2,3              |
+---------------+-----------------------------------------------------+----------------------------+
| 1,2,3/a,b,c,d | des valeurs des listes imbriquées seulement (,)     | 3,2,1/d,c,b,a              |
+---------------+-----------------------------------------------------+----------------------------+
| 1,2,3/a,b,c,d | des listes imbriquées et des val. de ces listes (,) | d,c,b,a/3,2,1              |
+---------------+-----------------------------------------------------+----------------------------+
