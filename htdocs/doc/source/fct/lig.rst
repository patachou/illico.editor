Transformations de ligne
#########################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

supprimer les lignes vides
===========================

Supprime toutes les lignes ne contenant que des espaces, tabulations et autres caractères d'espacement (saut de ligne, espaces insécables, retour-chariot...).


compléter avec la plus proche valeur consistante
=================================================

Inscrit explicitement des données implicites pour l'humain.

**paramètres**

  * par la valeur consistante juste : avant/après
  * condition d'application :

     * sans condition (défaut)
     * si valeur identique dans une autre colonne


**exemple**

  * par la valeur consistante juste : avant
  * sans condition

+----------+----------+
| original | résultat |
+==========+==========+
| A        | A        |
+----------+----------+
|          | **A**    |
+----------+----------+
|          | **A**    |
+----------+----------+
|          | **A**    |
+----------+----------+
| B        | B        |
+----------+----------+
|          |  **B**   |
+----------+----------+
|          |  **B**   |
+----------+----------+
| C        | C        |
+----------+----------+
|          |  **C**   |
+----------+----------+
| D        | D        |
+----------+----------+


**exemple**

L'exemple suivant permet, aux lignes 2 et 3 en gras, de montrer la différence de comportement entre

  * par la valeur consistante juste : avant
  * sans condition

et

  * par la valeur consistante juste : avant
  * si valeur identique dans (id)

+-------+----------+-------------------------+----------------------------------------+
| id    | original | résultat sans condition | résultat si valeur identique dans (id) |
+=======+==========+=========================+========================================+
| 1     |    A     |          A              |                   A                    |
+-------+----------+-------------------------+----------------------------------------+
| 1     |          |          A              |                   A                    |
+-------+----------+-------------------------+----------------------------------------+
| **2** |          |          A              |                                        |
+-------+----------+-------------------------+----------------------------------------+
| 2     |    B     |          B              |                   B                    |
+-------+----------+-------------------------+----------------------------------------+
| **3** |          |          B              |                                        |
+-------+----------+-------------------------+----------------------------------------+


remplacer avec la plus proche valeur consistante
=================================================

.. note:: Fonctionnement identique à *compléter avec la plus proche valeur consistante*.

Remplace des données implicites pour l'humain par des données explicites.

**paramètres**

  * valeur à remplacer
  * par la valeur consistante juste avant/après
  * condition d'application :

     * sans condition (défaut)
     * si valeur identique dans une autre colonne


**exemple**

  * valeur à remplacer : ``(idem)``
  * par la valeur consistante juste : avant
  * sans condition

+----------+----------+
| original | résultat |
+==========+==========+
| A        | A        |
+----------+----------+
|  (idem)  | **A**    |
+----------+----------+
|  (idem)  | **A**    |
+----------+----------+
|  (idem)  | **A**    |
+----------+----------+
| B        | B        |
+----------+----------+
| (idem)   |  **B**   |
+----------+----------+
| (idem)   |  **B**   |
+----------+----------+
| C        | C        |
+----------+----------+
| (idem)   |  **C**   |
+----------+----------+
| D        | D        |
+----------+----------+


recopier l'en-tête à chaque ligne
==================================

Remplace toutes les valeurs consistantes par l'en-tête ou préfixe/suffixe chaque valeur par l'en-tête de la colonne.

.. note:: Les valeurs vides ne sont pas modifiées.


**paramètres**

  * (recopier) avant/après/à la place
  * séparateur (optionnel)
  * neutraliser le préfixe (optionnel)

**original**

+-----+-----+-----+-----+
| A   | B   | C   | D   |
+=====+=====+=====+=====+
| 1   |     |     | 7   |
+-----+-----+-----+-----+
|     | 3   |     | 6   |
+-----+-----+-----+-----+
| 2   | 5   |     |     |
+-----+-----+-----+-----+

**résulat 1**

  * (recopier) *à la place*

+-----+-----+-----+-----+
| A   | B   | C   | D   |
+=====+=====+=====+=====+
| A   |     |     | D   |
+-----+-----+-----+-----+
|     | B   |     | D   |
+-----+-----+-----+-----+
| A   | B   |     |     |
+-----+-----+-----+-----+

**résultat 2**

  * (recopier) *avant*
  * séparateur : ``=``

+-------+-------+-----+-------+
| A     | B     | C   | D     |
+=======+=======+=====+=======+
| A=1   |       |     | D=7   |
+-------+-------+-----+-------+
|       | B=3   |     | D=6   |
+-------+-------+-----+-------+
| A=2   | B=5   |     |       |
+-------+-------+-----+-------+

**résultat 3**

  * (recopier) *après*
  * séparateur = ``=``

+-------+-------+-----+-------+
| A     | B     | C   | D     |
+=======+=======+=====+=======+
| 1=A   |       |     | 7=D   |
+-------+-------+-----+-------+
|       | 3=B   |     | 6=D   |
+-------+-------+-----+-------+
| 2=A   | 5=B   |     |       |
+-------+-------+-----+-------+

**original**

+----------+----------+
| [2006] A | [2006] B |
+==========+==========+
|    1     |    7     |
+----------+----------+

**résultat**

  * (recopier) *avant*
  * séparateur = ``=``
  * neutraliser le préfixe = ``[2006]``

+----------+----------+
| [2006] A | [2006] B |
+==========+==========+
|   A=1    |    B=7   |
+----------+----------+


compacter les lignes (agrégats : calculs, listes)
==================================================

Pour toutes les lignes identifiées par une même clé,
conserve la première occurrence de la clé et,
agrège (somme, produit, min, max, liste) les données des colonnes sélectionnées.

.. warning:: Une colonne n'est utilisable qu'une seule fois.

.. tip:: Pour créer plusieurs calculs à partir de la même colonne, copiez la colonne et sélectionnez chaque copie.

**paramètres**

  * clé
  * liste de

     * colonne
     * opération :

        * somme,
        * produit,
        * min,
        * max,
        * décompte (total),
        * décompte (valeurs distinctes),
        * liste

     * si liste, paramètre optionnel : séparateur de liste

**original**

+----------+-----+-------------+-------+
| individu | nom | inscription | année |
+==========+=====+=============+=======+
| 2543     | ABC | 60          | 2013  |
+----------+-----+-------------+-------+
| 2543     | ABC | 60          | 2014  |
+----------+-----+-------------+-------+
| 4792     | DEF | 60          | 2015  |
+----------+-----+-------------+-------+
| 2543     | ABC | 60          | 2015  |
+----------+-----+-------------+-------+

**résultat**

  * clé : individu
  * somme de : inscription
  * liste des années + séparateur ``,``

+----------+-----+-------------+----------------+
| individu | nom | inscription | année          |
+==========+=====+=============+================+
| 2543     | ABC | 180         | 2013,2014,2015 |
+----------+-----+-------------+----------------+
| 4792     | DEF | 60          | 2015           |
+----------+-----+-------------+----------------+


décaler vers le haut ou le bas
===============================

Décale les valeurs d'une colonne soit vers le haut, soit vers le bas, en appliquant une règle d'application

**paramètres**

  * sélection : colonne
  * direction : vers le haut ou vers le bas
  * condition d'application :

     * sans condition (défaut)
     * pour chaque série
     * pour chaque identifiant


**exemples**

+----+-----+----------+-------------------------+-------------------------------+
| id | val | vers bas | vers bas / série (id)   | vers bas / identifiant (id)   |
+====+=====+==========+=========================+===============================+
| x  | ABC |          |                         |                               |
+----+-----+----------+-------------------------+-------------------------------+
| y  | DEF | ``ABC``  |                         |                               |
+----+-----+----------+-------------------------+-------------------------------+
| y  | GHI | ``DEF``  |   ``DEF``               |   ``DEF``                     |
+----+-----+----------+-------------------------+-------------------------------+
| x  | JKL | ``GHI``  |                         |   ``ABC``                     |
+----+-----+----------+-------------------------+-------------------------------+
| x  | MNO | ``JKL``  |   ``JKL``               |   ``JKL``                     |
+----+-----+----------+-------------------------+-------------------------------+
