Transformations d'agrégat
##########################

.. contents:: Contenu
   :local:
   :backlinks: top

-------


Ces transformations sont pour la grande majorité des fonctions d'analyse.

-------------------------------


domaine de valeur : nb, %
==========================

.. note:: Fonction d'analyse.

Présente le domaine de valeur et calcule la répartition numérique et relative (pourcentage).


extraire un dictionnaire
=========================

.. note:: Fonction d'analyse

Résume la sélection de *n* colonnes à un dictionnaire et présente le nombre d'occurrences pour chaque combinaison de n-tuples de valeurs.


nombre d'occurrences
=====================

Ajoute une première colonne en début de tableau en reportant le nombre d'occurrences de la valeur rencontrée de la colonne sélectionnée.

.. note:: La nouvelle colonne reprend l'en-tête de la colonne sélectionnée préfixé par ``nb:``.


numéroter l'occurrence
=======================

Ajoute une première colonne en début de tableau avec le numéro d'ordre de l'occurrence de la valeur rencontrée dans la colonne sélectionnée.

.. note:: La nouvelle colonne reprend l'en-tête de la colonne sélectionnée préfixée par ``num:``.

**paramètres**

  * (numérotation) ascendante/descendante

**résultats**

+---------+-----------+------------+
| données | ascendant | descendant |
+=========+===========+============+
| 2010    |    1      |     3      |
+---------+-----------+------------+
| 2009    |    1      |     2      |
+---------+-----------+------------+
| 2010    |    2      |     2      |
+---------+-----------+------------+
| 2010    |    3      |     1      |
+---------+-----------+------------+
| 2009    |    2      |     1      |
+---------+-----------+------------+


connaître les chiffres clés
============================

.. note:: Fonction d'analyse.

Calcule pour la colonne sélectionnée

  * la cardinalité : **nombre de valeurs non-nulles**,
  * la somme,
  * l'espérance mathématique,
  * l'écart-type,
  * la plus petite valeur,
  * la plus grande valeur.

.. important:: Les calculs sont arrondis à 2 décimales.

.. important:: Pour une valeur contenant un nombre et une unité (cm, kg, etc.),
               la partie numérique est prise en compte pour l'opération.


connaître le format
====================

.. note:: Fonction d'analyse.

Produit une analyse de la syntaxe des données.

  * est unique ?
  * nombre de valeurs absentes
  * nb de valeurs différentes
  * longueur min-max
  * est un nombre entier non-signé ?
  * est un nombre entier négatif (signé) ?
  * est un nombre entier positif (signé) ?
  * est un nombre entier négatif ou positif (signé ou non) ?
  * est une heure ?
  * est un mail ?
  * est un code postal (France) ?
  * date au format AAAA-MM-JJ (ISO 8601) ?
  * contient espaces consécutifs ?
  * commence/se termine par des espaces ?
  * ne contient que des espaces ou tabulations ?

.. tip:: Pour chaque test, l'expression rationnelle correspondante utilisée est mentionnée.


mesurer la longueur des valeurs
================================

Ajoute une première colonne en début de tableau avec le nombre de caractères de la valeur rencontrée dans la colonne sélectionnée.

.. note:: La nouvelle colonne reprend l'en-tête de la colonne sélectionnée préfixée par ``longueur:``.

.. tip:: La fonction *VALEURS EN LISTE : AGRÉGATS -> compter le nombre d'éléments* sans séparateur constitue une alternative.


explorer les données
=====================

.. note:: Fonction d'analyse.

Enchaînement de tableaux croisés dynamiques.

Pour chaque tableau intermédiaire d'analyse, il est possible d'exporter :

  * le tableau d'analyse pour les copier-coller dans un tableur,
  * les données au format HTML pour les copier-coller dans un tableur,
  * les données au format CSV (téléchargement du fichier).

**paramètres**

  * la fonction d'agrégat

     * sur des valeurs textes ou numériques

        * compter [1]_
        * compter sans tenir compte des répétitions

     * sur des valeurs numériques

        * sommer
        * compter et sommer
        * minimum et maximum

  * la colonne de faits sur laquelle s'effectue l'agrégat
  * les axes d'analyses : les dimensions

.. [1] pour l'action de *compter*, la sélection d'une colonne de faits est neutre (il s'agit d'un décompte de lignes).

.. tip:: À l'exception du premier axe d'analyse, les axes d'analyse peuvent être ajoutés et modifiés au fur et à mesure.


matrice d'adjacence
====================

.. note:: Fonction d'analyse.

Construit un tableau croisé-dynamique à 2 dimensions.

**paramètres**

  * (colonne 1) : valeurs
  * (colonne 2) : répartition
  * options de tri des colonnes résultats


convertir dans un système de comptage mixte
============================================

Ajoute en début de tableau autant de colonnes que nécessaire pour convertir les valeurs de la colonne sélectionnée.

**paramètres**

  * colonne sélectionnée
  * pour chaque unité, la conversion vers l'unité de mesure immédiatement inférieure

**exemples**

décomposer un nombre de secondes en jours, heures, minutes, secondes

  * 1 seconde
  * 1 minute = 60 secondes
  * 1 heure  = 60 minutes
  * etc.

décomposer un stock (nombre de boîtes) en chargements, caisses, palettes, boîte

  * 1 boîte
  * 1 palette = 80 boîtes
  * 1 caisse  = 6 palettes
  * 1 chargement = 30 caisses
  * etc.

décomposer une combinaison, par exemple une fréquentation --- lundi, mardi, mercredi, jeudi, vendredi --- codée sous la forme ``LMMJV``

Ainsi,

  * lundi et mercredi => ``L.M..`` => `10100`
  * mardi et vendredi => ``.M..V`` => `01001`

Décomposer la donnée brute en un ensemble de colonnes décrivant chaque jour revient à définir le système suivant :

  * 1 vendredi
  * 1 jeudi = 10 vendredi
  * 1 mercredi = 10 jeudi
  * 1 mardi = 10 mercredi
  * 1 lundi = 10 mardi


obtenir les méta-données
=========================

.. note:: Fonction d'analyse.

Liste les méta-données pour chaque colonne sélectionnée :

  * est-ce que les valeurs sont uniques ?
  * nombre de ligne sans valeur (valeur vide)
  * nombre de ligne renseignées (valeur non-vide)
  * cardinalité : nombre de valeurs uniques différentes

