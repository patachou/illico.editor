Évaluer l'intégrité des données
################################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

Comparer 2 versions d'un même jeu de données
=============================================

.. note:: Fonction d'analyse et de traitement.

Charge un second jeu de données.

.. note:: Les colonnes des deux jeux de données peuvent avoir des intitulés différents.

.. important:: Dans les deux sources de données, les colonnes doivent être dans le même ordre.

Filtre ou exclut les lignes identiques ; ou applique une mise à jour.

**paramètres**

* définition de la clé : sélection d'une ou plusieurs colonnes
* action (choix unique)

   * extraire les données communes aux deux jeux de données
   * extraire les enregistrements clés identiques, reste de la ligne diffèrent
   * mettre à jour le jeu de données A avec les données de B
   * mettre à jour le jeu de données B avec les données de A
   * identifier les anomalies (analyse)

* traitement des collisions du fichier source A (choix unique), conserver les valeurs

   * de la ligne de la première occurrence
   * de la ligne de la dernière occurrence

* traitement des collisions du fichier source B (choix unique), conserver les valeurs

   * de la ligne de la première occurrence
   * de la ligne de la dernière occurrence


*Identifier les anomalies (analyse)* permet de détecter ces anomalies.

.. tip:: Le résultat rappelle la localisation de l'enregistrement dans chacun des deux jeux de données.

          Exemple : **A:1** / **B:4** signifie que l'enregistrement a été identifié à la première ligne de A et 4ème ligne de B.


Rapprocher 2 écritures des mêmes données
=========================================

.. note:: Fonction d'analyse et de traitement.

Rapprocher les valeurs d'une colonne des données chargées et une colonne d'un autre jeu de données en appliquant une série de traitement pour augmenter le nombre d'appariement.

Le rapprochement

**paramètres**

* définition de la clé (source A)

* comparaison sensible (oui, non)

   * à la casse (minuscule/majuscule)
   * aux accents
   * aux articles
   * aux caractères non-alphanumériques
   * à l'ordre des mots

* (obligatoire) ignorer

   * les espaces consécutifs
   * les espaces début/fin

* jeu de données à comparer (source B)

* rapprocher selon (option)

   * la distance d'édition (<= 5)
   * le ratio de similarité (>= 0.5)

* type de traitement (option)

   * conserver la meilleure proposition
   * analyser (simuler)


**traitement**

*conserver la meilleure proposition* ajoute 2 colonnes au jeu de données d'origine :

  * le meilleure score d'appariement (supérieur ou égal à 0.5)
  * la valeur de la meilleure proposition d'appariement.

Cette option produit un tableau de toutes les propositions et leur score d'appariement.

**fonction d'analyse**

*Analyser (simuler)* produit un tableau de toutes les propositions et leur score d'appariement (supérieurs ou égaux à 0.5).
