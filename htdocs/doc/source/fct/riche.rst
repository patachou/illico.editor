Transformations d'enrichissement
#################################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

fusionner, compléter ou exclure par jointure
=============================================

.. note:: Fonction d'analyse et de traitement.

Permet de réunir deux jeux de données en indiquant la **clé de jointure** pour chaque jeu de données.

Les jeux de données sont identifiés par les termes suivants :

  * **fichier maître** : les données courantes,
  * **fichier lookup** : le second fichier de données.

.. important:: Si une valeur est répétée 2 fois dans le fichier maître,
      et 3 fois dans le fichier lookup,
      fusionner les deux fichiers produira 6 (3x2) combinaisons d'appariements.

Les options disponibles permettent de conserver :

  * uniquement les correspondances exactes
      Lignes pour lesquelles les clés de jointure existent dans les deux jeux de données.

  * les lignes du fichier maître absentes du lookup
      Les clés de jointure n'existent que dans le fichier maître.

  * les lignes du lookup absentes du fichier maître
      Les clés de jointure n'existent que dans le fichier lookup.

  * les lignes du fichier maître enrichies par le lookup
      Ajoute à la fin du fichier maître les correspondances avec le fichier lookup.

  * les lignes du lookup enrichies par le fichier maître
      Ajoute à la fin du fichier lookup les correspondances avec le fichier maître.

  * toutes les lignes des deux fichiers enrichies.
      Le fichier maître est complété à droite par les rapprochements avec le fichier lookup.

      Les données du fichier lookup sans rapprochement possible sont ajoutées à la fin du tableau.

  * uniquement les lignes sans correspondance
      Conserve les lignes du fichier maître sans correspondance avec le fichier lookup.

      Ajoute à la fin du tableau les données du fichier lookup sans correspondance.

  * analyser (simuler)
      N'effectue aucune modification des données.

      Indique les cardinalités sur les lignes rapprochées et les lignes sans correspondance.

      * ``1-0`` : clés uniques du fichier maître, absentes du fichier lookup,
      * ``1-1`` : clés uniques, apparaissant 1 seule fois dans chaque fichier,
      * ``1-n`` : clés uniques du fichier maître, avec plusieurs correspondances dans le fichier lookup,
      * ``0-1`` : clés uniques du fichier lookup, absentes du fichier maître,
      * ``n-1`` : clés uniques du fichier lookup, avec plusieurs correspondances dans le fichier maître,
      * ``n-m`` : clés doublons apparaissant plusieurs fois dans chaque fichier,
      * ``n-0`` : clés doublons dans le fichier maître, sans correspondance dans le fichier lookup,
      * ``0-n`` : clés doublons dans le fichier lookup, sans correspondance dans le fichier maître,
      * lignes du fichier master sans clé (valeur vide),
      * lignes du fichier lookup sans clé (valeur vide.


**paramètres**

  * préfixe (des en-têtes de colonnes du fichier maître)
  * préfixe (des en-têtes de colonnes du fichier lookup)

Le préfixe sera ajouté sur chaque en-tête des colonnes.

.. tip:: Cette option permet de distinguer la source de données lorsque les noms des colonnes sont identiques entre les deux sources.


chiffrer/déchiffrer une donnée
===============================

Ajoute une colonne en début de tableau en reportant la valeur chiffrée/déchiffrée selon l'algorithme de chiffrement de Blaise de Vigénère.

.. tip:: La page d'accueil contient un exemple d'implémentation pour protéger l'adresse mail de contact.

.. note:: Les caractères inconnus de la clé de chiffrement sont conservés à l'identique.

.. important:: Cette transformation est limitée aux caractères codés sur 7 bits (caractères autorisés pour une adresse mail) :

      *  ``0123456789``
      *  ``abcdefghijklmnopqrstuvwxyz``
      *  ``ABCDEFGHIJKLMNOPQRSTUVWXYZ``
      *  ``-.@_``


créer une empreinte pour chaque donnée
=======================================

Ajoute une colonne en début de tableau comportant le résultat du hash des valeurs.


changer le système de numération
=================================

Ajoute une colonne en début de tableau comportant la conversion des nombres d'une colonne d'une base du système de numération à un autre

**paramètres**

  * base du système de numération de la colonne sélectionnée

     * binaire
     * octale
     * décimale
     * hexadécimale

  * base du système de numération du résultat

     * binaire
     * octale
     * décimale
     * hexadécimale
