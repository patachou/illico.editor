Transformations de filtre
##########################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

retenir les lignes ...
=======================

Retient ou exclut des lignes en fonction de la correspondance exacte à une valeur

**paramètres**

  * action (choix unique)

     * retenir les lignes situées juste avant la valeur témoin
     * retenir les lignes situées juste après la valeur témoin
     * retenir les lignes qui contiennent cette valeur
     * retenir les lignes qui ne contiennent pas cette valeur

  * valeur témoin (recherchée)

**original**

+-----+---------+
| num | données |
+=====+=========+
|  1  |  abc    |
+-----+---------+
|  2  |  abc    |
+-----+---------+
|  3  |  abc    |
+-----+---------+
|  4  |  def    |
+-----+---------+
|  5  |  ghi    |
+-----+---------+
|  6  |  abc    |
+-----+---------+
|  7  |  abc    |
+-----+---------+

**résultat 1**

  * avant cette valeur : *abc*

+-----+---------+
| num | données |
+=====+=========+
|  5  |  ghi    |
+-----+---------+

**résultat 2**

  * après cette valeur : *abc*

+-----+---------+
| num | données |
+=====+=========+
|  4  |  def    |
+-----+---------+

**résultat 3**

  * égal à cette valeur : *abc*

+------+----------+
| num  | données  |
+======+==========+
|  1   |  abc     |
+------+----------+
|  2   |  abc     |
+------+----------+
|  3   |  abc     |
+------+----------+
|  6   |  abc     |
+------+----------+
|  7   |  abc     |
+------+----------+

**résultat 4**

  * différent de cette valeur : *abc*

+------+---------+
| num  | données |
+======+=========+
|  4   |  def    |
+------+---------+
|  5   |  ghi    |
+------+---------+


première ou dernière occurrence de chaque série
================================================

Conserve la première ou dernière occurrence d'une valeur qui se répète sur plusieurs lignes consécutives.

**original**

+-----+------------+
| num | données    |
+=====+============+
|  1  | abc        |
+-----+------------+
|  2  | abc        |
+-----+------------+
|  3  | def        |
+-----+------------+
|  4  | def        |
+-----+------------+
|  5  | def        |
+-----+------------+
|  6  | abc        |
+-----+------------+
|  7  | abc        |
+-----+------------+

**résultat 1**

  * premières occurrences

+-----+------------+
| num | données    |
+=====+============+
|  1  | abc        |
+-----+------------+
|  3  | def        |
+-----+------------+
|  6  | abc        |
+-----+------------+

**résultat 2**

  * dernières occurrences

+-----+------------+
| num | données    |
+=====+============+
|  2  | abc        |
+-----+------------+
|  5  | def        |
+-----+------------+
|  7  | abc        |
+-----+------------+


lignes contenant des valeurs vides
===================================

.. note:: Fonction d'analyse et de traitement.

identifie les lignes pour lesquelles la sélection des colonnes contient (selon l'option)

  * soit au moins une valeur vide
  * soit uniquement des valeurs vides

**paramètres**

  * sélection de colonnes sur lesquelles va porter l'action
  * condition de suppression

     * au moins une valeur vide
     * uniquement des valeurs vides

  * action (choix unique)

     * filtrer
     * exclure
     * marquer [1]_
     * analyser (simuler) [2]_

.. [1] *Marquer* ajoute une colonne en début de tableau avec une étiquette "oui" lorsque la condition est vérifiée

.. [2] indique le nombre de lignes identifiées et rappelle le nombre de lignes total du jeu de données.


suivre une cohorte (regex)
===========================

Ajoute une colonne en début de tableau avec un indicateur
 - ``trouvé`` si la ligne de données décrivant l'individu indique qu'il possède le caractère recherché,
 - ``suivi`` s'il s'agit d'un autre caractère lié à un individu qui possède également le caractère recherché.

.. note:: L'en-tête de la nouvelle colonne rappellera

       - le caractère recherché,
       - la colonne correspondant au caractère,
       - et celle correspondant à l'individu.

Il est également possible de filtrer ou d'exclure la cohorte (la population *suivie*).

**paramètres**

  * colonne (de recherche du motif)

     * motif (expression rationnelle)
     * sensible à la casse : oui/non

  * action (choix unique)

     * filtrer
     * exclure
     * marquer

  * colonne (décrivant l'individu)


singletons, doublons...
========================

.. note:: Fonction d'analyse et de traitement.

Filtre, exclut ou marque les doublons identifiés par des valeurs identiques sur toutes les colonnes sélectionnées (critère d'unicité).

  * action (choix unique)

     * filtrer
     * exclure
     * marquer [3]_
     * marquer et filtrer
     * analyser (simuler)

.. [3] *Marquer* ajoute une colonne en début de tableau avec un identifiant pour chaque groupe de doublons.
       L'en-tête de cette colonne rappelle le nombre de colonnes sélectionnées (critère d'unicité).

L'analyse permet de connaître combien de lignes identiques --- sur les colonnes sélectionnées --- génèrent de doublons.


échantillonner (Fisher-Yates)
==============================

Réduit le jeu de données à un ensemble d'enregistrements tirés aléatoirement en se basant sur le mélange de Fisher–Yates.

.. note:: L'ordre des lignes de données n'est pas modifié.

**paramètres**

  * taille de l'échantillon


échantillonner avec pondérations (Fisher-Yates)
================================================

Réduit le jeu de données à un ensemble d'enregistrements tirés aléatoirement en se basant sur le mélange de Fisher-Yates effectué sur des segments [4]_ définis par

.. [4] un segment est un sous-ensemble d'individus partageant les mêmes caractéristiques

* le libellé du segment,
* la taille (nombre d'individus) du segment.

.. important:: Pour chaque segment, la taille de l'échantillon doit impérativement être inférieure ou égale au nombre d'éléments du segment.

.. note:: L'ordre des lignes de données n'est pas modifié.

**paramètres**

  * colonne décrivant le segment auquel appartient l'enregistrement
  * colonne contenant le nombre d'enregistrements à tirer aléatoirement dans ce segment


**exemple**

Prenons pour exemple le résultat d'un sondage où une catégorie de personne est sur-représentée.


**original**

+-----+-----------+
| num | catégorie |
+=====+===========+
|  1  |     A     |
+-----+-----------+
|  2  |     A     |
+-----+-----------+
|  3  |     A     |
+-----+-----------+
|  4  |     A     |
+-----+-----------+
|  5  |     A     |
+-----+-----------+
|  6  |     B     |
+-----+-----------+
|  7  |     B     |
+-----+-----------+


Nous voulons obtenir la répartition suivante :

+-----------+--------+
| catégorie | nombre |
+===========+========+
|     A     |    3   |
+-----------+--------+
|     B     |    2   |
+-----------+--------+

Il suffit pour cela de fusionner les deux tableaux avec la transformation d'enrichissement
**fusionner, compléter ou exclure des données par jointure**

**résultat intermédiaire**

+-----+-----------+--------+
| num | catégorie | nombre |
+=====+===========+========+
|  1  |     A     |    3   |
+-----+-----------+--------+
|  2  |     A     |    3   |
+-----+-----------+--------+
|  3  |     A     |    3   |
+-----+-----------+--------+
|  4  |     A     |    3   |
+-----+-----------+--------+
|  5  |     A     |    3   |
+-----+-----------+--------+
|  6  |     B     |    2   |
+-----+-----------+--------+
|  7  |     B     |    2   |
+-----+-----------+--------+

puis d'appliquer un tirage aléatoire en respectant les pondérations

**exemple de résultat (aléatoire)**

  * segment : colonne *catégorie*
  * nombre d'éléments par segment : colonne *nombre*

+-----+-----------+--------+
| num | catégorie | nombre |
+=====+===========+========+
|  1  |     A     |    3   |
+-----+-----------+--------+
|  2  |     A     |    3   |
+-----+-----------+--------+
|  4  |     A     |    3   |
+-----+-----------+--------+
|  6  |     B     |    2   |
+-----+-----------+--------+
|  7  |     B     |    2   |
+-----+-----------+--------+
