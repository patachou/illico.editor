Préférences utilisateur
########################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

configuration des préférences utilisateur
==========================================

Les préférences utilisateur sont disponibles via le menu **configurer** (liste déroulante à gauche) :

  * modifier le séparateur de colonne
  * export
  * aperçu CSV, tableau, texte


modifier le séparateur
=======================

Changer de séparateur reste une opération délicate.
En effet, si vos données contiennent déjà le caractère que vous voulez utiliser comme séparateur, certaines applications qui recevront les données peuvent ne pas charger correctement les données.

Permet de vérifier si le changement de séparateur perturbera ou non.

.. note:: Cette action est historisée.

.. tip:: Selon vos contraintes et les logiciels utilisés, vous pouvez également être amené à encapsuler les données avec un **caractère d'échappement** --- par exemple ``"``, ``'``, ``\``.


export
=======

Configuration spécifique pour l'export au format CSV.

**Modifier le nom de l'export CSV (défaut : date_heure)**

  * désactivé par défaut
  * permet de basculer entre les modes :

     * à l'export, possibilité de nommer le fichier avant de le sauver sur disque
     * à l'export, le fichier porte le nom par défaut ``illico_aaaa-mm-jj_hhmmss``


aperçu CSV, tableau, texte
===========================

**Interpréter < > et & comme du HTML**

  *  désactivé par défaut
  *  permet de basculer entre les modes :

     * interpréter les balises HTML (mise en forme)
     * ne pas interpréter les balises HTML


**aperçu texte : ligne de séparation entre l'en-tête et les données**

  * désactivé par défaut
  * permet de préciser le caractère à répéter pour dessiner la ligne (par défaut ``----``)
