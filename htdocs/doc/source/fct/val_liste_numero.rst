Transformations de liste (numéroter)
####################################

.. contents:: Contenu
   :local:
   :backlinks: top

-------


numéroter les valeurs
======================

Ajoute une colonne en début de tableau comportant la numérotation des valeurs en liste selon la séquence

::

    = valeur de départ + (pas x nombre de lignes déjà numérotées)


**paramètres**

  * liste des valeurs

     * sélection (colonne)
     * séparateur des valeurs en liste

  * règle de numérotation

     * titre de la colonne
     * valeur de départ
     * pas (incrément)
     * les listes vides :

        * sont ignorées
        * contiennent une seule valeur *vide*

.. note:: La valeur de départ et le pas peuvent être des nombres négatifs et décimaux.
