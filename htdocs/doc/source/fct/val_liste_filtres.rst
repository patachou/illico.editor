Transformations de liste (filtres)
###################################

.. contents:: Contenu
   :local:
   :backlinks: top


filtrer ou exclure les valeurs d'une liste
===========================================

Ajoute une colonne au début du tableau avec le résultat de l'opération de comparaison entre les valeurs d'une liste et la valeur d'une autre colonne.

.. note:: Les valeurs vides de la liste ou la valeur à comparer, si vide, sont ignorées.

**paramètres**

  * séparateur
  * test d'égalité numérique

     * égal, non égal à
     * supérieur/inférieur ou égal à
     * strictement supérieur/inférieur à

  * colonne de comparaison


réduire la liste à certaines clés
==================================

Ajoute une colonne au début du tableau avec le résultat de l'opération de filtre (conserver ou exclure) des couples clé/valeur par rapport à une autre colonne.

**paramètres**

  * séparateur des éléments de la colonne à traiter
  * opérateur (clé/valeur)
  * action

     * conserver
     * exclure

  * séparateur des éléments de la colonne contenant la liste des clés à conserver ou exclure
