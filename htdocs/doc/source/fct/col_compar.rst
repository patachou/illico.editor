Comparaisons de colonne
########################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

comparer des colonnes 2 par 2
==============================

Ajoute des colonnes en début de tableau contenant le résultat de la comparaison des valeurs des colonnes prises deux par deux.

+-----------+-----------+------------------+
| colonne 1 | colonne 2 |   résultat       |
+===========+===========+==================+
|   2010    |  2010     |  v\. égales      |
+-----------+-----------+------------------+
|           |  2010     |  v\. à droite    |
+-----------+-----------+------------------+
|   2010    |           |  v\. à gauche    |
+-----------+-----------+------------------+
|   2010    |  2009     |  v\. différentes |
+-----------+-----------+------------------+
|           |           |  aucune valeur   |
+-----------+-----------+------------------+


calculer la distance d'édition / colonnes
==========================================

.. note:: La distance d'édition est le nombre de caractères à ajouter,
    supprimer, modifier pour passer d'une écriture à une autre.

Ajoute une colonne en début de tableau contenant la distance d'édition entre les valeurs de deux colonnes.


calculer la distance d'édition / lignes
========================================

Ajoute une colonne en début de tableau avec comme valeur de retour la valeur de la colonne des valeurs comparées lorsque la valeur rencontrée est proche de la valeur de la ligne courante.

.. note:: L'en-tête de la nouvelle colonne rappelle la colonne des valeurs comparées et le seuil de la distance d'édition.

.. tip:: Les valeurs de retours de colonnes différentes seront listées dans le même ordre.

.. important:: Le temps de traitement peut être très long.

**paramètres**

  * valeurs comparées (colonne)

     * (distance d'édition) strictement inférieure à

  * valeurs de retour (colonne)

     * séparateur

**original**

+--------------+------------------------+
| individu     | adresse                |
+==============+========================+
| M\. DUPONT   | 14 bd des roses        |
+--------------+------------------------+
| M\. DUPOND   | 3 rue des jonchères    |
+--------------+------------------------+
| M\. DU PONT  | 15 boulevard des roses |
+--------------+------------------------+

**résultat 1**

  * valeurs comparées : colonne *individu*

     * (distance d'édition) strictement inférieure à 2

  * valeurs de retour : colonne *individu*

     * séparateur : , ``virgule espace``

+-------------------------+--------------+------------------------+
|  ~ (3) individu         | individu     | adresse                |
+=========================+==============+========================+
| M\. DUPOND, M\. DU PONT | M\. DUPONT   | 14 bd des roses        |
+-------------------------+--------------+------------------------+
| M\. DUPONT              | M\. DUPOND   | 3 rue des jonchères    |
+-------------------------+--------------+------------------------+
| M\. DUPONT              | M\. DU PONT  | 15 boulevard des roses |
+-------------------------+--------------+------------------------+

**résultat 2**

  * valeurs de retour : colonne *adresse*

+---------------------------------------------+-------------------------+--------------+------------------------+
| ~ (3) individu                              | ~ (3) individu          | individu     | adresse                |
+=============================================+=========================+==============+========================+
| 3 rue des jonchères, 15 boulevard des roses | M\. DUPOND, M\. DU PONT | M\. DUPONT   | 14 bd des roses        |
+---------------------------------------------+-------------------------+--------------+------------------------+
| 14 bd des roses                             | M\. DUPONT              | M\. DUPOND   | 3 rue des jonchères    |
+---------------------------------------------+-------------------------+--------------+------------------------+
| 14 bd des roses                             | M\. DUPONT              | M\. DU PONT  | 15 boulevard des roses |
+---------------------------------------------+-------------------------+--------------+------------------------+


identifier le min ou max / colonnes
====================================

Ajoute une colonne en début de tableau avec le résultat de la recherche du minimum ou du maximum sur plusieurs colonnes.

.. note:: La colonne résultat rappellera l'objectif de la transformation :

         - ``min`` ou ``max``
         - ``(x)`` avec x = nombre de colonnes sélectionnées
         - ``/0`` ou ``/_``

         exemple :

         - objectif : identifier le ``minimum`` parmi ``5`` colonnes sélectionnées, les valeurs vides seront ``traduites par zéro``
         - en-tête résultat : ``minimum(5)/0``

**paramètres**

  * identifier (choix unique)

     * minimum
     * maximum

  * les valeurs vides sont (choix unique)

     * ignorées
     * traduites par zéro


comparer une valeur et plusieurs colonnes
==========================================

  * comparaison (au choix)

     * numérique

        * égal, non égal à
        * supérieur/inférieur ou égal à
        * strictement supérieur/inférieur à

     * de texte

        * identique/différent

  * valeurs de retour

     * liste de valeurs
     * liste des intitulés des colonnes

  * séparateur des valeurs de retour


.. important:: Pour une valeur commençant par des chiffres et se terminant
  par des lettres, **pour les comparaisons numériques** seule la partie numérique est prise en compte pour la
  comparaison.

  exemples :
    -  33.3%, 33,3%, 33.3 % seront équivalents à 33.3

    -  2011-2012 est équivalent à 2011

.. tip:: L'en-tête de la colonne résultat rappelle

         - la colonne de référence
         - la comparaison
         - le nombre de colonnes comparées
         - le type de résultat ``val`` (liste de valeurs) ou ``col`` (liste des en-têtes des colonnes qui valident la condition)
