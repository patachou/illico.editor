Compatibilités
###############

.. contents:: Contenu
   :local:   
   :backlinks: top

-------

général
========

fonction explorer (les données)
--------------------------------

  * ne tolère pas la sélection --- clic sur une valeur --- d'une valeur contenant la balise ``<br />`` [1]_
  * ne tolère pas les balises HTML inconsistances

.. [1] La balise ``<br>`` est en revanche correctement gérée.


Libre/OpenOffice
=================

fonction d'analyse (synthèses)
-------------------------------

  * pour récupérer la ligne des totaux, préférez le texte non-formaté [2]_
  * pour conserver les espaces en début/fin et espaces multiples, préférez le texte non-formaté

.. [2] collage spécial (``Ctrl + Maj + v`` au lieu de ``Ctrl + v``)


export HTML
------------

  * Oocalc/Localc ne gère pas plusieurs couleurs dans une même cellule


Firefox et Libre/OpenOffice
============================

  * Oowriter/Lowriter gère la mise en forme limitée (gras, souligné, italique) mais ni la couleur de fond ni celle de texte des cellules [3]_

.. [3] Pour cette utilisation, préférez Chrome ou Vivaldi plutôt que Firefox


Chrome/Vivaldi
===============

  * *sélection du fichier* : Chrome ne recharge pas la source si elle est identique, sélectionnez un autre fichier puis celui désiré [4]_

.. [4] Si n'avez pas besoin de conserver l'historique, presser simplement F5 (réinitialise l'historique).


Excel
======

A l'export *tableau* de données contenant ``<br />`` ou ``<br>`` pour générer des sauts de ligne :

  * Excel interprète chaque saut de ligne comme une nouvelle cellule et fusionne verticalement les autres colonnes pour conserver globalement la structure du tableau
  * Excel ne gère pas plusieurs couleurs dans une même cellule [5]_

.. [5] Copiez le résultat HTML dans un traitement de texte Word puis recollez le tableau obtenu dans Excel.

