Architecture
#############

.. contents:: Contenu
   :local:
   :backlinks: top

-------

Organisation du code source
============================

Cette application Web fonctionne sans serveur et repose sur des sources HTML5 et Javascript :

**sources**

  * **index.html** : page d'accueil : liens vers Illico, documentation, tutoriels, mail de contact

  * **illicoEditor.html** : interface utilisateur Illico Editor

  * **css** : répertoire de style de l'application ::

      style.css
      page-menu.css (bandeau supérieur)
      page-ctrl.css (panneau gauche)
      page-main.css (panneau central)

  * **scripts** : répertoire des sources Javascript ::

      main.js
      sort.js
      version.js

**ressources**

  * **doc** : documentation
  * **tuto** : tutoriels et cas concrets

.. note:: Ces ressources sont accessibles *via* la page d'accueil et depuis l'application Illico.

La documentation et les tutoriels sont livrés dans trois formes :

  * code-source au format reStructuredText,
  * format HTML (généré avec Sphinx),
  * format PDF (généré avec Sphinx).


Conventions de programmation
=============================

Le code-source suit globalement les conventions suivantes :

+------------+----------------------------------------------------------------------------+
| variable   | usage                                                                      |
+============+============================================================================+
| a, ar      | tableau (1 dimension)                                                      |
+------------+----------------------------------------------------------------------------+
| aa         | tableau de tableaux (2 dimensions)                                         |
+------------+----------------------------------------------------------------------------+
| d          | tableau (1 dimension) utilisé pour mettre à jour une ligne du tableau data |
+------------+----------------------------------------------------------------------------+
| data       | tableau de données (data[i][c] avec i n° ligne, c n° de colonne)           |
+------------+----------------------------------------------------------------------------+
| di         | tableau (1 dimension) d'indices des lignes (ou colonnes) à supprimer       |
+------------+----------------------------------------------------------------------------+
| b          | booléen                                                                    |
+------------+----------------------------------------------------------------------------+
| c          | indice (numéro) de colonne                                                 |
+------------+----------------------------------------------------------------------------+
| cpt        | utilisé pour compter/décompter (incrément)                                 |
+------------+----------------------------------------------------------------------------+
| cs         | sélection multiple (balise HTML ``<select>``)                              |
+------------+----------------------------------------------------------------------------+
| e          | élément HTML (balises HTML ``<select>``, ``<table>``, etc.)                |
+------------+----------------------------------------------------------------------------+
| h          | en-tête nouvelle colonne (String) pour compléter le tableau header         |
+------------+----------------------------------------------------------------------------+
| header     | tableau d'en-tête des données (1 dimension)                                |
+------------+----------------------------------------------------------------------------+
| i, j, k    | compteurs incrémentaux (Integer ou Float)                                  |
+------------+----------------------------------------------------------------------------+
| l, l1, l2  | longueurs de chaîne ou de tableau                                          |
+------------+----------------------------------------------------------------------------+
| m          | message feed-back (consigné dans l'historique) (String)                    |
+------------+----------------------------------------------------------------------------+
| n          | nombre (Integer ou Float)                                                  |
+------------+----------------------------------------------------------------------------+
| o          | option sélectionnée parmi une liste d'options (String ou Boolean)          |
+------------+----------------------------------------------------------------------------+
| p          | liste des paramètres inscrits dans l'historique (Array ou String)          |
+------------+----------------------------------------------------------------------------+
| r          | chaîne de remplacement, valeur de retour                                   |
+------------+----------------------------------------------------------------------------+
| reg        | expression rationnelle / motif (regular expression)                        |
+------------+----------------------------------------------------------------------------+
| s          | chaîne de caractères de liaison ou séparateur (String)                     |
+------------+----------------------------------------------------------------------------+
| sc         | option spécifique expression régulière : sensible à la casse               |
+------------+----------------------------------------------------------------------------+
| t          | texte long (String)                                                        |
+------------+----------------------------------------------------------------------------+
| trad       | table de traduction des options (tableau associatif de constantes)         |
+------------+----------------------------------------------------------------------------+
| v, v1, v2  | valeur (String ou Integer)                                                 |
+------------+----------------------------------------------------------------------------+
| x          | id d'un élément HTML                                                       |
+------------+----------------------------------------------------------------------------+
| {x}lkp     | variable du fichier lookup (par opposition à {x} pour le fichier maître)   |
+------------+----------------------------------------------------------------------------+

.. tip:: Plus une variable est utilisée souvent, plus son nom est court.

.. warning:: Les lettres **f** et **g** sont utilisées pour des fonctions *ad hoc*.

.. note:: Les parties à réécrire sont indiquées par *FIXME* et *TODO*.


Quelques chiffres
==================

.. généré automatiquement

.. literalinclude:: count.txt


Métriques JSHint
=================

**Code JS principal**

**mars 2025**

::

    There are 540 functions in this file.
    Function with the largest signature take 9 arguments, while the median is 2.
    Largest function has 75 statements in it, while the median is 3.
    The most complex function has a cyclomatic complexity value of 36 while the median is 2.

source : http://jshint.com/


Ajouter une nouvelle transformation
====================================

Pour ajouter une nouvelle fonctionnalité rapidement, inspirez-vous d'une
fonction existante :

  1. **illicoEditor.html** :

     -  écrivez la description de la fonction dans la liste des fonctionnalités de l'élement ``class="catalogue"`` (cf. lignes > L155)
     -  attribuez un numéro sans conflit avec ceux existants (ordre croissant)
     -  décrivez le masque de saisie dans une *div* ``id='action__[xx]'``

  2. **main.js** complétez si nécessaire les fonctions javascript :

     -  ``masquerTout()``
     -  ``majSelecteurs()``
     -  ``plus1Ligne()``
     -  ``moins1Ligne()``

  3. **main.js** complétez

     -  le corps de la fonction JS dans la liste des transformations : ``ILLICO : TRANSFORMATIONS``
