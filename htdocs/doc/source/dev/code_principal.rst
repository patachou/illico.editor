Code source principal
######################

.. contents:: Contenu
   :local:
   :backlinks: top

-------

Organisation
=============

**Liste des fonctions de premier niveau**

.. généré automatiquement

.. literalinclude:: toc_script_principal.txt


Longueur des fonctions
=======================

**Liste des fonctions de premier niveau**

.. note:: Compte toute instruction, ligne vide ou commentaire dans les fonctions.

Globalement,

 * 65% des fonctions font moins de 45 lignes de code,
 * 12% des fonctions font plus de  80 lignes de code.
 *  2% des fonctions font plus de 150 lignes de code.

.. généré automatiquement

.. literalinclude:: count_function.txt
   :emphasize-lines: 184, 247, 274
