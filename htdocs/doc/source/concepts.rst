Présentation et concepts
=========================


Quoi ?
-------

Avec Illico, vous pouvez depuis un seul outil

  * analyser : domaine de valeurs, répartition, unicité, valeurs vides,
  * pointer/comparer les données de plusieurs sources,
  * comparer plusieurs versions d'un même jeu de données,
  * croiser les sources : enrichir les données,
  * transformer les données : pivot, normalisation,
  * traiter/corriger en masse :

      * syntaxe,
      * lexique,
      * sémantique,
      * règles métiers,

  * construire un référentiel :

      * normalisation,
      * identification des doublons,

  * préparer des restitutions :

      * listing,
      * indicateurs statistiques.

.. important::

    Illico réduit la complexité de la qualification de données à sa simple expression.


Pourquoi ?
-----------

Illico permet de réduire de 80% votre temps consacré à la qualification de données

  * en rendant accessibles des traitements complexes sans pré-requis,
  * en minimisant le nombre d'actions pour obtenir un résultat,
  * en documentant à la volée vos actions,
  * en simplifiant le chargement de données,
  * en simplifiant la récupération des résultats.


Qui ?
------

Du fait de fonctionnalités très avancées et nécessitant aucun pré-requis, l'outil s'adresse à tous sans distinction.

Les utilisateurs actuels d'Illico sont avant tout responsables

  * du contrôle qualité des données,
  * de l'enrichissement de données par croisement de plusieurs sources,
  * de la mise en forme ou de l'exploitation d'annuaire,
  * de la production d'indicateurs statistiques simples/multidimensionnels,
  * de la qualification de données,
  * d'intégration des systèmes d'information.


Quand ?
--------

Il faut compter entre 2 et 3 heures de pratique pour bien comprendre le catalogue des fonctionnalités.

En quelques jours, vous serez à l'aise sur les fonctionnalités qui **vous** sont nécessaires.

.. tip:: Au début, commencez par poser sur le papier la suite d'actions à
    entreprendre pour analyser, corriger ou re-structurer vos données.


Comment ?
----------

Choix techniques
~~~~~~~~~~~~~~~~~

La compatibilité repose sur les standards HTML5/CSS3/JS,

.. attention:: Les navigateurs peuvent réagir légèrement différemment sur certaines fonctionnalités.
..

 * ne dépend d'aucune bibliothèque,
 * ne fait pas appel à du code tiers local ou distant,
 * ne dialogue avec aucune base de données locale ou distante,
 * ne dialogue avec aucun serveur Web,
 * ne modifie pas votre fichier de données source,
 * conserve les données jusqu'à la suppression du cache du navigateur.


Développement
~~~~~~~~~~~~~~

Le développement suit le principe *ASAP* -- *as simple as possible*.

 * ne manipule que des chaînes de caractères,
 * dans un grand tableau,
 * s'affranchit des différentes notations des décimales,
 * les valeurs vides -- *nulles* -- sont ignorées et ne provoquent pas d'erreur dans les calculs,
 * cloisonne chaque traitement pour éviter des effets de bords,
 * repose sur des algorithmes et une programmation objet simples.


Qualité
~~~~~~~~

Régulièrement, le code source est intégralement revu pour en améliorer sa maintenance :

  * application des recommandations JSLint/JSHint,
  * réorganisation du code source,
  * documentation JSDoc,
  * tests de non-régression.


Licence d'utilisation
----------------------

Illico Editor est distribué sous licence :download:`GNU GPL V3 </_static/licence/GNU_GPL.txt>`.

Pour plus d'information, reportez-vous à la `Free Software Fundation <http://www.fsf.org/>`__.

::

    Illico Editor - data quality Swiss Army knife
    Copyright (C) 2025 Arnaud COCHE < i l l i c o . e d i t o r /// n e t - c . f r >

    This program is free software: you can redistribute it
    and/or modify it under the terms of the GNU General Public
    License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option)
    any later version.

    This program is distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
    PURPOSE.
    See the GNU General Public License for more details.
