Un outil magique ? Oui, Illico Editor !
========================================

Liens
------

La dernière version **stable** est disponible sous la forme d'une archive compressée
qui contient l'ensemble de cette documentation, les tutoriels ainsi que les sources.

-  archives complètes :
   `zip <http://illico.ti-nuage.fr/releases/illicoEditor_latest.zip>`__ /
   `tgz <http://illico.ti-nuage.fr/releases/illicoEditor_latest.tar.gz>`__
-  en ligne :
   `site Web <http://illico.ti-nuage.fr>`__ /
   `accès Illico <http://illico.ti-nuage.fr/illicoEditor.html>`__
   (les données restent sur votre poste)
-  documentation :
   `format PDF <../latex/IllicoEditor.pdf>`__


Un assistant pour qualifier vos données
----------------------------------------

Fin 2011,
je tentais de résoudre un problème bien connu :

  * apporter un outil **simple et efficace**
  * aux utilisateurs métiers
  * en matière de qualification de données.

**Simple**
    c'est-à-dire sans devoir apprendre un langage de programmation ou se servir d'une base de données.

**Efficace**
    autrement dit sans délai.


Orienté utilisateur métier
---------------------------


Illico Editor est un couteau-suisse de la qualification de données,

  * développé dès l'origine pour permettre à tous,
  * de traiter, préparer, rectifier facilement et rapidement des listings,
  * sans pré-requis technique.


Des transformations prêtes à emploi
------------------------------------

Illico possède plus de 150 fonctionnalités classiques ou complexes.

Pour ne citer que quelques-unes :

analyse de données
    valeurs non-saisies, domaines et répartition des valeurs...

croisement de fichiers
    comparaison, enrichissement, exclusion...

correction en masse
    pivot, corrections syntaxiques, conditionnelles, manipulation des données en liste...

recherche et filtre
    n-ième occurrence, doublon, isoler ou exclure une cohorte...

restitution
    enchaînements de tableaux croisé-dynamiques, tableaux de synthèse...
