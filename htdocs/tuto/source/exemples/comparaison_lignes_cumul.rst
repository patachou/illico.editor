comparer des lignes dans un fichier cumul
==========================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


contexte
---------

On souhaite comparer deux jeux de données décrivant deux périodes successives

* à partir de deux extractions de données (au format identique) décrivant les deux périodes
* ou à partir d'un unique fichier cumul des deux périodes

.. tip:: Pour produire un fichier cumul, le plus simple consiste

        * à utiliser la fonctionnalité *CREER UN JEU DE DONNEES -> créer une liste à partir d'un texte/tableau*
        * par copier-coller des deux jeux de données (en supprimant la première ligne d'en-tête du second fichier)

Exemple : la situation de plusieurs personnes, en comparant deux sources de données : l'une extraite en 2021 et l'autre en 2022 (par exemple *prévisionnel* et *réalisé*).

+-------+-------+---------------+------------+------------+
| src   | id    | situation     | début      | fin        |
+=======+=======+===============+============+============+
| 2021  |  1    | contractuel   | 2022-01-01 | 2022-01-31 |
+-------+-------+---------------+------------+------------+
| 2021  |  2    | titulaire     | 2022-01-01 | 2099-12-31 |
+-------+-------+---------------+------------+------------+
| 2021  |  3    | contractuel   | 2022-06-01 | 2022-06-30 |
+-------+-------+---------------+------------+------------+
| 2021  |  4    | collaborateur | 2018-01-01 | 2022-12-31 |
+-------+-------+---------------+------------+------------+
| 2021  |  5    | collaborateur | 2019-01-01 | 2019-12-31 |
+-------+-------+---------------+------------+------------+
| 2022  |  1    | contractuel   | 2022-02-01 | 2022-02-28 |
+-------+-------+---------------+------------+------------+
| 2022  |  2    | titulaire     | 2022-01-01 | 2099-12-31 |
+-------+-------+---------------+------------+------------+
| 2022  |  3    | contractuel   | 2022-06-01 | 2022-06-22 |
+-------+-------+---------------+------------+------------+
| 2022  |  4    | collaborateur | 2018-01-01 | 2022-12-31 |
+-------+-------+---------------+------------+------------+


mode opératoire
----------------

La première étape consiste à identifier les personnes qui n'ont aucun changement, donc les lignes identiques dans les deux sources.

La deuxième étape consiste à supprimer les lignes décrivant les personnes qui n'ont pas effectué de changement.

La troisième étape consiste à distinguer les arrivées, départs et les personnes qui n'ont pas la même situation dans les deux sources.


1. identifier les personnes qui n'ont pas changé de situation
--------------------------------------------------------------

FILTRES -> singletons, doublons...

**paramètres**

::

    sélection          : id, situation, début, fin
    pour la sélection  : marquer les doublons

+---------------+-------+-------+---------------+------------+------------+
| ! doublons(4) | src   | id    | situation     | début      | fin        |
+===============+=======+=======+===============+============+============+
|               | 2021  |  1    | contractuel   | 2022-01-01 | 2022-01-31 |
+---------------+-------+-------+---------------+------------+------------+
| 1             | 2021  |  2    | titulaire     | 2022-01-01 | 2099-12-31 |
+---------------+-------+-------+---------------+------------+------------+
|               | 2021  |  3    | contractuel   | 2022-06-01 | 2022-06-30 |
+---------------+-------+-------+---------------+------------+------------+
| 2             | 2021  |  4    | collaborateur | 2018-01-01 | 2022-12-31 |
+---------------+-------+-------+---------------+------------+------------+
|               | 2021  |  5    | collaborateur | 2019-01-01 | 2019-12-31 |
+---------------+-------+-------+---------------+------------+------------+
|               | 2022  |  1    | contractuel   | 2022-02-01 | 2022-02-28 |
+---------------+-------+-------+---------------+------------+------------+
| 1             | 2022  |  2    | titulaire     | 2022-01-01 | 2099-12-31 |
+---------------+-------+-------+---------------+------------+------------+
|               | 2022  |  3    | contractuel   | 2022-06-01 | 2022-06-22 |
+---------------+-------+-------+---------------+------------+------------+
| 2             | 2022  |  4    | collaborateur | 2018-01-01 | 2022-12-31 |
+---------------+-------+-------+---------------+------------+------------+

.. tip:: Chaque numéro en première colonne correspond à une personne qui possède exactement la même situation sur les deux extractions.


2. supprimer les personnes qui n'ont pas effectué de changement
----------------------------------------------------------------

FILTRES -> retenir les lignes...

**paramètres**

::

    sélection          : ! doublons(4) 
    retenir les lignes : qui contiennent ...
    ... valeur témoin  : (laisser vide)

+---------------+-------+-------+---------------+------------+------------+
| ! doublons(4) | src   | id    | situation     | début      | fin        |
+===============+=======+=======+===============+============+============+
|               | 2021  |  1    | contractuel   | 2022-01-01 | 2022-01-31 |
+---------------+-------+-------+---------------+------------+------------+
|               | 2021  |  3    | contractuel   | 2022-06-01 | 2022-06-30 |
+---------------+-------+-------+---------------+------------+------------+
|               | 2021  |  5    | collaborateur | 2019-01-01 | 2019-12-31 |
+---------------+-------+-------+---------------+------------+------------+
|               | 2022  |  1    | contractuel   | 2022-02-01 | 2022-02-28 |
+---------------+-------+-------+---------------+------------+------------+
|               | 2022  |  3    | contractuel   | 2022-06-01 | 2022-06-22 |
+---------------+-------+-------+---------------+------------+------------+


3. distinguer les arrivées, les départs et les personnes qui changent de situation
-----------------------------------------------------------------------------------

FILTRES -> singletons, doublons...

**paramètres**

::

    sélection          : id
    pour la sélection  : marquer les doublons

+-----------------+---------------+-------+-------+---------------+------------+------------+
| ! doublons(1)   | ! doublons(4) | src   | id    | situation     | début      | fin        |
+=================+===============+=======+=======+===============+============+============+
| 1               |               | 2021  |  1    | contractuel   | 2022-01-01 | 2022-01-31 |
+-----------------+---------------+-------+-------+---------------+------------+------------+
| 2               |               | 2021  |  3    | contractuel   | 2022-06-01 | 2022-06-30 |
+-----------------+---------------+-------+-------+---------------+------------+------------+
|                 |               | 2021  |  5    | collaborateur | 2019-01-01 | 2019-12-31 |
+-----------------+---------------+-------+-------+---------------+------------+------------+
| 1               |               | 2022  |  1    | contractuel   | 2022-02-01 | 2022-02-28 |
+-----------------+---------------+-------+-------+---------------+------------+------------+
| 2               |               | 2022  |  3    | contractuel   | 2022-06-01 | 2022-06-22 |
+-----------------+---------------+-------+-------+---------------+------------+------------+

* chaque numéro en première colonne correspond à une personne qui n'a pas la même situation entre les deux jeux de données, autrement dit, qui a changé de situation
* quand il n'y a pas de numéro en première colonne

  * soit il s'agit d'une arrivée (la source indique 2022, aucune information pour 2021)
  * soit il s'agit d'un départ (la source indique 2021, aucune information pour 2022)
