fidélisation, suivi d'une cohorte
==================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


contexte
---------

Le cas pratique proposé est le suivant : on dispose d'un tableau de suivi d'un effectif --- les individus sont ici identifiés par un numéro 1,2,3 --- à partir duquel on souhaite connaître :

- le nombre d'individu arrivés ou partis durant la période d'observation
- le nombre d'individu qui ont changé de statut
- les statuts qui sont majoritairement les premiers ou derniers statuts des trajectoires/parcours de ces individus


+---+--------+-------+------+
| x | statut | début | fin  |
+===+========+=======+======+
| 1 |   A    | 2019  | 2019 |
+---+--------+-------+------+
| 2 |   A    | 2020  | 2021 |
+---+--------+-------+------+
| 1 |   B    | 2020  | 2022 |
+---+--------+-------+------+
| 1 |   C    | 2023  | 2023 |
+---+--------+-------+------+
| 1 |   A    | 2024  | 2024 |
+---+--------+-------+------+
| 3 |   C    | 2023  | 2024 |
+---+--------+-------+------+
| 2 |   D    | 2022  | 2022 |
+---+--------+-------+------+
| 2 |   F    | 2023  | 2023 |
+---+--------+-------+------+


On prendra comme hypothèse que le tableau est cumulatif, au sens où pour chaque individu, la lecture successive des lignes rend compte des statuts et périodes successifs de l'individu (ordre chronologique).

Les colonnes *début* et *fin* devenant inutiles par la suite, elles sont supprimées avec **COLONNES -> supprimer ou conserver**. 


mode opératoire
----------------

Les étapes consisteront à créer des listes d'état pour chaque individu.

1. pour chaque individu, chaîner les états afin de reconstituer l'histoire de chaque individu
2. marquer/identifier les premiers et derniers états de chaque individu
3. transformer en données unitaires/atomiques la suite d'état
4. analyser


1. chaîner les états
---------------------

**LIGNES -> compacter plusieurs lignes**

**paramètres**

::

     clé        : x
     liste      : statut
     séparateur : ->

**résultat**

+---+------------+
| x | statut     |
+===+============+
| 1 | A->B->C->A |
+---+------------+
| 2 |  A->D->F   |
+---+------------+
| 3 |   C        |
+---+------------+


2. marquer/identifier les premiers et derniers états de chaque individu
------------------------------------------------------------------------

**VALEURS -> préfixer et/ou suffixer**

**paramètres**

::

     colonne : statut
     préfixe : DEB->
     suffixe : ->FIN

**résultat**

+---+----------------------+
| x |        statut        |
+===+======================+
| 1 | DEB->A->B->C->A->FIN |
+---+----------------------+
| 2 | DEB->A->D->F->FIN    |
+---+----------------------+
| 3 | DEB->C->FIN          |
+---+----------------------+


3. transformer en données unitaires/atomiques la suite d'état
--------------------------------------------------------------

**TRADUIRE LES LISTES -> traduire les listes en une matrice**

À partir du résultat obtenu, quatre scénarios sont possibles :

* connaître les relations entre l'état d'origine et le reste de la trajectoire
* connaître les relations entre l'état d'origine et chaque état suivant de la trajectoire
* connaître les relations entre les états, de proche en proche
* connaître les relations entre tous les états

.. important:: Si vous souhaitez tester les quatre scénarios,

   * sauvez/exportez ici votre jeu de données
   * et recharger-le avant les étapes 3.1, 3.2, 3.3, et 3.4


3.1 connaître les relations entre l'état d'origine et le reste de la trajectoire
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. note:: Le scénario consiste à répéter deux fois la même transformation.

**paramètres**

::

    sélection              : statut
    séparateur             : , (ici, caractère arbitraire absent des données)
    clé => valeur          : ->
    'a => b => c' signifie : a => (b => c)

**résultat**

+---+----------------------+-----------------+
| x |        statut        | DEB             |
+===+======================+=================+
| 1 | DEB->A->B->C->A->FIN | A->B->C->A->FIN |
+---+-----------------+----+-----------------+
| 2 | DEB->A->D->F->FIN    | A->D->F->FIN    |
+---+----------------------+-----------------+
| 3 | DEB->C->FIN          | C->FIN          |
+---+----------------------+-----------------+

La même transformation sera appliquée cette fois sur la colonne *DEB*.

**paramètres**

::

    sélection                : DEB
    séparateur               : , (ici, caractère arbitraire absent des données)
    clé => valeur            : ->
    'a => b => c' signifie   : a => (b => c)

**résultat**

+---+----------------------+-----------------+--------------+-----+
| x |        statut        | DEB             | A            |  C  |
+===+======================+=================+==============+=====+
| 1 | DEB->A->B->C->A->FIN | A->B->C->A->FIN | B->C->A->FIN |     |
+---+-----------------+----+-----------------+--------------+-----+
| 2 | DEB->A->D->F->FIN    | A->D->F->FIN    | D->F->FIN    |     |
+---+----------------------+-----------------+--------------+-----+
| 3 | DEB->C->FIN          | C->FIN          |              | FIN |
+---+----------------------+-----------------+--------------+-----+


3.2 connaître les relations entre l'état d'origine et chaque état suivant de la trajectoire
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. note:: Le scénario consiste à répéter deux fois la même transformation.

**paramètres**

::

    sélection                : statut
    séparateur               : , (ici, caractère arbitraire absent des données)
    clé => valeur            : ->
    séparateur des résultats : -> (séparateur important ici du fait de l'interprétation sélectionnée ci-dessous)
    'a => b => c' signifie   : (a => b), (a => c)

**résultat**


+---+----------------------+-----------------+
| x |        statut        | DEB             |
+===+======================+=================+
| 1 | DEB->A->B->C->A->FIN | A->B->C->A->FIN |
+---+----------------------+-----------------+
| 2 | DEB->A->D->F->FIN    | A->D->F->FIN    |
+---+----------------------+-----------------+
| 3 | DEB->C->FIN          | C->FIN          |
+---+----------------------+-----------------+


La même transformation sera appliquée cette fois sur la colonne *DEB*.

**paramètres**

::

    sélection                : DEB
    séparateur               : , (ici, caractère arbitraire absent des données)
    clé => valeur            : ->
    séparateur des résultats : ->
    'a => b => c' signifie   : (a => b), (a => c)

**résultat**

+---+----------------------+-----------------+--------------+-----+
| x |        statut        | DEB             | A            |  C  |
+===+======================+=================+==============+=====+
| 1 | DEB->A->B->C->A->FIN | A->B->C->A->FIN | B->C->A->FIN |     |
+---+----------------------+-----------------+--------------+-----+
| 2 | DEB->A->D->F->FIN    | A->D->F->FIN    | D->F->FIN    |     |
+---+----------------------+-----------------+--------------+-----+
| 3 | DEB->C->FIN          | C->FIN          |              | FIN |
+---+----------------------+-----------------+--------------+-----+

.. note:: Si les résultats sont identiques au scénario 3.1, le traitement et l'interprétation du résultat en sont différentes :

    * en 3.1, le traitement a produit une colonne A qui décrit un seul élément (une liste constituée d'une suite de valeurs) *par rapport* à *A* (plus exactement *DEB->A*)
    * en 3.2, le traitement a produit une colonne A qui décrit directement un ensemble de valeurs *par rapport* à *DEB->A*
    
    Ainsi ``DEB->A->B->C->A->FIN,DEB->A->F->FIN`` aurait été transformé
    
    * d'après le scénario 3.1, avec le séparateur de résultat *+*, en colonne A = ``B->C->A->FIN+F->FIN`` (2 éléments séparés par ``+``, chaque élément décrivant une suite de valeurs)
    * d'après le scénario 3.2, avec le séparateur de résultat *+*, en colonne A = ``B+C+A+FIN+F+FIN`` (6 éléments distincts, séparés par ``+``)


3.3 connaître les relations entre les états, de proche en proche
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**paramètres**

::

    sélection                : statut
    séparateur               : , (ici, caractère arbitraire absent des données)
    clé => valeur            : ->
    séparateur des résultats : +
    'a => b => c' signifie   : (a => b), (b => c)

**résultat**

+---+----------------------+-----+-------+---+-----+---+-----+
| x |        statut        | DEB | A     | B |  C  | D |  F  |
+===+======================+=====+=======+===+=====+===+=====+
| 1 | DEB->A->B->C->A->FIN |  A  | B+FIN | C |  A  |   |     |
+---+----------------------+-----+-------+---+-----+---+-----+
| 2 | DEB->A->D->F->FIN    |  A  | D     |   |     | F | FIN |
+---+----------------------+-----+-------+---+-----+---+-----+
| 3 | DEB->C->FIN          |  C  |       |   | FIN |   |     |
+---+----------------------+-----+-------+---+-----+---+-----+


3.4 connaître les relations entre tous les états
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**paramètres**

::

    sélection                : statut
    séparateur               : , (ici, caractère arbitraire absent des données)
    clé => valeur            : ->
    séparateur des résultats : +
    'a => b => c' signifie : (a => b), (b => c), (a => c)

**résultat**

+---+----------------------+-------------+---------------+---------+-------+-------+-----+
| x |        statut        | DEB         | A             | B       |  C    | D     |  F  |
+===+======================+=============+===============+=========+=======+=======+=====+
| 1 | DEB->A->B->C->A->FIN | A+B+C+A+FIN | B+C+A+FIN+FIN | C+A+FIN | A+FIN |       |     |
+---+----------------------+-------------+---------------+---------+-------+-------+-----+
| 2 | DEB->A->D->F->FIN    | A+D+F+FIN   | D+F+FIN       |         |       | F+FIN | FIN |
+---+----------------------+-------------+---------------+---------+-------+-------+-----+
| 3 | DEB->C->FIN          | C+FIN       |               |         |  FIN  |       |     |
+---+----------------------+-------------+---------------+---------+-------+-------+-----+


4. analyser
-------------

Une fonction d'analyse est dédiée aux listes de valeurs : **VALEURS EN LISTE : AGRÉGATS -> répartition/distribution**.
