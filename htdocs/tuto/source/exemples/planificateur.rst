système d'aide à la décision (inscriptions)
============================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


contexte
---------

Dans le cadre d'organisation d'événements -- *colloques, formations, etc.* -- et lorsque les participants peuvent indiquer plusieurs disponibilités, le système d'inscriptions produit généralement une liste avec

  * en colonne : les dates,
  * en ligne : la description de la disponibilité des participants.

**exemple : événement ouvert le 1er lundi de juin, juillet et août 2019**

+-------------+------------+------------+------------+
| participant | 2019-06-03 | 2019-07-01 | 2019-08-05 |
+=============+============+============+============+
| Arthur      |     0      |     1      |     0      |
+-------------+------------+------------+------------+
| Lancelot    |     0      |     1      |     1      |
+-------------+------------+------------+------------+
| Tristan     |     1      |     1      |     1      |
+-------------+------------+------------+------------+
| Perceval    |     1      |     0      |     0      |
+-------------+------------+------------+------------+
|      ...    |    ...     |    ...     |    ...     |
+-------------+------------+------------+------------+

Nous souhaitons identifier les participants :

  1. ayant indiqué qu'une seule date (contrainte forte),
  2. ayant indiqué deux des trois dates (contrainte faible),
  3. ayant indiqué toutes les dates (aucune contrainte).

L'objectif étant de placer le maximum de participants du groupe 2.


mode opératoire
----------------

La première étape consistera à transformer les données pour obtenir en face de chaque participant, la liste des dates pour lesquelles il s'est inscrit.

La seconde étape vise à compacter l'information.

La dernière étape est un pivot pour basculer d'un point de vue *participant* à un point de vue *combinaison de dates*.


1. préparer les données
------------------------

**VALEURS -> traduire ou supprimer**

**paramètres**

::

    colonnes           : 2019-06-03, 2019-07-01, 2019-08-05
    valeur exacte      : 0
    sera remplacée par : (laisser vide)


**LIGNES -> recopier l'en-tête à chaque ligne**

**paramètres**

::

    recopier l'en-tête     : à la place de la valeur
    séparateur             : (laisser vide)
    neutraliser le préfixe : (laisser vide)

+-------------+------------+------------+------------+
| participant | 2019-06-03 | 2019-07-01 | 2019-08-05 |
+=============+============+============+============+
| Arthur      |            | 2019-07-01 |            |
+-------------+------------+------------+------------+
| Lancelot    |            | 2019-07-01 | 2019-08-05 |
+-------------+------------+------------+------------+
| Tristan     | 2019-06-03 | 2019-07-01 | 2019-08-05 |
+-------------+------------+------------+------------+
| Perceval    | 2019-06-03 |            |            |
+-------------+------------+------------+------------+
|      ...    |    ...     |    ...     |    ...     |
+-------------+------------+------------+------------+


2. compacter l'information
---------------------------

**COLONNES -> concaténer en boucle**

**paramètres**

::

    colonnes              : 2019-06-03, 2019-07-01, 2019-08-05
    liste des séparateurs : ,


**VALEURS EN LISTE -> enlever une des valeurs**

**paramètres**

::

    séparateur       ; ,
    valeur à effacer : (laisser vide)
    colonne          : concat_boucle

Les anciennes colonnes de dates, inutiles car redondantes, seront supprimées avec **COLONNES -> supprimer ou conserver**.

+----------------------------------+-------------+
| concat_boucle                    | participant |
+==================================+=============+
| 2019-07-01                       | Arthur      |
+----------------------------------+-------------+
| 2019-07-01,2019-08-05            | Lancelot    |
+----------------------------------+-------------+
| 2019-07-01,2019-08-05,2019-06-03 | Tristan     |
+----------------------------------+-------------+
| 2019-06-03                       | Perceval    |
+----------------------------------+-------------+
|      ...                         |    ...      |
+----------------------------------+-------------+


3. changer de point de vue
---------------------------

**COLONNES -> copier une colonne**

**paramètres**

::

    colonne  : participant
    position : juste après
    en-tête  : participant - copie

**LIGNES -> compacter**

**paramètres**

::

    clé des lignes à compacter       : concat_boucle
    créer des listes de valeurs avec : participant (liste + séparateur ,)
    créer des listes de valeurs avec : participant - copie (décompte)


**VALEURS EN LISTE : AGRÉGATS-> compter le nombre d'éléments**

**paramètres**

::

    colonne         : concat_boucle
    séparateur      : ,
    les répétitions : seront ignorées


4. résultat
------------

Le tableau résultat comportera au final pour chaque combinaison de dates

  * le nombre de dates de la combinaison
  * la combinaison de dates
  * le nombre de participants :  il s'agit de nos groupes de participants

     * 1 : une seule date,
     * 2 : 2 dates,
     * 3 : toutes les dates.

  * la liste des participants pour cette combinaison.

+----------------------------------+----------+---------------------+
| combinaison disponibilités       | nb dates | participant - liste |
+==================================+==========+=====================+
| 2019-07-01                       |    1     |  Arthur, ...        |
+----------------------------------+----------+---------------------+
| 2019-07-01,2019-08-05            |    2     |  Lancelot, ...      |
+----------------------------------+----------+---------------------+
| 2019-07-01,2019-08-05,2019-06-03 |    3     |  Tristan, ...       |
+----------------------------------+----------+---------------------+
| 2019-06-03                       |    1     |  Perceval, ...      |
+----------------------------------+----------+---------------------+
|      ...                         |    ...   |    ...              |
+----------------------------------+----------+---------------------+

Sur un jeu de donnée plus important, le groupe 1 va aider à évaluer si le nombre minimum requis de participant est atteint pour confirmer la séance/session.

Dans notre cas, il s'agit des dates *2019-06-03* et *2019-07-01*.
