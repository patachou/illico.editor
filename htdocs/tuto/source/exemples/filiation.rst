recomposer une filiation à partir d'une table parent-enfant
============================================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


contexte
---------

Une façon compacte de décrire une filiation -- arbre généalogique, organigramme fonctionnel, etc. -- est de coder les relations de proche en proche.

**tableau d'origine**

+----------+-----------+-------------------------+
| parent   | enfant    | description de l'enfant |
+==========+===========+=========================+
| Patrick  | Paul      |       ...               |
+----------+-----------+-------------------------+
| Patrick  | Bérénice  |       ...               |
+----------+-----------+-------------------------+
| Mathilde | Paul      |       ...               |
+----------+-----------+-------------------------+
| Paul     | Amélie    |       ...               |
+----------+-----------+-------------------------+
| Amélie   | Maya      |       ...               |
+----------+-----------+-------------------------+
| Paul     | Alfred    |       ...               |
+----------+-----------+-------------------------+
| Peter    | Hélène    |       ...               |
+----------+-----------+-------------------------+
| Philip   |           |                         |
+----------+-----------+-------------------------+

.. note:: on prendra comme hypothèse que chaque nom représente une même personne (pas d'homonyme).


Ce qui peut se traduire par le parcours de deux forêts d'arbres :

**a. dans l'ordre chronologique**

::

    Patrick
      |
      + Paul
      |   |
      |   + Amélie
      |   |   |
      |   |   + Maya
      |   | 
      |   + Alfred
      |
      + Bérénice

    Mathilde
      |
      + Paul
          |
          + Amélie
          |   |
          |   + Maya
          | 
          + Alfred

    Peter
      |
      + Hélène

    Philip

**b. dans l'ordre inverse**

::

    Alfred
      |
      + Paul
         |
         + Patrick
         + Mathilde

    Maya 
     |
     + Amélie
         |
         + Paul
            |
            + Patrick
            + Mathilde
        
    Bérénice
       |
       + Patrick

    Hélène
      |
      + Peter

    Philip


Prenons comme objectif de traduire ces filiations dans une structure de données plus *classique* afin d'en faciliter l'analyse.

**résultat recherché**

+----------+----------+--------+--------------+-------------+
| ancêtre  | parent   | enfant | petit-enfant | description |
+==========+==========+========+==============+=============+
| Patrick  | Paul     | Amélie |     Maya     |  ...        |
+----------+----------+--------+--------------+-------------+
| Patrick  | Paul     | Alfred |              |  ...        |
+----------+----------+--------+--------------+-------------+
| Patrick  | Bérénice |        |              |  ...        |
+----------+----------+--------+--------------+-------------+
| Mathilde | Paul     | Amélie |     Maya     |  ...        |
+----------+----------+--------+--------------+-------------+
| Mathilde | Paul     | Alfred |              |  ...        |
+----------+----------+--------+--------------+-------------+
| Peter    | Hélène   |        |              |  ...        |
+----------+----------+--------+--------------+-------------+
| Philip   |          |        |              |             |
+----------+----------+--------+--------------+-------------+

.. tip:: Ce tableau représente la combinaison des résultats obtenus avec la fonctionnalité **COLONNES -> recréer une filiation (relation parent-enfant)** en appliquant sur le jeu d'origine les deux options :
         
         * premier parent connu,
         * dernier parent connu.


mode opératoire
----------------

Les actions à réaliser sont les suivantes :

1. identifier les ancêtres (colonne de gauche)
2. recréer les différents niveaux hiérarchiques (ici trois niveaux, les 4 premières colonnes)
3. connaître les descendants (quelle que soit leur position dans la généalogie)
4. raccrocher la colonne *description* (c'est-à-dire les autres données)


1. identifier les ancêtres
---------------------------

Les ancêtres sont les individus qui n'ont pas de parent.
Ce sont des individus connus dans la colonne *parent* mais qui n'apparaissent pas dans la colonne *enfant*.

Pour identifier ces individus, il faut faire une jointure pour tester **l'absence de relation**.

**ENRICHISSEMENT -> fusionner, compléter ou exclure des données par jointure**

**paramètres**

::

    fichier maître : tableau d'origine
    clé de jointure du fichier maître : parent
    
    fichier lookup : tableau d'origine
    clé de jointure du fichier lookup : enfant
    
    conserver : les lignes du fichier maître absentes du fichier lookup

**résultat**

+----------+-----------+----------------+
| parent   | enfant    | autres données |
+==========+===========+================+
| Patrick  | Paul      |       ...      |
+----------+-----------+----------------+
| Patrick  | Bérénice  |       ...      |
+----------+-----------+----------------+
| Mathilde | Paul      |       ...      |
+----------+-----------+----------------+
| Peter    | Hélène    |       ...      |
+----------+-----------+----------------+
| Philip   |           |                |
+----------+-----------+----------------+

La colonne *autres données* sera supprimée, et les premières colonnes renommées.

+----------+-----------+
| ancêtre  | parent    |
+==========+===========+
| Patrick  | Paul      |
+----------+-----------+
| Patrick  | Bérénice  |
+----------+-----------+
| Mathilde | Paul      |
+----------+-----------+
| Peter    | Hélène    |
+----------+-----------+
| Philip   |           |
+----------+-----------+

Le résultat obtenu décrit les deux premières générations.


2. recréer les différents niveaux hiérarchiques
------------------------------------------------

Les descendants seront ajoutés par itérations successives.


intégrer la 3ème génération
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour cela, il s'agit d'une simple jointure entre le tableau d'origine et la liste des ancêtres déjà chargée comme fichier *maître* dans Illico.

**ENRICHISSEMENT -> fusionner, compléter ou exclure des données par jointure**

**paramètres**

::

    fichier maître   : liste des ancêtres (données chargées dans Illico)
    clé de jointure du fichier maître : parent

    fichier lookup   : tableau d'origine
    clé de jointure du fichier lookup : parent
        préfixer par : [1]

    conserver : les lignes du fichier maître enrichies par le lookup

+----------+----------+------------+------------+--------------------+
| ancêtre  | parent   | [1] parent | [1] enfant | [1] autres données |
+==========+==========+============+============+====================+
| Patrick  | Paul     |  Paul      |  Amélie    |      ...           |
+----------+----------+------------+------------+--------------------+
| Patrick  | Paul     |  Paul      |  Alfred    |      ...           |
+----------+----------+------------+------------+--------------------+
| Patrick  | Bérénice |            |            |      ...           |
+----------+----------+------------+------------+--------------------+
| Mathilde | Paul     |  Paul      |  Amélie    |      ...           |
+----------+----------+------------+------------+--------------------+
| Mathilde | Paul     |  Paul      | Alfred     |      ...           |
+----------+----------+------------+------------+--------------------+
| Peter    | Hélène   |            |            |      ...           |
+----------+----------+------------+------------+--------------------+
| Philip   |          |            |            |                    |
+----------+----------+------------+------------+--------------------+

Le résultat sera retravaillé de la manière suivante :

 - supprimer la colonne *[1] parent* qui fait redondance avec la colonne *parent* (clé de jointure)
 - supprimer la colonne *[1] autres données*
 - renommer la colonne *[1] enfant* -> *enfant*

+---------+--------+--------+
| ancêtre | parent | enfant |
+=========+========+========+
| Patrick | Paul   | Amélie |
+---------+--------+--------+
|   ...   |   ...  |   ...  |
+---------+--------+--------+

intégrer la 4ème génération
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

De la même façon, une jointure permet de ramener la 4ème génération.

*ENRICHISSEMENT -> fusionner, compléter ou exclure des données par jointure**

**paramètres**

::

    fichier maître   : liste des 3 premières générations (données chargées dans Illico)
    clé de jointure du fichier maître : enfant
    
    fichier lookup   : tableau d'origine
    clé de jointure du fichier lookup : parent
        préfixer par : [1]
    
    conserver : les lignes du fichier maître enrichies par le lookup


+----------+----------+------------+------------+------------+--------------------+
| ancêtre  | parent   | enfant     | [1] parent | [1] enfant | [1] autres données |
+==========+==========+============+============+============+====================+
| Patrick  | Paul     |  Amélie    |  Amélie    |  Maya      |   ...              |
+----------+----------+------------+------------+------------+--------------------+
| Patrick  | Paul     |  Alfred    |  Alfred    |            |   ...              |
+----------+----------+------------+------------+------------+--------------------+
| Patrick  | Bérénice |            |            |            |   ...              |
+----------+----------+------------+------------+------------+--------------------+
| Mathilde | Paul     |  Amélie    |  Amélie    | Maya       |   ...              |
+----------+----------+------------+------------+------------+--------------------+
| Mathilde | Paul     |  Alfred    |  Alfred    |            |   ...              |
+----------+----------+------------+------------+------------+--------------------+
| Peter    | Hélène   |            |            |            |   ...              |
+----------+----------+------------+------------+------------+--------------------+
| Philip   |          |            |            |            |                    |
+----------+----------+------------+------------+------------+--------------------+


Le résultat sera retravaillé de la manière suivante :

 - supprimer la colonne *[1] parent* qui fait redondance avec la colonne *enfant* (clé de jointure)
 - supprimer la colonne *[1] autres données*
 - renommer la colonne *[1] enfant* -> *petit-enfant*

+---------+--------+--------+--------------+
| ancêtre | parent | enfant | petit-enfant |
+=========+========+========+==============+
| Patrick | Paul   | Amélie | Maya         |
+---------+--------+--------+--------------+
|   ...   |   ...  |   ...  |   ...        |
+---------+--------+--------+--------------+


3. connaître les descendants
-----------------------------

Identifier les descendants revient à retrouver le dernier descendant de chaque filiation.

ajouter une nouvelle colonne
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**COLONNES -> ajouter une colonne**

**paramètres**

::

    colonne : descendant
    après   : petit-enfant


identifier le descendant de la filiation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**COLONNES -> projeter des valeurs sur d'autres colonnes**

**paramètres**

::

    colonnes :
      - ancêtre
      - parent
      - enfant
      - petit-enfant
      - descendant
    
    action : projeter à droite

**résultat**

+-----+---------------+
| ... |  descendant   |
+=====+===============+
| ... |  Maya         |
+-----+---------------+
| ... |  Alfred       |
+-----+---------------+
| ... |  Bérénice     |
+-----+---------------+
| ... |    ...        |
+-----+---------------+


4. raccrocher les "autres données" aux descendants
---------------------------------------------------

À partir du tableau obtenu :

+----------+----------+------------+--------------+------------+
| ancêtre  | parent   | enfant     | petit-enfant | descendant |
+==========+==========+============+==============+============+
| Patrick  | Paul     |  Amélie    |    Maya      | Maya       |
+----------+----------+------------+--------------+------------+
| Patrick  | Paul     |  Alfred    |              | Alfred     |
+----------+----------+------------+--------------+------------+
| Patrick  | Bérénice |            |              | Bérénice   |
+----------+----------+------------+--------------+------------+
| Mathilde | Paul     |  Amélie    |   Maya       | Maya       |
+----------+----------+------------+--------------+------------+
| Mathilde | Paul     |  Alfred    |              | Alfred     |
+----------+----------+------------+--------------+------------+
| Peter    | Hélène   |            |              | Hélène     |
+----------+----------+------------+--------------+------------+
| Philip   |          |            |              | Philip     |
+----------+----------+------------+--------------+------------+

On réintègre les données qui décrivent les *enfants* (la dernière colonne du tableau d'origine), autrement dit les *autres données*.


**ENRICHISSEMENT -> fusionner, compléter ou exclure des données par jointure**

**paramètres**

::

    fichier maître   : tableau de travail
    clé de jointure du fichier maître : descendant
    
    fichier lookup   : tableau d'origine
    clé de jointure du fichier lookup : enfant
        préfixer par : [1]
    
    conserver        : les lignes du fichier maître enrichies par le lookup


+----------+----------+------------+--------------+------------+------------+------------+----------------+
| ancêtre  | parent   | enfant     | petit-enfant | descendant | [1] parent | [1] enfant | description... |
+==========+==========+============+==============+============+============+============+================+
| Patrick  | Paul     |  Amélie    |    Maya      | Maya       | Amélie     | Maya       |   ...          |
+----------+----------+------------+--------------+------------+------------+------------+----------------+
| Patrick  | Paul     |  Alfred    |              | Alfred     | Paul       | Alfred     |   ...          |
+----------+----------+------------+--------------+------------+------------+------------+----------------+
| Patrick  | Bérénice |            |              | Bérénice   | Patrick    | Bérénice   |   ...          |
+----------+----------+------------+--------------+------------+------------+------------+----------------+
| Mathilde | Paul     |  Amélie    |   Maya       | Maya       | Amélie     | Maya       |   ...          |
+----------+----------+------------+--------------+------------+------------+------------+----------------+
| Mathilde | Paul     |  Alfred    |              | Alfred     | Paul       | Alfred     |   ...          |
+----------+----------+------------+--------------+------------+------------+------------+----------------+
| Peter    | Hélène   |            |              | Hélène     | Peter      | Hélène     |   ...          |
+----------+----------+------------+--------------+------------+------------+------------+----------------+
| Philip   |          |            |              | Philip     |            |            |                |
+----------+----------+------------+--------------+------------+------------+------------+----------------+

Pour obtenir le tableau final, il suffit de supprimer les colonnes *[1] parent* et *[1] enfant* (partiellement redondantes et inutiles ici).

+----------+----------+------------+--------------+------------+----------------+
| ancêtre  | parent   | enfant     | petit-enfant | descendant | description... |
+==========+==========+============+==============+============+================+
| Patrick  | Paul     |  Amélie    |    Maya      | Maya       |   ...          |
+----------+----------+------------+--------------+------------+----------------+
| Patrick  | Paul     |  Alfred    |              | Alfred     |   ...          |
+----------+----------+------------+--------------+------------+----------------+
|   ...    |  ...     |   ...      |   ...        |  ...       |   ...          |
+----------+----------+------------+--------------+------------+----------------+

