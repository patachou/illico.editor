créer autant de lignes que d'évènements (produit cartésien)
===========================================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


contexte
--------

Dans cette situation, nous voulons construire un tableau vide pour suivre les actions de formation/certification d'un ensemble d'individus.

Par exemple pour 2 individus, sur la période 2010 à 2012 :

+----------+-------+----------------+---------------+
| individu | année | type évènement | nom évènement |
+==========+=======+================+===============+
| Philippe | 2010  | formation      |               |
+----------+-------+----------------+---------------+
| Philippe | 2010  | certification  |               |
+----------+-------+----------------+---------------+
| Philippe | 2011  | formation      |               |
+----------+-------+----------------+---------------+
| Philippe | 2011  | certification  |               |
+----------+-------+----------------+---------------+
| Philippe | 2012  | formation      |               |
+----------+-------+----------------+---------------+
| Philippe | 2012  | certification  |               |
+----------+-------+----------------+---------------+
| Marie    | 2010  | formation      |               |
+----------+-------+----------------+---------------+
| Marie    | 2010  | certification  |               |
+----------+-------+----------------+---------------+
| Marie    | 2011  | formation      |               |
+----------+-------+----------------+---------------+
| Marie    | 2011  | certification  |               |
+----------+-------+----------------+---------------+
| Marie    | 2012  | formation      |               |
+----------+-------+----------------+---------------+
| Marie    | 2012  | certification  |               |
+----------+-------+----------------+---------------+

Si nous avons 1 000 individus à suivre, nous devrions construire un tableau de ``1 000 individus x 3 années x 2 évènements`` 
soit 6 000 lignes !

produit cartésien
------------------

L'astuce consiste à effectuer un produit cartésien.

.. note:: Le produit cartésien de deux jeux de données A et B génère autant de combinaisons qu'il y a d'éléments dans les deux jeux de données (`wikipedia <https://fr.wikipedia.org/wiki/Produit_cart%C3%A9sien>`__).

Autrement dit, si A contient 5 lignes et B 3 lignes, alors le produit cartésien construit une liste de 15 lignes en combinant les éléments de A et les éléments de B.

mode opératoire
---------------

Il n'existe pas de fonction dans Illico pour effectuer en une seule action le produit cartésien. Il sera nécessaire de procéder en deux temps :

* création d'une clé commune aux deux jeux de données
* effectuer une fusion des deux ensembles via **ENRICHISSEMENT -> fusionner, compléter ou exclure des données par jointure**


1. préparer les jeux de données
--------------------------------

Construire 3 jeux de données :

  * la liste des individus
      1 000 lignes de données : Philippe, Marie, …

  * la liste des années
      3 lignes de données : 2010, 2011, 2012

  * la liste des évènements
      2 lignes de données : formation, certification

2. créer une clé commune
-------------------------

Pour ces 3 jeux de données, ajoutons une colonne vide avec **COLONNES -> ajouter une colonne** --- ou
initialisée avec une valeur arbitraire, ici ``xx`` --- mais volontairement identique dans nos 3 sources de données.

Nos 3 sources contiennent désormais 2 colonnes, une avec les données et une avec la clé de jointure.

**liste des individus**

+-----+----------+
| clé | individu |
+=====+==========+
| xx  | Philippe |
+-----+----------+
| xx  | Marie    |
+-----+----------+
| xx  | ...      |
+-----+----------+

**liste des années**

+-----+-------+
| clé | année |
+=====+=======+
| xx  | 2010  |
+-----+-------+
| xx  | 2011  |
+-----+-------+
| xx  | 2012  |
+-----+-------+

**liste des évènements**

+-----+----------------+
| clé | type évènement |
+=====+================+
| xx  | formation      |
+-----+----------------+
| xx  | certification  |
+-----+----------------+

3. produit cartésien
---------------------

En utilisant la fonction **ENRICHISSEMENT -> fusionner, compléter ou exclure des données par jointure** 
avec la liste des individus et la liste des années, 
nous pouvons créer autant de lignes que de combinaisons d'individus et d'années.

**paramètres**

::

    fichier maître : liste des individus
    fichier lookup : liste des années
    conserver : uniquement avec les correspondances exactes
    clé de jointure : la colonne vide (ou clé arbitraire, ici "xx")

+-----+----------+-----+-------+
| clé | individu | clé | année |
+=====+==========+=====+=======+
| xx  | Philippe | xx  | 2010  |
+-----+----------+-----+-------+
| xx  | Philippe | xx  | 2011  |
+-----+----------+-----+-------+
| xx  | Philippe | xx  | 2012  |
+-----+----------+-----+-------+
| xx  | Marie    | xx  | 2010  |
+-----+----------+-----+-------+
| xx  | Marie    | xx  | 2011  |
+-----+----------+-----+-------+
| xx  | Marie    | xx  | 2012  |
+-----+----------+-----+-------+
| xx  | ...      | xx  | ...   |
+-----+----------+-----+-------+

L'étape précédente sera répétée avec les mêmes paramètres et cette fois la liste des évènements.

**paramètres**

::

    fichier maître : liste des individus ventilée par année
    fichier lookup : liste des évènements
    conserver : uniquement avec les correspondances exactes
    clé de jointure : la colonne vide (ou clé arbitraire, ici "xx")

+-----+----------+-----+-------+-----+----------------+
| clé | individu | clé | année | clé | type évènement |
+=====+==========+=====+=======+=====+================+
| xx  | Philippe | xx  | 2010  | xx  | formation      |
+-----+----------+-----+-------+-----+----------------+
| xx  | Philippe | xx  | 2010  | xx  | certification  |
+-----+----------+-----+-------+-----+----------------+
| xx  | Philippe | xx  | 2011  | xx  | formation      |
+-----+----------+-----+-------+-----+----------------+
| xx  | Philippe | xx  | 2011  | xx  | certification  |
+-----+----------+-----+-------+-----+----------------+
| xx  | Philippe | xx  | 2012  | xx  | formation      |
+-----+----------+-----+-------+-----+----------------+
| xx  | Philippe | xx  | 2012  | xx  | certification  |
+-----+----------+-----+-------+-----+----------------+
| xx  | Marie    | xx  | 2010  | xx  | formation      |
+-----+----------+-----+-------+-----+----------------+
| xx  | Marie    | xx  | 2010  | xx  | certification  |
+-----+----------+-----+-------+-----+----------------+
| xx  | Marie    | xx  | 2011  | xx  | formation      |
+-----+----------+-----+-------+-----+----------------+
| xx  | Marie    | xx  | 2011  | xx  | certification  |
+-----+----------+-----+-------+-----+----------------+
| xx  | Marie    | xx  | 2012  | xx  | formation      |
+-----+----------+-----+-------+-----+----------------+
| xx  | Marie    | xx  | 2012  | xx  | certification  |
+-----+----------+-----+-------+-----+----------------+
| xx  | ...      | xx  | ...   | xx  | ...            |
+-----+----------+-----+-------+-----+----------------+

4. terminer la trame
---------------------

Pour finir, nous supprimerons les trois colonnes ``clé`` et complèterons cette trame avec les colonnes de données à saisir. 
Par exemple, **COLONNES -> ajoutez la colonne** ``nom d'évènement`` pour obtenir le tableau en début de tutoriel.


.. tip:: Une autre approche est possible par le biais des listes

         * **COLONNES -> ajouter une colonne** : ``2010-2011-2012/formation-certification``
         * **TRADUIRE LES LISTES -> traduire les listes en colonnes** (séparateur ``/``)
         * **TRADUIRE LES LISTES -> traduire les lises en lignes** (séparateur ``-``, à effectuer sur les deux colonnes créées)
