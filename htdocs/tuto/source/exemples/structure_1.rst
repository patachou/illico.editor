changer de point de vue (sans exploiter l'en-tête)
===================================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


contexte
---------

Prenons comme exemple un jeu de données où les données sont des lignes de longueur variable, de la forme suivante :

+----------+---------------+--------+---------------+--------+---------------+---------+-----+
| projet   | participant 1 | rôle 1 | participant 2 | rôle 2 | participant 3 | rôle 3  | ... |
+==========+===============+========+===============+========+===============+=========+=====+
|    P1    | toto          | resp.  | tata          | membre | titi          | membre  | ... |
+----------+---------------+--------+---------------+--------+---------------+---------+-----+
|    P2    | tata          | resp.  | tutu          | membre |               |         | ... |
+----------+---------------+--------+---------------+--------+---------------+---------+-----+
| ...      | ...           | ...    | ...           | ...    | ...           | ...     | ... |
+----------+---------------+--------+---------------+--------+---------------+---------+-----+

Certaines lignes n'ont qu'un ou deux participants et d'autres ont plus de vingt participants.

Le nombre de colonne *utiles* varient donc à chaque ligne. Le jeu de données est difficile à exploiter en l'état.

L'objectif est de construire une liste avec pour chaque acteur, ses rôles dans les différents projets.


.. note:: Pour vous entraîner, reportez-vous à la fonctionnalité **COLONNES -> transposer (n groupes de colonnes)** avant de vous exercer avec la procédure qui suit.


mode opératoire
----------------

Les étapes consisteront à construire pour chaque projet une longue liste contenant à la fois les participants et rôles, puis de traduire cette liste en ligne et en colonne.

1. construire une liste clé-valeurs
------------------------------------

Pour construire une telle liste, il faut d'une part un premier séparateur pour distinguer chaque couple clé-valeurs de la liste, et un second pour délimiter les deux parties *clé* et *valeur* au sein d'un couple clé-valeur.

Nous utiliserons une concaténation en boucle en indiquant deux séparateurs :

* ``=`` séparera la clé et la valeur dans un couple clé-valeur
* ``/`` délimitera chaque couple au sein de la liste.

.. tip:: Pour une meilleure lisibilité, les deux séparateurs seront entourés d'un espace avant et après.

**COLONNES -> concaténer en boucle**

**paramètres**

::

    colonnes : participant 1, rôle 1, participant 2, rôle 2...
    liste de séparateurs :
    ` = `
    ` / `

**résultat**

+----------+-----------------------------------------------------+---------------+-----+
| projet   | résultat                                            | participant 1 | ... |
+==========+=====================================================+===============+=====+
|    P1    | toto = resp. / tata = membre / titi = membre / ...  | toto          | ... |
+----------+-----------------------------------------------------+---------------+-----+
|    P2    | tata = resp. / tutu = membre                        | tata          | ... |
+----------+-----------------------------------------------------+---------------+-----+
| ...      | ...                                                 | ...           | ... |
+----------+-----------------------------------------------------+---------------+-----+

.. note:: Les colonnes du tableau d'origine (*participant N* et *rôle N*) qui se retrouvent désormais à droite du tableau, seront supprimées avant de continuer.


2. restructurer les données pour obtenir une ligne par rôle par projet
-----------------------------------------------------------------------

**TRADUIRE LES LISTES -> traduire les listes en lignes**

**paramètre**

::

    séparateur : ` / `

+----------+---------------+
| projet   | résultat      |
+==========+===============+
|    P1    | toto = resp.  |
+----------+---------------+
|    P1    | tata = membre |
+----------+---------------+
|    P1    | titi = membre |
+----------+---------------+
|    P1    | ...           |
+----------+---------------+
|    P2    | tata = resp.  |
+----------+---------------+
|    P2    | tutu = membre |
+----------+---------------+
| ...      | ...           |
+----------+---------------+


3. supprimer les lignes inconsistantes
---------------------------------------

Dans un projet, le nombre de participants est variable.
Or, le tableau obtenu contient pour tous les projets autant de lignes que de participants potentiels.

Il faut dont supprimer les lignes inconsistantes.


**FILTRES -> retenir les lignes**

**paramètres**

::

    colonne      : résultat
    qui ne contiennent pas : ` = `


4. diviser la colonne clé-valeur en 2 colonnes
-----------------------------------------------

**TRADUIRE LES LISTES -> traduire les listes en colonnes**

**paramètres**

::

    colonne    : résultat
    séparateur : ` = `
    direction  : de la gauche vers la droite

+----------+-------------+--------+
| projet   | participant | rôle   |
+==========+=============+========+
|    P1    | toto        | resp.  |
+----------+-------------+--------+
|    P1    | tata        | membre |
+----------+-------------+--------+
|    P1    | titi        | membre |
+----------+-------------+--------+
|    P1    | ...         | ...    |
+----------+-------------+--------+
|    P2    | tata        | resp.  |
+----------+-------------+--------+
|    P2    | tutu        | membre |
+----------+-------------+--------+
| ...      | ...         | ...    |
+----------+-------------+--------+


.. tip:: La forme du tableau obtenu est un format *pivot* qui simplifie les agrégations par projet, par individu ou par rôle.


5. recomposer les associations
-------------------------------

L'objectif est d'obtenir une matrice *transposée* de la matrice d'origine avec les individus en ligne et les projets en colonne.

.. note:: Les transformations à combiner pour réaliser une telle matrice sont présentées dans les tutoriels :

       6. modifier la structure des données
       7. traiter les doublons
       8. traduire les données en ligne, colonne et matrice

La reconstitution d'une telle matrice s'appuie sur 6 temps :

  1. créer l'association ``projet = rôle`` en concaténant les colonnes dans cet ordre,
  2. rassembler dans une même colonne pour chaque participant, toutes les associations obtenues (**COLONNES -> pivoter**)
  3. supprimer les anciennes colonnes *rôle* et *projet*, désormais inutiles,
  4. numéroter les occurrences de *participant*,
  5. retenir les lignes qui commencent par 1 afin de supprimer les lignes en doublons qui décrivent le même individu,
  6. traduire en une matrice les associations en précisant l'opérateur ``=`` et le séparateur utilisée à l'étape 2.

.. tip:: La fonction **LIGNES -> COMPACTER LES LIGNES** simplifie en une opération les étapes *numéroter + filtrer à 1*.
