compter des jours d'absence/présence sur une période
=====================================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


contexte
---------

Pour faciliter l'enregistrement des congés --- ou des présences ---
les données sont souvent formatées sous forme de séquences :

+--------+-------------+------------+----------+
| nom    | début congé | fin congé  | nb jours |
+========+=============+============+==========+
| Dupont | 23/03/2020  | 27/03/2020 |   5      |
+--------+-------------+------------+----------+
| Dupont | 02/04/2020  | 08/04/2020 |   5      |
+--------+-------------+------------+----------+
| Dupont | 10/04/2020  | 10/04/2020 |   1      |
+--------+-------------+------------+----------+

Imaginons que la direction de l'établissement souhaite identifier les agents ont pris au moins trois jours de congé entre le 07 avril et le 10 avril.

Sans l'aide d'un calendrier, il n'est pas possible de confirmer que l'agent Dupont a pris au moins deux jours de congé sur la période.

Le calcul peut s'avérer long et fastidieux avec un grand nombre d'agents.


mode opératoire
----------------

Dans un premier temps, nous voulons exclure les enregistrements en dehors de la période d'observation.
Pour les enregistrements conservés, il s'agira de calculer le nombre de jours ouvrés communs à la période d'observation et la description du congé.



Les étapes seront :

  1. définir la période d'observation
  3. expliciter les périodes à calculer
  3. retenir les événements concernés par la période d'observation
  4. compter les jours avant/après/pendant la période d'observation
  5. agréger par individu


1. définir la période d'observation
------------------------------------

Ajoutons deux nouvelles colonnes avec les dates de la période d'observation

**COLONNES -> ajouter une colonne**

**paramètres**

::

    en-tête          : début
    initialiser avec : 07/04/2020


**paramètres**

::

    en-tête          : fin
    initialiser avec : 10/04/2020


**résultat**

+--------+-------------+------------+----------+------------+------------+
| nom    | début congé | fin congé  | nb jours |   début    |    fin     |
+========+=============+============+==========+============+============+
| Dupont | 23/03/2020  | 27/03/2020 |    5     | 07/04/2020 | 10/04/2020 |
+--------+-------------+------------+----------+------------+------------+
| Dupont | 02/04/2020  | 08/04/2020 |    5     | 07/04/2020 | 10/04/2020 |
+--------+-------------+------------+----------+------------+------------+
| Dupont | 10/04/2020  | 10/04/2020 |    1     | 07/04/2020 | 10/04/2020 |
+--------+-------------+------------+----------+------------+------------+


2. expliciter les périodes à calculer
--------------------------------------

**REMPLACER... SI... -> remplacer un motif (regex) par une valeur**

**paramètres**

::

    motif à remplacer : (..)/(..)/(....)
    par               : $3$2$1
    sélection         :
       début congé
       fin congé
       début
       fin


**résultat**

+--------+-------------+-----------+----------+----------+----------+
| nom    | début congé | fin congé | nb jours |  début   |   fin    |
+========+=============+===========+==========+==========+==========+
| Dupont |  20200323   |  20200327 |    5     | 20200407 | 20200410 |
+--------+-------------+-----------+----------+----------+----------+
| Dupont |  20200402   |  20200408 |    5     | 20200407 | 20200410 |
+--------+-------------+-----------+----------+----------+----------+
| Dupont |  20200410   |  20200410 |    1     | 20200407 | 20200410 |
+--------+-------------+-----------+----------+----------+----------+


3. retenir les événements concernés par la période d'observation
-----------------------------------------------------------------

**INTERVALLES -> combiner deux intervalles**

**paramètres**

::

    intervalle 1 :
       début congé
       fin congé
    intervalle 2 :
       début
       fin
    combiner     : intersection : plus petit intervalle commun


**résultat**

+-----------------------------------------------+--------+-------------+------------+----------+----------+----------+
| [ début congé ; fin congé ] ? [ début ; fin ] | nom    | début congé | fin congé  | nb jours |  début   |   fin    |
+===============================================+========+=============+============+==========+==========+==========+
|              interv. disjoints                | Dupont |  20200323   |  20200327  |     5    | 20200407 | 20200410 |
+-----------------------------------------------+--------+-------------+------------+----------+----------+----------+
|           20200407   -   20200408             | Dupont |  20200402   |  20200408  |     5    | 20200407 | 20200410 |
+-----------------------------------------------+--------+-------------+------------+----------+----------+----------+
|           20200410   -   20200410             | Dupont |  20200410   |  20200410  |     1    | 20200407 | 20200410 |
+-----------------------------------------------+--------+-------------+------------+----------+----------+----------+

Nous renommerons la colonne résultat avec **EN-TÊTE -> renommer l'en-tête** en *période commune*.

Nous ne conserverons que les données exploitables.


**FILTRES -> retenir les lignes...**

**paramètres**

::

    sélection          : période commune
    retenir les lignes : qui ne contiennent pas cette valeur
    valeur témoin      : interv. disjoints


**résultat**

+------------------------+--------+-------------+-----------+----------+----------+----------+
|     période commune    | nom    | début congé | fin congé | nb jours |  début   |   fin    |
+========================+========+=============+===========+==========+==========+==========+
| 20200407  -  20200408  | Dupont |  20200402   |  20200408 |     5    | 20200407 | 20200410 |
+------------------------+--------+-------------+-----------+----------+----------+----------+
| 20200410  -  20200410  | Dupont |  20200410   |  20200410 |     1    | 20200407 | 20200410 |
+------------------------+--------+-------------+-----------+----------+----------+----------+


**TRADUIRE LES LISTES -> traduire les listes en colonnes**

**paramètres**

::

    traduire en éclatant : de la gauche à la droite
    sélection            : période commune
    séparateur           : ` - ` (espace avant et après)
    action               : remplacer la colonne

Nous renommerons les deux colonnes résultats avec **EN-TÊTE -> renommer l'en-tête** en *de* et *à*.


**résultat**

+----------+----------+--------+-------------+-----------+----------+----------+----------+
|    de    |    à     | nom    | début congé | fin congé | nb jours |  début   |   fin    |
+==========+==========+========+=============+===========+==========+==========+==========+
| 20200407 | 20200408 | Dupont |  20200402   |  20200408 |     5    | 20200407 | 20200410 |
+----------+----------+--------+-------------+-----------+----------+----------+----------+
| 20200410 | 20200410 | Dupont |  20200410   |  20200410 |     1    | 20200407 | 20200410 |
+----------+----------+--------+-------------+-----------+----------+----------+----------+


**REMPLACER... SI... -> remplacer un motif (regex) par une valeur**

**paramètres**

::

    motif à remplacer : (....)(..)(..)
    par               : $3/$2/$1
    sélection         :
       de
       à
       début congé
       fin congé
       début
       fin


**résultat**

+------------+------------+--------+-------------+------------+----------+------------+------------+
|     de     |      à     | nom    | début congé | fin congé  | nb jours |    début   |     fin    |
+============+============+========+=============+============+==========+============+============+
| 07/04/2020 | 08/04/2020 | Dupont | 02/04/2020  | 08/04/2020 |     5    | 07/04/2020 | 10/04/2020 |
+------------+------------+--------+-------------+------------+----------+------------+------------+
| 10/04/2020 | 10/04/2020 | Dupont | 10/04/2020  | 10/04/2020 |     1    | 07/04/2020 | 10/04/2020 |
+------------+------------+--------+-------------+------------+----------+------------+------------+

.. tip:: Au format de date `SSAA/MM/JJ`, le tri alphabétique réalise un tri chronologique.


4. compter les jours avant/après/pendant la période d'observation
------------------------------------------------------------------

Nous compterons les nombres de jours ouvrés sur la période calculés.

**TEMPS -> calculer une durée (jours) entre 2 dates**

**paramètres**

::

    format          : jj mm ssaa
    séparateur      : `/`
    début           : de (colonne)
    fin             : à  (colonne)
    dernier jour    : est inclus
    nombre de jours : ouvrés (lundi à vendredi)


**résultat**

+------------------+------------+------------+--------+-------------+------------+----------+------------+------------+
| nb jours [de,à]  |     de     |      à     | nom    | début congé | fin congé  | nb jours |    début   |     fin    |
+==================+============+============+========+=============+============+==========+============+============+
|        2         | 07/04/2020 | 08/04/2020 | Dupont | 02/04/2020  | 08/04/2020 |     5    | 07/04/2020 | 10/04/2020 |
+------------------+------------+------------+--------+-------------+------------+----------+------------+------------+
|        1         | 10/04/2020 | 10/04/2020 | Dupont | 10/04/2020  | 10/04/2020 |     1    | 07/04/2020 | 10/04/2020 |
+------------------+------------+------------+--------+-------------+------------+----------+------------+------------+


5. agréger par individu
------------------------

Deux approches sont possibles :

**AGRÉGATS -> explorer les données**

**paramètres**

::

    sommer         : nb jours [de, à]
    selon les axes : nom


**STRUCTURE -> compacter les lignes**

**paramètres**

::

   clés des lignes à compacter : nom
   calculer                    :
                la somme de
                nb jours [de, à]

.. warning:: Le traitement *compacter les lignes* ne conservera qu'une seule ligne de détail.
