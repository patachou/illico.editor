formater des adresses postales
===============================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


contexte
---------

Pour faciliter une présentation homogène et propre des adresses,
La Poste préconise d'écrire une adresse sous la forme suivante :

::

    adresse ligne 1 (38 caractères)
    adresse ligne 2 (38 caractères)
    adresse ligne 3 (38 caractères)
    code postal VILLE
    PAYS

La plupart du temps, pour des raisons d’exploitation,
les logiciels contiennent les adresses dans des champs séparés sous la forme suivante :

+--------+--------+----------+-----------+-----------+------------------+-------------+-------+------+
| nom    | prénom | civilité | adresse 1 | adresse 2 | adresse 3        | code postal | ville | pays |
+========+========+==========+===========+===========+==================+=============+=======+======+
| Dupont | Albert | M\.      |           |           | avenue des roses | 54029       | Nancy |      |
+--------+--------+----------+-----------+-----------+------------------+-------------+-------+------+

L'objectif est d'éviter l'écriture suivante de l'adresse complète

::

    M. Albert Dupont


    avenue des roses
    54029 Nancy


et d'obtenir plutôt la présentation suivante

::

    M. Albert Dupont
    avenue des roses
    54029 Nancy


mode opératoire
----------------

Par défaut, Illico gère les lignes comme des enregistrements indépendants.

Or, nous voulons ici ramener plusieurs informations dans la même cellule sans bouleverser la structure du fichier.

Nous allons nous appuyer sur le format HTML qui interprète la balise html ``<BR>`` comme un saut de ligne.

Les étapes seront :

  1. créer une liste contenant toutes les informations constituant l’adresse,
  2. supprimer les éléments vides de la liste car ces éléments vont générer des lignes vides,
  3. activer l'interprétation HTML pour visualiser le résultat,
  4. récupérer le résultat dans un tableur.


0. modifier l'affichage
------------------------

Dans un premier temps, nous allons modifier le comportement d'Illico

**CONFIGURER -> aperçu CSV, texte, tableau**

**paramètres**

::

    les caractères < > & ... : ne seront pas interprétés


1. créer l'adresse complète
----------------------------

**COLONNES -> concaténer**

**paramètres**

::

    colonne 1 : adresse 1
    séparateur : <BR>
    colonne 2 : adresse 2
    séparateur : <BR>
    colonne 3 : adresse 3
    séparateur : <BR>
    colonne 4 : code postal
    séparateur :  (espace)
    colonne 5 : ville
    en-tête : adresse complète

**résultat**

::

    <BR><BR>avenue des roses<BR>54029 Nancy


2. supprimer les lignes vides
------------------------------

**VALEURS EN LISTE -> supprimer les valeurs vides**

**paramètres**

::

    colonne : adresse complète
    séparateur : <BR>

**résultat**

::

    avenue des roses<BR>54029 Nancy


3. visualiser le résultat
--------------------------

Nous pouvons modifier l'affichage dans sa configuration d'origine.


**CONFIGURER -> aperçu CSV, texte, tableau**

**paramètres**

::

    les caractères < > & ... : seront interprétés comme du code HTML


**résultat**

::

    avenue des roses
    54029 Nancy

L'adresse complète ne contient plus de ligne vide.

Pour récupérer ce résultat, utilisez l'export HTML.

.. tip:: Si le logiciel de destination ne conserve pas l'adresse complète dans une seule cellule, utilisez la fonction **VALEURS -> encapsuler** et ajoutez des guillemets comme préfixe et suffixe.
