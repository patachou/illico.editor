comparer des groupes de colonnes
=================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


contexte
---------

Lorsque l'on souhaite comparer deux jeux de données décrivant deux versions des mêmes données, une approche simple consiste à

1. à fusionner les deux jeux de données
2. à comparer deux à deux les colonnes issues de la première et seconde sources de données.

Avec un grand nombre de colonnes à comparer, cette approche est fastidieuse.

Pour simplifier la présentation, on imagine avoir à disposition le résultat d'une fusion/jointure et devoir comparer les colonnes de la source (1) {X, Y, Z} et celles de la source (2) {X, Y, Z}.

+------+-------+-------+-------+-------+-------+-------+
|  id  | [1] X | [1] Y | [1] Z | [2] X | [2] Y | [2] Z |
+======+=======+=======+=======+=======+=======+=======+
|   3  |  100  |  abc  |  AA   |  50   |  def  |       |
+------+-------+-------+-------+-------+-------+-------+
|   5  |   25  |   gh  |       |  25   |   gh  |       |
+------+-------+-------+-------+-------+-------+-------+
|  10  |   15  |   kl  |       |  25   |   kl  |       |
+------+-------+-------+-------+-------+-------+-------+


mode opératoire
----------------

La première étape consiste à associer l'information portée par l'intitulé de colonne et les valeurs à comparer.

La deuxième étape consiste à construire de deux listes correspondant aux groupes de colonnes à comparer respectivement celles de la première source et celles de la seconde.

La troisième étape permettra de trouver la différence symétrique, c'est-à-dire les valeurs distinctes, existant d'un côté ou de l'autre mais pas les deux à la fois.

La quatrième étape réduira le jeu de données aux seules lignes contenant des différences.

Enfin, la dernière étape amènera à traduire le résultat sous le forme d'une matrice des seules colonnes dont les valeurs sont différentes d'un jeu de données à l'autre.


1. associer l'en-tête et les données
-------------------------------------

.. important:: Dans cet exemple, au préalable, les intitulés des colonnes auront été nommés avec des intitulés distincts/uniques.


LIGNES -> recopier l'en-tête à chaque ligne

**paramètres**

::

    colonnes      : les colonnes préfixées par [1]
    (recopier)    : avant
    séparateur    : :
    neutraliser le préfixe : [1]_ ([1] suivi d'un espace)

+------+------------+-----------+-----------+-------+-------+-------+
|  id  |   [1] X    |   [1] Y   |   [1] Z   | [2] X | [2] Y | [2] Z |
+======+============+===========+===========+=======+=======+=======+
|   3  |  **X:100** | **Y:abc** |  **Z:AA** |    50 |   def |       |
+------+------------+-----------+-----------+-------+-------+-------+
|   5  |  **X:25**  | **Y:gh**  |           |    25 |   gh  |       |
+------+------------+-----------+-----------+-------+-------+-------+
|  10  |  **X:15**  | **Y:kl**  |           |    25 |   kl  |       |
+------+------------+-----------+-----------+-------+-------+-------+

L'étape sera répétée à l'identique sur les colonnes provenant de la seconde source.

LIGNES -> recopier l'en-tête à chaque ligne

**paramètres**

::

    colonnes      : les colonnes préfixées par [2]
    (recopier)    : avant
    séparateur    : :
    neutraliser le préfixe : [2]_ ([2] suivi d'un espace)

+------+--------+-------+-------+-----------+-----------+--------+
|  id  | [1] X  | [1] Y | [1] Z |   [2] X   |   [2] Y   | [2] Z  |
+======+========+=======+=======+===========+===========+========+
|   3  |  X:100 | Y:abc |  Z:AA |  **X:50** | **Y:def** |        |
+------+--------+-------+-------+-----------+-----------+--------+
|   5  |  X:25  | Y:gh  |       |  **X:25** | **Y:gh**  |        |
+------+--------+-------+-------+-----------+-----------+--------+
|  10  |  X:15  | Y:kl  |       |  **X:25** | **Y:kl**  |        |
+------+--------+-------+-------+-----------+-----------+--------+


2. construire des listes à comparer
------------------------------------

COLONNES -> concaténer en boucle

**paramètres**

::

    colonnes    : les colonnes préfixées par [1]
    séparateurs : ,

+----------------------+------+--------+-------+-------+-----------+-----------+--------+
| concat_boucle        |  id  | [1] X  | [1] Y | [1] Z |   [2] X   |   [2] Y   | [2] Z  |
+======================+======+========+=======+=======+===========+===========+========+
| **X:100,Y:abc,Z:AA** |   3  |  X:100 | Y:abc |  Z:AA |    X:50   |   Y:def   |        |
+----------------------+------+--------+-------+-------+-----------+-----------+--------+
|  **X:25,Y:gh,**      |   5  |  X:25  | Y:gh  |       |    X:25   |   Y:gh    |        |
+----------------------+------+--------+-------+-------+-----------+-----------+--------+
|  **X:15,Y:kl,**      |  10  |  X:15  | Y:kl  |       |    X:25   |   Y:kl    |        |
+----------------------+------+--------+-------+-------+-----------+-----------+--------+

L'étape sera répétée à l'identique sur les colonnes provenant de la seconde source.


COLONNES -> concaténer en boucle

**paramètres**

::

    colonnes    : les colonnes préfixées par [2]
    séparateur  : ,

+--------------------+---------------------+------+--------+-------+-------+-----------+-----------+--------+
|   concat_boucle    | concat_boucle       |  id  | [1] X  | [1] Y | [1] Z |   [2] X   |   [2] Y   | [2] Z  |
+====================+=====================+======+========+=======+=======+===========+===========+========+
|  **X:50,Y:def,**   |   X:100,Y:abc,Z:AA  |   3  |  X:100 | Y:abc |  Z:AA |    X:50   |   Y:def   |        |
+--------------------+---------------------+------+--------+-------+-------+-----------+-----------+--------+
|   **X:25,Y:gh,**   |   X:25,Y:gh,        |   5  |  X:25  | Y:gh  |       |    X:25   |   Y:gh    |        |
+--------------------+---------------------+------+--------+-------+-------+-----------+-----------+--------+
|   **X:25,Y:kl,**   |   X:15,Y:kl,        |  10  |  X:15  | Y:kl  |       |    X:25   |   Y:kl    |        |
+--------------------+---------------------+------+--------+-------+-------+-----------+-----------+--------+


3. déduire les différences
---------------------------

VALEURS EN LISTE : ENRICHISSEMENT -> déduire un sous-ensemble à partir de 2 listes

**paramètres**

::

    liste A     : concat_boucle (prendre la première colonne)
    séparateur  : ,
    liste B     : concat_boucle (prendre l'autre/la seconde colonne)
    séparateur  : ,
    conserver les éléments de : A ou de B, sauf ceux communs (diff. symétrique)
    occurrences multiples     : ne conserver qu'une seule occurrence
    en séparant par           : ,

+----------------------------------+--------------------+---------------------+------+--------+-------+-------+-----------+-----------+--------+
|       A ou exclusif B (unique)   |   concat_boucle    | concat_boucle       |  id  | [1] X  | [1] Y | [1] Z |   [2] X   |   [2] Y   | [2] Z  |
+==================================+====================+=====================+======+========+=======+=======+===========+===========+========+
| **X:50,X:100,Y:def,Y:abc,Z:AA**  |    X:50,Y:def,     |   X:100,Y:abc,Z:AA  |   3  |  X:100 | Y:abc |  Z:AA |    X:50   |   Y:def   |        |
+----------------------------------+--------------------+---------------------+------+--------+-------+-------+-----------+-----------+--------+
|                                  |     X:25,Y:gh,     |    X:25,Y:gh,       |   5  |  X:25  | Y:gh  |       |    X:25   |   Y:gh    |        |
+----------------------------------+--------------------+---------------------+------+--------+-------+-------+-----------+-----------+--------+
| **X:25,X:15**                    |     X:25,Y:kl,     |    X:15,Y:kl,       |  10  |  X:15  | Y:kl  |       |    X:25   |   Y:kl    |        |
+----------------------------------+--------------------+---------------------+------+--------+-------+-------+-----------+-----------+--------+


4. conserver les lignes avec des différences
---------------------------------------------

On exclut les lignes dont les données (1) {X,Y,Z} et (2) {X,Y,Z} sont identiques.

FILTRES -> retenir les lignes ...

**paramètres**

::

    colonne            : A ou exclusif B (unique)
    retenir les lignes : qui ne contiennent pas...
    ... valeur témoin  : (laisser vide)

+----------------------------------+----------------------+----------------------+------+--------+-------+-------+-----------+-----------+--------+
|       A ou exclusif B (unique)   |   concat_boucle      | concat_boucle        |  id  | [1] X  | [1] Y | [1] Z |   [2] X   |   [2] Y   | [2] Z  |
+==================================+======================+======================+======+========+=======+=======+===========+===========+========+
| **X:50,X:100,Y:def,Y:abc,Z:AA**  |    X:50,Y:def,       |   X:100,Y:abc,Z:AA   |   3  |  X:100 | Y:abc |  Z:AA |    X:50   |   Y:def   |        |
+----------------------------------+----------------------+----------------------+------+--------+-------+-------+-----------+-----------+--------+
| **X:25,X:15**                    |     X:25,Y:kl,       |    X:15,Y:kl,        |  10  |  X:15  | Y:kl  |       |    X:25   |   Y:kl    |        |
+----------------------------------+----------------------+----------------------+------+--------+-------+-------+-----------+-----------+--------+

.. tip:: *FILTRES -> lignes contenant des valeurs vides* permet d'aboutir au même résultat.


5. présenter le résultat
-------------------------

COLONNES -> supprimer ou conserver

**paramètres**

::

    action    : supprimer
    colonnes  : concat_boucle, concat_boucle (les deux)


TRADUIRE LES LISTES -> traduire les listes en une matrice

**paramètres**

::

    colonne       : A ou exclusif B (unique)
    séparateur    : ,
    clé => valeur : : (il s'agit du séparateur utilisé dans l'étape n°1)
    séparateur des résultats : _??_ (?? suivi et précédé d'un espace)

+--------------------------------+------+--------+-------+-------+-----------+-----------+--------+---------------+----------------+---------+
|     A ou exclusif B (unique)   |  id  | [1] X  | [1] Y | [1] Z |   [2] X   |   [2] Y   | [2] Z  |       X       |        Y       |    Z    |
+================================+======+========+=======+=======+===========+===========+========+===============+================+=========+
| X:50,X:100,Y:def,Y:abc,Z:,Z:AA |   3  |  X:100 | Y:abc |  Z:AA |    X:50   |   Y:def   |   Z:   | **50 ?? 100** | **def ?? abc** | **AA**  |
+--------------------------------+------+--------+-------+-------+-----------+-----------+--------+---------------+----------------+---------+
| X:25,X:15                      |  10  |  X:15  | Y:kl  |  Z:   |    X:25   |   Y:kl    |   Z:   | **25 ?? 15**  |                |         |
+--------------------------------+------+--------+-------+-------+-----------+-----------+--------+---------------+----------------+---------+
