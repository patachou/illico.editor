rapprocher des écritures inversées (2 sources)
===============================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


contexte
---------

On se trouve en présence de plusieurs liste d'individus. 
Il s'agit de rapprocher des noms/prénoms orthographiés différemment entre les deux sources et faisant référence à un même individu.

Nous avons déjà vu au début de ce tutoriel le calcul de la distance d'édition pour déterminer des variations mineures entre les écritures.
Ici, nous rapprocherons des écritures inversées -- par exemple confusion entre le nom et le prénom :

  * Paul Marie / Marie Paul
  * Hubert Philippe / Philippe Hubert

ou des écritures mélangées, noms ou prénoms composés ou avec des particules placées dans un ordre différent -- erreurs fréquentes avec les noms/prénoms d'origines étrangères.


**source A**

+-----------------------+
|       Nom Prénom      |
+=======================+
| Alphonse Alfred       |
+-----------------------+
| Théo     Dupont       |
+-----------------------+
|          ...          |
+-----------------------+


**source B**

+-----------------------+
|      Nom Prénom       |
+=======================+
| Théo     Bulle        |
+-----------------------+
| Julie    Delarue      |
+-----------------------+
| Alfred Alphonse       |
+-----------------------+
|         ...           |
+-----------------------+



mode opératoire
----------------

  1. construire des dénominateurs communs
  2. fusionner les jeux de données en appariant les dénominateurs communs
  3. calculer les ratios de similarité entre les valeurs originales.


1. construire des dénominateurs communs
----------------------------------------

Nous cherchons à rapprocher des écritures qui sont différentes d'un jeu de données à l'autre. 
Pour cela, nous allons construire une donnée de part et d'autre, qui servira de dénominateur commun.

**COLONNES -> copier une colonne**

**paramètres**

::

    position : juste avant
    en-tête  : dénominateur commun

**VALEURS EN LISTE -> trier les valeurs**

**paramètres**

::

    séparateur       : (espace)
    tri              : alphabétique
    ordre            : ascendant
    valeurs répétées : conservées à l'identique

Cette étape sera répétée sur le second jeu de données.



2. fusionner les jeux de données en appariant les dénominateurs communs
------------------------------------------------------------------------

Les deux jeux de données seront fusionnés sur le nouvelle colonne servant de dénominateur commun.

.. note:: Si vous avez commencé par travailler le fichier A puis sur le fichier B,
          le jeu de données actuellement chargé (fichier maître) est le fichier B.

**enrichissement -> fusionner, compléter ou exclure des données par jointure**

**paramètres**

::

    clé de jointure du fichier maître : "dénominateur commun"
    préfixer par                      : [B]
    
    clé de jointure du fichier lookup : "dénominateur commun"
    préfixer par                      : [A]
    
    règles de fusion                  : uniquement les correspondances exactes


3. calculer les ratios de similarité entre les valeurs originales
------------------------------------------------------------------

Nous allons ajouter deux indicateurs : l'un sous forme décimale, l'autre sous forme de fraction.

Calculons ces deux indicateurs.

**VALEURS EN LISTE : COMPARAISON -> calculer le ratio de similarité entre 2 listes**

**paramètres**

::

    liste A    : "[A] Nom Prénom"
    séparateur :  (espace)
    liste B    : "[B] Nom Prénom"
    séparateur :  (espace)
    résultat   : décimal


**VALEURS EN LISTE : COMPARAISON -> calculer le ratio de similarité entre 2 listes**

**paramètres**

::

    liste A    : "[A] Nom Prénom"
    séparateur :  (espace)
    liste B    : "[B] Nom Prénom"
    séparateur :  (espace)
    résultat   : fraction


**résultat**

+-----------------+-----------------+----------------------+-----------------------+
| [A] Nom Prénom  | [B] Nom Prénom  | similarité (décimal) | similarité (fraction) |
+=================+=================+======================+=======================+
| Alphonse Alfred | Alfred Alphonse |      1               |     2/2               |
+-----------------+-----------------+----------------------+-----------------------+
|          ...    |         ...     |     ...              |     ...               |
+-----------------+-----------------+----------------------+-----------------------+


.. warning:: Comme tout rapprochement de données, l'outil sert avant tout d'aide à la décision.

            Il revient à la charge du responsable métier de vérifier si le rapprochement est valide.
