changer de point de vue (en exploitant l'en-tête)
==================================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


contexte
---------

La matrice obtenue à la toute fin de l'exercice précédent décrit en ligne les participations d'individu à différents projets (en colonne).

+-------------+----------+----------+---------------------+
| participant |    P1    |    P2    | (autres projets...) |
+=============+==========+==========+=====================+
| toto        | resp.    |          | ...                 |
+-------------+----------+----------+---------------------+
| tata        | membre   | resp.    | ...                 |
+-------------+----------+----------+---------------------+
| titi        | membre   |          | ...                 |
+-------------+----------+----------+---------------------+
| ...         | ...      | ...      | ...                 |
+-------------+----------+----------+---------------------+
| tutu        |          | membre   | ...                 |
+-------------+----------+----------+---------------------+
| ...         | ...      | ...      | ...                 |
+-------------+----------+----------+---------------------+

.. note:: On remarque que l'information est portée par l'en-tête : il est difficile en l'état de savoir qui participe à plus de 3 projets.

L'objectif est de reconstruire un format *pivot* -- cf. précédent exercice -- avec trois colonnes : *participant*, *projet*, *rôle*.


mode opératoire
----------------

La première étape consiste à rendre explicite une information stockée dans l'en-tête.

La seconde étape consiste à traduire des listes en lignes et en colonnes.

En fonction du nombre de projets, autrement dit du nombre de colonnes, on pourra préférer la solution proposée à l'exercice suivant -- si le nombre de concaténations à exécuter est faible -- ou l'astuce proposée ci-dessous si le nombre de colonnes est très élevé.

1. exploiter les données en en-tête
------------------------------------

**LIGNES -> recopier l'en-tête à chaque ligne**

**paramètres**

::

    sélection              : toutes les colonnes décrivant un projet
    position               : avant
    séparateur             : ` = `
    neutraliser le préfixe : (laisser vide)

**résultat**

+-------------+-------------------+-------------------+---------------------+
| participant |    P1             |    P2             | (autres projets...) |
+=============+===================+===================+=====================+
| toto        |    P1 = resp.     |                   | ...                 |
+-------------+-------------------+-------------------+---------------------+
| tata        |    P1 = membre    |    P2 = resp.     | ...                 |
+-------------+-------------------+-------------------+---------------------+
| titi        |    P1 = membre    |                   | ...                 |
+-------------+-------------------+-------------------+---------------------+
| ...         | ...               | ...               | ...                 |
+-------------+-------------------+-------------------+---------------------+
| tutu        |                   |    P2 = membre    | ...                 |
+-------------+-------------------+-------------------+---------------------+
| ...         | ...               | ...               | ...                 |
+-------------+-------------------+-------------------+---------------------+

À partir de cette situation, on se retrouve dans un cas similaire à l'exercice précédent.


2. concaténation en boucle
---------------------------

**COLONNES -> concaténer en boucle**

**paramètres**

::

    colonnes   : P1, P2, P3...
    séparateur : ` / `


3. traduire en lignes
----------------------

**TRADUIRE LES LISTES -> traduire les listes en lignes**

**paramètres**

::

    colonne    : concat_boucle (la nouvelle colonne)
    séparateur : ` / `


4. traduire en colonnes
------------------------

**TRADUIRE LES LISTES -> traduire les listes en colonnes**

**paramètres**

::

    colonne    : concat_boucle
    séparateur : ` = `
    direction  : de la gauche vers la droite

En renommant les colonnes *concat_boucle_1* et *concat_boucle_2*, on obtient le tableau suivant.


**résultat**

+-------------+----------+--------+
| participant | projet   | rôle   |
+=============+==========+========+
| toto        |    P1    | resp.  |
+-------------+----------+--------+
| tata        |    P1    | membre |
+-------------+----------+--------+
| tata        |    P2    | resp.  |
+-------------+----------+--------+
| titi        |    P1    | membre |
+-------------+----------+--------+
| ...         |    P1    | ...    |
+-------------+----------+--------+
| tutu        |    P2    | membre |
+-------------+----------+--------+
| ...         | ...      | ...    |
+-------------+----------+--------+
