créer un fichier cumul à partir de deux sources aux formats proches
====================================================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


contexte
---------

On souhaite combiner deux jeux de données décrivant deux périodes --- par exemple deux années --- afin d'obtenir un unique fichier cumul des deux périodes.

Cependant,

* les deux fichiers présentent des colonnes dans un ordre différent
* certaines colonnes n'existent que dans un seul des deux jeux de données

La solution simple qui consiste copier-coller les données du premier jeu puis à copier-coller celles du second jeu ne peut donc pas être appliquée ici.

**source 1**

+-------+-------+------+------------+------------+
| agent | début | fin  | rubrique A | rubrique B |
+=======+=======+======+============+============+
|   x   | 2010  | 2011 |    100     |            |
+-------+-------+------+------------+------------+
|   y   | 2010  | 2011 |    80      |    20      |
+-------+-------+------+------------+------------+

**source 2**

+-------+-------+------+------------+------------+------------+
| agent | début | fin  | rubrique A | rubrique C | rubrique D |
+=======+=======+======+============+============+============+
|   x   | 2011  | 2012 |    90      |            |     10     |
+-------+-------+------+------------+------------+------------+
|   z   | 2011  | 2012 |    80      |    10      |     10     |
+-------+-------+------+------------+------------+------------+


mode opératoire
----------------

Créer un format pivot pour les deux jeux de données en restructurant les données (format pivot)

Adjoindre le second jeu de données sous le premier

Restructurer les données pour obtenir le format initial (les colonnes initiales et résultats seront peut-être dans un ordre différent).


.. important:: Chaque colonne doit porter un intitulé unique de colonne.


1. format pivot pour les deux sources de données 
-------------------------------------------------

source 1
~~~~~~~~~

LIGNES -> recopier l'en-tête à chaque ligne

**paramètres**

::

    sélection          : rubrique A, rubrique B
    recopier l'en-tête : avant la valeur
    séparateur         : :: (arbitraire)

**résultat**

+-------+-------+------+-----------------+----------------+
| agent | début | fin  | rubrique A      | rubrique B     |
+=======+=======+======+=================+================+
|   x   | 2010  | 2011 | rubrique A::100 |                |
+-------+-------+------+-----------------+----------------+
|   y   | 2010  | 2011 | rubrique A::80  | rubrique B::20 |
+-------+-------+------+-----------------+----------------+


COLONNES -> concaténer / fusionner (ou concaténer en boucle)

**paramètres**

::

    sélection          : rubrique A, rubrique B
    séparateur         : , (arbitraire)

**résultat**

+-------------------------------+-------+-------+------+-----------------+----------------+
| concat_boucle                 | agent | début | fin  | rubrique A      | rubrique B     |
+===============================+=======+=======+======+=================+================+
| rubrique A::100               |   x   | 2010  | 2011 | rubrique A::100 |                |
+-------------------------------+-------+-------+------+-----------------+----------------+
| rubrique A::80,rubrique B::20 |   y   | 2010  | 2011 | rubrique A::80  | rubrique B::20 |
+-------------------------------+-------+-------+------+-----------------+----------------+


COLONNES -> supprimer ou conserver

**paramètres**

::

    action             : supprimer
    sélection          : rubrique A, rubrique B

+-------------------------------+-------+-------+------+
| concat_boucle                 | agent | début | fin  |
+===============================+=======+=======+======+
| rubrique A::100               |   x   | 2010  | 2011 |
+-------------------------------+-------+-------+------+
| rubrique A::80,rubrique B::20 |   y   | 2010  | 2011 |
+-------------------------------+-------+-------+------+



PREEFERENCES UTILISATEUR -> modifier le séparateur de colonne

Si nécessaire, modifiez le séparateur pour avoir le même sur les deux jeux de données résultats intermédiaires.

**paramètres**

::

    nouveau séparateur : modifiez si nécessaire


BANDEAU -> aperçu CSV (laisser l'onglet ouvert)


source 2
~~~~~~~~~

LIGNES -> recopier l'en-tête à chaque ligne

**paramètres**

::

    sélection          : rubrique A, rubrique C, rubrique D
    recopier l'en-tête : avant la valeur
    séparateur         : :: (arbitraire)

**résultat**

+-------+-------+------+----------------+----------------+----------------+
| agent | début | fin  | rubrique A     | rubrique C     | rubrique D     |
+=======+=======+======+================+================+================+
|   x   | 2011  | 2012 | rubrique A::90 |                | rubrique D::10 |
+-------+-------+------+----------------+----------------+----------------+
|   z   | 2011  | 2012 | rubrique A::80 | rubrique C::10 | rubrique D::10 |
+-------+-------+------+----------------+----------------+----------------+


COLONNES -> concaténer / fusionner (ou concaténer en boucle)

**paramètres**

::

    sélection          : rubrique A, rubrique C, rubrique D
    séparateur         : , (arbitraire)

**résultat**

+----------------------------------------------+-------+-------+------+----------------+----------------+----------------+
| concat_boucle                                | agent | début | fin  | rubrique A     | rubrique C     | rubrique D     |
+==============================================+=======+=======+======+================+================+================+
| rubrique A::90,,rubrique D::10               |   x   | 2011  | 2012 | rubrique A::90 |                | rubrique D::10 |
+----------------------------------------------+-------+-------+------+----------------+----------------+----------------+
| rubrique A::80,rubrique C::10,rubrique D::10 |   z   | 2011  | 2012 | rubrique A::80 | rubrique C::10 | rubrique D::10 |
+----------------------------------------------+-------+-------+------+----------------+----------------+----------------+


COLONNES -> supprimer ou conserver

**paramètres**

::

    action             : supprimer
    sélection          : rubrique A, rubrique C, rubrique D

+----------------------------------------------+-------+-------+------+
| concat_boucle                                | agent | début | fin  |
+==============================================+=======+=======+======+
| rubrique A::90,,rubrique D::10               |   x   | 2011  | 2012 |
+----------------------------------------------+-------+-------+------+
| rubrique A::80,rubrique C::10,rubrique D::10 |   z   | 2011  | 2012 |
+----------------------------------------------+-------+-------+------+


PREEFERENCES UTILISATEUR -> modifier le séparateur de colonne

Si nécessaire, modifiez le séparateur pour avoir le même sur les deux jeux de données résultats intermédiaires.

**paramètres**

::

    nouveau séparateur : modifiez si nécessaire


BANDEAU -> aperçu CSV (laisser l'onglet ouvert)


2. constituer un fichier cumul à partir des deux formats pivots
----------------------------------------------------------------

CREER UN JEU DE DONNEES -> créer une liste à partir d'un texte/tableau

**paramètres**

::

    contenu     : 1. copier le contenu du premier onglet "aperçu CSV",
                  2. se positionner à la fin,
                  3. ajouter une valeur témoin "XXXXXXX"
                  4. copier le contenu de l'autre onglet "aperçu CSV"
                  5. rechercher "XXXXXXX"
                  6. supprimer l'en-tête du deuxième copier-coller
                  7. et vérifier le raccord (supprimer "XXXXXXX")
    séparateur  : le séparateur commun aux deux tableaux copiés-collés


**résultat**

+----------------------------------------------+-------+-------+------+
| concat_boucle                                | agent | début | fin  |
+==============================================+=======+=======+======+
| rubrique A::100                              |   x   | 2010  | 2011 |
+----------------------------------------------+-------+-------+------+
| rubrique A::80,rubrique B::20                |   y   | 2010  | 2011 |
+----------------------------------------------+-------+-------+------+
| rubrique A::90,,rubrique D::10               |   x   | 2011  | 2012 |
+----------------------------------------------+-------+-------+------+
| rubrique A::80,rubrique C::10,rubrique D::10 |   z   | 2011  | 2012 |
+----------------------------------------------+-------+-------+------+


3. restructurer les données et les colonnes d'origine
------------------------------------------------------

TRADUIRE LES LISTES -> traduire les listes en une matrice

**paramètres**

::

    sélection      : concat_boucle
    séparateur     : , (celui choisi à l'étape 1)
    clé => valeur  : :: (celui choisi à l'étape 1)

**résultat**

+----------------------------------------------+-------+-------+------+---------------+-----+---------------+--------------+--------------+
| concat_boucle                                | agent | début | fin  | rubrique A    |     |  rubrique B   |  rubrique C  | rubrique D   |
+==============================================+=======+=======+======+===============+=====+===============+==============+==============+
| rubrique A::100                              |   x   | 2010  | 2011 |  100          |     |               |              |              |
+----------------------------------------------+-------+-------+------+---------------+-----+---------------+--------------+--------------+
| rubrique A::80,rubrique B::20                |   y   | 2010  | 2011 |  80           |     |     20        |              |              |
+----------------------------------------------+-------+-------+------+---------------+-----+---------------+--------------+--------------+
| rubrique A::90,,rubrique D::10               |   x   | 2011  | 2012 |  90           |     |               |              |      10      |
+----------------------------------------------+-------+-------+------+---------------+-----+---------------+--------------+--------------+
| rubrique A::80,rubrique C::10,rubrique D::10 |   z   | 2011  | 2012 |  80           |     |               |    10        |      10      |
+----------------------------------------------+-------+-------+------+---------------+-----+---------------+--------------+--------------+

COLONNES -> supprimer ou conserver

**paramètres**

::

    sélection      : concat_boucle

**résultat**

+-------+-------+------+---------------+-----+---------------+--------------+--------------+
| agent | début | fin  | rubrique A    |     |  rubrique B   |  rubrique C  | rubrique D   |
+=======+=======+======+===============+=====+===============+==============+==============+
|   x   | 2010  | 2011 |  100          |     |               |              |              |
+-------+-------+------+---------------+-----+---------------+--------------+--------------+
|   y   | 2010  | 2011 |  80           |     |     20        |              |              |
+-------+-------+------+---------------+-----+---------------+--------------+--------------+
|   x   | 2011  | 2012 |  90           |     |               |              |      10      |
+-------+-------+------+---------------+-----+---------------+--------------+--------------+
|   z   | 2011  | 2012 |  80           |     |               |    10        |      10      |
+-------+-------+------+---------------+-----+---------------+--------------+--------------+

.. note:: Une colonne vide peut apparaître si, pour une ligne d'une des sources, une des colonnes ne contenait pas de valeur (dans l'exemple : avant-dernière colonne de la source 2, ligne 1).
