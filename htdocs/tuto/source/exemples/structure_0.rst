transposer une matrice (approche globale)
==========================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


contexte
---------

Généralement, la structure d'un jeu de données découle du système de collecte de données : collecter/stocker l'information le plus facilement possible.

Dans le cas présent, nous souhaitons transposer la matrice des données : autrement dit basculer les lignes en colonne et réciproquement.

La première colonne deviendra le nouvel en-tête et l'en-tête actuel deviendra la première colonne du résultat.

.. tip:: Pour se représenter le résultat à obtenir, nous pouvons effectuer une analyse préliminaire avec **MATRICE -> calculer la transposée**.

données d'origine : 2 lignes de données, 5 colonnes

+---+---+---+---+----+
| A | B | C | D | E  |
+===+===+===+===+====+
| 1 | 2 | 3 | 4 | 5  |
+---+---+---+---+----+
| 6 | 7 | 8 | 9 | 10 |
+---+---+---+---+----+

résultat : 4 lignes de données, 3 colonnes

+---+---+----+
| A | 1 | 6  |
+===+===+====+
| B | 2 | 7  |
+---+---+----+
| C | 3 | 8  |
+---+---+----+
| D | 4 | 9  |
+---+---+----+
| E | 5 | 10 |
+---+---+----+


1. intégrer l'en-tête pour le pivot
------------------------------------

La plus simple façon d'intégrer l'en-tête dans la partie données du tableau est d'exporter le résultat

  * soit *via* l'export *tableau*
  * soit *via* l'export *CSV*

puis de copier-coller le résultat dans **CHARGER DES DONNÉES -> créer un jeu de données à partir d'un texte/tableau**.

Il suffira de sélectionner la première ligne du texte (donc l'en-tête), de la copier-coller au début, de vérifier le séparateur et de valider.

L'en-tête apparaît désormais également dans la toute première ligne de données.


2. construire une colonne factice pour le pivot
------------------------------------------------

Une colonne factice sera ajoutée pour servir de clé de pivot : **COLONNES -> ajouter une colonne**.

**paramètres**

::

    avant            : A
    en-tête          : %
    initialiser avec : 1 (la valeur importe peu)


3. construire des listes de valeurs par pivot
----------------------------------------------

Nous allons ensuite créer une liste de valeurs par colonne avec la transformation **COLONNES -> pivoter (construire une liste)**.

::

    pivot         : %
    créer des listes de valeurs avec :
        colonne A : séparateur "///"
        colonne B : séparateur "///"
        colonne C : séparateur "///"
        colonne D : séparateur "///"
        colonne E : séparateur "///"

**résultat**

+-----------+-----------+-----------+-----------+------------+---+---+---+---+---+----+
| {A}///    |  {B}///   |   {C}///  | {D}///    | {E}///     | % | A | B | C | D | E  |
+===========+===========+===========+===========+============+===+===+===+===+===+====+
| A///1///6 | B///2///7 | C///3///8 | D///4///9 | E///5///10 | 1 | A | B | C | D | E  |
+-----------+-----------+-----------+-----------+------------+---+---+---+---+---+----+
| A///1///6 | B///2///7 | C///3///8 | D///4///9 | E///5///10 | 1 | 1 | 2 | 3 | 4 | 5  |
+-----------+-----------+-----------+-----------+------------+---+---+---+---+---+----+
| A///1///6 | B///2///7 | C///3///8 | D///4///9 | E///5///10 | 1 | 6 | 7 | 8 | 9 | 10 |
+-----------+-----------+-----------+-----------+------------+---+---+---+---+---+----+


4. supprimer les colonnes et lignes inutiles
----------------------------------------------

Nous appliquerons ma transformation **AGRÉGATS -> numéroter les occurrences** sur la colonne *%* ayant servi au pivot.

puis la transformation **FILTRES -> retenir les lignes**.

::

    sélection          : num:%
    retenir les lignes : qui contiennent
    valeur témoin      : 1

Enfin, nous supprimerons la première et les dernières colonnes : num:%, 1, A, B, C, D, E.


**résultat**

+-----------+-----------+-----------+-----------+------------+
| {A}///    |  {B}///   |   {C}///  | {D}///    | {E}///     |
+===========+===========+===========+===========+============+
| A///1///6 | B///2///7 | C///3///8 | D///4///9 | E///5///10 |
+-----------+-----------+-----------+-----------+------------+


5. former les nouvelles lignes de données
-------------------------------------------

La transformation **COLONNES -> concaténer en boucle** permet réunir toutes les colonnes en une seule.

Nous supprimerons les colonnes : {A}///, {B}///, {C}///, {D}///, {E}///.


**résultat**

+---------------+
| concat_boucle |
+===============+
|   A///1///6   |
+---------------+
|   B///2///7   |
+---------------+
|   C///3///8   |
+---------------+
|   D///4///9   |
+---------------+
|   E///5///10  |
+---------------+

La transformation **TRADUIRE LES LISTES -> traduire les listes en lignes** convertira ``///`` en colonne.


6. recréer l'en-tête
---------------------

La dernière étape consiste à exporter en tableau ou en CSV et à supprimer la toute première ligne qui comporte l'en-tête.

De cette façon, la ligne de données ``A | 1 | 6`` deviendra le nouvel en-tête.
