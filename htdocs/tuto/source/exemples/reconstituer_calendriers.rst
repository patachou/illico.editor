reconstituer des calendriers
=============================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


contexte
---------

Certaines applications permettent de coder des périodes --- d'activité,  de présence, etc. --- sous une forme compacte : une date de début et une date de fin.

Dans ces conditions, si le jeu de données décrit un ensemble de personne ou d'activité, il est difficile de connaître le nombre maximum de personnes étaient présentes sur un même jour.

L'objectif est de transformer cette forme compacte sous une forme développée, en autant de jours que décrit la période.

Le traitement consistera globalement 

* à transformer les périodes en autant de jours que décrivent ces périodes
* à filtrer/exclure les jours de la semaine qui ne nous intéressent pas : pour exemple on exclura les samedis et dimanches

+------------+-------------+-----------+
|   début    |      fin    |  activité |
+============+=============+===========+
| 2023-01-01 |  2023-01-15 |   ABC     |
+------------+-------------+-----------+
| 2023-01-10 |  2023-01-13 |   DEF     |
+------------+-------------+-----------+


approche par conversion numérique (6 étapes, 9 transformations)
----------------------------------------------------------------

Il s'agit de l'approche la plus longue en terme d'étapes, proposée ici pour se familiariser avec plusieurs subtilités.

mode opératoire
~~~~~~~~~~~~~~~~

Il s'agit de transformer l'intervalle de date en intervalle de nombre, de traduire ces intervalles en autant de lignes, d'identifier et d'exclure les samedis et dimanches.

1. créer une date de référence
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On identifie la plus petite date de la colonne *début* : ici le 2023-01-01 est un dimanche.

**COLONNES -> ajouter une colonne**

**paramètres**

::

    avant            : début
    en-tête          : 1er jour
    initialiser avec : 2023-01-01


+------------+------------+------------+-----------+
|  1er jour  |   début    |     fin    |  activité |
+============+============+============+===========+
| 2023-01-01 | 2023-01-01 | 2023-01-15 |   ABC     |
+------------+------------+------------+-----------+
| 2023-01-01 | 2023-01-10 | 2023-01-13 |   DEF     |
+------------+------------+------------+-----------+


2. transformer l'intervalle de date en un intervalle de nombre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**TEMPS -> calculer une durée (jours) entre 2 dates**

**paramètres**

::

    format                          : ssaa-mm-jj
    début                           : 1er jour
    fin                             : début
    le dernier jour de l'intervalle : est inclus
    nombre de jours                 : calendaires (lundi à dimanche)

+------------------------------------------------+-----+
| nb jours [1er jour,début (inclus)]/calendaires | ... |
+================================================+=====+
|                          1                     | ... |
+------------------------------------------------+-----+
|                          10                    | ... |
+------------------------------------------------+-----+


**TEMPS -> calculer une durée (jours) entre 2 dates**

**paramètres**

::

    format                          : ssaa-mm-jj
    début                           : 1er jour
    fin                             : fin
    le dernier jour de l'intervalle : est inclus
    nombre de jours                 : calendaires (lundi à dimanche)

+----------------------------------------------+-----+
| nb jours [1er jour,fin (inclus)]/calendaires | ... |
+==============================================+=====+
|                          15                  | ... |
+----------------------------------------------+-----+
|                          13                  | ... |
+----------------------------------------------+-----+


Les deux colonnes seront renommées et réordonnées pour obtenir :

+--------------+------------+------------+------------+------------+-----------+
| nb jr début  | nb jr fin  |  1er jour  |   début    |     fin    |  activité |
+==============+============+============+============+============+===========+
|      1       |     15     | 2023-01-01 | 2023-01-01 | 2023-01-15 |   ABC     |
+--------------+------------+------------+------------+------------+-----------+
|      10      |     13     | 2023-01-01 | 2023-01-10 | 2023-01-13 |   DEF     |
+--------------+------------+------------+------------+------------+-----------+


3. générer toutes les valeurs de l'intervalle de nombre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**INTERVALLES -> compléter un intervalle**

**paramètres**

::

    début      : nb jr début
    fin        : nb jr fin
    pas        : 1 (valeur par défaut)
    séparateur : -

+---------------------------------------+--------------+------------+------------+------------+------------+-----------+
| [ nb jr début ; nb jr fin ] (complet) | nb jr début  | nb jr fin  |  1er jour  |   début    |     fin    |  activité |
+=======================================+==============+============+============+============+============+===========+
| 1.00-2.00-.....-14.00-15.00           |      1       |     15     | 2023-01-01 | 2023-01-01 | 2023-01-15 |   ABC     |
+---------------------------------------+--------------+------------+------------+------------+------------+-----------+
| 10.00-11.00-........-12.00-13.00      |      10      |     13     | 2023-01-01 | 2023-01-10 | 2023-01-13 |   DEF     |
+---------------------------------------+--------------+------------+------------+------------+------------+-----------+


4. convertir ces intervalles en autant de lignes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**TRADUIRE LES LISTES -> traduire les listes en lignes**

**paramètres**

::

    séparateur : -

+---------------------------------------+--------------+------------+------------+------------+------------+-----------+
| [ nb jr début ; nb jr fin ] (complet) | nb jr début  | nb jr fin  |  1er jour  |   début    |     fin    |  activité |
+=======================================+==============+============+============+============+============+===========+
|           1.00                        |      1       |     15     | 2023-01-01 | 2023-01-01 | 2023-01-15 |   ABC     |
+---------------------------------------+--------------+------------+------------+------------+------------+-----------+
|           2.00                        |      1       |     15     | 2023-01-01 | 2023-01-01 | 2023-01-15 |   ABC     |
+---------------------------------------+--------------+------------+------------+------------+------------+-----------+
|           ...                         |     ...      |     ...    |      ...   |     ...    |      ...   |     ...   |
+---------------------------------------+--------------+------------+------------+------------+------------+-----------+
|           14.00                       |      1       |     15     | 2023-01-01 | 2023-01-01 | 2023-01-15 |   ABC     |
+---------------------------------------+--------------+------------+------------+------------+------------+-----------+
|           15.00                       |      1       |     15     | 2023-01-01 | 2023-01-01 | 2023-01-15 |   ABC     |
+---------------------------------------+--------------+------------+------------+------------+------------+-----------+
|           10.00                       |      10      |     13     | 2023-01-01 | 2023-01-10 | 2023-01-13 |   DEF     |
+---------------------------------------+--------------+------------+------------+------------+------------+-----------+
|           11.00                       |      10      |     13     | 2023-01-01 | 2023-01-10 | 2023-01-13 |   DEF     |
+---------------------------------------+--------------+------------+------------+------------+------------+-----------+
|           12.00                       |      10      |     13     | 2023-01-01 | 2023-01-10 | 2023-01-13 |   DEF     |
+---------------------------------------+--------------+------------+------------+------------+------------+-----------+
|           13.00                       |      10      |     13     | 2023-01-01 | 2023-01-10 | 2023-01-13 |   DEF     |
+---------------------------------------+--------------+------------+------------+------------+------------+-----------+


5. convertir ce nombre en jour de la semaine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**COLONNES -> ajouter une colonne**

**paramètres**

::

    en-tête          : 7
    initialiser avec : 7


**CALCULS -> calculer une opération sur 2 colonnes**

**paramètres**

::

    opérande 1   : [ nb jr début ; nb jr fin ] (complet)
    opérateur    : modulo
    opérande 2   : 7
        =        : num jour

Ainsi, la colonne *num jour* varie de 0 à 6 : 1 pour dimanche (2023-01-01), 2 pour lundi, ... 5 pour vendredi, 6 pour samedi.


6. exclure les samedis et les dimanches
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On exclut les dimanches.

**FILTRER -> retenir les lignes...**

**paramètres**

::

    sélection          : num jour
    retenir les lignes : qui ne contiennent pas...
    ... valeur témoin  : 1 (2023-01-01 est un dimanche)


On exclut les samedis.

**FILTRER -> retenir les lignes...**

**paramètres**

::

    sélection          : num jour
    retenir les lignes : qui ne contiennent pas...
    ... valeur témoin  : 0

La fonction d'analyse *agrégats -> domaine de valeur* agrége le nombre d'occurrence pour une même date (date représentée par un nombre de jour par rapport à la date de référence 2023-01-01).

::

    compter les lignes : [ nb jour début ; nb jour fin ] (complet)


On trouve 4 jours :

  * 10
  * 11
  * 12
  * 13

qui représentent :

  * 10ème jour depuis 2023-01-01 -> 2023-01-10 (mardi)
  * 11ème jour depuis 2023-01-01 -> 2023-01-11 (mercredi)
  * 12ème jour depuis 2023-01-01 -> 2023-01-12 (jeudi)
  * 13ème jour depuis 2023-01-01 -> 2023-01-13 (vendredi)


approche par jointure (3 étapes, 4 transformations)
----------------------------------------------------

mode opératoire
~~~~~~~~~~~~~~~~

Pour cette approche, le filtre est le résultat d'une jointure en excluant les éléments communs entre notre jeu de données et un calendrier complet de référence

1. créer un calendrier des dates à exclure
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**CREER UN JEU DE DONNEES -> créer un calendrier**

**paramètres**

::

    début      : 2023-01-01
    fin        : 2023-01-15
    jours      : lundi, mardi, mercredi, jeudi, vendredi (tous sauf samedi et dimanche)
    séparateur : (tabulation)


Le jeu de données générés sera ensuite exporté au format CSV et nommé : calendrier2023.tsv


2. générer l'intervalle de dates pour chaque période
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Charger le jeu de données à traiter (cf. début du tutoriel) puis appliquer la transformation

**TEMPS -> compléter un intervalle de date**

**paramètres**

::

    format                       : ssaa-mm-jj
    séparateur                   : ,
    début                        : début
    fin                          : fin
    dernier jour de l'intervalle : inclus

+-----------------------------------+------------+-------------+-----------+
| [début à fin (inclus)]/intervalle |   début    |      fin    |  activité |
+===================================+============+=============+===========+
| 2023-01-01,2023-01-02...          | 2023-01-01 |  2023-01-15 |   ABC     |
+-----------------------------------+------------+-------------+-----------+
| 2023-01-10,2023-01-11...          | 2023-01-10 |  2023-01-13 |   DEF     |
+-----------------------------------+------------+-------------+-----------+


**TRADUIRE LES LISTES -> traduire les listes en lignes**

**paramètres**

::

    sélection  : [début à fin (inclus)]/intervalle
    séparateur : ,

+-----------------------------------+------------+-------------+-----------+
| [début à fin (inclus)]/intervalle |   début    |      fin    |  activité |
+===================================+============+=============+===========+
| 2023-01-01                        | 2023-01-01 |  2023-01-15 |   ABC     |
+-----------------------------------+------------+-------------+-----------+
| 2023-01-02                        | 2023-01-01 |  2023-01-15 |   ABC     |
+-----------------------------------+------------+-------------+-----------+
| ...                               | ...        |     ...     |   ...     |
+-----------------------------------+------------+-------------+-----------+
| 2023-01-15                        | 2023-01-01 |  2023-01-15 |   ABC     |
+-----------------------------------+------------+-------------+-----------+
| 2023-01-10                        | 2023-01-10 |  2023-01-13 |   DEF     |
+-----------------------------------+------------+-------------+-----------+
| 2023-01-11                        | 2023-01-10 |  2023-01-13 |   DEF     |
+-----------------------------------+------------+-------------+-----------+
|   ...                             |  ...       |    ...      |   ...     |
+-----------------------------------+------------+-------------+-----------+
| 2023-01-13                        | 2023-01-10 |  2023-01-13 |   DEF     |
+-----------------------------------+------------+-------------+-----------+


3. exclure les dates communes aux deux jeux de données
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**ENRICHISSEMENT -> fusionner, compléter ou exclure des données par jointure**

**paramètres**

::

    clé de jointure fic. maître : [début,fin] (complet)
    préfixer par                : (ne rien saisir)
    ---------------------------------------------------------------------
    encodage                    : UTF-8
    fic. lookup (csv...)        : calendrier2023.tsv
    séparateur de colonne       : tabulation
    préfixer par                : (ne rien saisir)
    ---------------------------------------------------------------------
    conserver                   : uniquement les correspondances exactes


On trouve :

  * 2023-01-10 (mardi)
  * 2023-01-11 (mercredi)
  * 2023-01-12 (jeudi)
  * 2023-01-13 (vendredi)


approche par filtre direct (3 étapes, 5 transformations)
---------------------------------------------------------

Pour cette approche, l'intervalle de date est calculé directement (toutes les dates) et chaque date est convertie en jour de la semaine.

1. générer toutes les dates de l'intervalle
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Charger le jeu de données à traiter (cf. début du tutoriel) puis appliquer la transformation

**TEMPS -> compléter un intervalle de date**

**paramètres**

::

    format                       : ssaa-mm-jj
    séparateur                   : ,
    début                        : début
    fin                          : fin
    dernier jour de l'intervalle : inclus

+-----------------------------------+------------+-------------+-----------+
| [début à fin (inclus)]/intervalle |   début    |      fin    |  activité |
+===================================+============+=============+===========+
| 2023-01-01,2023-01-02...          | 2023-01-01 |  2023-01-15 |   ABC     |
+-----------------------------------+------------+-------------+-----------+
| 2023-01-10,2023-01-11...          | 2023-01-10 |  2023-01-13 |   DEF     |
+-----------------------------------+------------+-------------+-----------+


**TRADUIRE LES LISTES -> traduire les listes en lignes**

**paramètres**

::

    sélection  : [début à fin (inclus)]/intervalle
    séparateur : ,

+-----------------------------------+------------+-------------+-----------+
| [début à fin (inclus)]/intervalle |   début    |      fin    |  activité |
+===================================+============+=============+===========+
|          2023-01-01               | 2023-01-01 |  2023-01-15 |   ABC     |
+-----------------------------------+------------+-------------+-----------+
|          2023-01-02               | 2023-01-01 |  2023-01-15 |   ABC     |
+-----------------------------------+------------+-------------+-----------+
|          ...                      | ...        |     ...     |   ...     |
+-----------------------------------+------------+-------------+-----------+
|          2023-01-15               | 2023-01-01 |  2023-01-15 |   ABC     |
+-----------------------------------+------------+-------------+-----------+
|          2023-01-10               | 2023-01-10 |  2023-01-13 |   DEF     |
+-----------------------------------+------------+-------------+-----------+
|          2023-01-11               | 2023-01-10 |  2023-01-13 |   DEF     |
+-----------------------------------+------------+-------------+-----------+
|            ...                    |  ...       |    ...      |   ...     |
+-----------------------------------+------------+-------------+-----------+
|          2023-01-13               | 2023-01-10 |  2023-01-13 |   DEF     |
+-----------------------------------+------------+-------------+-----------+


2. convertir en jour de la semaine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**TEMPS -> donner le jour de la semaine**

**paramètres**

::

    format    : ssaa-mm-jj
    sélection : [début à fin (inclus)]/intervalle


3. exclure les samedis et dimanches
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* en deux actions :
   * **filtrer -> retenir les lignes...** qui ne contiennent pas *samedi*
   * **filtrer -> retenir les lignes...** qui ne contiennent pas *dimanche*

ou bien

* en deux actions :
   * **colonnes -> ajouter une colonne** "jours à exclure" initialisée à "samedi,dimanche"
   * **valeurs en liste : filtres -> filtrer ou exclure les valeurs d'une liste** quand la valeur est trouvée dans la colonne "jours à exclure" (séparateur ,)
