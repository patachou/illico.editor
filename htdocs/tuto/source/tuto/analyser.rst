Analyser pour produire rapidement des statistiques simples
==========================================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------

Une brève analyse permet de repérer quelques problèmes.

Répartition des interventions par service
-----------------------------------------

En général, l'analyse de données commence par évaluer quelles sont les valeurs en présence et 
leur répartition. Dans Illico, la répartition de données utilise un agrégat équivalent au 
GROUP BY en SQL, ou aux tableaux croisés dynamiques dans Excel.
Comme l'ordinateur est sensible aux différences typographiques, la répartition des valeurs les 
révèle.

Pour lancer cette fonction d'analyse allez dans le menu *transformer et analyser* puis *Agrégats ->* 
**domaine de valeur : nb, %**.

| |transformation domaine de valeur|

..

Voici le résultat pour l'analyse des services de notre jeu de données.

| |domaine de valeur|

..

.. note:: Ce tableau de synthèse peut être exporté vers un tableur (*tableau*) ou un fichier *CSV*.

.. important:: Le classement est sensible à la casse (majuscule, minuscule) : « budget » et « Budget », 
               « rh » et « RH ».
               Dans notre jeu de données, les données ne sont pas qualifiées car « compta » et 
               « comptabilité » désignent le même service, nous verrons plus loin comment homogénéiser 
               rapidement ces données.

Compter le nombre de valeurs dans une liste
--------------------------------------------

Toujours sur le même jeu de données, essayons maintenant de connaître la répartition du 
nombre de contrats. Les contrats sont enregistrés dans la colonne "Nb mois effectués", les 
uns après les autres en indiquant simplement l'année et le nombre de mois travaillés. Il y a  
parfois plusieurs valeurs par cases, le jargon informatique les appelle *données multivaluées*.
Il faut commencer par compter ces données multivaluées. Puis compter les valeurs obtenues par 
ce premier résultat. On obtiendra ainsi la répartition du nombre de contrats.

Pour commencer, lancez *valeurs en liste : agrégats->* **compter le nombre d'éléments** 
et choisissez la colonne 'Nb mois effectués' avec le séparateur virgule.

| |liste de prestations|

..

**Résultat**

Illico ajoute une colonne qui totalise les valeurs repérées. Ce n'est pas encore le 
résultat final, ni à dire vrai le résultat attendu. Un problème discret et une mauvaise
orthographe faussent les calculs.

+---------------------------+---------------------+--------------+---------------------+
| nb élts:Nb mois effectués | Contact             | Service      | Nb mois effectués   |
+===========================+=====================+==============+=====================+
| 1                         | Patrick BRAVO       | compta       | 2009=6              |
+---------------------------+---------------------+--------------+---------------------+
| 2                         | Sylvia LEDUC        | budget       | 2010=2,2011=1       |
+---------------------------+---------------------+--------------+---------------------+
| 2                         | Pierre MARCHAND     | juridique    | 2010=3,2011=2       |
+---------------------------+---------------------+--------------+---------------------+
| 0                         | Thérèse FITZ        | informatique |                     | 
+---------------------------+---------------------+--------------+---------------------+
| 2                         | Gabriel LEMARQUIS   | RH           | 2010=12,2011=3      |
+---------------------------+---------------------+--------------+---------------------+
| 1                         | Jean-Marc FICHE     | comptabilité | 2010=4              |
+---------------------------+---------------------+--------------+---------------------+
| 1                         | Gabrielle LEMARQUIS | rh           | 2009=2              |
+---------------------------+---------------------+--------------+---------------------+
| 1                         | Stéphanie LEPOINT   | Budget       | 2010=11;2011=2      |
+---------------------------+---------------------+--------------+---------------------+

..

.. important:: Pour la dernière ligne, comme la donnée est mal formatée --- un point-virgule au lieu 
               d'une virgule --- l'agrégation ne détecte qu'une seule valeur dans la liste.
               Mme LEMARQUIS apparaît deux fois, avec un prénom orthographié différemment.

Terminons cependant notre comptage. Lancez à nouveau *Agrégats ->* **domaine de valeur : nb, %**
 en choisissant la colonne ajoutée par Illico. Nous comptons donc les valeurs présentes
 dans cette colonne.

+-----------------------------+--------+-----------+
| nb élts : Nb mois effectués | nombre | ratio     |
+=============================+========+===========+
| 0                           | 1      | 12.50 %   |
+-----------------------------+--------+-----------+
| 1                           | 4      | 50.00 %   |
+-----------------------------+--------+-----------+
| 2                           | 3      | 37.50 %   |
+-----------------------------+--------+-----------+
| **TOTAL**                   | **8**  | **100 %** |
+-----------------------------+--------+-----------+

..

**Lecture des résultats**

  * 1 intervenant n'a pas de contrat (0)
  * 4 intervenants n'ont eu qu'1 seul contrat (1)
  * 3 intervenants ont eu 2 contrats (2)

.. note:: Bien entendu ces résultats sont faux à cause des problèmes repérés précédemment. Dans la suite 
          de ce tutoriel nous allons apprendre à les corriger. **Soyez toujours vigilant sur la qualité 
          de vos données avant d'effectuer un traitement.** 

.. |transformation domaine de valeur| image:: /_static/images/7/0.png
                          :scale: 100%

.. |liste de prestations| image:: /_static/images/7/1.png
                          :scale: 100%

.. |domaine de valeur| image:: /_static/images/7/2.png
                          :scale: 100%
