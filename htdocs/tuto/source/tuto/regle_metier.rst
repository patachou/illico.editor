appliquer une règle métier
===========================

.. contents:: Contenu
   :local:
   :backlinks: top

-------

On va donc mettre met les bons termes dans les bonnes cases. Voici différents outils, qu'on peut appliquer 
sur la colonne Service. Ayez bien en tête que votre fichier pourrait être beaucoup plus gros avec beaucoup 
de variations de termes, difficiles voir impossible à gérer par des succession de chercher/remplacer.

.. note:: Une **règle métier** désigne un recodage pour rendre la donnée intelligible.
          Par exemple, la conversion d'un code ou d'un acronyme en un libellé complet.


Modifier une valeur par un autre
---------------------------------

On est dans le cas du chercher/remplacer, pratique pour des choses simples. Utilisez la transformation 
*Valeurs ->* **traduire ou supprimer** pour remplacer ``RH`` par ``ressources humaines``.


Modifier conditionnellement les valeurs
----------------------------------------

Plusieurs transformations modifient une valeur d'une colonne en fonction des valeurs d'une autre colonne :

**remplacer... si...**

  * définir des valeurs conditionnelles (regex [1]_)
  * définir des valeurs conditionnelles (numérique)
  * mixer les valeurs de 2 colonnes

      mixe 2 colonnes avec une règle de priorité : par exemple, si la colonne A n'a pas de valeur, prendre la valeur de la colonne B ; dans le cas contraire, utiliser la valeur de A (quelque soit la valeur de B).

  * remplacer un motif (regex) par une valeur

      reprend la logique du ``SI ... ALORS ...`` (pas de SINON) et s'applique sur plusieurs colonnes en une seule action.

.. [1] *regex*, contraction de *regular expression*, désigne une expression rationnelle c'est-à-dire un motif de recherche (cf. `regex <http://illico.ti-nuage.fr/doc/build/html/memo/regex.html>`__).
   Les expressions rationnelles constituent un langage concis --- un peu déroutant au début --- simple et terriblement efficace.
   Prenez le temps d'en étudier la syntaxe (cf. `regex <http://illico.ti-nuage.fr/doc/build/html/memo/regex.html>`__).


modifier des valeurs présentées sous forme de liste
----------------------------------------------------

**valeurs en liste**

  * remplacer une valeur

         recode spécifiquement une des valeurs


avec une autre liste de référence, recoder des valeurs présentées en liste
---------------------------------------------------------------------------

**valeurs en liste : enrichissement**

  * recoder des clés par des valeurs (ou l'inverse)

         cas particulier où dans une liste sous une forme ``clé1:valeur1, clé2:valeur2, ...`` on souhaite recoder uniquement les clés ou les valeurs


  * déduire un sous-ensemble à partir de 2 listes

         exécute une opération ensembliste — union, intersection, différence — entre 2 listes de valeurs.


rapprocher des données d'une autre source
------------------------------------------

**enrichissement**

  * fusionner, compléter ou exclure des données par jointure


Exemple
--------

**Remplacer conditionnellement**

Uniformiser les variantes d'écriture pour respecter un lexique précis s'effectue avec
la transformation *remplacer... si... ->* **définir des valeurs conditionnelles (regex)**.

Les variantes ``compta``, ``comptabilité``, ``Compta``, ``compta.`` seront recodées en ``comptabilité``.

| |compta|

.. |compta| image:: /_static/images/6/1.png
                          :scale: 100%

