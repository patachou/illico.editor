modifier la structure des données
==================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


décomposer un champ "nom prénom" en deux champs
------------------------------------------------

approche naïve
~~~~~~~~~~~~~~~

Dans le fichier de données d'exercice, les individus sont identifiés par une colonne comportant à la fois les noms et les prénoms.

Dans une approche dite *naïve*, nous utiliserons la transformation **TRADUIRE LES LISTES -> traduire les listes en colonnes**.

::

    paramètres
    
    traduire en éclatant : de la gauche vers la droite
    sélection            : colonne "1 - Contact"
    séparateur           : <espace>
    action               : remplacer la colonne


.. important:: Cette approche ne fonctionnera pas avec des noms ou prénoms multiples/composés ou comportant des particules.

+------------+------------+---------------+-------------------+
| Contact_1  | Contact_2  | Service       | Nb mois effectués |
+============+============+===============+===================+
| Patrick    | BRAVO      | compta        | 2009=6            |
+------------+------------+---------------+-------------------+
| Sylvia     | LEDUC      | budget        | 2010=2,2011=1     |
+------------+------------+---------------+-------------------+
| Pierre     | MARCHAND   | juridique     | 2010=3,2011=2     |
+------------+------------+---------------+-------------------+
| Thérèse    | FITZ       | informatique  |                   |
+------------+------------+---------------+-------------------+
| Gabriel    | LEMARQUIS  | RH            | 2010=12,2011=3    |
+------------+------------+---------------+-------------------+
| Jean-Marc  | FICHE      | comptabilité  | 2010=4            |
+------------+------------+---------------+-------------------+
| Gabrielle  | LEMARQUIS  | rh            | 2009=2            |
+------------+------------+---------------+-------------------+
| Stéphanie  | LEPOINT    | Budget        | 2010=11;2011=2    |
+------------+------------+---------------+-------------------+


approche générale
~~~~~~~~~~~~~~~~~~

.. note:: Pour cet exemple, le fichier source a été modifié pour avoir des noms composés d'une particule (exemple : *LE MARQUIS*).

Le principe repose sur des *expressions rationnelles* [1]_ --- *regular expression* (regex) en anglais --- pour délimiter le nom du prénom, y compris dans des configurations complexes avec des noms composés ou des particules.

.. [1] (cf. `regex <http://illico.ti-nuage.fr/doc/build/html/memo/regex.html>`__).

Les expressions rationnelles permettent de découper une valeur en plusieurs parties ; les variables $1, $2... représentent les différentes parties du motif.

.. tip:: L'aide sur les expressions rationnelles est également disponible depuis le bandeau supérieur et comporte un exemple sur les *NOM Prénoms*.

        | |aide regex|


::

    ^([A-Z \-']*) (.*)$ => ($1) ($2)
    # dans une liste de "NOM Prénom",
    # $1 sélectionne les noms simples et composés,
    # $2 les prénoms simples et composés.


Le fichier d'exemple possède une configuration inversée : Prénoms NOMS. Nous allons donc modifier l'expression rationnelle fournie dans l'aide en nous inspirant des motifs suivants :

::

    a?b*c+
    # a répété 0 ou 1 fois, b 0...n fois, c au moins 1 fois

    a??b*?c+?
    # idem
    # mais les motifs ramèneront de préférence les correspondances les plus courtes

En utilisant le caractère ``?``, nous obtenons ``^(.*?) ([A-Z \-']*)$`` avec.

::

    ^(.*?) : sélectionne les prénoms : la plus petite chaîne correspondant au motif
    
    ([A-Z \-']*)$ : les prénoms (en majuscule).

Appliquons de nouveau la transformation **STRUCTURE -> remplacer un motif (regex) par une valeur**

::

    paramètres
    
    motif à remplacer   : ^(.*?) ([A-Z \-']*)$
    par                 : $1+$2 (nous obtiendrons 'nom+prénom'
    sensible à la casse : oui

+----------------------------+---------------+-------------------+
| Contact                    | Service       | Nb mois effectués |
+============================+===============+===================+
| Patrick ``+`` BRAVO        | compta        | 2009=6            |
+----------------------------+---------------+-------------------+
| Sylvia ``+`` LEDUC         | budget        | 2010=2,2011=1     |
+----------------------------+---------------+-------------------+
| Pierre ``+`` MARCHAND      | juridique     | 2010=3,2011=2     |
+----------------------------+---------------+-------------------+
| Thérèse ``+`` FITZ         | informatique  |                   |
+----------------------------+---------------+-------------------+
| Gabriel ``+`` LE MARQUIS   | RH            | 2010=12,2011=3    |
+----------------------------+---------------+-------------------+
| Jean-Marc ``+`` FICHE      | comptabilité  | 2010=4            |
+----------------------------+---------------+-------------------+
| Gabrielle ``+`` LE MARQUIS | rh            | 2009=2            |
+----------------------------+---------------+-------------------+
| Stéphanie ``+`` LEPOINT    | Budget        | 2010=11;2011=2    |
+----------------------------+---------------+-------------------+

..

.. tip:: La fonction d'analyse **REMPLACER... SI... -> tester un motif (regex)** renvoie les correspondances et non-correspondances à un motif donné 
       afin de vérifier la validité et pertinence d'un motif donné.


Nous pouvons ensuite appliquer de nouveau **TRADUIRE LES LISTES -> traduire les listes en colonne** (cf. approche naïve) avec le paramètre ``+`` pour séparer sans erreur nom et prénom.


transformer en une matrice des données en liste
------------------------------------------------

Dans le jeu de données de test, les prestations sont présentées sous une forme compacte : des listes.

Remettre ces informations sous une forme exploitable représente souvent une série conséquente de copier-coller.

Des transformations sont prévues pour éviter ces manipulations sources d'erreur.

La transformation **COLONNES -> traduire les listes en une matrice** permet notamment de construire des colonnes pour chaque année et d'inscrire le nombre de mois relatif à cette année.


| |matrice|


..

::

    paramètres
    
    sélection             : colonne "3 - Nb mois effectués"
    séparateur            :  ,  (dans la liste, les éléments sont séparés par une virgule)
    
    partie optionnelle
    
    clé => valeur         :  =  (dans la liste, chaque élément représente un couple de {clé => valeur} )
    séparateur (résultat) : <inutile ici>
    a => b => c           : <inutile ici>

| |matrice résultats|

..

.. note:: En fonction de vos données, les nouvelles colonnes peuvent être ordonnées différemment (par exemple « 2010 » avant « 2009 »).

On remarque ici une colonne vide à la fin qui correspond à la ligne « Thérèse FITZ ».

**cas particulier**

Si un des intervenants participe plusieurs fois dans la même année, seule la dernière mention dans la liste sera prise en compte (2010=3, 2010=7).

Le paramètre *séparateur de résultat* modifie ce comportement et délimitera toutes les occurrences (3+7).

::

    Exercice :

    1. modifiez le jeu de données et indiquez pour Sylvia 2010=2,2010=1
    2. indiquez cette fois en paramètre "séparateur de résultat" le caractère suivant : +
    3. observez le résultat

.. |aide regex| image:: /_static/images/3/1.png
                          :scale: 100%

.. |matrice| image:: /_static/images/10/1.png
                          :scale: 100%

.. |matrice résultats| image:: /_static/images/10/2.png
                          :scale: 100%
