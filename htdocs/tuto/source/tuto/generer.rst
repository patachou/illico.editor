traduire les données en ligne, colonne et matrice
==================================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------

.. important:: Dans le jeu de données d'exercice, la dernière ligne contient un ``;`` à la place du caractère attendu ``,``.
               Cette anomalie est volontaire pour vous inviter à toujours vérifier vos données avant de les transformer.


créer des lignes (valeurs en liste)
------------------------------------

Toujours sur le même fichier d'exemple, nous souhaitons désormais passer
d'un fichier qui décrit des individus --- une ligne par individu --- à un fichier qui décrit des prestations.

Comme un individu réalise plusieurs prestations, nous voulons autant de lignes que de prestations.

La transformation **TRADUIRE LES LISTES -> traduire les listes en lignes** réalise cette action.

**paramètres**

::

    colonne    : Nb mois effectués
    séparateur : ,


**résultat**

+-------------------+-----------+-------------------+
| Contact           | Service   | Nb mois effectués |
+===================+===========+===================+
|   ...             |  ...      | ...               |
+-------------------+-----------+-------------------+
| Sylvia LEDUC      | budget    | ``2010=2``        |
+-------------------+-----------+-------------------+
| Sylvia LEDUC      | budget    | ``2011=1``        |
+-------------------+-----------+-------------------+
|   ...             | ...       |  ...              |
+-------------------+-----------+-------------------+
| Pierre MARCHAND   | juridique | ``2010=3``        |
+-------------------+-----------+-------------------+
| Pierre MARCHAND   | juridique | ``2011=2``        |
+-------------------+-----------+-------------------+
|   ...             | ...       |  ...              |
+-------------------+-----------+-------------------+
| Gabriel LEMARQUIS | RH        | ``2010=12``       |
+-------------------+-----------+-------------------+
| Gabriel LEMARQUIS | RH        | ``2011=3``        |
+-------------------+-----------+-------------------+
|   ...             | ...       |  ...              |
+-------------------+-----------+-------------------+

..

Les prestations des individus ayant effectué plusieurs prestations sont désormais représentées sur des lignes distinctes --- une ligne par prestation.


modifier les listes de clés/valeurs
-----------------------------------

.. note:: Nous annulerons l'étape précédente afin de repartir du fichier d'origine.

Dans le fichier d'exemple, on voit sous une forme compacte à la fois les années et durées des prestations.
Sur la deuxième ligne on trouve ainsi ``2010=2,2011=1`` qui signifie ``2 mois en 2010 et 1 mois en 2011``.

La transformation **VALEURS EN LISTE : STRUCTURE -> modifier les associations (clés/valeurs)** permet ici :

  * de retirer les années et de ne conserver que les durées,
  * de retirer les durées et de ne conserver que les années,
  * d'inverser le sens de l'association pour recoder l'information ``2010=2,2011=1`` en ``2=2010,1=2011``.

De manière générale, si l'information est codée sous la forme d'une liste de ``clés:valeurs`` alors il est possible de :

  * conserver les clés uniquement,
  * conserver les valeurs uniquement,
  * inverser le sens des associations.

**paramètres**

::

    colonne       : Nb mois effectués
    séparateur    : ,
    clé => valeur : =
    action        : inverser le sens des associations


**résultat**

+-------------------+-----------+-------------------+
| Contact           | Service   | Nb mois effectués |
+===================+===========+===================+
| Patrick BRAVO     | compta    | ``6=2009``        |
+-------------------+-----------+-------------------+
| Sylvia LEDUC      | budget    | ``2=2010,1=2011`` |
+-------------------+-----------+-------------------+
| Perre MARCHAND    | juridique | ``3=2010,2=2011`` |
+-------------------+-----------+-------------------+
|   ...             | ...       |  ...              |
+-------------------+-----------+-------------------+

..


exploiter les couples clés/valeurs (matrice)
---------------------------------------------

.. hint :: Pour cet exemple, nous partirons du résultat obtenu à l'exercice précédent.

À l'exercice précédent, nous avons inversé le sens des associations entre les années et les durées.

Dès lors, il est possible, avec la transformation **TRADUIRE LES LISTES -> traduire les listes en une matrice**, de créer une colonne pour chaque durée.

**paramètres**

::

    colonne       : Nb mois effectués
    séparateur    : ,
    clé => valeur : =

**résultat**

| |matrice|

..

Comme pour l'exercice précédent, pour la dernière ligne, le ``;`` est incorrect. À la place, une ``,`` était attendue.

D'un coup d'oeil, nous déduisons une plus grande fréquence des contrats de 2 et 3 mois.

.. note: Charger le fichier initial et appliquer directement cette fonctionnalité pour obtenir la distribution des durées et les années en colonne.

.. |matrice| image:: /_static/images/13/1.png
                         :scale: 100%
