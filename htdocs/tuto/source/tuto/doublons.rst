traiter les doublons
=====================

.. contents:: Contenu
   :local:
   :backlinks: top

-------

.. note:: Ici, `normaliser les données <https://fr.wikipedia.org/wiki/Forme_normale_(bases_de_donn%C3%A9es_relationnelles)>`__ consiste à créer un dictionnaire de correspondance.


.. tip:: Pour se représenter le résultat à obtenir, nous pouvons effectuer une analyse préliminaire avec

         * **AGRÉGATS -> domaine** (sur une seule colonne)
         * **AGRÉGATS -> dictionnaire** (cas général)


approche 1 : normaliser une colonne
------------------------------------

Transformation **STRUCTURE -> créer des identifiants (Merise 2FN)**

Nous allons ici, pour chaque service, générer un numéro unique --- clé --- et
reporter la correspondance entre ce numéro et le nom du service dans un nouveau fichier.

.. note:: Il s'agit de la deuxième forme normale de la méthode Merise.

**résultat : table de correspondance**

+----------------+-----+
| Service        | clé |
+================+=====+
| comptabilité   | 1   |
+----------------+-----+
| budget         | 2   |
+----------------+-----+
| juridique      | 3   |
+----------------+-----+
| informatique   | 4   |
+----------------+-----+
| rh             | 5   |
+----------------+-----+

Chaque service est désormais codé sous la forme d'une clé (numérotation incrémentale).

Le résultat est un tableau de correspondance entre la clé --- code --- et le nom du service.
Dans le tableau original, l'identifiant remplace l'ancienne valeur.

Cette table de correspondance contient chaque service, une seule fois avec son identifiant.


approche 2 : isoler les singletons/doublons
--------------------------------------------

Ici, la méthode est différente : il s'agit d'identifier des groupes de
doublons.

La transformation **FILTRES -> singletons, doublons...** affiche
systématiquement une analyse de la situation.

Elle permet également de

  * filtrer : conserver uniquement les enregistrements en doublons, triplons...
  * exclure : conserver uniquement les enregistrements uniques (singletons)
  * marquer :

     * ajouter en première colonne un identifiant pour chaque groupe de doublons
     * aucun identifiant particulier ne sera ajouté pour les singletons

  * marquer et filtrer
  * analyser : aucun traitement, affiche seulement le tableau d'analyse

**exemple**

*marquer* les doublons de la colonne service

+-----------------+----------------+
| ! doublons(1)   |  Service       |
+=================+================+
| 1               | comptabilité   |
+-----------------+----------------+
| 2               | budget         |
+-----------------+----------------+
|                 | juridique      |
+-----------------+----------------+
|                 | informatique   |
+-----------------+----------------+
| 3               | rh             |
+-----------------+----------------+
| 1               | comptabilité   |
+-----------------+----------------+
| 3               | rh             |
+-----------------+----------------+
| 2               | budget         |
+-----------------+----------------+

**analyse**

+------------------------------+----------------+------------------------+
|                              | nb de lignes   | nb valeurs différentes |
+==============================+================+========================+
| situation initiale           | 8              | 5                      |
+------------------------------+----------------+------------------------+
| => dont lignes singletons    | 2              | 2                      |
+------------------------------+----------------+------------------------+
| => dont lignes doublons...   | 6              | 3                      |
+------------------------------+----------------+------------------------+

.. note:: Lecture : le fichier original contient 8 lignes mais uniquement 5 noms de services différents.

          * sur les 5 noms, 2 services ne sont représentés qu'une seule fois (2 des 8 lignes),
          * et 3 services sont répétés plusieurs fois dans le fichier (6 des 8 lignes).

L'identifiant du groupe de doublon est ajouté en première colonne du tableau.

approche 3.1 : numéroter les occurrences
-----------------------------------------

La transformation **AGRÉGATS -> numéroter les occurrences** numérote chaque occurrence :

  * valeur rencontrée pour la première fois : 1
  * valeur rencontrée pour la seconde fois : 2
  * etc.

La numérotation est ascendante ou descendante.

**exemple**

Numérotation ascendante.

+---------------+----------------+
| num:Service   |  Service       |
+===============+================+
| 1             | comptabilité   |
+---------------+----------------+
| 1             | budget         |
+---------------+----------------+
| 1             | juridique      |
+---------------+----------------+
| 1             | informatique   |
+---------------+----------------+
| 1             | rh             |
+---------------+----------------+
| 2             | comptabilité   |
+---------------+----------------+
| 2             | rh             |
+---------------+----------------+
| 2             | budget         |
+---------------+----------------+

Le numéro est ajouté en première colonne.


approche 3.2 : réduire les occurrences multiples
-------------------------------------------------

.. hint:: En reprenant le résultat de l'exemple précédent, nous pouvons désormais nous concentrer sur les doublons.

Pour cela, nous appliquerons la transformation **FILTRES -> retenir les lignes...**.

Cette transformation présente quatre règles :

  * retenir les lignes situées juste avant la valeur indiquée
  * celles situées juste après la valeur indiquée
  * celles qui contiennent la valeur indiquée
  * celles qui ne contiennent pas la valeur indiquée

Dans l'exemple de l'exercice précédent, filtrons les lignes contenant ``1`` pour obtenir le résultat ci-dessous.

+-----------------+----------------------------+
| num:Service     |  Service                   |
+=================+============================+
| 1               | comptabilité               |
+-----------------+----------------------------+
| 1               | budget                     |
+-----------------+----------------------------+
| 1               | juridique                  |
+-----------------+----------------------------+
| 1               | informatique               |
+-----------------+----------------------------+
| 1               | rh                         |
+-----------------+----------------------------+

La liste obtenue est plus courte car elle ne contient qu'une seule fois le nom du service.


.. tip:: Pour obtenir la dernière occurrence de chaque valeur répétée, utilisez une numérotation descendante à l'étape précédente.


approche 4 : série
-------------------

Dans le cas où les données sont triées sur la colonne à dédoublonner --- et uniquement dans ce cas ---
la transformation **FILTRES -> première ou dernière occurrence de chaque série** ne conservera qu'une seule occurrence de chaque donnée.

.. note:: Annulons l'étape précédente et modifions le fichier de données sous un éditeur de texte.

Chaque **série continue** est ainsi réduite à un seul élément.


approche 5 : compacter
-----------------------

Autre situation, le jeu de données peut représenter des enregistrements pour lesquels nous souhaitons conserver qu'une seule occurrence, agrégeant les autres champs/dimensions.

La transformation **LIGNES -> compacter les lignes (agrégats : calculs, listes)** attend comme paramètre une colonne identifiante.

.. warning:: les autres dimensions seront réduites **aux valeurs de la première occurrence**.


::

    paramètres

    clé     : (colonne) Services
    agréger : (colonne) Contact : décompte
    agréger : (colonne) Nb mois effectués : liste + séparateur `,`

+---------+--------------+------------------------------+
| Contact | Service      | Nb mois effectués            |
+=========+==============+==============================+
| 2       | comptabilité | 2009=6,2010=4                |
+---------+--------------+------------------------------+
| 2       | budget       | 2010=2,2011=1,2010=11;2011=2 |
+---------+--------------+------------------------------+
| 1       | juridique    | 2010=3,2011=2                |
+---------+--------------+------------------------------+
| 1       | informatique |                              |
+---------+--------------+------------------------------+
| 2       | rh           | 2010=12,2011=3,2009=2        |
+---------+--------------+------------------------------+
