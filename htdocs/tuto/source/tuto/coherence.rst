vérifier la cohérence entre 2 colonnes
=======================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------

.. note:: Un **dictionnaire** est une table de correspondance entre un code et un libellé.


contexte
---------

Préalable à l'exploitation du jeu de données, la vérification de certaines colonnes, notamment lorsqu'elles décrivent la même information sous la forme d'un code et d'un libellé associé est indispensable.


approche exploratoire
----------------------

L'approche exploratoire consiste à identifier les anomalies et à affiner en conservant la possibilité d'étudier les données des autres colonnes. 

Pour cela, nous utiliserons **AGRÉGATS -> explorer les données** en deux temps :

  1. nous vérifierons qu'à chaque libellé correspond *au plus* 1 code
  2. nous vérifierons qu'à chaque code correspond *au plus* 1 libellé


**jeu de données**

+-------+------------+
| code  |  libellé   |
+=======+============+
| AAB   |   1984     |
+-------+------------+
| AAA   |   1983     |
+-------+------------+
| AAB   |   1983     |
+-------+------------+



**paramètres du test : au plus 1 code**

::

    compter une seule fois  : (colonne) code
    selon les axes suivants : (colonne) libellé
                            : (colonne) code

En triant par ordre décroissant le nombre d'occurrence trouvée (la colonne *ratio*) nous trouvons le nombre de codes différents associé à chaque libellé :

+------+----+
|  x   | nb |
+======+====+
| 1983 | 2  |
+------+----+
| 1984 | 1  |
+------+----+

en cliquant sur la ligne ``1983 | 2``, nous retrouvons bien les 2 codes différents pour le même libellé *1983*.

+-----+----+
|  x  | nb |
+=====+====+
| AAA | 1  |
+-----+----+
| AAB | 1  |
+-----+----+


**paramètres du test : au plus 1 libellé**

::

    compter une seule fois  : (colonne) libellé
    selon les axes suivants : (colonne) code
                            : (colonne) libellé


.. note:: En effectuant les deux tests, nous pouvons également vérifier que pour tout code il y a *au moins* une valeur significative (non-vide).

.. tip:: La plus-value de cette approche est de permettre à chaque instant d'extraire toutes les colonnes pour le jeu de données observé.


approche rapide pour des jeux de données de faible taille
----------------------------------------------------------

Pour des petits jeux de données, la fonction d'analyse **AGRÉGATS -> matrice d'adjacence** permet d'obtenir une représentation des combinaisons de codes et de libellés.


+--------+---------+---------+--------+----------+
| code   |   1983  |   1984  | TOTAUX | ratio    |
+========+=========+=========+========+==========+
| AAB    |    1    |    1    |   2    | 66.67 %  |
+--------+---------+---------+--------+----------+
| AAA    |    1    |         |   1    | 33.33 %  |
+--------+---------+---------+--------+----------+
| TOTAUX |    2    |    1    |   3    |          |
+--------+---------+---------+--------+----------+
| ratio  | 66.67 % | 33.33 % |        | 100.00 % |
+--------+---------+---------+--------+----------+

Ce tableau de synthèse est triable sur les colonnes des libellés *1983* et *1984*.

En triant une par une chaque colonne, il est facile de faire apparaître en première ligne la clé attendue (*1984* -> *AAB*) et rien dans les autres lignes.

En cas d'anomalie, les premières lignes indiqueront les clés en erreurs ; colonne *1983* -> lignes *AAB* et *AAA* sont incompatibles.


approche rapide pour des jeux de données de grande taille
----------------------------------------------------------

Lorsque le nombre de combinaison rend inconfortable la lecture de la matrice d'adjacence inconfortable, une autre solution consiste à extraire directement le dictionnaire de clés/valeurs.

Pour cela, la fonction d'analyse **AGRÉGATS -> dictionnaire** produirait le résultat suivant :

+-------+--------+
| code  | nombre |
+=======+========+
| AAB   |   2    |
+-------+--------+
| AAA   |   1    |
+-------+--------+

En triant la colonne *nombre*, les anomalies apparaîtront en haut du tableau.
