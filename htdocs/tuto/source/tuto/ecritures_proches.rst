détecter des écritures proches
===============================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


calculer la distance d'édition
-------------------------------

.. note:: La distance de Levenshtein --- ou distance d'édition --- correspond au nombre d'ajout,
    de suppression ou de modification de caractère sur une suite/chaîne de caractère pour obtenir une autre chaîne.

**COLONNES : COMPARAISON -> calculer la distance d'édition / lignes**

| |paramètres distance d'édition|

..

Lorsque plusieurs correspondances sont trouvées, elles sont listées et séparées par le séparateur de retour (ici ``,``).

| |résultats distance d'édition|

..

Ici, deux correspondances de *Gabriel(le) LEMARQUIS* nous permettent de détecter une possible erreur de saisie.

L'administrateur fonctionnel pourra décider de fusionner les enregistrements s'il s'agit de deux enregistrements désignant un même individu.

.. important:: la distance d'édition est efficace lorsque le seuil est faible par rapport à la longueur des mots évalués.

.. |paramètres distance d'édition| image:: /_static/images/8/1.png
                          :scale: 100%

.. |résultats distance d'édition| image:: /_static/images/8/2.png
                          :scale: 100%
