Importer/exporter
==================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


Charger le fichier de données d'exercice
-----------------------------------------

  1. Sélectionnez *menu gauche -> charger des données* puis **importer un fichier texte ou CSV**.

  | |chargement d'un fichier 1/3|

..

  2. Sélectionnez le fichier à charger en parcourant les répertoires ou par glisser-déposer.

  | |chargement d'un fichier 2/3|

.. note:: Le séparateur de colonne est déduit automatiquement à partir de la ligne d'en-tête.
..

  3. Pour ce tutoriel, ne touchez pas aux options et validez. Le chargement est instantané, l'écran 
     ne change pas,  mais Illico affiche les dimensions du fichier CSV et un bouton d'annulation sous 
     *le bandeau supérieur*. Vous pouvez vérifier que le fichier a été chargé avec *bandeau supérieur ->* 
     **journal de bord** (il s'ouvre dans un nouvel onglet).

     Le chargement est

        * enregistré dans le journal de bord situé dans le bandeau supérieur
        * annulable


.. tip:: Le nombre de colonnes et de lignes de données sont indiquées sous le bandeau supérieur.

  | |message résultat|

..

  4. Visualisez avec *bandeau supérieur ->* **tableau** (s'ouvre dans un nouvel onglet)

  | |accents 1/2|

..

  5. Les caractères bizarres viennent du choix d'encodage du fichier. Vous pouvez le modifier en revenant 
     sur la page d'Illico. Choisissez le menu *encodage ->* **UTF-8 / International**, validez (le fichier 
     est rechargé) puis visualisez de nouveau le résultat : les accents s'affichent correctement (le 
     fichier d'exemple est encodé en UTF-8).

  | |accents 2/2|

..

  6. De même, modifiez le **séparateur de colonne** et observez le résultat.

Uniformiser les données
-----------------------

Vous avez pu constater que votre liste du personnel n'est pas uniforme : l'orthographe et la typographie 
varient. Si ça ne vous a pas sauté aux yeux, réouvrez un **tableau** et observez bien. Avec des gros 
fichiers on rate facilement ces défauts. Or pour bien analyser les données il faut les uniformiser. 
Illico vous aide à trouver les défauts et à les corriger avec des fonctions d'analyse rapide, de recherche 
de similarité (distance d'édition, dédoublonnage), de recherche de motifs (expressions rationnelles ou 
*regex*) et de corrections simples ou complexes.

Ces transformations sont présentées dans les tutoriels des chapitres suivants :

  * `analyser <../tuto/analyser.html>`__
  * `appliquer une règle métier <../tuto/regle_metier.html>`__
  * `écritures proches <../tuto/ecritures_proches.html>`__
  * `traiter les doublons <../tuto/doublons.html>`__


Exporter les données transformées
----------------------------------

Le bandeau supérieur contient quatre formats d'export :

  * **export** : fichier à télécharger, export de type CSV, à charger dans une autre application ou dans un éditeur de texte [1]_,
  * **aperçu CSV** : un nouvel onglet s'ouvre avec le contenu du fichier CSV
  * **tableau** : un nouvel onglet s'ouvre avec une mise en forme des données, compatible par copier-coller avec des tableurs [2]_,
  * **texte** : un nouvel onglet s'ouvre avec un texte qui utilise des tabulations pour représenter visuellement les colonnes [3]_.


.. [1] Notepad++, Scite, Geany...
.. [2] Excel, LibreOffice...
.. [3] tableau machine à écrire...


.. note:: Pour l'export au format *tableau*, l'interprétation de la mise en forme varie selon le navigateur et le logiciel en destination du copier-coller.
          Certains logiciels ne récupèrent pas toute la mise en forme.


.. |chargement d'un fichier 1/3| image:: /_static/images/1/1.png
                                 :scale: 100%

.. |chargement d'un fichier 2/3| image:: /_static/images/1/2.png
                                 :scale: 100%

.. |message résultat| image:: /_static/images/1/0.png
                                 :scale: 100%

.. |accents 1/2| image:: /_static/images/1/3.png
                                 :scale: 100%

.. |accents 2/2| image:: /_static/images/1/4.png
                                 :scale: 100%

.. |export HTML| image:: /_static/images/9/1.png
                                 :scale: 100%

.. |export csv| image:: /_static/images/9/2.png
                                 :scale: 100%

.. |export texte| image:: /_static/images/9/3.png
                                 :scale: 100%
