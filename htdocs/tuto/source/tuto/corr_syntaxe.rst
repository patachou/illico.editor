Corriger la syntaxe
====================

.. contents:: Contenu
   :local:
   :backlinks: top

-------

L'observation du fichier et l'analyse rapide ont permis de repérer des problèmes. Il y a plusieurs 
façons de les corriger. Tout le monde connait le chercher/remplacer, c'est assez rapide et par habitude 
c'est ce qu'on commence par faire. Mais tout comme l'analyse est plus efficace que l'observation 
(aviez-vous *vu* le point-virgule ?), il y a des techniques plus fiables.

Voici une méthode pas à pas, avec certains outils d'Illico. On simplifie avant de transformer pour 
uniformiser les données.

D'abord, on modifie la casse des caractères. Ensuite on met les bons termes dans les bonnes cases. Utiliser 
les termes professionnels, les termes du métier, rend les données plus intelligibles. On dit qu'on "applique 
une règle métier". Enfin la détection de doublons et de similarités va aider à rendre les données homogènes.

Modifier la casse des caractères
---------------------------------

Pour uniformiser rapidement le contenu de la colonne Service. 
On pourrait aussi utiliser cet outil sur la colonne Contact où l'on trouve souvent du "désordre 
typographique". On verra plus tard comment séparer d'abord les noms et prénoms.

Trois transformations permettent de corriger la casse des caractères :

CAPITALES
    toutes les lettres sont converties en capitale d'imprimerie
minuscules
    toutes les lettres sont converties en minuscule
Majuscules
    la première lettre --- majuscule --- est en capitale, les autres sont converties en minuscule. Une option permet d'appliquer cette règle à chaque mot ; par exemple, *jean-pierre* deviendra alors *Jean-Pierre*.

Nous utiliserons ici la transformation *Valeurs ->* **minuscules**.

.. note:: Les accents seront également convertis.

+---------------------+--------------+-------------------+
| Contact             | Service      | Nb mois effectués |
+=====================+==============+===================+
| Patrick BRAVO       | compta       | 2009=6            |
+---------------------+--------------+-------------------+
| Sylvia LEDUC        | budget       | 2010=2,2011=1     |
+---------------------+--------------+-------------------+
| Pierre MARCHAND     | juridique    | 2010=3,2011=2     |
+---------------------+--------------+-------------------+
| Thérèse FITZ        | informatique |                   |
+---------------------+--------------+-------------------+
| Gabriel LEMARQUIS   | rh           | 2010=12,2011=3    |
+---------------------+--------------+-------------------+
| Jean-Marc FICHE     | comptabilité | 2010=4            |
+---------------------+--------------+-------------------+
| Gabrielle LEMARQUIS | ``rh``       | 2009=2            |
+---------------------+--------------+-------------------+
| Stéphanie LEPOINT   | ``budget``   | 2010=11;2011=2    |
+---------------------+--------------+-------------------+
