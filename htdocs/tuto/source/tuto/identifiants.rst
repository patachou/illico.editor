identifier des données manquantes
==================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


Nous allons comparer un jeu de données et une liste de référence dans le but d'identifier les données attendues et absentes dans notre jeu de données.



**jeu de données**

+-------+------------+
|  id   | disponible |
+=======+============+
| 200   |    oui     |
+-------+------------+
| 220   |    oui     |
+-------+------------+
| 230   |    oui     |
+-------+------------+
| 240   |    non     |
+-------+------------+
| 250   |    oui     |
+-------+------------+
| 280   |    non     |
+-------+------------+
| 290   |    non     |
+-------+------------+
| 310   |    oui     |
+-------+------------+


constituer un référentiel
--------------------------

Commençons par générer la liste de références attendues avec **CHARGEMENTS DE DONNEES ALTERNATIFS -> générer des identifiants**.

**paramètres**

::

    valeur de départ : 200
    pas              :  10
    limite           : 300

**liste de références attendues (référentiel)**

+--------------+
| identifiants |
+==============+
|     200      |
+--------------+
|     210      |
+--------------+
|     220      |
+--------------+
|     230      |
+--------------+
|     240      |
+--------------+
|     250      |
+--------------+
|     260      |
+--------------+
|     270      |
+--------------+
|     280      |
+--------------+
|     290      |
+--------------+
|     300      |
+--------------+


comparer les références attendues avec le jeu de données
---------------------------------------------------------

La recherche de *différences* entre les deux sources de données s'effectuera avec la transformation
**ENRICHISSEMENT -> fusionner, compléter ou exclure des données par jointure** en utilisant les colonnes d'identifiants comme clé de jointure.

**paramètres**

::

    clé de jointure du fichier maître : id (jeu de données)
    préfixer par                      : `[data] `

    clé de jointure du fichier lookup : identifiants (liste de référence)
    préfixer par                      : `[ref] `
    conserver                         : uniquement les lignes sans correspondances


Le résultat renverra :

* **les données manquantes** : la liste des références attendues mais absentes de notre jeu de données
* **les données en trop** : les références de notre jeu de données qui n'existent pas dans le référentiel

Ces deux ensembles peuvent être obtenues en modifiant la règle de fusion :

::

    conserver                         : les lignes du fichier maître absentes du lookup

    ou

    conserver                         : les lignes du lookup absentes du fichier maître
