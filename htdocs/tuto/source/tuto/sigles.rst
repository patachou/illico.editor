créer des sigles
=================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


réduire l'information (initiales)
----------------------------------

.. note:: Pour cet exemple, le fichier source a été modifié pour avoir des noms composés d'une particule (« LE MARQUIS »)

Créer des sigles consiste à retenir les premières lettres de chaque mot.

En utilisant une expression rationnelle, il est possible de détecter des limites de mots avec le motif ``\b``.
Cependant un mot composé de deux parties séparées par un tiret sera considéré comme un seul et même mot.

La solution consiste à utiliser la fonction **REMPLACER... SI... -> définir des valeurs conditionnelles (regex)** en *redéfinissant* ce que nous considérons comme une limite de mot.

**paramètres**

::

    si (colonne) Contact : ^(\S)[^ \-]*(..)[^ \-]*(..)?[^ \-]*(..)?[^ \-]*(..)?.*$

      sensible à la casse : oui

    alors (colonne) Contact : $1.$2.$3.$4.$5.

.. note :: Ici, nous considérons que les sigles possèderont au plus 5 initiales.


Décomposition de l'expression

::

    ^
      (\S)[^ \-]*
        (..)[^ \-]*
        (..)?[^ \-]*
        (..)?[^ \-]*
      (..)?.*
    $

+------------------+-----------+----------------------------------------------------------------------------------------------------+
| motif            | variable  | description                                                                                        |
+==================+===========+====================================================================================================+
| ``^``            |           | correspond au début de la chaîne (premier caractère à gauche)                                      |
+------------------+-----------+----------------------------------------------------------------------------------------------------+
| ``(\S)[^ \-]*``  | ``$1``    | première lettre du premier mot, ``[^ -]\*`` le reste est ignoré jusqu'au prochain espace ou tiret  |
+------------------+-----------+----------------------------------------------------------------------------------------------------+
| ``(..)[^ \-]*``  | ``$2``    | ``(..)`` : le premier ``.`` chope l'espace ou le tiret, le second la première lettre du second mot |
+------------------+-----------+----------------------------------------------------------------------------------------------------+
| ``(..)?[^ \-]*`` | ``$3``    | ``(..)?`` : le ``?`` rend cette condition facultative                                              |
|                  |           | (lorsque prénom et nom comportent au moins 3 mots)                                                 |
+------------------+-----------+----------------------------------------------------------------------------------------------------+
| ``(..)?[^ \-]*`` | ``$4``    | ``(..)?`` : le ``?`` prend son sens lorsque prénom et nom comportent au moins 4 mots               |
+------------------+-----------+----------------------------------------------------------------------------------------------------+
| ``(..)?.*``      | ``$5``    | ``.*`` : comme il s'agit du dernier mot, l'expression ``[^ \-]*`` est simplifiée en ``.*``         |
+------------------+-----------+----------------------------------------------------------------------------------------------------+
| ``$``            |           | correspond à la fin de la chaîne (dernier caractère à droite)                                      |
+------------------+-----------+----------------------------------------------------------------------------------------------------+

**résultat**

+-------------------------+--------------------------------+
| contact (nom complet)   | contact (initiales uniquement) |
+=========================+================================+
| Patrick BRAVO           | P\. B\.\.\.\.                  |
+-------------------------+--------------------------------+
| Sylvia LEDUC            | S\. L\.\.\.\.                  |
+-------------------------+--------------------------------+
| Pierre MARCHAND         | P\. M\.\.\.\.                  |
+-------------------------+--------------------------------+
| Thérèse FITZ            | T\. F\.\.\.\.                  |
+-------------------------+--------------------------------+
| Gabriel LE MARQUIS      | G\. L\. M.\.\.                 |
+-------------------------+--------------------------------+
| Jean-Marc FICHE         | J\.-M\. F.\.\.                 |
+-------------------------+--------------------------------+
| Gabrielle LE MARQUIS    | G\. L\. M.\.\.                 |
+-------------------------+--------------------------------+
| Stéphanie LEPOINT       | S\. L\.\.\.\.                  |
+-------------------------+--------------------------------+

L'exercice suivant permet de réduire les points.

réduire les répétitions d'un même caractère
--------------------------------------------

Plusieurs solutions permettent de réduire les points répétés à une seule occurrence.

  * par une expression rationnelle
  * en considérant le résultat obtenu comme une liste


par une expression rationnelle
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. hint:: Reprenez le résultat obtenu à l'étape précédente.

Utilisez maintenant la fonction **REMPLACER... SI... -> définir des valeurs conditionnelles (regex)**.

**paramètres**

::

    si (colonne) Contact : (\.{2,})

      sensible à la casse : oui

    alors (colonne) Contact : .


+--------------+---------------------------------------------------------------------------------------+
| motif        | description                                                                           |
+==============+=======================================================================================+
| ``(\.{2,})`` | sélectionne une chaîne constituée d'au moins 2 points (``\.``)                        |
+--------------+---------------------------------------------------------------------------------------+
| ``.``        | remplacer par un unique point (``.``)                                                 |
+--------------+---------------------------------------------------------------------------------------+

**résultat**

+-------------------------+-----------------------------------+
| contact (nom complet)   |  contact (initiales uniquement)   |
+=========================+===================================+
| Patrick BRAVO           | P\. B\.                           |
+-------------------------+-----------------------------------+
| Sylvia LEDUC            | S\. L\.                           |
+-------------------------+-----------------------------------+
| Pierre MARCHAND         | P\. M\.                           |
+-------------------------+-----------------------------------+
| Thérèse FITZ            | T\. F\.                           |
+-------------------------+-----------------------------------+
| Gabriel LE MARQUIS      | G\. L\. M\.                       |
+-------------------------+-----------------------------------+
| Jean-Marc FICHE         | J\.-M\. F\.                       |
+-------------------------+-----------------------------------+
| Gabrielle LE MARQUIS    | G\. L\. M\.                       |
+-------------------------+-----------------------------------+
| Stéphanie LEPOINT       | S\. L\.                           |
+-------------------------+-----------------------------------+


en considérant le résultat obtenu comme une liste
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. hint:: Reprenez le résultat obtenu à l'étape précédente.

Utilisez maintenant la fonction **VALEURS EN LISTE -> enlever une des valeurs**.

**paramètres**

::

    séparateur : .

    valeur à effacer : (laissez vide)

Le résultat sera identique à l'alternative avec une expression rationnelle.

