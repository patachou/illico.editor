Avant de commencer
===================

Liens
------

La dernière version **stable** est disponible sous la forme d'une archive compressée
qui contient l'ensemble de cette documentation, les tutoriels ainsi que les sources.

-  archives complètes :
   `zip <http://illico.ti-nuage.fr/releases/illicoEditor_latest.zip>`__ /
   `tgz <http://illico.ti-nuage.fr/releases/illicoEditor_latest.tar.gz>`__
-  en ligne :
   `site Web <http://illico.ti-nuage.fr>`__ /
   `accès Illico <http://illico.ti-nuage.fr/illicoEditor.html>`__
   (les données restent sur votre poste)
-  tutoriels :
   `format PDF <../latex/IllicoEditor_tutoriels.pdf>`__


Prise en main
--------------

Jeu de données d'exercice
~~~~~~~~~~~~~~~~~~~~~~~~~~

Le jeu de données utilisé dans les tutoriels est disponible `en local <../html/_static/exemple/liste_du_personnel.csv>`__ (dans le répertoire *exemple*) et `en ligne <http://illico.ti-nuage.fr/tuto/build/html/_static/exemple/liste_du_personnel.csv>`__.
Il contient une courte liste de personnes ayant effectué plusieurs prestations de service pour 
votre entreprise. Votre but est d'analyser ce petit fichier pour vérifier le temps de travail 
effectué par chaque personne sur chaque type d'activité. Vous faites habituellement cela avec 
un tableur et des feuilles de calcul. Mais le manque de cohérence des données rend l'analyse lourde 
et pénible. Ce court tutoriel montre comment Illico facilite ce travail avec une suite d'opérations 
simples.


Interface graphique
~~~~~~~~~~~~~~~~~~~~

Le **bandeau supérieur** regroupe le journal de bord, les aperçus et exports de données.

  | |bandeau supérieur|


Le **menu gauche** regroupe les fonctionnalités de chargement, transformation et les préférences utilisateur.

  | |menu gauche 1/3|

  | |menu gauche 2/3|

  | |menu gauche 3/3|

.. |bandeau supérieur| image:: /_static/images/0/1.png
                                        :scale: 100%

.. |menu gauche 1/3| image:: /_static/images/0/2.png
                                        :scale: 100%

.. |menu gauche 2/3| image:: /_static/images/0/3.png
                                        :scale: 100%

.. |menu gauche 3/3| image:: /_static/images/0/4.png
                                        :scale: 100%

