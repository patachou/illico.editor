Tutoriels
==========


Introduction
-------------

.. toctree::
   :maxdepth: 1

   intro


Cas concrets
-------------

.. toctree::
   :maxdepth: 1
   :numbered: 1

   intro
   tuto/import_export
   tuto/analyser
   tuto/corr_syntaxe
   tuto/regle_metier
   tuto/ecritures_proches
   tuto/structure
   tuto/doublons
   tuto/generer
   tuto/sigles
   tuto/coherence
   exemples/adresses
   exemples/cartesien
   exemples/ecritures_inversees
   tuto/identifiants
   exemples/structure_0
   exemples/structure_1
   exemples/structure_2
   exemples/filiation
   exemples/planificateur
   exemples/periodes
   exemples/comparaison_colonnes
   exemples/comparaison_lignes_cumul
   exemples/cumul_formats_proches
   exemples/reconstituer_calendriers
   exemples/fidelisation
