/**
 * @license
 * Illico Editor - data quality Swiss Army knife
 * Copyright (C) 2025 Arnaud COCHE < i l l i c o . e d i t o r /// n e t - c . f r >
 *
 * This program is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the GNU General Public License for more details.
 */

/*jslint plusplus: true, continue: true, regexp: true, browser: true, devel: true, indent: 4, esversion: 6 */


/**
 * @function sortTable
 * tri le contenu d'une table HTML
 * @param {string} id - identifiant d'un objet HTML TABLE contenant les tableaux de synthèse
 * @param {number}  c - indice de colonne sur laquelle s'effectue le tri
 * @param {boolean} o - tri ascendant (TRUE) ou tri descendant (FALSE)
 * @param {string} ft - format : texte (t) ou numérique (n)
 * FIXME : changer les valeurs du format ft : (t) en (texte) et (n) en (numérique)
 */
function sortTable(id, c, o, ft) {
    "use strict";
    var e = document.getElementById(id),
        tr = e.getElementsByTagName('tr'),
        l = tr.length,
        k = [], // index : valeur -> [indices de colonnes]
        ks = [], // index à trier
        a = [],
        v = '',
        i = 0;

    // si le tableau contient des chaînes de texte représentant des numériques
    // alors st2float assure
    // - qu'espace et espaces insécables sont ignorées
    // - les caractères situés après le nombre (par exemple, l'unité) sont ignorés
    function f(a, b) { return st2float(a) - st2float(b); }

    // création de l'arborescence des index pour le tri
    for (i = 0; i < l; i++) {
        v = tr[i].getElementsByTagName('td')[c].innerHTML;
        if (! k.hasOwnProperty(v) ) {
            k[v] = [];
            ks.push(v);
        }
        k[v].push(i);
    }

    // selon les options, le tri demandé
    switch (ft) {
    case 'n':
        if (o) { ks.sort(f); }
          else { ks.sort(f).reverse(); }
        break;
    case 't':
        /* falls through */
    default:
        if (o) { ks.sort(); }
          else { ks.sort().reverse(); }
        break;
    }

    // on récupère tous les noeuds enfants (il s'agit des lignes)
    for (i = 0; i < l; i++) { a[a.length] = e.removeChild(tr[0]); }

    // on remet en place toutes les lignes, dans l'ordre attendu
    for (i = 0; i < ks.length; i++) {
        while (k[ks[i]].length > 0) {
            v = k[ks[i]].shift();
            e.appendChild(a[v]);
        }
    }
}
