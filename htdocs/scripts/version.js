/**
 * @license
 * Illico Editor - data quality Swiss Army knife
 * Copyright (C) 2025 Arnaud COCHE < i l l i c o . e d i t o r /// n e t - c . f r >
 *
 * This program is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the GNU General Public License for more details.
 */

/**
 * @file numéro de la Version Disponible (ou Version Distante) sur le site Web officiel
 *
 * en local, à l'ouverture de la page index.html,
 * le JS appelle ce fichier sur le site officiel et affecte la valeur à vd.
 * Ce numéro de version est ensuite comparé à la variable locale vc (Version Courante)
 */
vd = "25.03-2";
