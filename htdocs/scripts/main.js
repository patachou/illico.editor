/**
 * @license
 * Illico Editor - data quality Swiss Army knife
 * Copyright (C) 2025 Arnaud COCHE < i l l i c o . e d i t o r /// n e t - c . f r >
 *
 * This program is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the GNU General Public License for more details.
 */

/**
 * @file script principal
 */

/*jslint browser: true, continue: true, devel: true, esversion: 6, indent: 4, laxbreak: true, multistr: true, plusplus: true, regexp: true */
/*global Blob, URL, css_msg, css_histo, css_export, sortTable */

/*
  conventions
  ------------------------------------------------------------------------------

  conventions générales

  -> nom des variables           :   1 usage = 1 nom (cf. documentation, section architecture)
  -> JSLint/JSHint               :   objectif moins de 10 erreurs

  objectif : se rapprocher de

  - http://snowdream.github.io/javascript-style-guide/javascript-style-guide/fr/

  et si la situation le permet, on s'inspirera des bonnes pratiques

  - http://eloquentjavascript.net/index.html
*/

/*
  dette technique
  ------------------------------------------------------------------------------

  Integer.indexOf et String.indexOf ? (remplacer Accents/Caractères)
*/

/*
  pour mémo
  ------------------------------------------------------------------------------

  1. array.slice   => ne modifie pas l'original / retourne une portion de l'original

  2. array.splice  => modifie l'original (ajout/insertion) / retourne les éléments supprimés

  3. HTML : https://css-tricks.com/complete-guide-table-element/

  4. JS : destructuring assignment

  - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment
  - http://codeisbae.com/nats-notes-destructuring-assignment/
  - https://scottwhittaker.net/es6/2016/08/30/destructuring.html
  - https://hacks.mozilla.org/2015/05/es6-in-depth-destructuring/
*/


///////////////////////////////////////////////////////////////////////////////
//                       BASE : FONCTIONS GÉNÉRALISTES                       //
///////////////////////////////////////////////////////////////////////////////


// ........................................................ constantes globales


// pour l'IHM
const A_O__AVEC_ACCENT = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöø';
const A_O__SANS_ACCENT = 'AAAAAAaaaaaaOOOOOOOoooooo';
const E_Z__AVEC_ACCENT = 'ÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
const E_Z__SANS_ACCENT = 'EEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz';
// pour les traitements
const A_Z__AVEC_ACCENT = A_O__AVEC_ACCENT + E_Z__AVEC_ACCENT;
const A_Z__SANS_ACCENT = A_O__SANS_ACCENT + E_Z__SANS_ACCENT;
const PONCTUATION      = '! "#%\'()*+,-./:;<=>?@\\[]^_`{|}~§«°»–—’“„…&';
const PONCTUATION_COMP = PONCTUATION + '\u00a0'; // + espace insécable
const UN_JOUR_EN_MS    = 1000 * 60 * 60 * 24; /* en millisecondes */


// .......................................................... JS Date surcharge


/**
 * @function Date.prototype.today
 * @returns {string} partie "yyyy-mm-jj" (temps local) d'un horodatage
 * @see     {@link horodatage}
 */
Date.prototype.today = function () {
    "use strict";
    var d = this.getDate(),  // entre 1 et 31
        m = this.getMonth(), // entre 0 et 11
        y = this.getFullYear();

    return                                y    + "-"
         + ((m + 1 < 10) ? "0" : "") + (m + 1) + "-"
         + (   d   < 10  ? "0" : "") +    d    ;
};

/**
 * @function Date.prototype.UTCtoday
 * @returns {string} partie "yyyy-mm-jj" (temps UTC) d'un horodatage
 * @see     {@link trierDate}
 */
Date.prototype.UTCtoday = function () {
    "use strict";
    var d = this.getUTCDate(),  // entre 1 et 31
        m = this.getUTCMonth(), // entre 0 et 11
        y = this.getUTCFullYear();

    return                                y    + "-"
         + ((m + 1 < 10) ? "0" : "") + (m + 1) + "-"
         + (   d   < 10  ? "0" : "") +    d    ;
};

/**
 * @function Date.prototype.timeNow
 * @returns {string} partie "hh:mm:ss" d'un horodatage
 * @see     {@link horodatage}
 */
Date.prototype.timeNow = function () {
    "use strict";
    var h = this.getHours(),
        m = this.getMinutes(),
        s = this.getSeconds();

    return (h < 10 ? "0" : "") + h
         + (m < 10 ? "0" : "") + m
         + (s < 10 ? "0" : "") + s;
};

/**
 * raccourci pour obtenir un horodatage complet
 * utilisé pour le nom par défaut de l'export CSV
 * @returns {string} horodatage composé des parties "yyyy-mm-jj" et "hh:mm:ss"
 * @see     {@link Date.prototype.today}
 * @see     {@link Date.prototype.timeNow}
 */
function horodatage() {
    "use strict";
    var d = new Date();
    return d.today() + '_' + d.timeNow();
}


// ....................................................................... Date


/**
 * détermine la fonction/algorithme de conversion d'une chaîne décrivant une date au format f
 * vers une chaîne décrivant une date au format SSAA-MM-JJ
 * @params  {string} o - format (jj mm ssaa), (mm jj ssaa), (ssaa mm jj)
 * @returns {function} function/algorithme de conversion vers le format SSAA-MM-JJ
 */
function convertirDate(o) {
    "use strict";

    if (o === 'jj mm ssaa') {
      return function(x) { return x.replace(/(\.|\/| )/g,'-').replace(/^(..)-(..)-(....)$/,'$3-$2-$1'); };
    }

    if (o === 'mm jj ssaa') {
      return function(x) { return x.replace(/(\.|\/| )/g,'-').replace(/^(..)-(..)-(....)$/,'$3-$1-$2'); };
    }

    if (o === 'ssaa mm jj') {
      return function(x) { return x.replace(/(\.|\/| )/g,'-'); };
    }
} // fin convertirDate

/**
 * vérifie si l'intervalle ouvert [d1, d2[ est correct
 * et calcule le nombre de jours de l'intervalle (dernier jour exclu)
 * @params  {date}     d1 - début
 * @params  {date}     d2 - fin
 * @returns {string/number} (intervalle inversé), (intervalle trop court), nombre de jours de l'intervalle
 * @see     {@link UN_JOUR_EN_MS}
 */
function nbJoursIntervalleDernierJourExclu(d1, d2) {
    "use strict";

    if(d2.getTime()  <  d1.getTime() ) { return 'intervalle inversé'; }
    // else
    if(d2.getTime() === d1.getTime() ) { return 'intervalle trop court'; }
    // cas général
    return Math.ceil( (d2.getTime() - d1.getTime() ) / UN_JOUR_EN_MS);
} // fin nbJoursIntervalleDernierJourExclu

/**
 * vérifie si l'intervalle fermé [d1, d2] est correct
 * et calcule le nombre de jours de l'intervalle (dernier jour inclus)
 * @params  {date}     d1 - début
 * @params  {date}     d2 - fin
 * @returns {string/number} (intervalle inversé), nombre de jours de l'intervalle
 * @see     {@link UN_JOUR_EN_MS}
 */
function nbJoursIntervalleDernierJourInclus(d1, d2) {
    "use strict";

    if(d2.getTime() < d1.getTime() ) { return 'intervalle inversé'; }
    // else
    return Math.ceil( (d2.getTime() - d1.getTime() ) / UN_JOUR_EN_MS) +
          1;
          /*(Math.sign(d2.getTime() - d1.getTime() ) || 1); // retire ou ajoute 1 jour (pour la borne) */
} // fin nbJoursIntervalleDernierJourInclus


// ............................................................... HTML Element


/**
 * raccourci pour document.getElementById()
 * @param   {string} x - id élément HTML
 * @returns {Object} élément HTML recherché
 */
function dgebi(x) {
    "use strict";
    return document.getElementById(x);
}

/**
 * raccourci pour document.getElementById().value
 * @param   {string} x - id élément HTML
 * @returns {string} valeur de l'élément recherché
 * @see     {@link dgebi}
 * @see     {@link vide}
 */
function valeur(x) {
    "use strict";
    return dgebi(x).value;
}

/**
 * raccourci pour document.getElementById().options[this.selectedIndex]
 * @param   {string} x - id élément HTML
 * @return  {string} valeur sélectionnée
 * @see     {@link dgebi}
 */
function selection(x) {
    "use strict";
    var e = dgebi(x);

    return e.options[e.selectedIndex].value; // .text pour obtenir le libellé
}

/**
 * raccourci pour document.getElementById().value = ''
 * @param   {string} x - id élément HTML
 * @see     {@link dgebi}
 * @see     {@link valeur}
 */
function vide(x) {
    "use strict";
    dgebi(x).value = '';
}

/**
 * active un élément HTML
 * @param   {string} x - id élément HTML
 * @see     {@link dgebi}
 * @see     {@link desactiver}
 */
function activer(x) {
    "use strict";
    dgebi(x).disabled = false;
}

/**
 * active un élément HTML
 * @param   {string} x - id élément HTML
 * @see     {@link dgebi}
 * @see     {@link activer}
 */
function desactiver(x) {
    "use strict";
    dgebi(x).disabled = true;
}

/**
 * affiche un élément HTML
 * @param   {string} x - id élément HTML
 * @see     {@link dgebi}
 * @see     {@link masquer}
 * @see     {@link switchX}
 */
function afficher(x) {
    "use strict";
    dgebi(x).style.display = 'inline';
}

/**
 * masque un élément HTML
 * @param   {string} x - id élément HTML
 * @see     {@link dgebi}
 * @see     {@link afficher}
 * @see     {@link switchX}
 */
function masquer(x) {
    "use strict";
    dgebi(x).style.display = 'none';
}

/**
 * affiche|masque un élément HTML selon son état
 * @param   {string} x - id élément HTML
 * @see     {@link dgebi}
 * @see     {@link afficher}
 * @see     {@link masquer}
 */
function switchX(x) {
    "use strict";
    dgebi(x).style.display = dgebi(x).style.display === 'inline' ? 'none' : 'inline';
}

/**
 * copie le innerHTML d'un élément HTML dans un autre
 * @param   {string} x - id élément HTML cible
 * @param   {string} y - id élément HTML source (innerHTML à copier)
 * @see     {@link dgebi}
 * @see     {@link remplacerInnerHTML}
 */
function clonerInnerHTML(x, y) {
    "use strict";
    dgebi(x).innerHTML = dgebi(y).innerHTML;
}

/**
 * remplace le innerHTML d'un élément HTML par une valeur
 * @param   {string} x - id élément HTML cible
 * @param   {string} v - innerHTML
 * @see     {@link dgebi}
 * @see     {@link clonerInnerHTML}
 */
function remplacerInnerHTML(x, v) {
    "use strict";
    dgebi(x).innerHTML = v;
}

/**
 * supprime les balises modifiant la couleur d'un texte HTML
 * @param   {string} s - texte à modifier
 * @return  {string} texte sans balise <span class="color" style="color:...></span>
 * @see     {@link sansMiseEnForme}
 */
function sansCouleur(s) {
    "use strict";
    //return s.replace(/<span class="color" style="color:.*?">(.*?)<\/span class="color">/ig, "$1");
    return s.replace(/<span class="color" style="color:.*?">|<\/span class="color">/ig, '');
}

/**
 * supprime les balises modifiant la mise en forme d'un texte HTML
 * @param   {string} s - texte à modifier
 * @return  {string} texte sans balise <font color...>, <b>, <i>, etc.
 * @see     {@link sansCouleur}
 */
function sansMiseEnForme(s) {
    "use strict";
    return sansCouleur(s).replace(/<b>|<\/b>|<strong>|<\/strong>/ig, '')
                         .replace(/<i>|<\/i>|<em>|<\/em>/ig, '')
                         .replace(/<del>|<\/del>/ig, '')
                         .replace(/<big>|<\/big>/ig, '')
                         .replace(/<small>|<\/small>/ig, '')
                         .replace(/<span class="bold">|<\/span class="bold">/ig, '')
                         .replace(/<span class="big">|<\/span class="big">/ig, '')
                         .replace(/<span class="italic">|<\/span class="italic">/ig, '')
                         .replace(/<span class="small">|<\/span class="small">/ig, '')
                         .replace(/<span class="strike">|<\/span class="strike">/ig, '');
}


// ......................................................... HTML Table Element


/**
 * raccourci pour rows.length
 * @param   {Object} e - HTML élément
 * @returns {number} nombre de lignes du tableau HTML
 */
function taille(e) {
    "use strict";
    return e.rows.length;
}


// ........................................................ HTML Select Element


/**
 * désélectionne toutes les options d'un Select HTML et sélectionne n° o
 * @param   {string} x - id élément HTML
 * @param   {number} o - option à sélectionner
 */
function selectionnerLOption(x, o) {
    "use strict";
    var e = dgebi(x),
        i = 0;

    if (e.options.length <= o) { return; }

    for (i = 0; i < e.options.length; i++) { e.options[i].selected = false; }

    e.options[o].selected = true;
    e.options.selectedIndex = o;
}

/**
 * supprime toutes les options d'un Select
 * @param   {string} x - id élément HTML Select
 */
function viderOptionsSelect(x) {
    "use strict";
    dgebi(x).options.length = 0;
}

/**
 * permute 2 items (valeur, sélection) dans un Select
 * @param   {string} x - id élément HTML
 * @param   {number} i1 - indice ligne 1
 * @param   {number} i2 - indice ligne 2
 * FIXME : il y a sûrement plus simple en 3 étapes avec un objet pivot (temp) ou swap destructuring assignment ?
 */
function permuterOptions(x, i1, i2) {
    "use strict";
    var o1 = new Option(x[i1].text, x[i1].value),
        o2 = new Option(x[i2].text, x[i2].value);

    o1.selected = x[i1].selected;
    o2.selected = x[i2].selected;

    x[i1] = o2;
    x[i2] = o1;
}

/**
 * descend d'un niveau tous les éléments sélectionnées de la liste x
 * @param   {string} x - id élément HTML Select
 * @see     {@link dgebi}
 * @see     {@link up}
 * @see     {@link toTop}
 * @see     {@link toBottom}
 */
function down(x) {
    "use strict";
    var lo = dgebi(x).options,
        d = dgebi(x).options.length,
        b = lo[d - 1].selected,
        i = 0;

    if (lo.selectedIndex < 0) { return; }

    for (i = d - 2; i >= 0; i--) {
        b = b && lo[i].selected;
        if (!b) {
            if (lo[i].selected) { permuterOptions(lo, i, i + 1); }
            b = false;
        }
    }
}

/**
 * monte d'un niveau tous les éléments sélectionnées de la liste x
 * @param   {string} x - id élément HTML Select
 * @see     {@link dgebi}
 * @see     {@link down}
 * @see     {@link toTop}
 * @see     {@link toBottom}
 */
function up(x) {
    "use strict";
    var lo = dgebi(x).options,
        d = dgebi(x).options.length,
        b = lo.selectedIndex === 0,
        i = 0;

    if (lo.selectedIndex < 0) { return; }

    for (i = 1; i < d; i++) {
        b = b && lo[i].selected;
        if (!b) {
            if (lo[i].selected) { permuterOptions(lo, i - 1, i); }
            b = false;
        }
    }
}

/**
 * @summary remonte en haut de la liste tous les éléments sélectionnés de la liste x
 * @description ALGORITHME
 *  - parcours n°1 : (tous les éléments)
 *      - mémorise chaque élément sélectionné
 *      - recopie chaque élément non-sélectionné en fin de liste
 *  - parcours n°2 : (parmi les éléments mémorisés)
 *      - remplace les éléments en début de liste par les éléments sélectionnés
 *  - supprime les valeurs "au milieu" reliquat des précédentes valeurs de la liste
 * @param   {string} x - id élément HTML Select
 * @see     {@link dgebi}
 * @see     {@link up}
 * @see     {@link down}
 * @see     {@link toBottom}
 */
function toTop(x) {
    "use strict";
    var lo = dgebi(x).options,
        d = dgebi(x).options.length,
        i = 0,
        j = 0,
        a = [];

    // parcours n°1
    for (i = 0; i < d; i++) {
        if (lo[i].selected) {
            a[a.length] = {
                text:  lo[i].text,
                value: lo[i].value
            };
        } else {
            lo[lo.length] = new Option(lo[i].text, lo[i].value);
        }
    }

    // parcours n°2
    for (j = 0; j < a.length; j++) {
        lo[j] = new Option(a[j].text, a[j].value);
        lo[j].selected = true;
    }

    // suppression des valeurs incohérences (reliquat)
    for (i = j; i < d; i++) { lo[j] = null; }
} // fin toTop

/**
 * @summary descend en bas de la liste tous les éléments sélectionnés de la liste x
 * @description ALGORITHME
 *  - recopie en bas de la liste chaque élément sélectionné
 *  - supprime les valeurs originales recopiées
 * @param   {string} x - id élément HTML Select
 * @see     {@link dgebi}
 * @see     {@link up}
 * @see     {@link down}
 * @see     {@link toUp}
 */
function toBottom(x) {
    "use strict";
    var lo = dgebi(x).options,
        d = dgebi(x).options.length,
        j = 0,
        i = 0;

    for (i = 0; i < d; i++) {
        if (lo[j].selected) {
            lo[lo.length] = new Option(lo[j].text, lo[j].value);
            lo[lo.length - 1].selected = lo[j].selected;
            lo[j] = null;
        } else {
            j++;
        }
    }
}

/**
 * @summary descend en bas de la liste tous les éléments sélectionnés de la liste x
 * @description déplace les éléments sélectionnées dans la liste ll1,
 * de la liste ll1 vers la liste ll2,
 * en conservant l'ordre de départ (o = false) ou en triant (o = true)
 * dans l'ordre alphabétique les éléments déplacés (b = oui)
 * @param   {string} ll1 - id élément HTML Select
 * @param   {string} ll2 - id élément HTML Select
 * @param   {boolean}  o - trier le résultat (true = oui, false = non)
 * FIXME : convention nommage des variables
 */
function copierSelection(ll1, ll2, o) {
    "use strict";
    var l1 = dgebi(ll1),
        l2 = dgebi(ll2),
        d = l1.options.length,
        m = l2.options.length,
        i = 0,
        j = l1.options.selectedIndex;

    function f(a1, a2) { return parseInt(a1.value, 10) - parseInt(a2.value, 10); }

    for (i = 0; i < m; i++) { l2.options[i].selected = false; }
    for (i = j; i < d; i++) {
        if (l1.options[j].selected) {
            l2.options[m] = new Option(l1.options[j].text, l1.options[j].value);
            l2.options[m].selected = true;
            m++;
            l1.options[j] = null;
        } else { j++; }
    }

    // si o, alors trier
    if (o) { Array.prototype.sort.call(l2.options, f); }
}


// ..................................................................... String


/**
 * @summary transforme une chaîne de caractères en nombre à virgule flottante
 * @description interpréte les caractères suivants
 * espace "classique" et "insécable" sont détectés et ignorés
 * (,), (.), (:) comme séparateur de la partie décimale
 * @param   {string} s - chaîne de caractères représentant un nombre ou un temps
 * @example
 * // returns 12345.67
 * st2float("12 345,67");
 * @example
 * // returns 12.45
 * st2float("12:45");
 * @example
 * // returns 0
 * st2float("");
 * @returns {float} nombre converti en numérique
 */
function st2float(s) {
    "use strict";
    // espace "classique" et "insécable" sont détectés et ignorés
    var r = s.toString().split(' ').join('').split('\u00a0').join('').split(',').join('.').split(':').join('.');

    if (r === '') { r = '0'; }
    return parseFloat(r);
}

/**
 * mélange aléatoirement les caractères d'une chaîne de caractères
 * @param   {string} s - chaîne de caractères à mélanger
 * @returns {string} chaîne de caractères mélangés
 */
function shuffle(s) {
    "use strict";
    var parts = s.split(''),
        random = 0,
        temp = '',
        i = 0;

    for (i = parts.length; i > 0; '') {
        random = parseInt(Math.random() * i, 10);
        temp = parts[--i];
        parts[i] = parts[random];
        parts[random] = temp;
    }
    return parts.join('');
}

/**
 * renvoie la longueur de la partie entière d'une chaîne de caractères représentant un nombre
 * @description interpréte les (,), (.), (:) comme séparateur de la partie décimale
 * @param   {string} s - chaîne de caractères représentant un nombre
 * @example
 * // returns 3
 * longueurPartieEntiere("134.34");
 * @example
 * // returns 3
 * longueurPartieEntiere("134,34");
 * @example
 * // returns 3
 * longueurPartieEntiere("134:34");
 * @returns {number} longueur de la partie entière ou -1 si aucun séparateur de la partie décimale
 * @see     {@link nombreDecimales}
 * @see     {@link convertirDecimales}
 */
function longueurPartieEntiere(s) {
    "use strict";

    if (s.indexOf('.') >= 0) { return s.indexOf('.'); }
    if (s.indexOf(',') >= 0) { return s.indexOf(','); }
    if (s.indexOf(':') >= 0) { return s.indexOf(':'); }
    return s.length - 1; /** FIXME pourquoi (-1) ? */
}

/**
 * retourne le nombre n dont la partie décimale
 * a été multipliée par m et divisée par d
 * @param   {string} n - chaîne de caractères représentant un nombre avec partie décimale
 * @param   {number} m - entier, multiplicateur
 * @param   {number} d - entier, diviseur
 * @returns {string} nombre dont la partie décimale a été convertie
 * @see     {@link longueurPartieEntiere}
 * FIXME convention nommage variables
 */
function convertirDecimales(n, m, d) {
    "use strict";
    var l = 0,
        v = 0;

    if (! n.match(/^\d*[.,:]\d*$/g) ) { return n; } /* s'il s'agit d'une chaîne de texte */
    if (n.match(/^[.,:]$/g) ) { return n; } /* s'il s'agit du seul caractère séparateur de décimale */

    // si nécessaire, ajout du zéro inutile au début
    if (n.match(/^[.,:]\d+$/g) ) { n = '0' + n; } /* .5 => 0.5 */

    // si nécessaire, ajout du zéro inutile à la fin
    if (n.match(/^\d+[.,:]$/g) ) { n = n + '0'; } /* 2. => 2.0 */

    l = longueurPartieEntiere(n);
    v = Math.round(parseInt(n.substr(l + 1, n.length), 10) * m / d);

    return n.substr(0, l) + n.charAt(l) + v;
}

/**
 * @summary retourne la longueur de la partie décimale d'un nombre
 * @description interpréte les (,), (.), (:) comme séparateur de la partie décimale
 * @param   {number} n - chaîne de caractères représentant un nombre
 * @example
 * // returns 2
 * nombreDecimales("134.34");
 * @example
 * // returns 2
 * nombreDecimales("134,34");
 * @example
 * // returns 2
 * nombreDecimales("134:34");
 * @returns {number} longueur de la partie décimale
 * @see     {@link longueurPartieEntiere}
*/
function nombreDecimales(n) {
    "use strict";

    if (n.indexOf('.') >= 0) { return n.toString().split('.')[1].length; }
    if (n.indexOf(',') >= 0) { return n.toString().split(',')[1].length; }
    if (n.indexOf(':') >= 0) { return n.toString().split(':')[1].length; }
    return 0;
}

/**
 * retourne la plus grande longueur de la partie décimale entre 2 nombres
 * @param   {string} a - chaîne de caractères représentant un nombre
 * @param   {string} b - chaîne de caractères représentant un nombre
 * @returns {number} plus grande longueur de la partie décimale
 * @see     {@link nombreDecimales}
 */
function plusGrandePrecision(a, b) {
    "use strict";
    var x = nombreDecimales(a),
        y = nombreDecimales(b);

    return x > y ? x : y;
}

/**
 * supprime le délimiteur d'une chaîne de caractères
 * si ce dernier délimite le début ET la fin de la chaîne
 * @param   {string} s - chaîne de caractères à modifier
 * @param   {string} v - un caractère délimiteur
 * @returns {string} chaîne de caractères sans délimiteur ou chaîne non-modifiée
 */
function supprimerDelimiteurTexte(s, v) {
    "use strict";

    if (s.length > 1 && s.charAt(0) === v && s.charAt(s.length - 1) === v) {
        return s.substr(1, s.length - 2);
    }
    return s;
}

/**
 * supprime tous les caractères "blancs" : \n \t, espace, etc.
 * en début et fin d'une chaîne de caractères
 * @param   {string} s - chaîne de caractères à nettoyer
 * @returns {string} chaîne de caractères sans délimiteur
 * @see     {@link http://blog.stevenlevithan.com/archives/faster-trim-javascript|faster-trim}
 */
function trim(s) {
    "use strict";
    var ws = /\s/,
        i = 0;

    s = s.replace(/^\s\s*/, ''); // nettoie les espaces au début
    i = s.length;
    while (ws.test(s.charAt(--i))) {} // détermine le nombre d'espaces à la fin
    return s.slice(0, i + 1);
}

/**
 * supprime toute chaîne constituée de caractères non-significatifs : série d'espaces, tabulations, etc.
 * @param   {string} s - chaîne de caractères à nettoyer
 * @returns {string} chaîne de caractères significatifs ou chaîne vide
 */
function trimNL(s) {
    "use strict";
    return s.replace(/^(\s|\n)*\n/, '');
}

/**
 * complète la chaîne s avec le caractère c jusqu'à atteindre la longueur l
 * @param   {string} s - chaîne de caractères à compléter
 * @param   {number} l - longueur à obtenir
 * @param   {string} c - un caractère pour compléter
 * @returns {string} chaîne de caractères complétée
 */
function fillString(s, l, c) {
    "use strict";

    while (s.length < l) { s += c; }
    return s;
}

/**
 * supprime le retour chariot : \r
 * @param   {string} s - chaîne de caractères à nettoyer
 * @returns {string} chaîne de caractères nettoyée
 */
function sansRetourChariot(s) {
    "use strict";
    var s2 = s,
        l = s2.length;

    return s2.charAt(l - 1) === '\r' ? s2.substring(0, l - 1) : s2;
}

/**
 * calcule la distance d'édition entre deux chaînes de caractères
 * @param   {string} a - chaîne de caractères
 * @param   {string} b - chaîne de caractères
 * @returns {number} distance d'édition
 * @see     {@link https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#JavaScript|Levenshtein}
 */
function calculerDistanceEdition(a, b) {
    "use strict";
    var matrix = [],
        i = 0,
        j = 0;

    if (a.length === 0) { return b.length; }
    if (b.length === 0) { return a.length; }

    for (i = 0; i <= b.length; i++) { matrix[i] = [i]; }
    for (j = 0; j <= a.length; j++) { matrix[0][j] = j; }

    for (i = 1; i <= b.length; i++) {
        for (j = 1; j <= a.length; j++) {
            if (b.charAt(i - 1) === a.charAt(j - 1)) {
                matrix[i][j] = matrix[i - 1][j - 1];
            } else {
                matrix[i][j] = Math.min(matrix[i - 1][j - 1] + 1, // substitution
                               Math.min(matrix[i][j - 1] + 1, // insertion
                               matrix[i - 1][j] + 1)); // suppression
            }
        }
    }

    return matrix[b.length][a.length];
} // fin calculerDistanceEdition

/**
 * remplace dans la valeur c, chaque caractère connu
 * dans avc par son équivalent dans sns
 * (i.e. même position dans les 2 listes avc et sns)
 * @param   {string} s   - valeur
 * @param   {string} avc - alphabet des caractères à remplacer
 * @param   {string} sns - alphabet des caractères de remplacement (dans le même ordre)
 * @returns {string} chaîne contenant les caractères remplacés
 * @see     {@link remplacerPonctuation}
 * @see     {@link sansAccent}
 */
function remplacerCaracteres(s, avc, sns) {
    "use strict";
    var r = [],
        i = 0,
        j = 0;

    for (i = 0; i < s.length; i++) {
        j = avc.indexOf(s.charAt(i));
        if (j !== -1) {
            r[i] = sns[j];
        } else { r[i] = s.charAt(i); }
    }
    return r.join('');
}

/**
 * détermine le pluriel d'une chaîne de caractères
 * @param   {string} s - valeur
 * @returns {string} chaîne au pluriel
 * FIXME intégrer des listes d'exception
 * source : https://la-conjugaison.nouvelobs.com/regles/grammaire/le-pluriel-des-noms-121.php
 */
function pluriel(s) {
    "use strict";
    var a = '';

    // cas particulier : chaîne vide
    if (!s) { return s; }

    a = s.charAt(s.length - 1);

    // cas particulier : -al => -aux / -AL => -AUX
    if (s.match(/al$/) )   { return s.replace(/al$/, 'aux'); }
    if (s.match(/AL$/) )   { return s.replace(/AL$/, 'AUX'); }

    // cas particulier : -eau => -eaux / -EAU => -EAUX
    if (s.match(/eau$/) ) { return s + 'x'; }
    if (s.match(/EAU$/) ) { return s + 'X'; }

    // cas particulier : -aux -AUX
    if (s.match(/aux$/i) ) { return s; }

    // cas particulier : -s -S
    if (a === 's' || a === 'S') { return s; }

    // cas particulier : -x -X
    if (a === 'x' || a === 'X') { return s; }

    // cas particulier : -z -Z
    if (a === 'z' || a === 'Z') { return s; }

    // cas général
    return s + '(s)';
}


/**
 * retire les articles d'une chaîne de caractère
 * @param   {string} s - valeur
 */
function supprimerArticles(s) {
    "use strict";
    var regB = / (d'|l'|d’|l’)/ig,
        regD = / ((un|une|du|de|des|le|la|les) )+/ig,
        regE = /('|’)(un|une|du|de|des|le|la|les) /ig,
        regA = /^(d'|l'|d’|l’)/ig,
        regC = /^(un|une|du|de|des|le|la|les) /ig;

    s = s.replace(regB, " ");
    s = s.replace(regD, " ");
    s = s.replace(regE, "$1");
    s = s.replace(regA, "");
    s = s.replace(regC, "");

    return s;
}


// ...................................................................... Array


/**
 * retourne la plus petite valeur numérique (avec décimale)
 * @param   {Array} ar - tableau de chaînes de caractères représentant des nombres avec partie décimale
 * @returns {string} plus petite valeur numérique
 * @see     {@link maxArray}
 * @see     {@link st2float}
 */
function minArray(ar) {
    "use strict";
    var r = [];

    function f(a, b) { return st2float(a) - st2float(b); }

    r = ar.filter(x => x !== '').sort(f);

    if (r.length) {
      return r.shift();
    }
    return '';
}

/**
 * retourne la plus grande valeur numérique (avec décimale)
 * @param   {Array} ar - tableau de chaînes de caractères représentant des nombres avec partie décimale
 * @returns {string} plus grande valeur numérique
 * @see     {@link minArray}
 * @see     {@link st2float}
 */
function maxArray(ar) {
    "use strict";
    var r = [];

    function f(a, b) { return st2float(a) - st2float(b); }

    r = ar.filter(x => x !== '').sort(f);

    if (r.length) {
      return r.pop();
    }
    return '';
}

/**
 * retourne la somme (avec partie décimale) des éléments d'un tableau
 * @param   {Array} ar - tableau de chaînes de caractères représentant des nombres avec partie décimale
 * @returns {number} somme
 * @see     {@link moyArray}
 * @see     {@link st2float}
 */
function somArray(ar) {
    "use strict";

    function f(a, b) { return st2float(a) + st2float(b); }

    return ar.filter(x => x !== '').reduce(f, 0.0);
}

/**
 * retourne la moyenne (avec partie décimale) des éléments d'un tableau
 * @param   {Array} ar - tableau de chaînes de caractères représentant des nombres avec partie décimale
 * @returns {number} moyenne
 * @see     {@link somArray}
 * @see     {@link st2float}
 */
function moyArray(ar) {
    "use strict";
    var r = 0.0;

    r = somArray(ar) / ar.filter(x => x !== '').length;
    return r.toFixed(2);
}

/**
 * supprime les répétitions de valeurs dans un tableau (trié ou non)
 * @param   {Array} ar - tableau de valeurs
 * @returns {Array} tableau sans répétiton
 * @see     {@link https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Set|Set}
 */
function arrValUnique(a) {
    "use strict";
    var r = new Set(a);
    return [...r];
}

/**
 * compte le nombre d'éléments d'un tableau associatif
 * @param   {Array} ar - tableau associatif
 * @returns {number} nombre d'éléments
 */
function t2dlength(ar) {
    "use strict";
    var l = 0,
        v = '';

    for (v in ar) {
        if (ar.hasOwnProperty(v)) { l += 1; }
    }
    return l;
}

/**
 * retourne l'ensemble des valeurs de a si elles existent dans b
 * @param   {Array} a - tableau de valeurs
 * @param   {Array} b - tableau de valeurs
 * @returns {Array} tableau filtré
 * @see     {@link aMoinsB}
 * @see     {@link https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Set|Set}
 */
function aInterB(a, b) {
    "use strict";
    var s = new Set(b);
    return a.filter(function (x) { return s.has(x); });
}

/**
 * retourne l'ensemble des valeurs de a inconnues de l'ensemble b
 * @param   {Array} a - tableau de valeurs
 * @param   {Array} b - tableau de valeurs
 * @returns {Array} tableau filtré
 * @see     {@link aInterB}
 * @see     {@link https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Set|Set}
 */
function aMoinsB(a, b) {
    "use strict";
    var s = new Set(b);
    return a.filter(function (x) { return !s.has(x); });
}

/**
 * @summary renvoie le couple minmax de valeurs issues de 2 tableaux associatifs pour une même clé
 * @description pour une clé v recherchée dans 2 tableaux associatifs a et b,
 * détermine le couple (min, max) issu de la comparaison de
 * - min = minimum_parmi(a[v],b[v])
 * - max = maximum_parmi(a[v],b[v])
 * si v n'existe pas dans a (respectivement b), retourne 0 pour a (respectivement b)
 * @param   {Array} a - tableau associatif chaîne -> numérique
 * @param   {Array} b - tableau associatif chaîne -> numérique
 * @returns {Object} minmax
 * @returns {number} minmax.min
 * @returns {number} minmax.max
 */
function minmax(a, b, v) {
    "use strict";

    var va = a.hasOwnProperty(v) ? a[v].length : 0,
        vb = b.hasOwnProperty(v) ? b[v].length : 0;

    return { 'min': Math.min(va, vb), 'max': Math.max(va, vb) };
}

/**
 * modifie l'ordre des valeurs d'un tableau
 * en se basant sur une liste de positions
 * @param   {Array} t - tableau de valeurs
 * @param   {Array} l - tableau de positions
 * @returns {Array} tableau trié selon la liste de positions l
 * FIXME : convention nommage variables
 */
function rangerSelonSelection(t, l) {
    "use strict";
    var a = [],
        i = 0;

    for (i = 0; i < t.length; i++) { a.push(t[l[i]]); }
    return a;
}

/**
 * mélange le tableau passé en paramètre
 * en se basant sur l'algorithme de Fisher-Yates
 * @param   {Array} a - tableau de valeurs numériques
 * @returns {Array} tableau mélangé
 * @see     {@link http://bost.ocks.org/mike/shuffle|shuffle}
 */
function shuffle_array(a) {
    "use strict";
    var l = a.length,
        n = 0,
        i = 0;

    // while there remain elements to shuffle...
    while (l) {
        // pick a remaining elemnt...
        i = Math.floor(Math.random() * l--);

        // and swap it with the current element
        n = a[l];
        a[l] = a[i];
        a[i] = n;
    }
    return a;
}

/**
 * à partir d'un tableau à 1 dimension,
 * construit la liste des couples {valeur => [indices des occurrences]}
 * @param   {Array} a - tableau (1 dimension) de chaînes de caractères
 * @returns {Array} tableau de valeurs uniques
 * @see     {@link http://www.shamasis.net/2009/09/fast-algorithm-to-find-unique-items-in-javascript-array|find unique}
 * @see     {@link tab2hash}
 */
function tab1hash(a) {
    "use strict";

    var l = a.length,
        v = '',
        i = 0,
        h = [];

    for (i = 0; i < l; i++) {
        v = a[i];
        if (!h.hasOwnProperty(v)) { h[v] = []; }
        h[v].push(i);
    }

    return h;
}

/**
 * à partir d'un tableau à 2 dimensions,
 * construit la liste des couples {valeur => [indices des occurrences]}
 * @param   {Array}  a - tableau (à 2 dimensions) de chaînes de caractères
 * @param   {number} c - indice de colonne de la 2ème dimension
 * @returns {Array} tableau de valeurs uniques
 * @see     {@link tab1hash}
 */
function tab2hash(a, c) {
    "use strict";

    var l = a.length,
        v = '',
        i = 0,
        h = [];

    for (i = 0; i < l; i++) {
        v = a[i][c];
        if (!h.hasOwnProperty(v)) { h[v] = []; }
        h[v].push(i);
    }

    return h;
}

/**
 * @summary selon l'option o, supprime les lignes de ad dont les indices existent dans ai
 * @description hypothèse : la méthode splice ne manipule que des pointeurs
 * - conséquence : supprimer des lignes peut être réalisé sans copie du tableau original
 * - effet attendu : réduit l'empreinte mémoire au minimum lors des réductions de tableau
 * @param   {Array} ad - tableau de données (à filtrer)
 * @param   {Array} ai - tableau d'indices des lignes à supprimer
 */
function filtreArray(ad, ai) {
    "use strict";

    var i = 0;

    function f(a, b) { return parseFloat(a) - parseFloat(b); } /* @deprecated : @TODO parseInt ? */

    // on commence par supprimer les indices les plus élevés
    ai.sort(f).reverse();
    for (i = 0; i < ai.length; i++) { ad.splice(ai[i], 1); }
}


// ................................................... caractères spéciaux HTML


/**
 * remplace espérluettes, chevrons et guillemets en équivalent HTML
 * afin d'éviter leur interprétation par le navigateur
 * @param   {string} s - chaîne de caractères comportant des balises HTML
 * @returns {string} chaîne traduite
 * @see     {@link unescapeHTML}
*/
function escapeHTML(s) {
    "use strict";

    return s
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;');
}

/**
 * remplace en caractères simples
 * les espérluettes, chevrons et guillemets codés en HTML
 * @param   {string} s - chaîne de caractères comportant des caractères HTML protégés
 * @returns {string} chaîne traduite
 * @see     {@link escapeHTML}
 */
function unescapeHTML(s) {
    "use strict";

    return s
        .replace(/&amp;/g, '&')
        .replace(/&lt;/g,  '<')
        .replace(/&gt;/g,  '>')
        .replace(/&quot;/g, '"');
}

// ..... fin section

///////////////////////////////////////////////////////////////////////////////
//                      CRYPTO : FONCTIONS GÉNÉRALISTES                      //
///////////////////////////////////////////////////////////////////////////////


/*
  basé sur l'algorithme de chiffrement de Blaise de Vigénère
  Reference: http://www.chez.com/algor/vigenere.htm
  FIXME : déplacer ce commentaire dans les fonctions ?
*/


// .......................................................... constantes crypto


/**
 * liste des caractères valides pour en chiffrement de caractères sur 7 bits (ex: adresses mails)
 */
const AVAILABLE_CHAR_LIST = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-.@_';


// ..................................................................... crypto


/**
 * Génère une clé aléatoire à partir de permutation de caractères de la liste AVAILABLE_CHAR_LIST
 * @returns {string} clé aléatoire générée
 * @see     {@link AVAILABLE_CHAR_LIST}
 */
function creerCle() {
    "use strict";
    var t = AVAILABLE_CHAR_LIST + AVAILABLE_CHAR_LIST + AVAILABLE_CHAR_LIST + AVAILABLE_CHAR_LIST;

    t = shuffle(t);
    return t.slice(0, 200);
}

/**
 * déchiffre un texte (exemple d'utilisation : cf. index.html)
 * @param   {string} txt - texte à déchiffrer
 * @param   {string} key - clé de chiffrement
 * @returns {string} texte déchiffré
 * @see     {@link nsc}
 * FIXME : renommer en nospam_dechiffre ou autre chose
 */
function nsd(txt, key) {
    "use strict";
    var ch = '',
        index = 0,
        chK = '',
        indexK = 0,
        i = 0,
        j = 0,
        chResu = '',
        resu = '';

    for (i = 0; i < txt.length; i++) { // decodage caractère après caractère
        ch = txt.charAt(i);
        index = AVAILABLE_CHAR_LIST.indexOf(ch);
        if (index === -1) {
            resu += ch;
        } else {
            chK = key.charAt(i % key.length);
            indexK = AVAILABLE_CHAR_LIST.indexOf(chK);
            j = (index - indexK) % AVAILABLE_CHAR_LIST.length;
            if (j < 0) { j += AVAILABLE_CHAR_LIST.length; }
            chResu = AVAILABLE_CHAR_LIST.charAt(j);
            resu += chResu.toString();
        }
    }
    return resu;
} // fin nsd

/**
 * chiffre un texte
 * @param   {string} txt - texte à chiffrer
 * @param   {string} key - clé de chiffrement
 * @returns {string} texte déchiffré
 * @see     {@link nsd}
 * FIXME : renommer en nospam_chiffre ou autre chose
 */
function nsc(txt, key) {
    "use strict";
    var i = 0,
        ch = '',
        chK = '',
        indexK = 0,
        index = 0,
        chResu = '',
        resu = '';

    for (i = 0; i < txt.length; i++) { // encodage caractere après caractère
        ch = txt.charAt(i);
        chK = key.charAt(i % key.length);
        indexK = AVAILABLE_CHAR_LIST.indexOf(chK);
        index = AVAILABLE_CHAR_LIST.indexOf(ch);
        if (index === -1) {
            resu += ch;
        } else {
            chResu = AVAILABLE_CHAR_LIST.charAt((indexK + index) % AVAILABLE_CHAR_LIST.length);
            resu += chResu.toString();
        }
    }
    return resu;
} // fin nsc

// ..... fin section

///////////////////////////////////////////////////////////////////////////////
//                             ILLICO : VARIABLES                            //
///////////////////////////////////////////////////////////////////////////////


// ......................................................... variables globales


/* variables pour les données "maître" et "lookup" */

/**
 * zone pour glisser-déposer le fichier de données principale (maître)
 * cf. section "Gestion des évènements" (fin du script)
 */
var content = dgebi('action__charge_1'); // anciennement 'content'

/**
 * zone pour glisser-déposer le fichier de données secondaire (lookup)
 * cf. section "Gestion des évènements" (fin du script)
 */
var contentlkp = dgebi('action__enrich_1');

/**
 * zone pour glisser-déposer la version B à comparer
 * cf. section "Gestion des évènements" (fin du script)
 */
var contentcomp = dgebi('action__evalue_1');

/**
 * nom du fichier du jeu de données à charger (maître)
 */
var orig = '';

/**
 * nom du fichier du jeu de données à charger (lookup)
 */
var origlkp = '';

/**
 * jeu de données chargées (maître)
 */
var txt = '';

/**
 * jeu de données chargées (lookup)
 */
var txtlkp = '';

/**
 * en-tête du jeu de données (maître)
 */
var headers = [];

/**
 * en-tête du jeu de données (lookup)
 */
var headerslkp = [];

/**
 * lignes de données (maître)
 */
var data = [];

/**
 * lignes de données (lookup)
 */
var datalkp = [];

/**
 * @namespace
 * @description la configuration est mise à jour par displayFile() -> apercu_xxx() -> resultat()
 * @see      {@link displayFile}
 * @see      {@link apercu_csv}
 * @see      {@link apercu_txt}
 * @see      {@link resultat}
 * @property {string} detectsepa - (auto) ou (fix)
 * @property {string} sepa       - séparateur de colonne (maître)
 * @property {string} oldsepa    - précédent séparateur de colonne (maître)
 * @property {string} sepa_l     - caractère de remplacement des sauts de ligne encapsulées (maître) @default
 * @property {number} ign        - nombre de lignes à ignorer (maître) / position (numéro de ligne) de l'en-tête (maître) @default
 * @property {string} delim      - délimiteur (maître) @default
 * @property {string} fit        - (tronque) ou (adapte) le nombre de colonne (maître) @default
 * @property {string} readHTML   - inteprète (readHTML) ou non (noreadHTML) les caractères HTML des données sources (maître) @default
 * @property {string} tabcsv     - affiche en mode (html) ou (csv) les données sources (maître) @default
 * @property {string} nomCSV     - l'utilisateur modifie le nom de l'export (promptCSV) ou non (nopromptCSV) @default
 */
var confGlobal = {
    nom: "confGlobal",
    sepa: null, /* pour les exports */
    oldsepa: null,
    readHTML: null,
    tabcsv: null,
    nomCSV: null
};

/**
 * instance configuration spécifique au chargement CSV (master)
 */
var confChargeCSV    = {};

/**
 * instance configuration spécifique au chargement d'une liste de fichiers
 */
var confChargeListe  = {};

/**
 * instance configuration spécifique au chargement texte
 */
var confChargeTexte  = {};

/**
 * instance configuration spécifique à la génération d'une suite d'identifiants
 */
var confChargeIdent  = {};

/**
 * instance configuration spécifique à la génération d'un calendrier
 */
var confChargeCalendrier  = {};

/**
 * instance configuration spécifique au chargement CSV lookup
 */
var confChargeLkp    = {};

/**
 * instance configuration spécifique au chargement de la version B
 */
var confCompVersion  = {};

/**
 * instance configuration spécifique au chargement de la table de correspondance clé/valeur
 */
var confRecodeListes  = {};


/* variables spécifiques (pour l'instant) au fichier maître */

/**
 * @summary nombre de colonnes
 * @description recopié dans la variable dl interne à chaque fonction/traitement
 * afin d'éviter un re-calcul coûteux
 */
var datalen = 0;

/**
 * tableau associatif contenant un calcul d'agrégat pour valeur d'une colonne
 * @see     {@link referentiel}    domaine de valeurs => card contient le nombre d'occurrence
 * @see     {@link majreferentiel} domaine de valeurs => card contient le nombre d'occurrence
 * @see     {@link normalise}      normalisation      => card contient l'identifiant unique
 * @see     {@link majnormalise}   normalisation      => card contient l'identifiant unique
 * FIXME ({}) ?
 */
var card = [];

/**
 * support à la normalisation, tableau miroir de card
 */
var drac = [];

/**
 * tableau à 2 dimensions utilisé pour les exports CSV
 * comprenant la synthèse d'un calcul d'agrégat (par[i] = [v, card[v]])
 * @see     {@link exporterAvecSeparateurRef}
 * @see     {@link card}
 * @see     {@link drac}
 */
var par = [];

/**
 * indice de colonne utilisé pour certains exports
 * @see     {@link exporterAvecSeparateurRef}
 */
var cExport = 0;

/**
 * instance utilisée pour l'exploration de données
 * (Analyse Multi-Dimensionnelle Builder)
 * @see     {@link Amdb}
 * @see     {@link forer}
 */
var aamdB = null;

/**
 * tableau contenant 1 valeur : type du dernier chagement
 * parmi (fichier), (tableau), (liste de fichiers)
 */
var derChg = [];

/**
 * tableau contenant 1 valeur : type du dernier chagement
 * parmi (fichier)
 * (pour la source lookup)
 */
var derChgLkp = [];

/**
 * pour l'historisation des transformations (journal de bord)
 * @see     {@link Rollback}
 */
var rb;

/**
 * utilisé pour les exports via l'objet Blob pour contourner la limite du localStorage
 */
var results;

/**
 * saisi par l'utilisateur nom de l'export csv (global)
 * @default
 */
var ecsv = '';

// ..... fin section

///////////////////////////////////////////////////////////////////////////////
//                                ILLICO : IHM                               //
///////////////////////////////////////////////////////////////////////////////


// ....................................................... feedback utilisateur


/**
 * remplace le contenu de l'élément id=msg par un message et l'affiche
 * @param   {string} m - message
 * @see     {@link attention}
 */
function message(m) {
    "use strict";

    remplacerInnerHTML('msg', m);
    afficher('msg');
}

/**
 * remplace le contenu de l'élément id=msg par un message en rouge
 * @param   {string} m - message
 * @see     {@link message}
 */
function attention(m) {
    "use strict";
    remplacerInnerHTML('msg', '<span class="warning">' + m + '</span>');
    afficher('msg');
}

/**
 * renvoie un texte indiquant le nombre de colonne et de ligne
 * @param   {number} x - nombre de colonne
 * @param   {number} y - nombre de ligne
 */
function colonnesLignes(x, y){
    "use strict";
    return ''     + x + ' colonne' + (x > 1 ? 's' : '') +
           ' x '  + y + ' ligne'   + (y > 1 ? 's' : '');
}


// .......................................................... export de données


/**
 * masque le message affiché
 * et supprime les boutons d'export du message de résultat
 */
function resetmsg() {
    "use strict";

    masquer('msg');
    dgebi('msg').className = '';
}

/**
 * @summary affiche les boutons (export HTML" et "⇒ CSV" en fonction de l'option sélectionnée
 * @description masque/affiche les boutons d'export d'export HTML et CSV, selon l'option
 *  - (null) : les 2 boutons
 *  - nocsv  : que "export HTML"
 *  - nohtml : que "export CSV"
 * @param   {number} c   - indice de colonne
 * @param   {string} opt - option parmi -null-, (nocsv), (nohtml)
 * @see     {@link messageEtBoutons}
 * @see     {@link exporterRef}
 * @see     {@link exporterAvecSeparateurRef}
 */
function ajouterBoutonsExport(c, opt) {
    "use strict";

    // par défaut, on affiche les deux boutons d'export HTML/CSV
    opt = typeof opt === 'undefined' ? 'csvhtml' : opt;
    dgebi('msg').className = opt;
}

/**
 * @summary affiche le message m en fonction du nombre de ligne du résultat
 * @description affiche le message m
 *  - si le nombre de lignes n est trop élevé
 *    alors le résultat est accessible uniquement
 *    depuis un nouvel onglet via (en fonction du choix opt)
 *     - export HTML
 *     - export CSV
 *  - dans tous les cas, affiche ces 2 boutons d'export
 * @param   {number} c   - indice de colonne
 * @param   {string} m   - message à afficher
 * @param   {number} n   - nombre de ligne pour l'affichage
 * @param   {string} opt - option parmi -null-, (nocsv), (nohtml)
 * @see     {@link ajouterBoutonsExport}
 * FIXME n : une variable globale configurable par l'utilisateur ou via config.json ?
 */
function messageEtBoutons(c, m, n, opt) {
    "use strict";

    message(m);
    if (n > 15) { masquer('msg');  }
           else { afficher('msg'); }
    ajouterBoutonsExport(c, opt);
}

/**
 * stocke le jeu de données courant au format Fixed length
 * avec comme séparateur sepa
 * @see     {@link exportFixeLength}
 */
function sauverResultatFixeLength() {
    "use strict";
    var ch2 = '',
        i = 0,
        c = 0,
        lm = [], // longueur max
        lv = 0, // longueur d'une valeur (en tant que texte)
        v  = '',
        dt = [[], []],
        conf = confGlobal, // @deprecated, passer en paramètre ?
        dl = datalen;

    // 1ère passe : trouver la plus grande longueur pour chaque colonne
    for (c = 0; c < headers.length; c++) {
        // longueur max initialisée avec la longueur de l'en-tête pour cette colonne
        lm[c] = headers[c].length;

        // longueur du "texte" (des données) pour cette colonne
        if (conf.readHTML === 'readHTML') {
            for (i = 0; i < dl; i++) {
                lv = sansMiseEnForme(data[i][c]).length;
                lm[c] = lv > lm[c] ? lv : lm[c];
            }
        } else {
            for (i = 0; i < dl; i++) {
                lv = data[i][c].length;
                lm[c] = lv > lm[c] ? lv : lm[c];
            }
        }
        // préparation du résultat
        v = valeur('charLineHeader') + '';
        v = v.length === 0 ? ' ' : v.charAt(0);
        dt[0][c] = fillString(headers[c], lm[c], ' ');
        dt[1][c] = fillString('', lm[c], v);
    }

    // construction du résultat : en-tête
    ch2 = conf.readHTML === 'readHTML' ? dt[0].join(conf.sepa) : escapeHTML(dt[0].join(conf.sepa));
    ch2 += '<br />';

    // construction du résultat : ligne de séparation en-tête/données
    if (selection('lineHeader') === 'oui') { // @NEW variable conf //(dgebi('lineHeader').checked) {
        ch2 += conf.readHTML === 'readHTML' ? dt[1].join(conf.sepa) : escapeHTML(dt[1].join(conf.sepa));
        ch2 += '<br />';
    }

    if (conf.readHTML === 'readHTML') {
        for (i = 0; i < dl; i++) {
            dt = [];
            for (c = 0; c < headers.length; c++) {
                lv = sansMiseEnForme(data[i][c]).length;
                dt[c] = data[i][c];
                while (lv < lm[c]) {
                    dt[c] += ' ';
                    lv++;
                }
            }
            ch2 += dt.join(conf.sepa) + '<br />';
        }
    } else {
        for (i = 0; i < dl; i++) {
            dt = [];
            for (c = 0; c < headers.length; c++) {
                dt[c] = fillString(data[i][c], lm[c], ' ');
            }
            ch2 += escapeHTML(dt.join(conf.sepa)) + '<br />';
        }
    }

    console.log('[sauverResultatFixeLength] longueur totale : ' + ch2.length);
    results = ch2;
} // fin sauverResultatFixeLength

/**
 * ouvre un nouvel onglet avec une copie HTML du tableau référentiel courant
 * @param   {string} e - innerHTML d'un tableau HTML (<TABLE>....</TABLE>)
 * @see     {@link Date.prototype.timeNow}
 */
function exporterRef(e) {
    "use strict";
    var aBlob,
        url = window.location.href.split('/'),
        th = '',
        h = new Date().timeNow();

    url.pop();
    h = h.split('');
    h = h[0] + h[1] + ':' + h[2] + h[3] + ':' + h[4] + h[5];

    th = '<html lang="fr"><head><meta charset="utf-8"/>';
    th += '<title>Illico (référentiel) ' + h + '</title>';
//    th += '<link href="' + url.join('/') + '/css/style.css" rel="stylesheet" type="text/css" />';
    th += "<style>" + css_msg + "</style>";
    th += '</head>';
    th += '<body>';
//    th += '<script src="' + url.join('/') + '/scripts/sort.js"></script>';
    th += '<script>' + st2float.toString() + '</script>';
    th += '<script>' + sortTable.toString() + '</script>';
    th += '<div id="msg">';

    aBlob = new Blob([th, e, '</div></body></html>'], {type : 'text/html;charset=' + 'UTF-8', encoding : 'UTF-8'});
    window.open(URL.createObjectURL(aBlob, {oneTimeOnly : true}));
} // fin exporterRef

/**
 * ouvre un nouvel onglet avec une copie CSV du tableau référentiel courant
 * @see     {@link horodatage}
 * @see     {@link cExport}
 */
function exporterAvecSeparateurRef() {
    "use strict";
    var aBlob,
        s = ecsv || 'illico_REF_' + horodatage() + '.csv',
        choixencode = 'UTF-8',
        conf = confGlobal, // @deprecated, passer en paramètre ?
        i = 0,
        t = headers[cExport] + conf.sepa + 'nombre\n'; // cExport : variable globale

    for (i = 0; i < par.length; i++) { t += par[i].join(conf.sepa) + '\n'; }

    aBlob = new Blob(['\ufeff', t], {type: 'data:application/csv;charset=' + choixencode, encoding: choixencode});
    dgebi('dl_global').href = window.URL.createObjectURL(aBlob, {oneTimeOnly : true});
    dgebi('dl_global').download = s;
    dgebi('dl_global').click();
}

/**
 * ouvre un nouvel onglet avec une copie HTML du jeu de données courant
 * @see     {@link Date.prototype.timeNow}
 */
function exporter() {
    "use strict";
    var aBlob,
        h = new Date().timeNow(),
        url = window.location.href.split('/'),
        t = '';

    url.pop();

    h = h.split('');
    h = h[0] + h[1] + ':' + h[2] + h[3] + ':' + h[4] + h[5];

    t = '<title>Illico (tableau) ' + h + '</title>';
//    t += '<link href="' + url.join('/') + '/css/style.css" rel="stylesheet" type="text/css" />';
    t += "<style>" + css_export + "</style>";

    results = genererExport();

    console.log('[exporter] longueur totale : ' + results.length);
    aBlob = new Blob(['<html lang="fr"><head><meta charset="utf-8"/>', t, '</head><body class="export">', results, '</body></html>'], {type : 'text/html;charset=' + 'UTF-8', encoding : 'UTF-8'});
    window.open(URL.createObjectURL(aBlob, {oneTimeOnly : true}));

    //http://qnimate.com/an-introduction-to-javascript-blobs-and-file-interface/
    //https://medium.com/programmers-developers/convert-blob-to-string-in-javascript-944c15ad7d52
} // fin exporter

/**
 * déclenche le téléchargement au format CSV du jeu de données courant
 * FIXME : ne fonctionne que si choixencode vaut (UTF-8)
 * @see     {@link horodatage}
 */
function exportAvecSeparateur(choixencode, choixsepa) {
    "use strict";
    var dl = datalen,
        aBlob,
        conf = confGlobal, // @deprecated, passer en paramètre ?
        s = ecsv || 'illico_' + horodatage() + '.csv',
        i = 0;

    results = headers.join(choixsepa) + '\n';
    for (i = 0; i < dl; i++) {
        results += data[i].join(choixsepa) + '\n';
    }

    aBlob = new Blob(['\ufeff', results], {type: 'data:application/csv;charset=' + choixencode, encoding: choixencode});
    dgebi('dl_global').href = window.URL.createObjectURL(aBlob, {oneTimeOnly : true});
    if (conf.nomCSV === 'promptCSV') {
        ecsv = prompt("nom du fichier (Echap/annuler => date + heure)", s) || 'illico_' + horodatage() + '.csv';
        dgebi('dl_global').download = ecsv;
        rb.exp(ecsv);
    } else {
        s = 'illico_' + horodatage() + '.csv';
        dgebi('dl_global').download = s;
        rb.exp(s);
    }
    dgebi('dl_global').click();
} // fin exportAvecSeparateur

/**
 * ouvre un nouvel onglet avec une copie Fixed length, du jeu de données courant
 * @see     {@link Date.prototype.timeNow}
 */
function exportFixeLength() {
    "use strict";
    var aBlob,
        h = new Date().timeNow(),
        t = '';

    h = h.split('');
    h = h[0] + h[1] + ':' + h[2] + h[3] + ':' + h[4] + h[5];

    t = '<title>Illico (texte) ' + h + '</title>';

    sauverResultatFixeLength();

    aBlob = new Blob(['<html lang="fr"><head><meta charset="utf-8"/>', t, '</head><body><pre>', results, '</pre></body></html>'], {type : 'text/html;charset=' + 'UTF-8', encoding : 'UTF-8'});
    window.open(URL.createObjectURL(aBlob, {oneTimeOnly : true}));
}


// ......................................................... choix des colonnes


/**
 * initialise les valeurs (Option HTML) de la liste li (Select HTML)
 * préfixés par 00 ou 0 selon le nombre total de valeurs du tableau a
 * @param   {Object} li - Select Element id
 * @param   {Array}   a - tableau de valeurs
 * @param   {boolean} b - item sélectionné (TRUE) ou non (FALSE)
 */
function genererListe(li, a, b) {
    "use strict";
    var l = dgebi(li),
        i = 0,
        s = '',
        d = 1;

    l.options.length = 0;

    for (i = 0; i < a.length.toString().length; i++) {
        s += '0';
    }

    for (i = 0; i < a.length; i++) {
        if ( (i+1) % d === 0) { // i commence à 0
            s = s.substr(1);
            d *= 10; // 10, 100, 1 000, etc.
        }
        l.options[i] = new Option(s + (i +1) + ' - ' + a[i], i, b);
    }
} // fin genererListe

/**
 * déplace tous les éléments de la liste ll1 vers la liste ll2
 * @param   {Object} li1 - Select Element id
 * @param   {Object} li2 - Select Element id
 * @param   {boolean}  b - item sélectionné (TRUE) ou non (FALSE)
 */
function deplacerTout(ll1, ll2, b) {
    "use strict";

    genererListe(ll2, headers, b);
    viderOptionsSelect(ll1);
}

/**
 * prépare toutes les listes déroulantes de sélection de colonnes
 * de toutes les transformations
 */
function majSelecteurs() {
    "use strict";
    var ihtml = '',
        dds = document.querySelectorAll('[id^="col__"]');

    genererListe('col__valeurs_1', headers, false);
    ihtml = dgebi('col__valeurs_1').innerHTML;

/* cas général */

    function f(dd, i) { remplacerInnerHTML(dd.id, ihtml); }

    Array.prototype.forEach.call(dds, f);

/* cas particuliers */

    // mixer
    remplacerInnerHTML('col__remplacer_4-2', ihtml);

    // tester la présence (liste)
    remplacerInnerHTML('col__val_liste_compar_1-2', ihtml);

    // renommer
    selectionnerLOption('col__en_tete_1-2', 1); // sélectionne la deuxième colonne (index 0 = la première)
    selectionnerLOption('col__en_tete_1-3', 2); // sélectionne la troisième colonne
    selectionnerLOption('col__en_tete_1-4', 3); // sélectionne la quatrième colonne
    selectionnerLOption('col__en_tete_1-5', 4); // sélectionne la cinquième colonne
    selectionnerLOption('col__en_tete_1-6', 5); // sélectionne la sixième colonne

    // réordonner
    genererListe('list1', headers, false);
    viderOptionsSelect('list2');

    // décaler des dates selon 1 autre colonne
    remplacerInnerHTML('decaler_col_offset', ihtml);

} // fin majSelecteurs

/**
 * liste les en-têtes sélectionnés dans une sélection multiple de colonnes
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @returns {Array}  liste des en-têtes sélectionnés.
 */
function listeColSelection(cs) {
    "use strict";
    var a = [],
        c = 0;

    /** FIXME
        faire apparaître le numéro de la colonne
        pour distinguer les colonnes de même libellés
        (c + ' ' + headers[c])
    */
    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) { a[a.length] = headers[c]; }
    }
    return a;
}


// .......................................................... import de données


/**
 * 1. charge le contenu d'un fichier de données au format CSV
 * en tant que jeu de données principal (fichier maître)
 * => le texte initial est transformé en un tableau de lignes
 * 2. pré-sélectionne (déduit) un séparateur de colonne
 * @param   {Object} file  - File Object
 * @param   {Object} conf  - Configuration spécifique
 * @param   {string} previ - liste déroulante de colonne (génère une prévisualisation)
 * @returns {Array}  liste des en-têtes sélectionnés.
 * @see     {@link displayFileLkp}
 * @see     {@link copierTableau}
 * @see     {@link apercu_csv}
 * @see     {@link apercu_txt}
 * @see     {@link http://www.html5rocks.com|html5rocks}
 */
function displayFile(file, conf, previ) {
    "use strict";
    var reader = new window.FileReader();

    if (file === '') { return; } // undefined ? null ?

    reader.onload = function (event) {
        txt = event.target.result;

        // --- partie quasi-identique avec "displayFileLkp"

        var tab = {
            '|': 0,
            ';': 0,
            ',': 0,
            '.': 0,
            ':': 0,
            '/': 0,
            '\\': 0,
            '\t': 0,
            ' ': 0
        },
            s = '',
            c = '',
            i = 0,
            b = false,
            max = '';

        // on lit l'en-tête sur la ligne indiquée
        s = txt.split('\n')[conf.ign];

        // si le séparateur est déjà connu, on inscrit cette valeur déjà connue
        // et on poursuit le traitement
        if (confGlobal.detectsepa === 'fix') {
            max = conf.sepa;
            b = true;
        } else { // confGlobal.detectsepa === 'auto'

            max = ' '; // initialisation (arbitraire)
            for (i = 0; i < s.length; i++) {
                c = s.charAt(i);
                if (tab.hasOwnProperty(c)) {
                    tab[c]++;
                    b = true; // détection d'un séparateur depuis la ligne d'en-tête

                    if (c !== max) {
                        max = (tab[c] > tab[max]) || (tab[c] === tab[max] && c !== ' ') ? c : max;
                    }
                }
            }

        }

        if (!b) { // si la détection automatique d'un séparateur a échoué
            /* alors
             * on sélectionne parmi les caractères séparateurs (de la liste)
             * un qui n'apparaît jamais dans les données
             */
            // du plus courant au moins courant
            if (txt.indexOf(' ') < 0)  { max = ' ';  b = true; }
            if (txt.indexOf('\t') < 0) { max = '\t'; b = true; }
            if (txt.indexOf('/') < 0)  { max = '/';  b = true; }
            if (txt.indexOf('\\') < 0) { max = '\\'; b = true; }
            if (txt.indexOf('.') < 0)  { max = '.';  b = true; }
            if (txt.indexOf(',') < 0)  { max = ',';  b = true; }
            if (txt.indexOf(';') < 0)  { max = ';';  b = true; }
            if (txt.indexOf('|') < 0)  { max = '|';  b = true; }
        }

        if (!b) { // si la détection automatique ET la proposition ont échoué
            attention('impossible de détecter un caractère séparateur pertinent : arrêt');
            return;
        }

        conf.sepa = max;

        if(previ === 'previ_csv') {
          dgebi('sepa_chg_csv').value = conf.sepa;
        }
        if(previ === 'cles_rapport') {
          dgebi('sepa_rap_diff').value = conf.sepa;
        }
        // --- fin de la partie commune

       genheaders(conf, previ); // @deprecated : ancienne valeur 'previ_csv'
    }; // fin reader.onload

    reader.onerror = function () { attention('impossible de lire ' + file.name); };
    reader.readAsText(file, conf.encod);
} // fin displayFile

/**
 * charge le contenu d'un fichier de données au format CSV
 * en tant que jeu de données secondaire (fichier lookup)
 * @param   {Object} file - File Object
 * @param   {Object} conf - Configuration spécifique
 * @returns {Array}  liste des en-têtes sélectionnés.
 * @see     {@link displayFile}
 * @see     {@link genheaderslkp}
 * @see     {@link copierTableau}
 * @see     {@link apercu_csv}
 * @see     {@link apercu_txt}
 * @see     {@link http://www.html5rocks.com|html5rocks}
 */
function displayFileLkp(file, conf) {
    "use strict";
    var reader = new window.FileReader();

    if (file === '') { return; }

    resetmsg();

    derChgLkp = ['fichier'.bold() + ' : ' + file.name]; // cf. apercu_xxx()

    reader.onload = function (event) {

        // --- partie quasi-identique avec "displayFile"

        var tab = {
            '|': 0,
            ';': 0,
            ',': 0,
            '.': 0,
            ':': 0,
            '/': 0,
            '\\': 0,
            '\t': 0,
            ' ': 0
        },
            s = '',
            c = '',
            i = 0,
            b = false,
            max = ';',
            cptLlkp = 0;

        txtlkp = event.target.result;

        // compter le nombre de lignes de données
        cptLlkp = txtlkp.split('\n').length - 1;
        if (txtlkp[txtlkp.length - 1] !== '\n') { cptLlkp++; } // le dernier saut de ligne n'est pas forçément sur la dernière ligne
        cptLlkp--;  // pour obtenir le nombre de lignes, on soustrait la ligne d'en-tête

        // détection du séparateur
        s = txtlkp.split('\n')[0];
        max = ' '; // initialisation (arbitraire)
        for (i = 0; i < s.length; i++) {
            c = s.charAt(i);
            if (tab.hasOwnProperty(c)) {
                tab[c]++;
                b = true; // détection d'un séparateur depuis la ligne d'en-tête

                if (c !== max) {
                    max = (tab[c] > tab[max]) || (tab[c] === tab[max] && c !== ' ') ? c : max;
                }
            }
        }

        if (!b) { // si la détection automatique d'un séparateur a échoué
            /* alors
             * on sélectionne parmi les caractères séparateurs (de la liste)
             * un qui n'apparaît jamais dans les données
             */
            // du plus courant au moins courant
            if (txtlkp.indexOf(' ') < 0)  { max = ' ';  b = true; }
            if (txtlkp.indexOf('\t') < 0) { max = '\t'; b = true; }
            if (txtlkp.indexOf('/') < 0)  { max = '/';  b = true; }
            if (txtlkp.indexOf('\\') < 0) { max = '\\'; b = true; }
            if (txtlkp.indexOf('.') < 0)  { max = '.';  b = true; }
            if (txtlkp.indexOf(',') < 0)  { max = ',';  b = true; }
            if (txtlkp.indexOf(';') < 0)  { max = ';';  b = true; }
            if (txtlkp.indexOf('|') < 0)  { max = '|';  b = true; }
        }

        if (!b) { // si la détection automatique ET la proposition ont échoué
            attention('impossible de détecter un caractère séparateur pertinent : arrêt');
            return;
        }

        conf.sepa = max;

        dgebi('sepalkp').value = conf.sepa;
        // --- fin de la partie commune

        genheaderslkp(conf, 'selectlkp');

        dgebi('infolkp').textContent = colonnesLignes(headerslkp.length, cptLlkp);
    }; // fin reader.onload

    reader.onerror = function () { attention('impossible de lire ' + file.name); };
    reader.readAsText(file, conf.encod);
} // fin displayFileLkp


// ...................................................... affichage des données


/**
 * génère un export au format HTML ou CSV en fonction de la configuration (objet conf)
 * et interprète ou non les balises et caractères HTML contenus dans les données
 * @returns {string}  - tableau/texte prêt à être afficher (pour les exports)
 */
function genererExport() {
    "use strict";
    var i = 0,
        ch2 = '',
        conf = confGlobal,
        dl = datalen;

    function td(x, y) { return x + '<td>' + y + '</td>'; }
    function tr(x, y) { return x + '<tr>' + y.reduce(td, '') + '</tr>'; }
    function tdxHTML(x, y) { return x + '<td>' + escapeHTML(y) + '</td>'; }
    function trxHTML(x, y) { return x + '<tr>' + y.reduce(tdxHTML, '') + '</tr>'; }

    if (conf.tabcsv === 'csv') { // aperçu CSV
        // construction : ensemble de lignes (<br />)
        if (conf.readHTML === 'readHTML') {
            ch2 = headers.join(conf.sepa) + '<br />';
            for (i = 0; i < dl; i++) { ch2 += data[i].join(conf.sepa) + '<br />'; }
        } else {
            ch2 = escapeHTML(headers.join(conf.sepa)) + '<br />';
            for (i = 0; i < dl; i++) { ch2 += escapeHTML(data[i].join(conf.sepa)) + '<br />'; }
        }

        // encapsulation : pre
        ch2 = '<pre style="display:inline">' + ch2 + '</pre>';

    } else { // aperçu HTML
        // construction : tableau (tr/td)
        ch2 = conf.readHTML === 'readHTML' ? headers.reduce(td, '') : headers.reduce(tdxHTML, '');
        ch2 = `<tHead><tr>${ch2}</tr></tHead><tbody>`;
        ch2 += conf.readHTML === 'readHTML' ? data.reduce(tr, '') : data.reduce(trxHTML, '');
        ch2 += '</tbody>';

        // encapsulation : table + style:pre
        ch2 = `<table style="white-space:pre;">${ch2}</table>`;
    }

    return ch2;
} // fin genererExport

/**
 * @summary affiche le résultat
 * @description fonction appelée à la fin de chaque traitement
 *  - met à jour toutes les listes déroulantes (sélection des colonnes)
 *  - met à jour les compteurs de lignes et colonnes situés en haut de la page
 */
function resultat() {
    "use strict";

    // MAJ des listes déroulantes (sélection des colonnes)
    majSelecteurs();

    // MAJ compteurs
    dgebi('nbColLig').textContent = colonnesLignes(headers.length, data.length);
    datalen = data.length; // datalen et data sont 2 variables globales

    // réinitialise le message et masque les boutons d'export de l'élément HTML "message"
    resetmsg();
}

// ..... fin section

///////////////////////////////////////////////////////////////////////////////
//                              ILLICO : MOTEUR                              //
///////////////////////////////////////////////////////////////////////////////


// ............................................................. objet Rollback


/**
 * @summery crée un historique
 * @description l'historique peut revenir en arrière d'une étape
 * car il mémorise
 * - le nom de l'action
 * - les paramètres
 * - le résultat
 * - les données
 * - l'en-tête
 *
 * @TODO : suggestion : un seul tableau (pile) d'objet "événement" ?
 */
function Rollback() {
    "use strict";
    this.h = []; // dernière action (header)
    this.d = []; // dernière action (data)
    this.a = ''; // dernière action (action)
    this.s = ''; // dernière action (séparateur)
    this.m = []; // mémoire de toutes les actions
    this.p = []; // mémoire de tous les paramètres
    this.r = []; // mémoire de tous les résultats
    this.e = []; // mémoire de tous les exports

    // appelé en tout début d'une transformation (instantané)
    this.mem = function (a2, h2, d2, p2) {

        // nom de l'action, paramètres
        this.m[this.m.length] = [a2, 'action'];
        this.p[this.p.length] = p2;
        this.r[this.r.length] = '[]';
        this.e[this.e.length] = [];

        // MAJ de la dernière action (pour revenir en arrière)
        this.a = a2;
        this.h = null;
        this.h = JSON.parse(JSON.stringify(h2)); // = copie (équivalent à slice, 1 dimension)
        this.d = null;
        this.d = JSON.parse(JSON.stringify(d2)); // = copie (PAS équivalent à slice, 2 dimensions)
        this.s = confGlobal.sepa;
    };

    // appelé à la fin d'une transformation (résultat)
    this.memr = function (r2) { this.r[this.r.length - 1] = r2; };

    // appelé pour spécifier une catégorie d'action : les chargements de données
    this.chg = function () { this.m[this.m.length - 1][1] = 'chg'; };

    // appelé pour annuler une transformation
    this.rst = function () { this.m[this.m.length - 1][1] = 'rst'; };

    // appelé pour inscrire les noms des exports
    this.exp = function (r2) {
    var i = this.m.length - 1;
    while (i >= 0 && this.m[i][1] === 'rst') {
      i--;
    }

    this.e[i].push(r2);
  };
} // fin Rollback

/**
 * enregistre l'action, les paramètres (optionnel) et le type de l'action (optionnel)
 * @param   {string} act   - nom de l'action
 * @param   {string} param - message contenant les noms et valeurs des paramètres (optionnel)
 * @param   {string} type  - type de l'action (optionnel)
 */
"use strict"; // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Strict_Non_Simple_Params
function bkp(act, param = '[]', type = '') {
    rb.mem(act, headers, data, param);
    if (type === 'chg') {
        rb.chg();
    }
    dgebi('btn-annuler').value = 'annuler : ' + act;
    activer('btn-annuler');
}

/**
 * annule la dernière action : recharge les données dans l'état antérieur
 */
function rst() {
    "use strict";

    rb.rst();
    headers = rb.h;
    data = rb.d;
    confGlobal.sepa = rb.s;
    dgebi('oldsepa').value = confGlobal.sepa; // attention, ici il s'agit d'indiquer la précédente valeur active
    dgebi('btn-annuler').value = rb.a + ' annulé';
    desactiver('btn-annuler');

    resultat();
    message('"' + rb.a + '"' + " vient d'être annulé");
}

/**
 * affiche l'historique des traitements exécutés - sauf fonctions d'analyse
 * @see     {@link Rollback}
 */
function historique() {
    "use strict";
    var l = rb.m.length,
        t = '',
        i = 0,
        aBlob = '',
        url = window.location.href.split('/'),
        th = '';

    url.pop();

    th = '<html lang="fr"><head><meta charset="utf-8"/>';
    th += '<title>Journal de bord</title>';
//    th += '<link href="' + url.join('/') + '/css/style.css" rel="stylesheet" type="text/css" />';
    th += "<style>" + css_histo + "</style>";
    th += '</head>';
    th += '<body>';
    th += '<script>' + sortTable.toString() + '</script>';

    t = `<table style="white-space:pre;" id="historique">
        <thead><tr>
        <td>#</td><td>action</td><td>paramètres</td><td>résultat</td><td>fichiers</td>
        </tr></thead>
        <tbody>`;
    for (i = 0; i < l; i++) {
        t += '<tr>'
           + '<td>' + (i + 1) + '</td>';
        switch (rb.m[i][1]) {
        case 'action':
             t += '<td>' + rb.m[i][0] + '</td>';
             break;
        case 'rst':
             t += '<td class="cc">' + rb.m[i][0] + ' - ANNULÉ</td>';
             break;
        case 'chg':
             t += '<td class="chg">' + rb.m[i][0] + '</td>';
             break;
        }

        t += '<td>' + JSON.parse(rb.p[i]).join('<br>') + '</td>'
           + '<td>' + JSON.parse(rb.r[i]).join('<br>') + '</td>'
           + '<td>' + rb.e[i].join('<br>') + '</td>'
           + '</tr>';
    }
    t += '</tbody></table>';

    aBlob = new Blob([th, t, '</body></html>'], {type : 'text/html;charset=' + 'UTF-8', encoding : 'UTF-8'});
    window.open(URL.createObjectURL(aBlob, {oneTimeOnly : true}));
} // fin historique


// ................................................................. chargement


/**
 * transforme un texte (jeu de données brutes) en un tableau de données h + d
 * suivant les règles de l'objet de configuration conf
 * @param   {Array}   t    - texte représentant l'en-tête et les données
 * @param   {Array}   h    - header (sera modifié)
 * @param   {Array}   d    - data (sera modifié)
 * @param   {Object}  conf - configuration
 */
function texte2data(t, h, d, conf) {
    "use strict";
    var i = 0,
        j = 0;

    // nettoyage des retours chariots sur la ligne d'en-tête
    Array.prototype.push.apply(h, t[0].split(conf.sepa)); // ainsi, on ne modifie pas la référence h
    h[h.length - 1] = sansRetourChariot(h[h.length - 1]);

    // préparation du tableau d + gestion des valeurs nulles + nettoyage des retours chariots
    for (i = 0; i < t.length - 1; i++) { // on enlève l'en-tête (d'où le -1)
        d[i] = t[i + 1].split(conf.sepa);
        for (j = 0; j < h.length; j++) { if (typeof d[i][j] === 'undefined') { d[i][j] = ''; } }
        d[i][d[i].length - 1] = sansRetourChariot(d[i][d[i].length - 1]);
    }

    if (d[d.length - 1].join('') === '') { d.pop(); }    // on enlève la dernière ligne si vide
} // fin texte2data

/**
 * charge les données brutes : 1 enregistrement par ligne (simple)
 * en fonction du séparateur conf.sepa
 * @param   {Array}  lignes - lignes de données
 * @param   {Object} conf   - configuration
 * @see     {@link modeMultiLignes}
 * @see     {@link chargeDonnees}
 * @TODO @deprecated FIXME : fusionner avec texte2data ?
 */
function modeSimple(lignes, conf) {
    "use strict";
    var i = 0,
        c = 0;

    // initialisation
    data  = [];

    for (i = 0; i < lignes.length; i++) {
        data[i] = lignes[i].split(conf.sepa);
        c = data[i].length - 1;
        data[i][c] = sansRetourChariot(data[i][c]);
    }
}

/**
 * charge les données brutes : 1 enregistrement peut être multi-lignes
 * en fonction de délimiteur conf.delim
 * @param   {Array} lignes - lignes de données
 * @param   {Object} conf  - configuration
 * @see     {@link modeSimple}
 * @see     {@link chargeDonnees}
 * @TODO @deprecated FIXME : utiliser texte2data PUIS appliquer le traitement de rattrapage des multi-lignes
 */
function modeMultiLignes(lignes, conf) {
    "use strict";
    var i     = 0,
        l     = 0,
        nbCol = headers.length,
        pCut  = false,
        prec  = [],
        v     = '',
        k     = 0;

        // initialisation
        data  = [];

        for (i = 0; i < lignes.length; i++) {

            // on lit une nouvelle ligne de données
            l = lignes[i].split(conf.sepa);
            // on regarde si la ligne est coupée
            pCut = (prec.length + l.length - 1) <= nbCol; /** FIXME : pourquoi (l.length - 1) ? */

            /*
             * (conf.delim !== '')
             * sans délimiteur explicite => rien à faire
             *
             * (prec.length > 0)
             * ok si on a un reliquat à traiter
             */
            if (conf.delim !== '' && pCut && prec.length > 0) {
                // la ligne est coupée si elle COMMENCE par le délimiteur
                // et NE SE TERMINE PAS par ce délimiteur
                pCut = prec[prec.length - 1].charAt(0) === conf.delim && v.charAt(v.length - 1) !== conf.delim;
            }

/** FIXME : y a-t-il un algorithme plus simple à comprendre ? */

            if (pCut) {
                if (prec.length > 0) {
                    prec[prec.length - 1] = sansRetourChariot(prec[prec.length - 1]);
                    prec[prec.length - 1] += conf.sepa_l + l[0];
                } else { prec.push(l[0]); }
                prec = prec.concat(l.slice(1, l.length));
            } else {
                data[k] = prec;
                k++;
                prec = l;
            }
            v = prec[prec.length - 1];

        } // fin du for

        i = prec.length - 1;
        prec[i] = sansRetourChariot(prec[i]);
        data[k] = prec;
} // fin modeMultiLignes

/**
 * convertit toute valeur "nulle" en une chaîne de caractère vide ('')
 * en fonction du mode conf.fit, (adapte) ou (tronque) la longueur des lignes
 * @param   {Object} conf  - configuration
 * @see     {@link chargeDonnees}
 */
function convertirNullEnVide(conf) {
    "use strict";
    var i  = 0,
        c  = 0,
        mx = 0,
        nbCol = headers.length;

    // convertit les 'undefined' en chaîne de caractère de longueur 0
    for (i = 0; i < data.length; i++) {
        for (c = 0; c < data[i].length; c++) {
            if (typeof data[i][c] === 'undefined') { data[i][c] = ''; }
        }

        // enlève le caractère retour-chariot
        data[i][c - 1] = sansRetourChariot(data[i][c - 1]);

        // détermine la longueur adéquate
        if (conf.fit === 'tronque') {
            while (data[i].length > nbCol) { data[i].pop(); } /** FIXME : préférer splice ? */
        } else {
            mx = mx < data[i].length ? data[i].length : mx;
        }
    }

    // adapte la longueur des lignes ou de l'entête
    if (conf.fit === 'adapte') {
        while (headers.length < mx) {  headers.push(''); }
        for (i = 0; i < data.length; i++) {
            while (data[i].length < mx) {  data[i].push(''); }
        }
    }
} // fin convertirNullEnVide

/**
 * lit un fichier d'entrée f et préparer le message de l'historique p
 * @param   {File}   f - fichier de données brutes
 * @param   {Array}  p - tableau de détail du message
 * @param   {Object} conf - configuration
 * @see     {@link apercu_csv}
 */
function prepareChargeCSV(f, p, conf) {
    "use strict";
    var trad   = {
            '0': '(pas de multi-ligne)',
            '': '(multi-ligne sans délimiteur)',
            '"': '"',
            "'": "'",
            '¤': '¤',
            '|': '|',
            ',': ',',
            ';': ';',
            '-': '-',
            '/': '/',
            '.': '.',
            ':': ':',
            '#': '#',
            '_': '_',
            ' ': '(espace)',
            '	': '(tabulation)',
            '\n': '(saut de ligne \\n)', // valeur_HTML('&#10;') <=> '\n'
            '<br>': '(saut de ligne HTML br)'
        };

    // chargement brut des données
    confGlobal.detectsepa = 'fix';
    displayFile(f, conf, 'previ_csv');
//    genheaders(conf, 'previ_csv'); // @deprecated, dans displayFile

    attention('chargement en cours');

    confGlobal.oldsepa = confGlobal.sepa;
    confGlobal.sepa = conf.sepa;
    dgebi('oldsepa').value = confGlobal.oldsepa;

    // préparation message pour l'historique (spécifique chargement csv)
    p.push(['fichier'.bold() + ' : ' + f.name]);
    p.push(['  encodage'.bold() + ' : ' + conf.encod]);
    p.push(['  séparateur'.bold() + ' : ' + trad[conf.sepa]]);
    if (conf.ign) { p.push("  ignorer les premières lignes, l'en-tête est à la ligne".bold() + ' : ' + conf.ign); }
    p.push("  nombre de colonnes".bold() + ' : ' + (conf.fit === 'tronque' ? "déterminé par l'en-tête" : "déduit des lignes de données"));
    if (conf.delim !== '0') {
       p.push(['  délimiteur'.bold() + ' : ' + trad[conf.delim]]);
       p.push(['  traduire en liste'.bold() + ' : ' + trad[conf.sepa_l]]);
    }
} // fin prepareChargeCSV

/**
 * lit le contenu d'un texteArea et préparer le message de l'historique p
 * @param   {Array}  p    - tableau de détail du message
 * @param   {Object} conf - configuration
 * @see     {@link apercu_txt}
 */
function prepareChargeTableau(p, conf) {
    "use strict";
    var trad   = {
            '0': '(pas de multi-ligne)',
            '': '(multi-ligne sans délimiteur)',
            '"': '"',
            "'": "'",
            '¤': '¤',
            '|': '|',
            ',': ',',
            ';': ';',
            '-': '-',
            '/': '/',
            '.': '.',
            ':': ':',
            '#': '#',
            '_': '_',
            ' ': '(espace)',
            '	': '(tabulation)',
            '\n': '(saut de ligne \\n)', // valeur_HTML('&#10;') <=> '\n'
            '<br>': '(saut de ligne HTML br)'
        };

    // chargement brut des données
    confGlobal.detectsepa = 'fix';

    attention('chargement en cours');

    confGlobal.oldsepa = confGlobal.sepa;
    confGlobal.sepa = conf.sepa;
    dgebi('oldsepa').value = confGlobal.sepa;

    // préparation message pour l'historique (spécifique chargement tableau)
    p.push(['tableau'.bold()]);
    p.push(['  séparateur'.bold() + ' : ' + trad[conf.sepa]]);
    if (conf.ign) { p.push("  ignorer les premières lignes, l'en-tête est à la ligne".bold() + ' : ' + conf.ign); }
    p.push("  nombre de colonnes".bold() + ' : ' + (conf.fit === 'tronque' ? "déterminé par l'en-tête" : "déduit des lignes de données"));
    if (conf.delim !== '0') {
        p.push(['  délimiteur'.bold() + ' : ' + trad[conf.delim]]);
        p.push(['  traduire en liste'.bold() + ' : ' + trad[conf.sepa_l]]);
    }
} // fin prepareChargeTableau

/**
 * charge les données headers et data à partir du texte txt
 * @param   {Array}  p    - tableau de détail du message
 * @param   {Object} conf - configuration
 * @see     {@link modeMultiLignes}
 * @see     {@link modeSimple}
 * @see     {@link convertirNullEnVide}
 */
function chargeDonnees(p, conf) {
    "use strict";
    var lignes = [],
        m      = '';

    // assertion
    if (! txt.length) {
        attention('aucune donnée ?');
        return;
    }

    // mise à jour des variables globales
    lignes = txt.split('\n');

    // assertion
    if (conf.ign >= lignes.length) {
        attention('le nombre de lignes à ignorer est supérieur au nombre de lignes, corrigez svp');
        return;
    }

    // si tout est OK, alors on inscrit dans l'historique et on charge les données

    bkp('charger', JSON.stringify(p), 'chg');

    // initialisation des variables globales
    headers = [];
    data    = []; // par souci de cohérence (en fait, inutile)
// @TODO @deprecated FIXME, fusionner avec texte2data ?
    // on enlève la dernière ligne si vide
    if (lignes[lignes.length - 1] === '') { lignes.pop(); }

    // on supprime les lignes ignorées...
    if (conf.ign) { lignes.splice(0, conf.ign); }

    // on extrait l'en-tête
    headers = lignes.shift().split(conf.sepa);
    headers[headers.length - 1] = sansRetourChariot(headers[headers.length - 1]);

    // lecture des données
    if (conf.delim === '0') { // mode "simple" (vs mode "multi-lignes")
        modeSimple(lignes, conf);
    } else {
       modeMultiLignes(lignes, conf);
    }

    // remplace les valeurs "undefined" par des chaînes de longueur 0
    // adapte ou tronque la longueur des colonnes
    convertirNullEnVide(conf);

    resultat();
    m = colonnesLignes(headers.length, data.length);
    message(m);

    rb.memr(JSON.stringify([m]));
} // fin chargeDonnees

/**
 * lecture de données depuis le chargement csv
 */
function apercu_csv() {
    "use strict";
    var p = [];

    prepareChargeCSV(orig, p, confChargeCSV);
    chargeDonnees(p, confChargeCSV);
}

/**
 * lecture de données depuis le chargement csv
 */
function apercu_txt() {
    "use strict";
    var p = [];

    prepareChargeTableau(p, confChargeTexte);
    chargeDonnees(p, confChargeTexte);
}


// ......................................... chargement depuis une autre source


/**
 * crée un nouveau jeu de données à partir d'une sélection de fichiers avec :
 * - en première colonne : les noms des fichiers
 * - en seconde colonne : le champ rem
 * si l'option "ajouterFic" est sélectionnée,
 * la sélection s'ajoute au jeu de donnée existant (2 colonnes)
 * @param   {string} rem  - commentaire
 * @param   {Object} conf - configuration
 */
function creerListe(rem, conf) {
    "use strict";
    var a = dgebi('listeFic'),
        l = a.options.length,
        ajt = selection('ajouterFic'),
        i = 0,
        m = '',
        trad = {
         '|': '|',
         ';': ';',
         ',': ',',
         '.': '.',
         ':': ':',
         '/': '/',
         '\\': '\\',
         '\t': '(tabulation)',
         ' ': '(espace)'
        };

    // assertion
    if (l === 0) {
        attention("aucun fichier sélectionné ?");
        return;
    }

    derChg = ['liste de noms de fichiers'.bold()];
    if (rem) { derChg.push('  remarque'.bold() + ' : ' + rem); }
    derChg.push('  séparateur de colonne'.bold() + ' : ' + trad[conf.sepa]);
    derChg.push('  ajouter à la liste'.bold() + ' : ' + ajt);
    derChg.push('  sélection'.bold() + ' : ' + valeur('nbfic'));

    bkp('charger', JSON.stringify(derChg), 'chg');

    headers = ['fichiers', 'remarque'];

    // soit on crée un nouveau tableau de données
    if (ajt === "non") { data = []; }
    // soit on ajoute aux données existantes (2 colonnes)
    for (i = 0; i < l; i++) { data[data.length] = [a.options[i].value, rem]; }

    confGlobal.oldsepa = confGlobal.sepa;
    confGlobal.sepa = conf.sepa;
    dgebi('oldsepa').value = confGlobal.sepa;

    resultat();
    m = colonnesLignes(headers.length, data.length);
    message('terminé');

    rb.memr(JSON.stringify([m]));
} // fin creerListe

/**
 * remplace le jeu de données actuel par une suite de numéro générés par la suite arithmétique bornée suivante
 * pr + (x * pa) <= li
 * ou
 * pr + (x * pa) >= li
 * Le sens de la suite est déterminé par les bornes.
 * @param   {number} pr   - premier numéro (décimales acceptées)
 * @param   {number} pa   - pas d'incrément (décimales acceptées)
 * @param   {number} li   - borne supérieure (décimales acceptées)
 * @param   {Object} conf - configuration
 */
function genererIdentifiants(pr, pa, li, conf) {
    "use strict";
    var i = 0.0,
        dec = 0, // précision : nombre de décimales
        // les input HTML empêchent la saisie des espaces dans ces nombres
        pri = parseFloat(pr),
        pai = parseFloat(pa),
        lii = parseFloat(li),
        m = '',
        trad = {
         '|': '|',
         ';': ';',
         ',': ',',
         '.': '.',
         ':': ':',
         '/': '/',
         '\\': '\\',
         '\t': '(tabulation)',
         ' ': '(espace)'
        },

        p = JSON.stringify([
            ['premier'.bold()    + ' : ' + pr],
            ['pas'.bold()        + ' : ' + pa],
            ['borne'.bold()      + ' : ' + li],
            ['séparateur'.bold() + ' : ' + trad[conf.sepa]]
        ]);

    // assertion (sur les paramètres d'entrée)
    if (pr === '' || pa === '' || li === '') {
        attention("la description de la liste à construire (premier, pas, limite/max) n'est pas complète, corrigez svp");
        return;
    }

    bkp('créer des identifiants', p, 'chg');

    dec = plusGrandePrecision(pa, li);

    headers = ['identifiants'];
    data = null;
    data = [];
    if (pri < lii) {
        for (i = pri; i <= lii; i += pai) { data[data.length] = [i.toFixed(dec)]; }
        i -= pai;
    } else {
        for (i = pri; i >= lii; i += pai) { data[data.length] = [i.toFixed(dec)]; }
        i -= pai;
    }

    m = data.length + ' lignes (dernier identifiant : ' + i.toFixed(dec) + ')';

    confGlobal.oldsepa     = confGlobal.sepa;
    confGlobal.sepa        = conf.sepa;
    dgebi('oldsepa').value = confGlobal.sepa;

    resultat();
    message(m);

    rb.memr(JSON.stringify([m]));
} // fin genererIdentifiants


/**
 * remplace le jeu de données actuel par un calendrier définit par deux bornes et les jours de la semaine à inscrire
 * @param   {number} pr   - date de début
 * @param   {number} li   - date de fin
 * @param   {number} jrs  - sélection des jours parmi (lundi), (mardi), (mercredi), (jeudi), (vendredi), (samedi), (dimanche)
 * @param   {Object} conf - configuration
 */
function genererCalendrier(pr, li, jrs, conf){
    "use strict";
    var i = 0,
        m = '',
        d1 = new Date(pr),
        d2 = new Date(li),
        dv = new Date(pr),
        day = 60 * 60 * 24 * 1000, // représente 1 jour (en millisecondes)
        trad = {
          '|': '|',
          ';': ';',
          ',': ',',
          '.': '.',
          ':': ':',
          '/': '/',
          '\\': '\\',
          '\t': '(tabulation)',
          ' ': '(espace)'
        },
        trad_j = {
          '0' : 'dimanche',
          '1' : 'lundi',
          '2' : 'mardi',
          '3' : 'mercredi',
          '4' : 'jeudi',
          '5' : 'vendredi',
          '6' : 'samedi'
        },
        jrs_sem = new Map(),
        p = '';

    if (pr === '') {
        attention('date de début manquante');
        return;
    }

    if (li === '') {
        attention('date de fin manquante');
        return;
    }

    if ([...jrs].filter(x => x.selected).length === 0) {
        attention('quels jours ?');
        return;
    }

    if (d1.valueOf() > d2.valueOf()) {
        attention('bornes inversées');
        return;
    }

    p = JSON.stringify([
        ['début'.bold() + ' : ' + pr],
        ['fin'.bold() + ' : ' + li],
        ['jour(s)'.bold() + ' : <br>  ' + [...jrs].filter(x => x.selected).map(x => trad_j[x.value]).join('<br>  ')],
        ['séparateur'.bold() + ' : ' + trad[conf.sepa]]
    ]);

    bkp('créer un calendrier', p, 'chg');

    [...jrs].filter(x => x.selected).forEach((item, i) => {
      jrs_sem.set('' + item.value, item.value);
    });

    headers = ['date','jour'];
    data = null;
    data = [];

    // valueOf() est équivalent à getTime()
    while (dv.valueOf() <= d2.valueOf() ) {
      i++;
      if(jrs_sem.has('' + dv.getUTCDay() )) {
        data[data.length] = [dv.toISOString().substring(0,10), trad_j['' + dv.getUTCDay()]];
      }

      dv.setTime(d1.getTime() + i * day);
    }

    confGlobal.oldsepa = confGlobal.sepa;
    confGlobal.sepa = conf.sepa;
    dgebi('oldsepa').value = confGlobal.sepa;

    m = data.length + ' lignes (dernière date : ' + data[data.length - 1].slice().reverse().join(' ') + ')';

    resultat();
    message(m);

    rb.memr(JSON.stringify([m]));
} // fin genererCalendrier


// .................................... évaluer l'intégrité d'un jeu de données


/**
 * à partir d'un tableau de données d à deux dimensions,
 * retourne un tableau de clés {clé -> indice de colonne}
 * à partir de la sélection cs
 * @param   {Array}    d  - tableau de données à deux dimensions
 * @param   {Object}   cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {Array}    co - tableau des collisions
 * @param   {boolean}  o  - option, prendre première valeur rencontrée (false), prendre dernière valeur rencontrée (true)
 * @see     {@link comparer2Versions}
 */
function tab2key(d, cs, co, o){
    "use strict";
    var i   = 0,
        c   = 0,
        l   = d.length,
        icc = [],
        k   = '', // clé
        ks  = [], // tableau de clés
        dk  = []; // clés résultats

        // identification des colonnes à utiliser pour construire la clé
        for (c = 0; c < cs.length; c++) {
            if (cs[c].selected) { icc[icc.length] = c; }
        }

        // construction du tableau de clé de la version chargée
        for (i = 0; i < l; i++) {
            ks = [];
            for (c = 0; c < icc.length; c++) {
                ks[ks.length] = d[i][icc[c]];
            }
            k = JSON.stringify(ks);
            if (! dk.hasOwnProperty(k) ) { // si n'existe pas encore
                dk[k] = i;
            } else { // si existe déjà...
                // on note la collision
                if (! co.hasOwnProperty(k)) {
                    co[k] = 1;
                }
                co[k]++;

                // et on inscrit la nouvelle valeur (si souhaité)
                if (o) { dk[k] = i; }
            }
        }

  return dk;
} // fin tab2key

/**
 * afficher les collisions identifiée
 * @param   {Array} co - tableau des collisions pour [A] et [B]
 * @see     {@link tab2key}
 * @see     {@link comparer2Versions}
 */
function identifierCollisions(co) {
    "use strict";
    var t = '',
        k = '';

    if(Object.keys(co.A).length) {
        t += `clé(s) en anomalie dans A
              <table>
              <tr><th>nb lignes</th><th>clés</th></tr>`;
        for (k in co.A) {
            if (co.A.hasOwnProperty(k)) {
                t += '<tr><td>' + co.A[k] + '</td><td>' + k + '</td></tr>';
            }
        }
        t += '</table>';
    } else {
        t += "aucune anomalie dans A<br />";
    }
    if(Object.keys(co.B).length) {
        t += `clé(s) en anomalie dans B
              <table>
              <tr><th>nb lignes</th><th>clés</th></tr>`;
        for (k in co.B) {
            if (co.B.hasOwnProperty(k)) {
                t += '<tr><td>' + co.B[k] + '</td><td>' + k + '</td></tr>';
            }
        }
        t += '</table>';
    } else {
        t += "aucune anomalie dans B<br />";
    }

    message(t);
} // fin identifierCollisions

/**
 * comparer le jeu de données actuel et une autre version de ces mêmes données
 * @param   {Object}  cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string}  o  - traitement (pour "clés A = clés B")
 * @param   {boolean} ca - option, prendre première valeur rencontrée (première), prendre dernière valeur rencontrée (dernière)
 * @param   {boolean} cb - option, prendre première valeur rencontrée (première), prendre dernière valeur rencontrée (dernière)
 * - quand le reste de la ligne est identique
 * - quand le reste de la ligne diffère
 * - tout A, mis à jour avec B
 * - tout B, mis à jour avec A
 * - identifier les anomalies (analyse)
 * @see     {@link tab2key}
 * @see     {@link identifierCollisions}
 * @see     {@link texte2data}
 */
function comparer2Versions(cs, o, ca, cb) {
    "use strict";
    var i = 0,
        l = '',
        cc = '',
        dataB = [],
        headersB = [],
        dl = datalen,
        dlB = 0,
        ccA = [], // tableau de clés de la version A
        ccB = [], // tableau de clés de la version B
        co = [], // tableau de collisions
        jsonA = '',
        jsonB = '',
        m = '',
        data2 = [],
        headers2 = [],
        trad = {
         '|': '|',
         ';': ';',
         ',': ',',
         '.': '.',
         ':': ':',
         '/': '/',
         '\\': '\\',
         '\t': '(tabulation)',
         ' ': '(espace)'
        },

        p = JSON.stringify([
            ['fichier B'.bold()    + ' : ' + orig.name],
            ['  encodage'.bold()   + ' : ' + confCompVersion.encod],
            ['  séparateur'.bold() + ' : ' + trad[confCompVersion.sepa]],
            ['clé(s)'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['extraire les lignes'.bold() + ' : ' + o],
            ['traitement des collisions'.bold()],
            ['  dans A, conserver les valeurs de la ligne de la '.bold() + ca + " occurrence"],
            ['  dans B, conserver les valeurs de la ligne de la '.bold() + cb + " occurrence"]
        ]);

    if (o !== "identifier les anomalies (analyse)"){
      bkp('comparer deux versions', p);
    }

    texte2data(txt.split('\n'), headersB, dataB, confCompVersion); // @TODO : txt ? risque collision ?

    // construction du tableau de clé des deux versions des données
    co.A = [];
    co.B = [];
    ccA = tab2key(data, cs, co.A, ca === 'dernière');
    ccB = tab2key(dataB, cs, co.B, cb === 'dernière');

    if (o === 'identifier les collisions (analyse)') {
      identifierCollisions(co);
      return;
    }

    // traitement
    switch (o) {
        case 'quand le reste de la ligne est identique':
            // pour toutes les clés de A
            for (cc in ccA) {
                // si la clé est connue dans
                if (ccB.hasOwnProperty(cc)) {
                    jsonA = JSON.stringify(data[ccA[cc]]);
                    jsonB = JSON.stringify(dataB[ccB[cc]]);
                    // on compare les JSON obtenus
                    if (jsonA === jsonB) { // copie + 2 colonnes
                        data2.push(JSON.parse(JSON.stringify(data[ccA[cc]])));
                        data2[data2.length -1].push("A:" + (ccA[cc]+1)); // commence à 1 (et non 0)
                        data2[data2.length -1].push("B:" + (ccB[cc]+1)); // commence à 1 (et non 0)
                    }
                }
            }

            // copie + 2 colonnes
            headers2 = headers.slice().concat(["origine A:ligne", "origine B:ligne"]);
            break;

        case 'quand le reste de la ligne diffère':
            // pour toutes les clés de A
            for (cc in ccA) {
                // si la clé est connue dans
                if (ccB.hasOwnProperty(cc)) {
                    jsonA = JSON.stringify(data[ccA[cc]]);
                    jsonB = JSON.stringify(dataB[ccB[cc]]);
                    // on compare les JSON obtenus
                    if (jsonA !== jsonB) { // copie + 1 colonne
                        data2.push(JSON.parse(JSON.stringify(data[ccA[cc]])));
                        data2[data2.length -1].push("A:" + (ccA[cc]+1)); // commence à 1 (et non 0)
                        data2.push(JSON.parse(JSON.stringify(dataB[ccB[cc]])));
                        data2[data2.length -1].push("B:" + (ccB[cc]+1)); // commence à 1 (et non 0)
                    }
                }
            }

            // copie + 1 colonne
            headers2 = headers.slice().concat(["origine:ligne"]);
            break;

        case 'tout A, mis à jour avec B':
            l = headers.length;
            for (i = 0; i < dl; i++) { // copie + 3 colonnes
                data2[i] = data[i].slice().concat(["", "", "val. absente de B"]);
            }
            // pour toutes les clés de A
            for (cc in ccA) {
                // si la clé est connue dans
                if (ccB.hasOwnProperty(cc)) {
                    jsonA = JSON.stringify(data[ccA[cc]]);
                    jsonB = JSON.stringify(dataB[ccB[cc]]);
                    // on compare les JSON obtenus
                    if (jsonA !== jsonB) { // remplace + 3 colonnes
                        data2[ccA[cc]] = JSON.parse(JSON.stringify(dataB[ccB[cc]]));
                        data2[ccA[cc]][l] = "A:" + (ccA[cc]+1); // commence à 1 (et non 0)
                        data2[ccA[cc]][l + 1] = "B:" + (ccB[cc]+1); // commence à 1 (et non 0)
                        data2[ccA[cc]][l + 2] = "ligne mise à jour avec B";
                    } else { // 3 colonnes
                        data2[ccA[cc]][l] = "A:" + (ccA[cc]+1); // commence à 1 (et non 0)
                        data2[ccA[cc]][l + 1] = "B:" + (ccB[cc]+1); // commence à 1 (et non 0)
                        data2[ccA[cc]][l + 2] = "données identiques";
                    }
                }
            }

            // copie + 3 colonnes
            headers2 = headers.slice().concat(["données de A:ligne", "données de B:ligne", "état"]);
            break;

        case 'tout B, mis à jour avec A':
            l = headers.length;
            dlB = dataB.length;
            for (i = 0; i < dlB; i++) { // copie + 3 colonnes
                data2[i] = dataB[i].slice().concat(["", "", "val. absente de A"]);
            }
            // pour toutes les clés de B
            for (cc in ccB) {
                // si la clé est connue dans
                if (ccA.hasOwnProperty(cc)) {
                    jsonA = JSON.stringify(data[ccA[cc]]);
                    jsonB = JSON.stringify(dataB[ccB[cc]]);
                    // on compare les JSON obtenus
                    if (jsonA !== jsonB) { // remplace + 3 colonnes
                        data2[ccB[cc]] = JSON.parse(JSON.stringify(data[ccA[cc]]));
                        data2[ccB[cc]][l] = "A:" + (ccA[cc]+1); // commence à 1 (et non 0)
                        data2[ccB[cc]][l + 1] = "B:" + (ccB[cc]+1); // commence à 1 (et non 0)
                        data2[ccB[cc]][l + 2] = "ligne mise à jour avec A";
                    } else { // 3 colonnes
                        data2[ccB[cc]][l] = "A:" + (ccA[cc]+1); // commence à 1 (et non 0)
                        data2[ccB[cc]][l + 1] = "B:" + (ccB[cc]+1); // commence à 1 (et non 0)
                        data2[ccB[cc]][l + 2] = "données identiques";
                    }
                }
            }

            // copie + 3 colonnes
            headers2 = headersB.slice().concat(["données de A:ligne", "données de B:ligne", "état"]);
            break;

    }

    // mise à jour des données et de l'en-tête
    data = data2;
    headers = headers2;

    resultat();
    m = colonnesLignes(headers.length, data.length);
    message(m);

    rb.memr(JSON.stringify([m]));
} // fin comparer2Versions

/**
 * rapprocher le jeu de données actuel et une colonne d'un autre jeu de données
 * @param   {number}  c    - indice de colonne
 * @param   {string}  oca  - option, sensible à la casse (oui, non)
 * @param   {string}  oac  - option, sensible aux accents (oui, non)
 * @param   {string}  oar  - option, sensible aux articles (oui, non)
 * @param   {string}  ocn  - option, sensible aux caractères non-alphanumériques (oui, non)
 * @param   {string}  oor  - option, sensible à l'ordre des mots (oui, non)
 * @param   {string}  ope  - calculer (la distance d'édition), (le ratio de similarité)
 * @param   {string}  txt  - colonne source B
 * @param   {string}  o    -
 * @see     {@link remplacerCaracteres}
 * @see     {@link supprimerArticles}
 * @see     {@link similariteEntreSelon}
 * @see     {@link A_Z__AVEC_ACCENT}
 * @see     {@link A_Z__SANS_ACCENT}
 * @see     {@link PONCTUATION_COMP}
 */
function rapprocher(c, oca, oac, oar, ocn, oor, ope, txt, o) {
  "use strict";
  var i   = 0,
      j   = 0,
      dl  = datalen,
      a   = txt.split('\n'),
      a0   = txt.split('\n'),
      l   = a.length,
      r   = 0.0,
      d   = [],
      sns = [],
      v   = [],
      t   = '',
      nbCom = 0,
      nbTot = 0,

      p = JSON.stringify([
          ['clé'.bold()    + ' : ' + headers[c]],
          ['sensible'.bold() + ' : ' ],
          ['  à la casse'.bold()   + ' : ' + oca],
          ['  aux accents'.bold()  + ' : ' + oac],
          ['  aux articles'.bold() + ' : ' + oar],
          ['  aux caractères non-alphanumériques'.bold() + ' : ' + ocn],
          ["  à l'ordre des mots".bold() + ' : ' + oor],
          ['ignorer'.bold()],
          ['  les espaces consécutifs'.bold() + " : oui"],
          ['  les espaces début/fin'.bold()   + " : oui"],
          ['calculer'.bold() + " : " + ope],
          ['  conserver'.bold() + ' : ' + o] ]);

  if(o !== 'analyser') {
    bkp('rapprocher 2 écritures', p);
  }

  // préparation des données (source A)
  for (i = 0; i < dl; i++) {
    v[i] = data[i][c];
    d[i] = [];
  }


// pré-traitements sélectionnés par l'utilisateur

  // insensible à la casse
  if (oca === "non") {
    v.forEach((item, i) => { v[i] = item.toLowerCase(); });
    a.forEach((item, i) => { a[i] = item.toLowerCase(); });
  }

  // insensible aux accents
  if (oac === "non") {
    v.forEach((item, i) => { v[i] = remplacerCaracteres(item, A_Z__AVEC_ACCENT, A_Z__SANS_ACCENT); });
    a.forEach((item, i) => { a[i] = remplacerCaracteres(item, A_Z__AVEC_ACCENT, A_Z__SANS_ACCENT); });
  }

  // insensible aux articles
  if (oar === "non") {
    v.forEach((item, i) => { v[i] = supprimerArticles(item); });
    a.forEach((item, i) => { a[i] = supprimerArticles(item); });
  }

  // insensible aux caractères non-alphanumériques
  if (ocn === "non") {
    // préparation de la conversion des caractères non-alphanumériques
    for (i = 0; i < PONCTUATION_COMP.length; i++) { sns[i] = ' '; }

    v.forEach((item, i) => { v[i] = remplacerCaracteres(item, PONCTUATION_COMP, sns); });
    a.forEach((item, i) => { a[i] = remplacerCaracteres(item, PONCTUATION_COMP, sns); });
  }

  // insensible à l'ordre des mots
  if (oor === "non") {
    v.forEach((item, i) => { v[i].split(' ').sort().join(' '); });
    a.forEach((item, i) => { a[i].split(' ').sort().join(' '); });
  }


// traitements systématiques

  // insensible espaces consécutifs
  v.forEach((item, i) => { v[i] = item.replace(/ +/g, " "); });
  a.forEach((item, i) => { a[i] = item.replace(/ +/g, " "); });

  // insensible début/fin
  v.forEach((item, i) => { v[i] = trim(item); });
  a.forEach((item, i) => { a[i] = trim(item); });


// choix du calcul

  //
  if (ope === "la distance d'édition") {
    for (i = 0; i < dl; i++) {
        for (j = 0; j < l; j++) {
            r = calculerDistanceEdition(v[i], a[j]);
            if (r <= 5) {
              d[i].push( { score: r, val: a[j], pos: j } );
            }
        } // pour chaque valeur de B
    } // pour chaque ligne de A
  } // fin distance d'édition

  //
  if (ope === "le ratio de similarité") {
    for (i = 0; i < dl; i++) {
        for (j = 0; j < l; j++) {
            // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment
            [nbCom, nbTot] = similariteEntreSelon(v[i], a[j], ' ', ' ');
            r = (nbCom / nbTot).toFixed(2);
            if (r >= 0.5) {
              d[i].push( { score: r, val: a[j], pos: j } );
            }
        } // pour chaque valeur de B
    } // pour chaque ligne de A
  } // fin ratio de similarité

  function f(a, b){ return a.score - b.score; }

  if (ope === "la distance d'édition") {
    for (i = 0; i < dl; i++) { d[i].sort(f); }
  } else { // ratio de similarité
    for (i = 0; i < dl; i++) { d[i].sort(f).reverse(); }
  }

  t  = '<table style="white-space:pre;">';
  t += '<thead><tr><td>A</td><td>score</td><td>B</td></tr></thead>';
  t += '<tbody>';
  for (i = 0; i < dl; i++) {
    for (j = 0; j < d[i].length; j++) {
      t += '<tr><td>' + data[i][c] + '</td><td>' + d[i][j].score + '</td><td>' + a0[d[i][j].pos] + '</td></tr>';
    }
  }
  t += '</tbody></table>';

  switch(o) {
    case 'meilleure proposition':
      headers.unshift('valeur B', 'score');
      for (i = 0; i < dl; i++) {
         if(d[i].length) { data[i].unshift(a0[d[i][0].pos], '' + d[i][0].score); }
         else            { data[i].unshift('', ''); }
      }
      break;
    case 'toutes les propositions': break;
  }

  if (o !== 'analyser') {
    resultat();
//    rb.memr(JSON.stringify([m]));
  }

  messageEtBoutons(c, t, dl);

} // fin approximer


// ........................................................................ IHM


/**
 * interface avec 2 boutons (+) et (-) : supprime une ligne de champs de saisie
 * @param   {string} e - HTML Table Element id
 * @param   {number} n - numéro de ligne
 * @see     {@link plus1Ligne}
 */
function moins1Ligne(e, n) {
    "use strict";
    var t = dgebi(e),
        r = taille(t);

    if (typeof n === 'undefined') { n = 0; }
    if (r > 2 + n) { t.deleteRow(r - 2); }
}

/**
 * interface avec 2 boutons (+) et (-) : ajoute une ligne de champs de saisie
 * @param   {string} e - HTML Table Element id
 * @see     {@link moins1Ligne}
 */
function plus1Ligne(e) {
    "use strict";
    var t = dgebi(e),
        r = taille(t),
        s = 0,
        l = t.insertRow(r - 1);

    switch (e) {
    case 'tab_concat':
        s = r;
        l.insertCell(0).innerHTML += `<input type="text" id="sepa_${s}" placeholder="séparateur">
                                      <select id="col__colonnes_8-${s}"></select> / ${(s + 1)}`;
        clonerInnerHTML('col__colonnes_8-' + s, 'col__valeurs_1');
        break;
    case 'tab_pivot':
        s = r;
        l.insertCell(0).innerHTML += `<select id="col__colonnes_10-${s}"></select>
                                      <input type="text" id="pivot_delim-${s}" placeholder="délimitée par"> / ${s}`;
        clonerInnerHTML('col__colonnes_10-' + s, 'col__valeurs_1');
        break;
    case 'tab_comp':
        s = r;
        l.insertCell(0).innerHTML += `comparer<br /><select id="col__col_compar_1-g${s}"></select>
                                      et<br /><select id="col__col_compar_1-d${s}"></select> / ${r}`;
        clonerInnerHTML('col__col_compar_1-g' + s, 'col__valeurs_1');
        clonerInnerHTML('col__col_compar_1-d' + s, 'col__valeurs_1');
        break;
    case 'tab_dist_edit':
        s = r;
        l.insertCell(0).innerHTML += `calculer entre<br /><select id="col__col_compar_2-g${s}"></select>
                                      et<br /><select id="col__col_compar_2-d${s}"></select> / ${r}`;
        clonerInnerHTML('col__col_compar_2-g' + s, 'col__valeurs_1');
        clonerInnerHTML('col__col_compar_2-d' + s, 'col__valeurs_1');
        break;
    case 'tab_compact':
        s = r;
        l.insertCell(0).innerHTML += `<select id="col__lignes_8-${s}"></select>
                                      <select id="compact_ope-${s}" onChange="this.className=this.value;" onClick="this.className=this.value;">
              <option value="somme">somme</option>
              <option value="produit">produit</option>
              <option value="min">min</option>
              <option value="max">max</option>
              <option value="décompte">décompte</option>
              <option value="décompte (valeurs distinctes)">décompte (val. distinctes)</option>
              <option value="liste">liste</option>
          </select>
          <input type="text" id="compact_delim-${s}" placeholder="délimitée par"> / ${s}`;
        clonerInnerHTML('col__lignes_8-' + s, 'col__valeurs_1');
        break;
    case 'tab_forage':
        s = r - 1;
        l.insertCell(0);
        l.insertCell(1).innerHTML += `<select id="col__agregats_7-${s}"></select>`;
        clonerInnerHTML('col__agregats_7-' + s, 'col__valeurs_1');
        break;
    }
} // fin plus1Ligne

/**
 * masque toutes les fonctionnalités et remet à jour toutes les listes
 * - une fois au lancement
 * - à chaque sélection d'une nouvelle fonctionnalité
 */
function masquerTout() {
    "use strict";
    var i = 0,
        tcc = dgebi('tab_concat'),
        rcc = taille(tcc),
        tcp = dgebi('tab_comp'),
        rcp = taille(tcp),
        tca = dgebi('tab_compact'),
        rca = taille(tca),
        tde = dgebi('tab_dist_edit'),
        rde = taille(tde),
        tfg = dgebi('tab_forage'),
        rfg = taille(tfg),
        tpv = dgebi('tab_pivot'),
        rpv = taille(tpv);

    resetmsg();
    genererListe('list1', headers, false);
    viderOptionsSelect('list2');

console.warn("masquerTout à vérifier sur les appels moins1Ligne()");  // @TODO
    for (i = 0; i < rcc - 2; i++) { moins1Ligne('tab_concat'); }
    for (i = 0; i < rcp - 2; i++) { moins1Ligne('tab_comp'); }
    for (i = 0; i < rca - 2; i++) { moins1Ligne('tab_compact', 2); }
    for (i = 0; i < rde - 2; i++) { moins1Ligne('tab_dist_edit'); }
    for (i = 1; i < rfg - 2; i++) { moins1Ligne('tab_forage', 1); }
    for (i = 1; i < rpv - 2; i++) { moins1Ligne('tab_pivot'); }

    desactiver('valider_sepa');
    desactiver('valider_sepa_liste');
} // fin masquerTout

// ..... fin section

///////////////////////////////////////////////////////////////////////////////
//                          ILLICO : TRANSFORMATIONS                         //
///////////////////////////////////////////////////////////////////////////////


// .................................................................... valeurs


/**
 * ajoute un préfixe p et/un suffixe s à toutes les colonnes
 * sélectionnées dans cs, selon l'option o
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} p  - préfixe
 * @param   {string} s  - suffixe
 * @param   {number} o  - non-nulles uniquement (1), nulles uniquement (2), toutes (3)
 */
function encapsuler(cs, p, s, o) {
    "use strict";
    var cpt = 0,
        dl = datalen,
        pa = [],
        c = 0,
        i = 0,
        pl = '',
        m = '';

    pa.push(['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')]);
    if (p !== '') { pa.push(['préfixe'.bold() + ' : ' + p]); }
    if (s !== '') { pa.push(['suffixe'.bold() + ' : ' + s]); }
    pa.push(['  appliquer aux valeurs'.bold() + ' : ' + o]);
    bkp('préfixer et/ou suffixer', JSON.stringify(pa));

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
            for (i = 0; i < dl; i++) {
                switch (o) {
                case 'non-nulles uniquement':
                    if (data[i][c] !== "") {
                        data[i][c] = p + data[i][c] + s;
                        cpt++;
                    }
                    break;
                case 'nulles uniquement':
                    if (data[i][c] === "") {
                        data[i][c] = p + data[i][c] + s;
                        cpt++;
                    }
                    break;
                case 'toutes':
                    data[i][c] = p + data[i][c] + s;
                    cpt++;
                    break;
                }
            }
        }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    m = cpt === 0 ? 'aucune modification' : cpt + ' valeur' + pl + ' modifiée' + pl;
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin encapsuler

/**
 * met en capitales d'imprimerie toutes les lettres des valeurs
 * pour toutes les colonnes sélectionnées dans cs
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @see     {@link minuscules}
 * @see     {@link majuscules}
 */
function capitales(cs) {
    "use strict";
    var dl = datalen,
        i = 0,
        c = 0,
        pl = '',
        cpt = 0,
        m = '',
        s = '',

        p = JSON.stringify([['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')]]);
    bkp('CAPITALES', p);

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
            for (i = 0; i < dl; i++) {
                s = data[i][c];
                data[i][c] = data[i][c].toUpperCase();
                cpt += s === data[i][c] ? 0 : 1;
            }
        }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    m = cpt === 0 ? 'aucune modification' : cpt + ' valeur' + pl + ' corrigée' + pl;
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin capitales

/**
 * met en casse basse toutes les lettres des valeurs
 * pour toutes les colonnes sélectionnées dans cs
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @see     {@link capitales}
 * @see     {@link majuscules}
 */
function minuscules(cs) {
    "use strict";
    var dl = datalen,
        i = 0,
        c = 0,
        pl = '',
        cpt = 0,
        m = '',
        s = '',

        p = JSON.stringify([['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')]]);
    bkp('minuscules', p);

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
            for (i = 0; i < dl; i++) {
                s = data[i][c];
                data[i][c] = data[i][c].toLowerCase();
                cpt += s === data[i][c] ? 0 : 1;
            }
        }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    m = cpt === 0 ? 'aucune modification' : cpt + ' valeur' + pl + ' corrigée' + pl;
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin minuscules

/**
 * met en capitales d'imprimerie la première lettre
 * au premier mot ou à tous les mots
 * pour toutes les colonnes sélectionnées dans cs
 * @param   {Object}  cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {boolean} o  - appliquer à chaque mot parmi (oui), (non)
 * @see     {@link capitales}
 * @see     {@link minuscules}
 */
function majuscules(cs, o) {
    "use strict";
    var i = 0,
        c = 0,
        pl = '',
        cpt = 0,
        m = '',
        reg = '',
        s = '',
        dl = datalen,

        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  à chaque mot'.bold() + ' : ' + o]
        ]);
    bkp('majuscules', p);
/**
source : https://stackoverflow.com/questions/20690499/concrete-javascript-regular-expression-for-accented-characters-diacritics

  [A-zÀ-ú] // accepts lowercase and uppercase characters
  [A-zÀ-ÿ] // as above, but including letters with an umlaut (includes [ ] ^ \ × ÷)
  [A-Za-zÀ-ÿ] // as above but not including [ ] ^ \
  [A-Za-zÀ-ÖØ-öø-ÿ] // as above, but not including [ ] ^ \ × ÷

  https://symbl.cc/en/unicode/table/

https://stackoverflow.com/questions/2449779/why-cant-i-use-accented-characters-next-to-a-word-boundary
https://github.com/benjarwar/dial-congress/issues/12
https://stackoverflow.com/questions/10590098/javascript-regexp-word-boundaries-unicode-characters#comment72130442_10590516

solution retenue (et adaptée avec "+") :
https://copyprogramming.com/howto/javascript-regex-word-boundary-b-issue

 **/
    // selon l'option 'o', traite tous les mots ou seulement le premier mot rencontré
    reg = new RegExp("(?:[- .,;!?()]+|$|^)(.)", o === 'oui' ? 'g' : ''); // selon o : /\b(.)/g ou /\b(.)/

    for (c = 0; c < cs.length; c++) {
      if (cs[c].selected) {
        for (i = 0; i < dl; i++) {
          s = data[i][c];
          data[i][c] = data[i][c].toLowerCase().replace(reg, function (str) {
            return str.toUpperCase();
          });
        cpt += s === data[i][c] ? 0 : 1;
        }
      }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    m = cpt === 0 ? 'aucune modification' : cpt + ' valeur' + pl + ' corrigée' + pl;
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin majuscules

/**
 * calcule les cardinalités (nombre, proportion) des valeurs de la colonne c
 * et stocke ces cardinalités dans la variable globale card
 */
function majreferentiel(c) {
    "use strict";
    var v = '',
        dl = datalen,
        i = 0;

    card = null;
    card = [];
    for (i = 0; i < dl; i++) {
        v = data[i][c];
        if (!card.hasOwnProperty(v)) { card[v] = 0; }
        card[v]++;
    }
}

/**
 * fonction d'analyse, calcule sur la colonne c le nombre de lignes contenant la données val
 * @param   {number} c - indice de colonne
 * @param   {string} v - valeur recherchée
 * @see     {@link majreferentiel}
 */
function compter(c, v) {
    "use strict";
    var t = '',
        n = 0,
        dl = datalen;

    majreferentiel(c);

    n = (!card.hasOwnProperty(v)) ? 0 : card[v];

    par = null;
    par = [];
    par[0] = [v, n];
    par[1] = ['[complément]', (dl - n)];

    t = '<table style="white-space:pre;">'
      + '<thead><tr>'
      + '<td>valeurs '
      + '<a class="sbd" onclick="' + "sortTable('tb_cpt',0,true, 't');" + '">▼</a>'
      + '<a class="sbd" onclick="' + "sortTable('tb_cpt',0,false,'t');" + '">▲</a>'
      + '</td>'
      + '<td>nombre</td>'
      + '<td>ratio '
      + '<a class="sbd" onclick="' + "sortTable('tb_cpt',1,true, 'n');" + '">▼</a>'
      + '<a class="sbd" onclick="' + "sortTable('tb_cpt',1,false,'n');" + '">▲</a>'
      + '</td>'
      + '</tr></thead>'

      + '<tbody id="tb_cpt">'
      + '<tr>'
      + '<td>' + v + '</td>'
      + '<td>' + n + '</td>'
      + '<td>' + (n / dl * 100).toFixed(2) + ' %</td>'
      + '</tr><tr>'
      + '<td>[complément]</td>'
      + '<td>' + (dl - n) + '</td>'
      + '<td>' + ((1 - n / dl) * 100).toFixed(2) + ' %</td></tr>'
      + '</tbody>'

      + `<tfoot><tr>
         <td>TOTAL</td>
         <td>${datalen}</td>
         <td>100.00 %</td></tr></tfoot>
         </table>`;

    messageEtBoutons(c, t, 2);
} // fin compter

/**
 * @summary remplit l'espace à gauche/droite des valeurs pour obtenir une longueur spécifique
 * @description pour toutes les colonnes sélectionnées dans cs,
 * ajoute à la valeur autant de fois que nécessaire le caractère r
 * en partant de la direction o pour obtenir la longueur m
 * (si la longueur de la valeur est supérieure ou égale à m,
 * alors la valeur reste inchangée)
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} r - caractère de rempissage
 * @param   {string} o - option, direction parmi (gauche), (droite)
 * @param   {number} m - longueur à obtenir après remplissage
 */
function completer(cs, r, o, m) {
    "use strict";
    var l = 0,
        v = '',
        i = 0,
        j = 0,
        c = 0,
        g = '',
        d = '',
        dl = datalen,

        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  caractère'.bold() + ' : ' + r.charAt(0)],
            ['  direction'.bold() + ' : ' + o],
            ['  longueur'.bold() + ' : ' + m]
        ]);
    bkp('compléter', p);

    if (o === 'gauche') {
        g = r.charAt(0);
    } else {
        d = r.charAt(0);
    }

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
            for (i = 0; i < dl; i++) {
                v = data[i][c];
                l = v.length;
                if (l === 0) { continue; }
                for (j = m; j > l; j--) { v = g + v + d; } /** FIXME utiliser ++ plutôt que -- */
                data[i][c] = v;
            }
        }
    }

    resultat();
    message('terminé');
} // fin completer

/**
 * pour toutes les colonnes sélectionnées dans cs,
 * supprime les l caractères en partant de la direction o
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {number} l  - nombre de caractères à supprimer
 * @param   {string} o  - option, direction parmi (gauche), (droite)
 * @see     {@link conserver}
 */
function tronquer(cs, l, o) {
    "use strict";
    var d = 0,
        i = 0,
        c = 0,
        dl = datalen,

        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  n caractères'.bold() + ' : ' + l],
            ['  direction'.bold() + ' : ' + o]
        ]);
    bkp('tronquer', p);

    switch (o) {
    case 'gauche':
        for (c = 0; c < cs.length; c++) {
            if (cs[c].selected) {
                for (i = 0; i < dl; i++) {
                    d = data[i][c].length;
                    data[i][c] = data[i][c].substring(l, d);
                }
            }
        }
        break;
    case 'droite':
        for (c = 0; c < cs.length; c++) {
            if (cs[c].selected) {
                for (i = 0; i < dl; i++) {
                    d = data[i][c].length;
                    data[i][c] = data[i][c].substring(0, d - l);
                }
            }
        }
        break;
    }

    resultat();
    message('terminé');
} // fin tronquer

/**
 * pour toutes les colonnes sélectionnées dans cs,
 * conserve les l caractères en partant de la direction o
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {number} l  - nombre de caractères à conserver
 * @param   {string} o  - option, direction parmi (gauche), (droite)
 * @see     {@link tronquer}
 */
function conserver(cs, l, o) {
    "use strict";
    var d = 0,
        i = 0,
        c = 0,
        dl = datalen,

        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  n caractères'.bold() + ' : ' + l],
            ['  direction'.bold() + ' : ' + o]
        ]);
    bkp('conserver', p);

    switch (o) {
    case 'gauche':
        for (c = 0; c < cs.length; c++) {
            if (cs[c].selected) {
                for (i = 0; i < dl; i++) { data[i][c] = data[i][c].substring(0, l); }
            }
        }
        break;
    case 'droite':
        for (c = 0; c < cs.length; c++) {
            if (cs[c].selected) {
                for (i = 0; i < dl; i++) {
                    d = data[i][c].length;
                    data[i][c] = data[i][c].substring(d - l, d);
                }
            }
        }
        break;
    }

    resultat();
    message('terminé');
} // fin conserver

/**
 * pour toutes les colonnes sélectionnées dans cs,
 * remplace la valeur v_old par la valeur v_new
 * @param   {Object} cs    - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} v_old - valeur à remplacer
 * @param   {string} v_new - nouvelle valeur
 */
function remplacer(cs, v_old, v_new) {
    "use strict";
    var cpt = 0,
        m = '',
        dl = datalen,
        c = 0,
        i = 0,
        pl = '',
        p = '';

    if (v_new !== '') {
        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  valeur à traduire'.bold() + ' : ' + v_old],
            ['  par'.bold() + ' : ' + v_new]
        ]);
        bkp('traduire des valeurs', p);
    } else {
        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  valeur à supprimer'.bold() + ' : ' + v_old]
        ]);
        bkp('supprimer des valeurs', p);
    }

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
            for (i = 0; i < dl; i++) {
                if (data[i][c] === v_old) {
                    cpt++;
                    data[i][c] = v_new;
                }
            }
        }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    m = cpt === 0 ? 'aucune modification' : cpt + ' valeur' + pl + ' corrigée' + pl;
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin remplacer


// ......................................................... remplacer... si...


/**
 * fonction d'analyse, présente le résultat
 * d'une recherche par expression régulière m sur la colonne c
 * @param   {number}  c  - indice de colonne
 * @param   {string}  m  - motif recherché (regex)
 * @param   {string}  sc - sensible à la casse : non (i), oui ()
 * @param   {boolean} o  - option : correspondances (c), non-correspondances (nc)
 */
function testerMotif(c, m, sc, o) {
    "use strict";
    var i = 0,
        a = [],
        s = '',
        ar = [],
        reg = '',
        t = '',
        tot = 0,
        dl = datalen;

    // programmation défensive + aide utilisateur
    try {
        reg = new RegExp(m, sc + "gm");
    }
    catch (e) {
        attention(e.message);
        return;
    }

    if (o === 'c') {
        for (i = 0; i < dl; i++) {
            a = data[i][c].match(reg);
            if (a) { ar = ar.concat(a); } // @TODO ar.push(...a); // A revoir (2 colonnes : valeur + résultat regex ?)
        }
    } else {
        for (i = 0; i < dl; i++) {
            a = data[i][c].match(reg);
            if (! a) { ar = ar.concat(data[i][c]); } // @TODO ar.push(data[i][c]);
        }
    }

    // variables globales
    par = null;
    par = []; // pour export CSV des résultats de l'analyse
    //par.push(['valeurs','nombre']); // il y a déjà un "clé/valeur"
    card = tab1hash(ar);

    t  = '<table style="white-space:pre;">'
       + '<thead><tr>'
       + '<td>valeurs '
       + '<a class="sbd" onclick="' + "sortTable('tb_testMotif',0,true, 't');" + '">▼</a>'
       + '<a class="sbd" onclick="' + "sortTable('tb_testMotif',0,false,'t');" + '">▲</a>'
       + '</td>'
       + '<td>nombre '
       + '<a class="sbd" onclick="' + "sortTable('tb_testMotif',1,true, 'n');" + '">▼</a>'
       + '<a class="sbd" onclick="' + "sortTable('tb_testMotif',1,false,'n');" + '">▲</a>'
       + '</td></tr>'
       + '</thead>'
       + '<tbody id="tb_testMotif">';

    for (s in card) {
       if (card.hasOwnProperty(s)) {
           t += '<tr><td>' + escapeHTML(s) + '</td><td>' + card[s].length + '</td></tr>';
           tot += card[s].length;
           par.push([escapeHTML(s), card[s].length]);
       }
    }

    t += `</tbody>
          <tfoot><tr>
          <td>TOTAL</td>
          <td>${tot}</td>
          </tr></tfoot>
          </table>`;

    messageEtBoutons(c, '<table>' + t  + '</table>', 10);
} // fin testerMotif

/**
 * @summary met à jour une colonne en fonction du motif trouvé ou non dans une autre colonne
 * @description lorsque le motif r_mot (regex) est trouvé dans la colonne c
 * dans une évaluation sc sensible à la casse
 * - alors la colonne cc prend la nouvelle valeur msnsok (motif de retour regex)
 * - sinon, si siko, alors la colonne ccc prend la nouvelle valeur msnsko
 * @param   {number} c      - indice de colonne : valeurs testées
 * @param   {string} mavc   - motif recherché (regex)
 * @param   {number} cc     - indice de colonne : valeurs retours si motif trouvé
 * @param   {string} sc     - sensible à la casse : non (i), oui ()
 * @param   {string} msnsok - nouvelle valeur si la condition est vérifiée, peut contenir des éléments de regex $1, $2
 * @param   {string} siko   - option, traitement alternatif lorsque la condition n'est pas vérifié (true), ou pas d'action (false)
 * @param   {number} ccc    - indice de colonne : valeurs retours s'il n'y a pas de correspondance avec le motif
 * @param   {string} msnsko - nouvelle valeur si la condition n'est pas vérifiée
 */
function valeursConditionnellesRegex(c, mavc, cc, msnsok, sc, siko, ccc, msnsko) {
    "use strict";
    var cptok = 0,
        cptko = 0,
        reg = '',
        dl = datalen,
        i = 0,
        m = '',
        pl = '',
        p = [],
        trad = {
            'i': 'non',
            '' : 'oui'
            };

    p[0] = ['(si) colonne évaluée'.bold() + ' : ' + headers[c]];
    p[1] = ['  motif recherché'.bold() + ' : ' + mavc];
    p[2] = ['  sensible à la casse'.bold() + ' : ' + trad[sc]];
    p[3] = ['(alors) colonne modifiée'.bold() + ' : ' + headers[cc]];
    p[4] = ['  nouvelle valeur'.bold() + ' : ' + msnsok];

    if (siko) {
        p[5] = ['(sinon) colonne modifiée'.bold() + ' : ' + headers[ccc]];
        p[6] = ['  nouvelle valeur'.bold() + ' : ' + msnsko];
    }
    bkp('définir des valeurs conditionnelles (regex)', JSON.stringify(p));

    // programmation défensive + aide utilisateur
    try {
        reg = new RegExp(mavc, sc + "gm");
    }
    catch (e) {
        m = e.message;
        message(m);
        rb.memr(JSON.stringify([m]));
        return;
    }

    if (!siko) {
        for (i = 0; i < dl; i++) {
            if (data[i][c].match(reg) !== null) {
                data[i][cc] = data[i][c].replace(reg, msnsok);
                cptok++;
            }
        }
        pl = cptok > 1 ? 's' : '';
        m = cptok + ' correspondance' + pl + ' au motif (' + headers[cc] + ')';
    } else {
        for (i = 0; i < dl; i++) {
            if (data[i][c].match(reg) !== null) {
                data[i][cc] = data[i][c].replace(reg, msnsok);
                cptok++;
            } else {
                data[i][ccc] = msnsko;
                cptko++;
            }
        }
        pl = cptok > 1 ? 's' : '';
        m = cptok + ' correspondance' + pl + ' au motif (' + headers[cc] + ')';
        m += '<br />' + cptko + ' cas sans correspondance (' + headers[ccc] + ')';
    }

    resultat();
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin valeursConditionnellesRegex

/**
 * @summary met à jour une colonne en fonction d'une condition numérique entre deux colonnes
 * @description lorsque la condition numérique o est vérifiée entre la valeur
 * des colonnes c et cc
 * - alors la colonne ccc prend la nouvelle valeur msnsok
 * - sinon, si siko, la colonne cccc prend alors la nouvelle valeur msnsko
 * @param   {number} c      - indice de colonne : première colonne comparée
 * @param   {string} o      - opérateur de comparaison numérique (<),(≤),(>),(≥),(=),(≠)
 * @param   {number} cc     - indice de colonne : seconde colonne comparée
 * @param   {number} ccc    - indice de colonne : valeurs retours si motif trouvé
 * @param   {string} msnsok - nouvelle valeur si la condition est vérifiée
 * @param   {string} siko   - option, traitement alternatif lorsque la condition n'est pas vérifié (true), ou pas d'action (false)
 * @param   {number} cccc   - indice de colonne : valeurs retours si la condition n'est pas vérifiée
 * @param   {string} msnsko - nouvelle valeur si la condition n'est pas vérifiée
 */
function valeursConditionnellesNum(c, o, cc, ccc, msnsok, siko, cccc, msnsko) {
    "use strict";
    var cptok = 0,
        cptko = 0,
        p = [],
        m = '',
        pl = '',
        i = 0,
        dl = datalen,
        f = function(){};

    switch(o){
        case '<' : f = function(a, b) { return a <   b; }; break;
        case '≤' : f = function(a, b) { return a <=  b; }; break;
        case '>' : f = function(a, b) { return a >   b; }; break;
        case '≥' : f = function(a, b) { return a >=  b; }; break;
        case '=' : f = function(a, b) { return a === b; }; break;
        case '≠' : f = function(a, b) { return a !== b; }; break;
      }

    p[0] = ['(si) colonne 1'.bold() + ' : ' + headers[c]];
    p[1] = ['  opérateur'.bold() + ' : ' + o];
    p[2] = ['  colonne 2'.bold() + ' : ' + headers[cc]];
    p[3] = ['(alors) colonne modifiée'.bold() + ' : ' + headers[ccc]];
    p[4] = ['  nouvelle valeur'.bold() + ' : ' + msnsok];

    if (siko) {
        p[5] = ['(sinon) colonne modifiée'.bold() + ' : ' + headers[cccc]];
        p[6] = ['  nouvelle valeur'.bold() + ' : ' + msnsko];
    }
    bkp('définir des valeurs conditionnelles (numérique)', JSON.stringify(p));

    if (!siko) {
        for (i = 0; i < dl; i++) {
            if (f(st2float(data[i][c]),st2float(data[i][cc]))) {
                data[i][ccc] = msnsok;
                cptok++;
            }
        }

        pl = cptok > 1 ? 's' : '';
        m = cptok + ' ligne' + pl + ' vérifie' + (pl ? 'nt' : '') + ' la condition ' + '(' + headers[ccc] + ')';

    } else {
        for (i = 0; i < dl; i++) {
            if (f(st2float(data[i][c]),st2float(data[i][cc]))) {
                data[i][ccc] = msnsok;
                cptok++;
            } else {
                data[i][cccc] = msnsko;
                cptko++;
            }
        }

        pl = cptok > 1 ? 's' : '';
        m  = cptok + ' ligne' + pl + ' vérifie' + (pl ? 'nt' : '') + ' la condition ' + '(' + headers[ccc] + ')';
        pl = cptko > 1 ? 's' : '';
        m += '<br />' + cptko + ' ligne' + pl + ' modifiée' + pl + ' par le traitement alternatif ' + ' (' + headers[cccc] + ')';
    }

    resultat();
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin valeursConditionnellesNum

/**
 * retient une des deux valeurs v1 et v2, selon deux règles :
 * - conserver a selon la règle "conserver des valeurs" csv
 * - ignorer b selon la règle "ignorer des valeurs" igv
 * @param   {string} v1  - valeur
 * @param   {string} v2  - valeur
 * @param   {string} csv - règle pour conserver : conserver les valeurs vides (csvv), conserver les valeurs non-vides (csnv)
 * @param   {string} igv - règle pour ignorer : ignorer les valeurs vides (igvv), ignorer les valeurs non-vides (ignv)
 * @returns {number} valeur v1 ou v2, selon le choix des options
 * @see     {@link mixerColonnes}
 */
function choixParmi(v1, v2, csv, igv) {
    "use strict";
    var r = '';

    switch (csv) {
    case '---':
        r = (igv === 'igvv' && v2 === '') || (igv === 'ignv' && v2 !== '') ? v1 : v2;
        r = igv === '---' ? v2 : r;
        break;
    case 'csvv':
        r = (igv === 'igvv' && v2 === '') || (igv === 'ignv' && v2 !== '') ? v1 : v2;
        r = (v1 === '') ? v1 : r;
        break;
    case 'csnv':
        r = (igv === 'igvv' && v2 === '') || (igv === 'ignv' && v2 !== '') ? v1 : v2;
        r = (v1 !== '') ? v1 : r;
        break;
    }
    return r;
} // fin choixParmi

/**
 * @summary mixe le contenu de deux colonnes, selon la règle spécifiée
 * @description crée une nouvelle colonne avec les valeurs des deux colonnes c et cc
 * selon deux règles
 * - conserver les valeurs de c selon csv
 * - ignorer les valeurs de cc selon igv
 * @param   {number} c1  - indice de colonne : première colonne à mixer
 * @param   {number} c2  - indice de colonne : seconde colonne à mixer
 * @param   {string} h   - en-tête de la nouvelle colonne
 * @param   {string} csv - règle : conserver les valeurs vides (csvv), conserver les valeurs non-vides (csnv)
 * @param   {string} igv - règle :   ignorer les valeurs vides (igvv),   ignorer les valeurs non-vides (ignv)
 * @see     {@link choixParmi}
 */
function mixerColonnes(c1, c2, h, csv, igv) {
    "use strict";
    var v = '',
        dl = datalen,
        i = 0,
        trad = {
            'csvv': 'conserver les valeurs vides',
            'csnv': 'conserver les valeurs non-vides',
            'igvv': 'ignorer les valeurs vides',
            'ignv': 'ignorer les valeurs non-vides',
            '---': '---'
        },

        p = JSON.stringify([
            ['colonne 1'.bold() + ' : ' + headers[c1]],
            ['  option'.bold() + ' : ' + trad[csv]],
            ['colonne 2'.bold() + ' : ' + headers[c2]],
            ['  option'.bold() + ' : ' + trad[igv]]
        ]);
    bkp('mixer les valeurs de 2 colonnes', p);

    for (i = 0; i < dl; i++) {
        v = choixParmi(data[i][c1], data[i][c2], csv, igv);
        data[i].splice(0, 0, v);
    }
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin mixerColonnes

/**
 * remplace pour la colonne c
 * tous les motifs mavc par msns en tenant compte
 * de la sensibilité à la casse sc
 * @param   {number} c    - indice de colonne
 * @param   {string} mavc - motif des valeurs à remplacer (regex)
 * @param   {string} msns - texte de remplacement
 * @param   {string} sc     - sensible à la casse : non (i), oui ()
 * @see     {@link remplacerTousLesMotsPar}
 */
function remplacerMotPar(c, mavc, msns, sc) {
    "use strict";
    var reg = new RegExp(mavc, sc + "gm"),
        cpt = 0,
        dl = datalen,
        i = 0;

    for (i = 0; i < dl; i++) {
        if (data[i][c].search(reg) !== -1) {
             data[i][c] = data[i][c].replace(reg, msns);
             cpt++;
        }
    }
    return cpt;
}

/**
 * pour toutes les colonnes sélectionnées dans cs,
 * remplace tous les motifs mavc par msns en tenant compte
 * de la sensibilité à la casse sc
 * @param   {Object} cs    - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} mavc - motif des valeurs à remplacer (regex)
 * @param   {string} msns - texte de remplacement
 * @param   {string} sc     - sensible à la casse : non (i), oui ()
 * @see     {@link remplacerMotPar}
 */
function remplacerTousLesMotsPar(cs, mavc, msns, sc) {
    "use strict";
    var cpt = 0,
        c = 0,
        pl = '',
        reg = '',
        m = '',
        trad = {
            'i': 'non',
            '' : 'oui'
        },

        p = JSON.stringify([
             ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
             ['  motif à remplacer'.bold() + ' : ' + mavc],
             ['  par'.bold() + ' : ' + msns],
             ['  sensible à la casse'.bold() + ' : ' + trad[sc]]
        ]);
    bkp('remplacer un motif (regex) par une valeur', p);

    // programmation défensive + aide utilisateur
    try {
        reg = new RegExp(mavc, sc + "gm");
    }
    catch (e) {
        m = e.message;
        message(m);
        rb.memr(JSON.stringify([m]));
        return;
    }

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) { cpt += remplacerMotPar(c, mavc, msns, sc); }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    m = cpt === 0 ? 'aucune modification' : cpt + ' valeur' + pl + ' modifiée' + pl;
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin remplacerTousLesMotsPar


// ........................................................... valeurs en liste


/**
 * pour toutes les colonnes sélectionnées dans cs,
 * supprime les valeurs v des listes de valeurs séparées par sep
 * @param   {Object} cs  - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} sep - séparateur des valeurs dans la liste
 * @param   {string} v   - valeur à supprimer
 */
function effacerValeurDansListe(cs, sep, v) {
    "use strict";
    var d = [],
        d2 = [],
        l = 0,
        cpt = 0,
        c = 0,
        i = 0,
        j = 0,
        pl = '',
        m = '',
        dl = datalen,
        p = '';

    if (v !== '') {
        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  séparateur'.bold() + ' : ' + sep],
            ['  valeur recherchée'.bold() + ' : ' + v]
        ]);
        bkp('enlever une des valeurs (liste)', p);
    } else {
        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  séparateur'.bold() + ' : ' + sep]
        ]);
        bkp('supprimer les valeurs vides (liste)', p);
    }

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
            for (i = 0; i < dl; i++) {
                d = data[i][c].split(sep);
                l = d.length;
                d2 = [];
                for (j = 0; j < l; j++) {
                    if (d[j] !== v) {
                        d2[d2.length] = d[j];
                    } else {
                        cpt++;
                    }
                }
                data[i][c] = d2.join(sep);
            }
        }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    m = cpt === 0 ? 'aucune modification' : cpt + ' suppression' + pl;
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin effacerValeurDansListe

/**
 * pour toutes les colonnes sélectionnées dans cs,
 * remplace par r les valeurs v des listes de valeurs séparées par sep
 * @param   {Object} cs  - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} sep - séparateur des valeurs dans la liste
 * @param   {string} v   - valeur recherchée
 * @param   {string} r   - valeur de remplacement
 */
function remplacerValeurDansListe(cs, sep, v, r) {
    "use strict";
    var d = [],
        cpt = 0,
        dl = datalen,
        c = 0,
        i = 0,
        j = 0,
        pl = '',
        m = '',

        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  séparateur'.bold() + ' : ' + sep],
            ['  valeur recherchée'.bold() + ' : ' + v],
            ['  nouvelle valeur'.bold() + ' : ' + r]
        ]);
    bkp('remplacer une valeur (liste)', p);

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
            for (i = 0; i < dl; i++) {
                d = data[i][c].split(sep);
                for (j = 0; j < d.length; j++) {
                    if (d[j] === v) {
                        d[j] = r;
                        cpt++;
                    }
                }
                data[i][c] = d.join(sep);
            }
        }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    m = cpt === 0 ? 'aucune modification' : cpt + ' remplacement' + pl + ' de "' + v + '" par "' + r + '"';
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin remplacerValeurDansListe

/**
 * pour toutes les colonnes sélectionnées dans cs,
 * remplace par r les valeurs vides des listes de valeurs séparées par sep
 * @param   {Object}  cs  - Select HTML Element, sélecteur multiple de colonne
 * @param   {string}  sep - séparateur des valeurs dans la liste
 * @param   {string}  r   - valeur de remplacement
 * @param   {boolean} b   - ignorer les listes vides : oui (true), non (false)
 */
function remplacerValeurVideDansListe(cs, sep, r, b) {
    "use strict";
    var d = [],
        cpt = 0,
        c = 0,
        i = 0,
        j = 0,
        dl = datalen,
        pl = '',
        m = '',

        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  séparateur'.bold() + ' : ' + sep],
            ['  nouvelle valeur'.bold() + ' : ' + r],
            ['  modifier les listes sans élément'.bold() + ' : ' + (b ? 'non' : 'oui')]
        ]);
    bkp('remplacer les valeurs vides (liste)', p);

    if (!b) {
        for (c = 0; c < cs.length; c++) {
            if (cs[c].selected) {
                for (i = 0; i < dl; i++) {
                    d = data[i][c].split(sep);
                    for (j = 0; j < d.length; j++) {
                        if (d[j] === '') {
                            d[j] = r;
                            cpt++;
                        }
                    }
                    data[i][c] = d.join(sep);
                }
            }
        }
    } else {
        for (c = 0; c < cs.length; c++) {
            if (cs[c].selected) {
                for (i = 0; i < dl; i++) {
                    if (data[i][c] !== '') {
                        d = data[i][c].split(sep);
                        for (j = 0; j < d.length; j++) {
                            if (d[j] === '') {
                                d[j] = r;
                                cpt++;
                            }
                        }
                        data[i][c] = d.join(sep);
                    }
                }
            }
        }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    if (cpt === 0) {
        m = 'aucune modification';
    } else {
        m = cpt + ' valeur' + pl + ' vide' + pl + ' trouvée' + pl + ' et remplacée' + pl + ' par "' + r + '"';
    }
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin remplacerValeurVideDansListe

/**
 * pour toutes les colonnes sélectionnées dans cs,
 * trie les éléments de la liste de valeurs séparées par sep
 * - selon l'ordre r et le type de tri ft
 * - en supprimant les répétitions de valeurs si b
 * @param   {Object}  cs  - Select HTML Element, sélecteur multiple de colonne
 * @param   {string}  sep - séparateur des valeurs dans la liste
 * @param   {string}  r   - option, sens de tri : tri ascendant (asc), tri descendant (desc)
 * @param   {string}  ft  - option, type de tri : alphabétique (alpha), numérique (num)
 * @param   {boolean} b   - réduit les valeurs répétées à une seule occurrence : oui (true), non (false)
 */
function trierListe(cs, sep, r, ft, b) {
    "use strict";
    var i = 0,
        cpt = 0,
        c = 0,
        pl = '',
        m = '',
        dl = datalen,

        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  séparateur'.bold() + ' : ' + sep],
            ['  ordre'.bold() + ' : ' + r],
            ['  tri'.bold() + ' : ' + ft],
            ['  compacter'.bold() + ' : ' + (b ? 'oui' : 'non')]
        ]);
    bkp('trier les valeurs (liste)', p);

    function f(a, b) { return st2float(a) - st2float(b); }

    if (ft === 'alphabétique') {
        if (r === 'ascendant') {
            if (b) {
                for (c = 0; c < cs.length; c++) {
                    if (cs[c].selected) {
                        for (i = 0; i < dl; i++) {
                            cpt += data[i][c].split(sep).length;
                            data[i][c] = arrValUnique(data[i][c].split(sep)).sort().join(sep);
                            cpt -= data[i][c].split(sep).length;
                        }
                    }
                }
            } else { // b === false
                for (c = 0; c < cs.length; c++) {
                    if (cs[c].selected) {
                        for (i = 0; i < dl; i++) { data[i][c] = data[i][c].split(sep).sort().join(sep); }
                    }
                }
            }
        } else { // r === descendant
            if (b) {
                for (c = 0; c < cs.length; c++) {
                    if (cs[c].selected) {
                        for (i = 0; i < dl; i++) {
                            cpt += data[i][c].split(sep).length;
                            data[i][c] = arrValUnique(data[i][c].split(sep)).sort().reverse().join(sep);
                            cpt -= data[i][c].split(sep).length;
                        }
                    }
                }
            } else { // b === false
                for (c = 0; c < cs.length; c++) {
                    if (cs[c].selected) {
                        for (i = 0; i < dl; i++) { data[i][c] = data[i][c].split(sep).sort().reverse().join(sep); }
                    }
                }
            }
        }
    } else {// ft === numérique
        if (r === 'ascendant') {
            if (b) {
                for (c = 0; c < cs.length; c++) {
                    if (cs[c].selected) {
                        for (i = 0; i < dl; i++) {
                            cpt += data[i][c].split(sep).length;
                            data[i][c] = arrValUnique(data[i][c].split(sep)).sort(f).join(sep);
                            cpt -= data[i][c].split(sep).length;
                        }
                    }
                }
            } else { // b === false
                for (c = 0; c < cs.length; c++) {
                    if (cs[c].selected) {
                        for (i = 0; i < dl; i++) { data[i][c] = data[i][c].split(sep).sort(f).join(sep); }
                    }
                }
            }
        } else { // r === descendant
            if (b) {
                for (c = 0; c < cs.length; c++) {
                    if (cs[c].selected) {
                        for (i = 0; i < dl; i++) {
                            cpt += data[i][c].split(sep).length;
                            data[i][c] = arrValUnique(data[i][c].split(sep)).sort(f).reverse().join(sep);
                            cpt -= data[i][c].split(sep).length;
                        }
                    }
                }
            } else { // b === false
                for (c = 0; c < cs.length; c++) {
                    if (cs[c].selected) {
                        for (i = 0; i < dl; i++) { data[i][c] = data[i][c].split(sep).sort(f).reverse().join(sep); }
                    }
                }
            }
        }
    }

    resultat();
    if (b) {
       pl = cpt > 1 ? 's' : '';
       m = cpt === 0 ? "pas d'occurrence multiple" : cpt + ' valeur' + pl + ' répétée' + pl + ' compactée' + pl;
       message(m);
       rb.memr(JSON.stringify([m]));
    } else {
       message('terminé');
    }
} // fin trierListe

/**
 * pour toutes les colonnes sélectionnées dans cs,
 * pour toutes les listes de valeurs séparées par sep
 * supprime les n valeurs en partant de o
 * @param   {Object}  cs  - Select HTML Element, sélecteur multiple de colonne
 * @param   {number}  n   - nombre de valeurs à supprimer
 * @param   {string}  o   - option, direction : à gauche, à droite
 * @param   {string}  sep - séparateur des valeurs dans la liste
 * @see     {@link consValListe}
 */
function suppValListe(cs, n, o, sep) {
    "use strict";
    var i = 0,
        a = [],
        dl = datalen,
        l = 0,
        c = 0,

        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  nb valeurs à supprimer'.bold() + ' : ' + n],
            ['  sens'.bold() + ' : ' + o],
            ['  séparateur'.bold() + ' : ' + sep]
        ]);
    bkp('enlever n éléments/liste', p);

    if (o === 'à gauche') {
        for (c = 0; c < cs.length; c++) {
            if (cs[c].selected) {
                for (i = 0; i < dl; i++) {
                    a = data[i][c].split(sep);
                    a.splice(0, n);
                    data[i][c] = a.join(sep);
                }
            }
        }
    } else { // o === 'à droite'
        for (c = 0; c < cs.length; c++) {
            if (cs[c].selected) {
                for (i = 0; i < dl; i++) {
                    a = data[i][c].split(sep);
                    l = a.length;
                    a.splice(l - n, n);
                    data[i][c] = a.join(sep);
                }
            }
        }
    }

    resultat();
    message('terminé');
} // fin suppValListe

/**
 * pour toutes les colonnes sélectionnées dans cs,
 * pour toutes les listes de valeurs séparées par sep
 * conserve les n valeurs en partant de o
 * @param   {Object}  cs  - Select HTML Element, sélecteur multiple de colonne
 * @param   {number}  n   - nombre de valeurs à conserver
 * @param   {string}  o   - option, direction : à gauche, à droite
 * @param   {string}  sep - séparateur des valeurs dans la liste
 * @see     {@link suppValListe}
 */
function consValListe(cs, n, o, sep) {
    "use strict";
    var i = 0,
        a = [],
        dl = datalen,
        c = 0,
        l = 0,

        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  nb valeurs à conserver'.bold() + ' : ' + n],
            ['  sens'.bold() + ' : ' + o],
            ['  séparateur'.bold() + ' : ' + sep]
        ]);
    bkp('conserver n éléments/liste', p);

    if (o === 'à gauche') {
        for (c = 0; c < cs.length; c++) {
            if (cs[c].selected) {
                for (i = 0; i < dl; i++) {
                    a = data[i][c].split(sep);
                    a = a.splice(0, n); // splice() renvoie les éléments supprimés
                    data[i][c] = a.join(sep);
                }
            }
        }
    } else { // o === 'à droite'
        for (c = 0; c < cs.length; c++) {
            if (cs[c].selected) {
                for (i = 0; i < dl; i++) {
                    a = data[i][c].split(sep);
                    l = a.length;
                    a = a.splice(l - n, n); // splice() renvoie les éléments supprimés
                    data[i][c] = a.join(sep);
                }
            }
        }
    }

    resultat();
    message('terminé');
} // fin consValListe


// ............................................... valeurs en liste : numéroter


/**
 * crée une nouvelle colonne intitulée titre comportant la liste des numéros commençant
 * par pr et incrémentée à chaque valeur de pa, pour chaque élément des listes de valeurs de c,
 * séparées par sep
 * @param   {number}  c   - indice de colonne
 * @param   {string}  sep - séparateur des valeurs dans la liste
 * @param   {string}  h   - en-tête de colonne
 * @param   {number}  pr  - premier numéro (positif/négatif, avec ou sans partie décimale)
 * @param   {number}  pa  - incrément (positif/négatif, avec ou sans partie décimale)
 * @param   {boolean} o   - les listes vides : (sont ignorées), (contiennent une seule valeur 'vide')
 */
function numeroterValListe(c, s, h, pr, pa, o) {
    "use strict";
    var dec = 0,
        n = 0.0,
        v = '',
        dl = datalen,
        vl = 0,
        t = [],
        i = 0,
        j = 0,

        p = JSON.stringify([
            ['sélection'.bold() + ' : ' + headers[c]],
            ['  séparateur'.bold() + ' : ' + s],
            ['premier numéro'.bold() + ' : ' + pr],
            ['  pas'.bold() + ' : ' + pa],
            ['  les listes vides'.bold() + ' : ' + o]
        ]);
    bkp('numéroter les valeurs', p);

    dec = plusGrandePrecision(pr, pa);

    headers.unshift(h);

    if (o === 'sont ignorées') {
      for (i = 0; i < dl; i++) {
          if (data[i][c] === '') {
              data[i].unshift('');
              continue;
          }

          t = [];
          vl = data[i][c].split(s).length;
          for (j = 0; j < vl; j++) {
              n = j * st2float(pa) + st2float(pr);
              v = n.toFixed(dec);
              t.push(v);
          }
          data[i].unshift(t.join(s));
      }
    } else { // liste vide = liste contenant une seule valeur (une chaîne vide '')
      for (i = 0; i < dl; i++) {
          t = [];
          vl = data[i][c].split(s).length;
          for (j = 0; j < vl; j++) {
              n = j * st2float(pa) + st2float(pr);
              v = n.toFixed(dec);
              t.push(v);
          }
          data[i].unshift(t.join(s));
      }
    }

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin numeroterListe

// ............................................. valeurs en liste : comparaison


/**
 * @summary ajoute une colonne avec le résultat d'un test d'inclusion entre 2 colonnes (1 ligne = 1 valeur à comparer avec une liste de valeurs)
 * @description crée une nouvelle colonne avec le résultat du test d'inclusion des valeurs
 * de la colonne c par rapport aux listes de valeurs séparées par sep de la colonne ti.
 * valeurs retournées :
 * - oui : la valeur testée apparaît dans la liste de contrôle
 * - non : la valeur testée n'apparaît pas dans la liste de contrôle
 * - valeur vide : la valeur testée est vide
 * - liste vide : la liste de contrôle est vide
 * - valeur vide et liste vide : la valeur et la liste de contrôle sont vides
 * @param   {number} c   - indice de colonne des valeurs à comparer (1 ligne = 1 valeur à comparer)
 * @param   {number} ti  - indice de colonne contenant une liste de valeurs à chaque ligne
 * @param   {string} sep - séparateur de valeurs des listes
 */
function testInclusion(c, ti, sep) {
    "use strict";
    var v = '',
        d = [],
        dl = datalen,
        h = '',
        i = 0,

        p = JSON.stringify([
            ['comparer les valeurs de'.bold() + ' : ' + headers[c]],
            ['  avec les listes de'.bold() + ' : ' + headers[ti]],
            ['  délimitées par'.bold() + ' : ' + sep]
        ]);

    bkp("tester la présence d'une valeur (liste)", p);

    for (i = 0; i < dl; i++) {
        if (data[i][ti] === '') {
            if (data[i][c] === '') {
                v = 'valeur vide et liste vide';
            } else {
                v = 'liste vide';
            }
        } else {
            if (data[i][c] === '') {
                v = 'valeur vide';
            } else {
                d = data[i][ti].split(sep);
                v = d.some(x => x === data[i][c]) ? 'oui' : 'non';
            }
        }
        data[i].splice(0, 0, v);
    }

    h = headers[c] + '[?]/' + headers[ti];
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin testInclusion

/**
 * calcule le ratio de similarité entre les éléments des deux listes
 * et indique ce ratio au format o (décimal ou fraction) dans une nouvelle colonne
 * @param   {number} c1 - indice de colonne
 * @param   {number} c2 - indice de colonne
 * @param   {string} s1 - séparateur des valeurs dans les listes de c1
 * @param   {string} s2 - séparateur des valeurs dans les listes de c2
 * @param   {string} b  - option : (décimal), (fraction)
 * @see     {@link similariteEntreSelon}
 */
function ratioSimilarite(c1, c2, s1, s2, o) {
    "use strict";
    var nbCom = 0,
        nbTot = 0,
        dl = datalen,
        v = '',
        h = '',
        i = 0,

        p = JSON.stringify([
            ['colonne'.bold() + ' : ' + headers[c1]],
            ['  séparateur'.bold() + ' : ' + s1],
            ['colonne'.bold() + ' : ' + headers[c2]],
            ['  séparateur'.bold() + ' : ' + s2],
            ['option'.bold() + ' : ' + o]
        ]);
    bkp('calculer le ratio de similarité entre 2 listes', p);

    for (i = 0; i < dl; i++) {
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment
        [nbCom, nbTot] = similariteEntreSelon(data[i][c1], data[i][c2], s1, s2);

        // selon la forme souhaitée du résultat
        if (o === 'fraction') {
            v = '' + nbCom + '/' + nbTot;
        } else { // décimal
            v = '' + (nbCom / nbTot).toFixed(2);
        }
        data[i].splice(0, 0, v);
    }

    h = 'ratio [' + headers[c1] + '/' + headers[c2] + ']';
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin ratioSimilarite

/**
 * @summary ajoute une nouvelle colonne avec un résultat de comparaison du nombre d'éléments des listes de deux colonnes
 * @description le résultat de la comparaison peut être (=), (<), (>), (listes vides)
 * Remarque : Une liste sans élément est une liste de longueur 0.
 * (et non une liste d'un élément, l'élément vide)
 * @param   {number} c1 - indice de colonne
 * @param   {number} c2 - indice de colonne
 * @param   {string} s1 - séparateurs des valeurs des listes de c1
 * @param   {string} s2 - séparateurs des valeurs des listes de c2
 */
function comparerNbValListes(c1, s1, c2, s2) {
    "use strict";
    var n1 = 0,
        n2 = 0,
        v = '',
        h = '',
        dl = datalen,
        i = 0,

        p = JSON.stringify([
            ['colonne 1'.bold() + ' : ' + headers[c1]],
            ['  séparateur'.bold() + ' : ' + s1],
            [],
            ['colonne 2'.bold() + ' : ' + headers[c2]],
            ['  séparateur'.bold() + ' : ' + s2]
        ]);
    bkp("comparer les nombres d'éléments de 2 listes", p);

    for (i = 0; i < dl; i++) {
        n1 = data[i][c1] === '' ? 0 : data[i][c1].split(s1).length;
        n2 = data[i][c2] === '' ? 0 : data[i][c2].split(s2).length;

        if (n1 === n2) {
            if (n1 > 0) {
                v = '=';
            } else {
                v = 'listes vides';
            }
        } else {
            if (n1 > n2) {
                v = '>';
            } else {
                v = '<';
            }
        }
        data[i].splice(0, 0, v);
    }

    h = headers[c1] + ' <> ' + headers[c2];
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin comparerNbValListes


// ................................................ valeurs en liste : agrégats


/**
 * @summary calcule les cardinalités des listes de valeurs séparées par s de la colonne c
 * @description stocke les cardinalités dans la variable globale card
 * selon l'option o, ignore les listes vides ou les comptabilise comme des listes
 * à un seul élément, l'élément vide
 * @param   {number}  c - indice de colonne
 * @param   {string}  s - séparateur des valeurs dans les listes
 * @param   {boolean} o - option, ignorer les listes vides : oui (true), non (false)
 * @see     {@link card}
 * @see     {@link referentielListe}
 */
function majreferentielListe(c, s, o) {
    "use strict";
    var v = '',
        d = [],
        dl = datalen,
        i = 0,
        j = 0;

    card = [];
    for (i = 0; i < dl; i++) {
        if (data[i][c] !== '') {
            d = data[i][c].split(s);
            for (j = 0; j < d.length; j++) {
                v = d[j];
                if (!card.hasOwnProperty(v)) { card[v] = 0; }
                card[v]++;
            }
        } else {
            if (!o) {
                if (!card.hasOwnProperty('')) { card[''] = 0; }
                card['']++;
            }
        }
    }
} // fin majreferentielListe

/**
 * @summary fonction d'analyse, calcule la répartition des valeurs lorsque celles-ci sont stockées sous forme de liste de valeurs
 * @description fonction d'analyse permettant d'obtenir la répartition des valeurs
 * de la colonne c lorsque les valeurs sont stockées sous la forme
 * la forme de liste de valeurs séparées par sep
 *
 * @param   {number}  c - indice de colonne
 * @param   {string}  s - séparateur des valeurs dans les listes
 * @param   {boolean} o - option, ignorer les listes vides : oui (true), non (false)
 * @see     {@link card}
 * @see     {@link majreferentielListe}
 */
function referentielListe(c, s, o) {
    "use strict";
    var t = '',
        n = 0,
        i = 0,
        v = '';

    majreferentielListe(c, s, o);

    t  = '<table style="white-space:pre;">'
       + '<thead><tr>'
       + '<td>' + headers[c]
       + '<a class="sbd" onclick="' + "sortTable('tb_refl',0,true, 't');" + '">▼</a>'
       + '<a class="sbd" onclick="' + "sortTable('tb_refl',0,false,'t');" + '">▲</a>'
       + '</td>'
       + '<td>nombre</td>'
       + '<td>ratio'
       + '<a class="sbd" onclick="' + "sortTable('tb_refl',1,true, 'n');" + '">▼</a>'
       + '<a class="sbd" onclick="' + "sortTable('tb_refl',1,false,'n');" + '">▲</a>'
       + '</td>'
       + '</tr></thead>'
       + '<tbody id="tb_refl">';

    par = null;
    par = [];

    for (v in card) {
        if (card.hasOwnProperty(v)) { n += card[v]; }
    }
    for (v in card) {
        if (card.hasOwnProperty(v)) {
            t += '<tr>'
               + '<td>' + v + '</td>'
               + '<td>' +  parseInt(card[v], 10) + '</td>'
               + '<td>' + (parseInt(card[v], 10) / n * 100).toFixed(2) + ' %</td>'
               + '</tr>';
            par[i] = [];
            par[i][0] = v;
            par[i][1] = card[v];
            i++;
        }
    }

    t += `</tbody>
          <tfoot>
          <tr>
          <td>TOTAL</td>
          <td>${n}</td>
          <td>100.00 %</td></tr></tfoot>
          </table>`;
    messageEtBoutons(c, t, i);
} // fin referentielListe

/**
 * crée une nouvelle colonne avec le nombre de valeurs dans chaque liste de valeurs
 * de la colonne c, séparées par s.
 * L'option o permet d'ignorer les répétitions d'une même valeur dans la liste, ignorer (true), sinon (false)
 * @param   {number}  c - indice de colonne
 * @param   {string}  s - séparateur des valeurs dans les listes
 * @param   {boolean} o - option, ignorer les répétitions d'un même élément dans la liste, ignorer (true), sinon (false)
 */
function compterValListe(c, s, o) {
    "use strict";
    var n = 0,
        h = '',
        i = 0,
        dl = datalen,

        p = JSON.stringify([
            ['colonne'.bold() + ' : ' + headers[c]],
            ['  séparateur'.bold() + ' : ' + s],
            ['  ignorer les répétitions'.bold() + ' : ' + (o ? 'oui' : 'non')]
        ]);
    bkp("compter le nombre d'éléments (liste)", p);

    for (i = 0; i < dl; i++) {
        if (data[i][c] === '') {
            n = 0;
        } else {
            if (!o) {
                n = data[i][c].split(s).length;
            } else {
                n = new Set(data[i][c].split(s)).size;
            }
        }
        data[i].splice(0, 0, n.toString());
    }

    h = 'nb élts:' + headers[c];
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin compterValListe

/**
 * crée une nouvelle colonne avec la liste de valeurs séparées par s de la colonne c
 * en appliquant les règles de conservation o1 et o2
 * @param   {number}  c  - indice de colonne
 * @param   {string}  s  - séparateur des valeurs dans les listes
 * @param   {string}  o1 - option de portée (à une occurrence, pour chaque série), (globalement, dans toute la liste)
 * @param   {string}  o2 - option de conservation (premières occurrences), (dernières occurrences)
 */
function compacterListe(c, s, o1, o2) {
    "use strict";
    var v = '',
        dl = datalen,
        i = 0,
        h = '',
        f = function(){},

    p = [
        ['colonne'.bold() + ' : ' + headers[c]],
        ['  séparateur'.bold() + ' : ' + s],
        ['  réduire les répétitions'.bold() + ' : ' + o1]
        ];

    if (o1 === 'globalement, dans toute la liste') {
		p.push('  conserver les'.bold() + ' : ' + o2);
    }

    bkp('compacter les listes', JSON.stringify(p));

    switch(o1 + ' : ' + o2) {
        case 'globalement, dans toute la liste : premières occurrences' :
            f = function(a) {
                // le tableau est transformé en Set puis en tableau
                var s = new Set(a);
                return [...s];
            };
            break;
        case 'globalement, dans toute la liste : dernières occurrences' :
            f = function(a) {
                // le tableau est inversé, transformé en Set puis en tableau puis inversé de nouveau
                var s = new Set(a.reverse());
                return [...s].reverse();
            };
            break;
        case 'à une occurrence, pour chaque série : premières occurrences' :
            f = function(a) {
                // le tableau est lu avec filter, avec une valeur témoin, si la valeur lue est identique, elle est exclue du résultat
                return a.filter( (e, i, ar) => i === 0 || e != ar[i-1] );
            };
            break;
        case 'à une occurrence, pour chaque série : dernières occurrences' :
            f = function(a) {
                // le tableau est inversé puis lu avec filter, avec une valeur témoin, si la valeur lue est identique, elle est exclue du résultat
                return a.reverse().filter( (e, i, ar) => i === 0 || e != ar[i-1] ).reverse();
                // @TODO cette implémentation n'a pas d'utilité, sauf pour modèle pour une transformation à venir qui compacte en regard des clés et conservera certaines valeurs (l'ordre sera important)
            };
            break;
   } // fin switch

    for (i = 0; i < dl; i++) {
        if (data[i][c] === '') {
            v = '';
        } else {
            v = f(data[i][c].split(s)).join(s);
        }
        data[i].splice(0, 0, v);
    }

    if (o1 === 'globalement, dans toute la liste') {
        h = headers[c] + ' => .(' + (o2 === 'premières occurrences' ? 'p' : 'd') + '/G)';
    } else {
        h = headers[c] + ' => .(1/S)';
    }
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin compacterListe


/**
 * crée une nouvelle colonne avec le résultat de l'opération o sur les listes de valeurs
 * séparées par s de la colonne c
 * @param   {number} c - indice de colonne
 * @param   {string} s - séparateur des valeurs dans les listes
 * @param   {string} o - opérateur ensembliste : (min), (max), (moy), (sum)
 */
function agregerValeurs(c, s, o) {
    "use strict";
    var v = '',
        dl = datalen,
        i = 0,
        h = '',

        p = JSON.stringify([
            ['colonne'.bold() + ' : ' + headers[c]],
            ['  séparateur'.bold() + ' : ' + s],
            ['  opération'.bold() + ' : ' + o]
        ]);
    bkp('agréger les valeurs de la liste', p);

    switch (o) {
    case 'min':
        for (i = 0; i < dl; i++) {
            if (data[i][c] === '') {
                v = '';
            } else {
                v = minArray(data[i][c].split(s)).toString();
            }
            data[i].splice(0, 0, v);
        }
        break;
    case 'max':
        for (i = 0; i < dl; i++) {
            if (data[i][c] === '') {
                v = '';
            } else {
                v = maxArray(data[i][c].split(s)).toString();
            }
            data[i].splice(0, 0, v);
        }
        break;
    case 'moy':
        for (i = 0; i < dl; i++) {
            if (data[i][c] === '') {
                v = '';
            } else {
                v = moyArray(data[i][c].split(s)).toString();
            }
            data[i].splice(0, 0, v);
        }
        break;
    case 'som':
        for (i = 0; i < dl; i++) {
            if (data[i][c] === '') {
                v = '';
            } else {
                v = somArray(data[i][c].split(s)).toString();
            }
            data[i].splice(0, 0, v);
        }
        break;
    }

    h = o + '(' + headers[c] + ')';
    headers.splice(0,0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin agregerValeurs

/**
 * fonction d'analyse des listes de valeurs séparées par s
 * de la colonne c l'ordre ft des valeurs dans une liste est également vérifié
 * @param   {number} c  - indice de colonne
 * @param   {string} s  - séparateur des valeurs dans les listes
 * @param   {string} ft - ordre : (alpha), (num)
 */
function formatListe(c, s, ft) {
    "use strict";
    var nbL = 0,
        nbElts = 0,
        nbL1Elt = 0,
        nbLVides = 0,
        nbMoy = 0.0,
        nbMoyALVides = 0.0,
        nbMin = 0,
        nbMax = 0,
        nbLVRepetees = 0,
        nbLTriees = 0,
        t = '',
        dl = datalen,
        i = 0,
        j = 0,
        v = [], // liste de valeurs
        d = []; // liste de nombres

    function sf(a, b) { return st2float(a) - st2float(b); }

    for (i = 0; i < dl; i++) {
        v = [];

        if (data[i][c] === '') {
            nbLVides++;
            continue;
        }

        nbL++;
        v = data[i][c].split(s);
        nbElts += v.length;

        if (v.length === 1) {
            nbL1Elt++;
            continue;
        }

        if (nbL - nbL1Elt === 1) { // @FIXME @TODO : gestion des valeurs vides
            // exécuté qu'une fois, permet d'initialiser nbMin et nbMax
            nbMin = v.length;
            nbMax = v.length;
        } else {
            nbMin = v.length < nbMin ? v.length : nbMin;
            nbMax = v.length > nbMax ? v.length : nbMax;
        }

        if (ft === 'alpha') {
            if (v.length > 1 && JSON.stringify(v) === JSON.stringify(v.sort())) {   nbLTriees++; }
        } else {
            if (v.length > 1 && JSON.stringify(v) === JSON.stringify(v.sort(sf))) { nbLTriees++; }
        }

        if (ft === 'alpha') {
            if (v.length > 1 && JSON.stringify(arrValUnique(v).sort()) !== JSON.stringify(v.sort())) { nbLVRepetees++; }
        } else {
            d = [];
            if (v.length > 1) {
                for (j = 0; j < v.length; j++) { d[j] = st2float(v[j]); }
                if (JSON.stringify(arrValUnique(d).sort(sf)) !== JSON.stringify(d.sort(sf))) { nbLVRepetees++; }
            }
        }

    }

    nbMoy = nbElts / nbL;
    nbMoy = nbMoy.toFixed(2);
    nbMoyALVides = nbElts / (nbL + nbLVides);
    nbMoyALVides = nbMoyALVides.toFixed(2);

    t  = `<table style="white-space:pre;">
          <thead><tr><td>analyse : ${headers[c]}</td>
          <td>résultat</td>
          </tr></thead>
          <tbody>
          <tr><td>nombre de listes vides</td><td>${nbLVides}</td></tr>
          <tr><td>nombre de listes non-vides</td><td>${nbL}</td></tr>
          <tr><td><i>dont nombre de listes d'un seul élément</i></td><td>${nbL1Elt}</td></tr>
          <tr><td>nombre moyen d'éléments (listes avec élément)</td><td>${nbMoy}</td></tr>
          <tr><td>nombre moyen d'éléments (global)</td><td>${nbMoyALVides}</td></tr>`;
    if (nbL - nbL1Elt >= 1) {
        t += `<tr><td>nombre min-max d'éléments (> 1)</td><td>${nbMin}-${nbMax}</td></tr>`;
    }
    t += `<tr><td>nombre de listes avec des éléments répétés</td><td>${nbLVRepetees}</td></tr>
          <tr><td>nombre de listes triées</td><td>${nbLTriees}</td></tr>
          </tbody>
          </table>`;

    messageEtBoutons('', t, 10, 'nocsv');
} // fin formatliste

/**
 * calcule le nombre d'éléments commun et le nombre nombre total d'éléments entre 2 liste de données
 * @param   {string} d1 - donnée 1
 * @param   {string} d2 - donnée 2
 * @param   {string} s1 - séparateur des valeurs dans les listes de d1
 * @param   {string} s2 - séparateur des valeurs dans les listes de d2
 */
function similariteEntreSelon(d1, d2, s1, s2) {
  var nbCom = 0,
      nbTot = 0,
      v1 = d1 === '',
      v2 = d2 === '',
      a1 = [],
      a2 = [];

  if (!v1 && !v2) {
      a1 = arrValUnique(d1.split(s1));
      a2 = arrValUnique(d2.split(s2));
      nbTot = arrValUnique(a1.concat(a2)).length;
      nbCom = aInterB(a1, a2).length;
  } else {
      nbTot += v1 ? 0 : arrValUnique(d1.split(s1)).length;
      nbTot += v2 ? 0 : arrValUnique(d2.split(s2)).length;
  }

  return [nbCom, nbTot];
} // fin similariteEntreSelon


// ............................................... valeurs en liste : structure


/**
 * sur la sélection multiple cs de colonnes,
 * supprime le délimiteur v en début et à la fin des valeurs
 * dans les listes de valeurs séparées par s
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} s  - séparateur des valeurs dans les listes
 * @param   {string} v  - délimiteur (généralement guillemet, apostrophe)
 */
function suppDelimListe(cs, s, v) {
    "use strict";
    var d = [],
        d2 = '',
        l = 0,
        cpt = 0,
        cptL = 0,
        dl = datalen,
        c = 0,
        i = 0,
        j = 0,
        pl = '',
        plL = '',
        m = '',

        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  séparateur'.bold() + ' : ' + s],
            ['  délimiteur'.bold() + ' : ' + v]
        ]);
    bkp('supprimer leur délimiteur (liste)', p);

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
            headers[c] = supprimerDelimiteurTexte(headers[c], v);
            for (i = 0; i < dl; i++) {
                d = data[i][c].split(s);
                l = d.length;
                for (j = 0; j < l; j++) {
                    d2 = supprimerDelimiteurTexte(d[j], v);
                    if (d[j] !== d2) { cpt++; }
                    d[j] = d2;
                }
                if (data[i][c] !== d.join(s)) { cptL++; }
                data[i][c] = d.join(s);
            }
        }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    plL = cptL > 1 ? '' : 's';
    m = cpt === 0 ? 'aucune modification' : cpt + ' valeur' + pl + ' corrigée' + pl + ' (' + cptL + ' ligne' + plL + ' impactée' + plL + ')';
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin suppDelimListe

/**
 * sur la sélection multiple cs de colonnes,
 * supprime les espaces en début/fin des valeurs
 * dans les listes de valeurs séparées par s
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} s  - séparateur des valeurs dans les listes
 */
function supprimerEspacesListes(cs, s) {
    "use strict";
    var d = [],
        l = 0,
        v = '',
        cpt = 0,
        cptL = 0,
        dl = datalen,
        c = 0,
        i = 0,
        j = 0,
        pl = '',
        plL = '',
        m = '',

        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  séparateur'.bold() + ' : ' + s]
        ]);
    bkp('enlever les espaces en début/fin (liste)', p);

    for (c = 0; c < cs.length; c++) { // pour chaque colonne sélectionnée
        if (cs[c].selected) {
            headers[c] = trim(headers[c]);
            for (i = 0; i < dl; i++) { // pour chaque ligne
                d = data[i][c].split(s);
                l = d.length;
                for (j = 0; j < l; j++) { // pour chaque valeur de la liste
                    v = trim(d[j]);
                    if (d[j] !== v) { cpt++; }
                    d[j] = v;
                }
                v = d.join(s);
                if (data[i][c] !== v) { cptL++; }
                data[i][c] = v;
            }
        }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    plL = cptL > 1 ? '' : 's';
    m = cpt === 0 ? 'aucune modification' : cpt + ' valeur' + pl + ' corrigée' + pl + ' (' + cptL + ' ligne' + plL + ' impactée' + plL + ')';
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin supprimerEspacesListes

/**
 * vérifie que le séparateur s2 sélectionné par l'utilisateur
 * n'apparaît pas déjà dans les listes de valeurs séparées par s1
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} s1 - ancien séparateur des valeurs dans les listes
 * @param   {string} s2 - nouveau séparateur des valeurs dans les listes
 * @see     {@link changerSeparateurListes}
 */
function testerSeparateurListes(cs, s1, s2) {
    "use strict";
    var reg = new RegExp(s2, 'g'),
        match = '',
        tot = 0,
        cptd = [],
        t = '',
        dl = datalen,
        i = 0;

    function f(x, j) {
        cptd[j] = 0;
        if (x.selected) {
            for (i = 0; i < dl; i++) {
                t = data[i][j].split(s1).join('');
                while (match = reg.exec(t)) { // il ne s'agit pas d'un test d'égalité, ne pas mettre "=="
                     cptd[j] += match.length;
                     tot += match.length;
                }
            }
        }
    }

    Array.prototype.forEach.call(cs, f);

    // dans le cas le plus simple
    if (tot === 0) {
        message(`test OK pour le séparateur "${s2}"<br />Vous pouvez changer le séparateur sans risque.`);
        return;
    }

    // dans le cas où certaines valeurs contiennent déjà le caractère séparateur s
    t  = `<p>Le caractère ${s2} a été détecté ${tot} fois.<br />
          Il est préférable d'essayer un autre caractère séparateur.</p>
          <table style="white-space:pre;">
          <thead><tr><td>colonne concernée</td><td>nombre de détection</td></tr></thead>
          <tbody>`;

    // construction du tableau de résultat
    tot = 3;
    for (i = 0; i < cptd.length; i++) {
        if (cptd[i] !== 0) {
             t += '<tr><td>colonne : ' + headers[i] + '</td><td>' + cptd[i] + '</td></tr>';
             tot++;
        }
    }

    t += `</tbody>
          </table>`;

    messageEtBoutons('', t, tot, 'nocsv');
} // fin testerSeparateurListes

/**
 * sur la sélection multiple cs de colonnes,
 * modifie par s2 le séparateur de valeurs
 * dans les listes de valeurs séparées par s1
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} s1 - ancien séparateur des valeurs dans les listes
 * @param   {string} s2 - nouveau séparateur des valeurs dans les listes
 * @see     {@link testerSeparateurListes}
 */
function changerSeparateurListes(cs, s1, s2) {
    "use strict";
    var i = 0,
        dl = datalen,

        p = JSON.stringify([
                ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
                ['  séparateur actuel'.bold() + ' : <br> ' + s1],
                ['  nouveau séparateur'.bold() + ' : <br> ' + s2]
            ]);
    bkp('changer séparateur (liste)', p);

    function f(x, j) {
        if (x.selected) {
            for (i = 0; i < dl; i++) {
                data[i][j] = data[i][j].split(s1).join(s2);
            }
        }
    }

    Array.prototype.forEach.call(cs, f);

    resultat();
    message('terminé');

} // fin supprimerEspacesListes

/**
 * réalise l'opération a sur les listes de couples {clé - opérateur o - valeur}
 * séparés par le séparateur s
 * @param   {number} c - indice de colonne
 * @param   {string} s - séparateur des valeurs dans les listes
 * @param   {string} o - opérateur clé => valeur
 * @param   {string} a - opération : (conserver les clés uniquement), (conserver les valeurs uniquement), (inverser le sens de l'association)
 */
function modifierAssociations(c, s, o, a) {
    "use strict";
    var dl = datalen,
        p = JSON.stringify([
            ['colonne'.bold() + ' : ' + headers[c]],
            ['  séparateur'.bold() + ' : ' + s],
            [],
            ['  opérateur clé/valeur'.bold() + ' : ' + o],
            ['  opération'.bold() + ' : ' + a]
        ]),
        d = [],
        l = 0,
        m = 0,
        i = 0,
        j = 0;
    bkp('modifier les associations (clé/valeur)', p);

    switch (a) {
    case 'conserver les clés uniquement':
        for (i = 0; i < dl; i++) {
            d = data[i][c].split(s);
            l = d.length;
            for (j = 0; j < l; j++) {
                if (d[j] === '') { continue; }
                m = d[j].split(o).length;
                if (m !== 2) { continue; }
                d[j] = d[j].split(o)[0];
            }
            data[i][c] = d.join(s);
        }
        break;
    case 'conserver les valeurs uniquement':
        for (i = 0; i < dl; i++) {
            d = data[i][c].split(s);
            l = d.length;
            for (j = 0; j < l; j++) {
                if (d[j] === '') { continue; }
                m = d[j].split(o).length;
                if (m !== 2) { continue; }
                d[j] = d[j].split(o)[1];
            }
            data[i][c] = d.join(s);
        }
        break;
    case 'inverser le sens des associations':
        for (i = 0; i < dl; i++) {
            if (data[i][c] === '') { continue; }
            d = data[i][c].split(s);
            l = d.length;
            for (j = 0; j < l; j++) {
                if (d[j] === '') { continue; }
                m = d[j].split(o).length;
                if (m !== 2) { continue; }
                d[j] = d[j].split(o)[1] + o + d[j].split(o)[0];
            }
            data[i][c] = d.join(s);
        }
        break;
    }

    resultat();
    message('terminé');
} // fin modifierAssociations

/**
 * inverser l'ordre des valeurs des listes de la colonne c, selon l'opération o
 * @param   {number} c  - indice de colonne
 * @param   {string} s  - séparateur des valeurs dans les listes
 * @param   {string} a  - opération d'inversion de l'ordre (des valeurs des listes),(des valeurs des listes imbriquées seulement),(des listes imbriquées et des valeurs dans ces listes)
 * @param   {string} si - séparateur interne des listes imbriquées
 */
function inverserListes(c, s, a, si) {
    "use strict";
    var dl = datalen,
         h = '',
         i = 0,
         f = function(){},

         p = [
              ['colonne'.bold() + ' : ' + headers[c]],
              ['  séparateur'.bold() + ' : ' + s],
              ['  opération'.bold() + ' : ' + a]
         ];

    if (a !== 'des valeurs des listes') {
        p.push('  séparateur interne des listes imbriquées'.bold() + ' : ' + si);
    }

    bkp("inverser l'ordre des éléments dans les listes", JSON.stringify(p));

    // en fonction de l'opérateur, définition de la fonction de filtre
    switch(a) {
      case 'des valeurs des listes' :
        f = function(a) { return a.split(s).reverse().join(s); };
        break;
      case 'des valeurs des listes imbriquées seulement' :
        f = function(a) { return a.split(s).map(x => x.split(si).reverse().join(si)).join(s); };
        break;
      case 'des listes imbriquées et des valeurs dans ces listes' :
        f = function(a) { return a.split(s).map(x => x.split(si).reverse().join(si)).reverse().join(s); };
        break;
    }

    // traitement
    for (i = 0; i < dl; i++) {
        data[i][c] = f(data[i][c]);
    }

    resultat();
    message('terminé');
} // fin inverserListes


// ................................................. valeurs en liste : filtres


/**
 * filtre les valeurs des listes de la colonne c, selon l'opération o par rapport à cc
 * @param   {number} c  - indice de colonne
 * @param   {string} s  - séparateur des valeurs dans les listes
 * @param   {string} o  - opérateur de comparaison numérique (<),(≤),(>),(≥),(=),(≠)
 * @param   {string} cc - clé de comparaison
 */
function filtrerValeursListe(c, s, o, cc) {
    "use strict";
    var dl = datalen,
         h = '',
         i = 0,
         a = [],
         f = function(){},

         p = JSON.stringify([
             ['les valeurs de'.bold() + ' : ' + headers[c]],
             ['  séparateur'.bold() + ' : ' + s],
             ['sont '.bold() + ' : ' + o],
             ['à'.bold() + ' : ' + headers[cc]]
         ]);

     bkp("filtrer ou exclure les valeurs d'une liste", p);

     // en fonction de l'opérateur, définition de la fonction de filtre
     switch(o) {
       /* comparaison numérique */
       case '<' : f = function(a,b) { return a < b; };
         break;
       case '≤' : f = function(a,b) { return a <= b; };
         break;
       case '>' : f = function(a,b) { return a > b; };
         break;
       case '≥' : f = function(a,b) { return a >= b; };
         break;
       case '=' : f = function(a,b) { return a === b; };
         break;
       case '≠' : f = function(a,b) { return a !== b; };
         break;
     }

     // traitement
     for (i = 0; i < dl; i++) {
       if (data[i][c] === '' || data[i][cc] === '') {
         data[i].unshift('');
         continue;
       }
       a = data[i][c].split(s);
       data[i].unshift(a.filter(x => x !== '' && f(st2float(x), st2float(data[i][cc]))).join(s));
     }

     h = '[' + headers[c] + '] ' + o + ' ' + headers[cc];
     headers.splice(0, 0, h);

     resultat();
     message('terminé');
     rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // function filtrerValeursListe

/**
 * pour les couples clé/valeur de la colonne c dont les clés satisfont
 * la comparaison avec les éléments de la colonne c,
 * conserve ou exclut ces couples, selon l'opération ao
 * @param   {number} c  - indice de colonne
 * @param   {string} s  - séparateur des valeurs dans les listes
 * @param   {string} o  - opérateur clé => valeur
 * @param   {string} ao - opération : (conserver), (exclure)
 * @param   {number} cc - indice de colonne de la colonne de contrôle
 * @param   {string} sc - séparateur des valeurs dans les listes de la colonne de contrôle
 */
function reduireValeursListe(c, s, o, ao, cc, sc) {
    "use strict";
    var dl  = datalen,
         h  = '',
         i  = 0,
         a  = [],
         ac = new Set(),
         f = function(){},

         p = JSON.stringify([
             ['pour chaque couple clé/valeur de'.bold() + ' : ' + headers[c]],
             ['  séparateur'.bold() + ' : ' + s],
             ['  opérateur clé/valeur'.bold() + ' : ' + o],
             ['si la clé est trouvée dans'.bold() + ' : ' + headers[cc]],
             ['  séparateur'.bold() + ' : ' + sc],
             ['alors'.bold() + ' : ' + ao]
         ]);

    bkp("réduire la liste à certaines clés", p);

    // en fonction de l'opération, définition de la fonction de filtre
    switch(ao) {
      case 'conserver' : f = function(a) { return a.filter(x =>   ac.has(x.split(o)[0]) ); };
        break;
      case 'exclure'   : f = function(a) { return a.filter(x => ! ac.has(x.split(o)[0]) ); };
        break;
    }

    // traitement
    for (i = 0; i < dl; i++) {
        // split des valeurs de contrôle (cc) => tableau associatif
        ac = new Set(data[i][cc].split(sc) );
        // split des valeurs (c)
        a = data[i][c].split(s);

        data[i].unshift( f(a).join(s) );
    } // fin for

    h = ao + ' (' + headers[c] + ' | ' + headers[cc] + ')';
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // function reduireValeursListe


// .......................................... valeurs en liste : enrichissement


/**
 * utilise une table de correspondance pour recoder les valeurs dans une liste de valeurs
 * ou analyse la qualité des correspondances
 * @param   {number}  c - indice de colonne (liste de valeurs à traduire)
 * @param   {string}  s - séparateur des valeurs dans les listes à recoder
 * @param   {string}  t - table de correspondance,
 * @param   {string} ts - séparateur des colonnes dans la table de correspondance
 * @param   {string} ck - indice de colonne de clé dans la table de correspondance
 * @param   {string} cv - indice de colonne de valeur dans la table de correspondance
 * @param   {string}  o - option de traitement parmi (les valeurs sans correspondance), (uniquement les correspondances, sans recodage), (uniquement les correspondances, recodées), (toutes les valeurs, les recoder si correspondance)
 */
function recoderListes(c, s, t, ts, ck, cv, o) {
    "use strict";
    var i = 0,
       dl = datalen,
       aa = t.split('\n'),
       mp = new Map(),

       p = JSON.stringify([
               ['colonne'.bold() + ' : ' + headers[c]],
               ['  séparateur (de valeur)'.bold() + ' : ' + s],
               ['table de correspondance'.bold()],
               ['  longueur table recodage'.bold() + ' : ' +  aa.length],
               ['  séparateur (de colonne)'.bold() + ' : ' +  ts],
               ['  valeurs recherchées'.bold() + ' : ' +  aa[0].split(ts)[ck] ],
               ['  remplacées par'.bold() + ' : ' +  aa[0].split(ts)[cv] ],
               ['règle de fusion'.bold() + ' : ' +  o]
           ]);
   bkp("recoder des clés par des valeurs (ou l'inverse)", p);

    function f(x) {
      let y = [];
      y = trimNL(x).split(ts);
      mp.set(y[ck], y[cv]);
    }

    // correspondances => objet Map
    aa.shift(); // on supprime l'en-tête
    aa.forEach(f);

    switch(o) {
      case 'les valeurs sans correspondance':
          for (i = 0; i < dl; i++) {
              data[i].unshift(data[i][c].split(s).filter( x => ! mp.has(x) ).join(s) );
          }
          break;

      case 'uniquement les correspondances, sans recodage':
          for (i = 0; i < dl; i++) {
              data[i].unshift(data[i][c].split(s).filter( x => mp.has(x) ).join(s) );
          }
          break;

      case 'uniquement les correspondances, recodées':
          for (i = 0; i < dl; i++) {
              data[i].unshift(data[i][c].split(s).filter( x => mp.has(x) ).map( x => mp.get(x) ).join(s) );
          }
          break;

      case 'toutes les valeurs, les recoder si correspondance':
          for (i = 0; i < dl; i++) {
              data[i].unshift(data[i][c].split(s).map( x => mp.has(x) ? mp.get(x) : x ).join(s) );
          }
          break;
    } // fin du switch

    headers.unshift('~recodage');

    resultat();
    message('terminé');
} // recoderListes

/**
 * @summary combine les listes de valeurs de deux colonnes
 * @description crée une nouvelle colonne
 * avec une liste de couples clé - opérateur o - valeur séparés par s
 * à partir des valeurs des listes de valeurs de c1 séparées par s1
 * et des listes de valeurs de c2 séparées par s2.
 * Dans les couples clé => valeur
 * les valeurs des listes s1 deviennent les "clés",
 * et les valeurs des listes de s2 deviennent les "valeurs".
 * @param   {number} c1 - indice de la première colonne
 * @param   {number} c2 - indice de la seconde colonne
 * @param   {string} s1 - séparateur des valeurs dans les listes de la première colonne
 * @param   {string} s2 - séparateur des valeurs dans les listes de la seconde colonne
 * @param   {string} s  - séparateur des valeurs dans les listes résultats de la combinaison
 * @param   {string} o  - opérateur clé => valeur dans la liste résultat
 */
function combiner(c1, s1, c2, s2, s, o) {
    "use strict";
    var d = [],
        d1 = [],
        d2 = [],
        l1 = 0,
        l2 = 0,
        dl = datalen,
        i = 0,
        j = 0,
        h = '',

        p = JSON.stringify([
            ['colonne 1'.bold() + ' : ' + headers[c1]],
            ['  séparateur'.bold() + ' : ' + s1],
            [],
            ['colonne 2'.bold() + ' : ' + headers[c2]],
            ['  séparateur'.bold() + ' : ' + s2],
            [],
            ['nouveau séparateur'.bold() + ' : ' + s],
            ['opérateur (clé/valeur)'.bold() + ' : ' + o]
        ]);
    bkp('combiner deux listes', p);

    for (i = 0; i < dl; i++) {
        j = 0;
        d1 = data[i][c1].split(s1);
        d2 = data[i][c2].split(s2);
        l1 = d1.length;
        l2 = d2.length;

        d = [];
        if (l1 > l2) {
            for (j = 0;  j < l2; j++) { d[d.length] = d1[j] + o + d2[j]; }
            for (j = l2; j < l1; j++) { d[d.length] = d1[j] + o;         }
        } else {
            for (j = 0;  j < l1; j++) { d[d.length] = d1[j] + o + d2[j]; }
            for (j = l1; j < l2; j++) { d[d.length] =         o + d2[j]; }
        }

        data[i].splice(0, 0, d.join(s));
    }

    h = '[' + headers[c1] + ':' + headers[c2] + ']';
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin combiner

/**
 * @summary calcule une opération ensembliste entre deux colonnes contenant des listes de valeurs
 * @description crée une nouvelle colonne avec le résultat d'une opération
 * ensembliste o entre les données de 2 colonnes ca et cb
 * contenant des listes de valeurs séparées par respectivement sa et sb.
 * Le nombre d'occurrences de chaque valeur du résultat
 * varie en fonction de l'opérateur de compression oc.
 * L'algorithme se base sur une traduction de l'opération en
 * une combinaison de 3 sous-ensembles :
 * - éléments n'appartenant qu'à A
 * - éléments n'appartenant qu'à B
 * - éléments communs à A et B
 * @param   {number} ca - indice de colonne
 * @param   {number} cb - indice de colonne
 * @param   {string} sa - séparateur des valeurs dans les listes de ca
 * @param   {string} sb - séparateur des valeurs dans les listes de cb
 * @param   {string} o  - opération ensembliste : (A union B), (A inter B), (A moins B), (B moins A), (A ou exclusif B)
 * @param   {string} oc - opération de compression : (unique), (max), (min), (toutes), (nb A), (nb B)
 * @param   {string} sr - séparateur des valeurs de l'ensemble résultat
 */
function sousEnsemble(ca, sa, cb, sb, o, oc, sr) {
    "use strict";
    var a = [],
        b = [],
        c = [],
        p = '',
        lv = [],
        v = '',
        r = [],
        nb = 0,
        dl = datalen,
        i = 0,
        j = 0,
        h = '',
        trad = {
            'A union B': 'de A et de B (réunion)',
            'A inter B': 'communs à A et B (intersection)',
            "A moins B": "de A, n'appartenant pas à B (différence)",
            "B moins A": "de B, n'appartenant pas à A (différence)",
            'A ou exclusif B': 'de A ou de B, sauf ceux communs à A et B (différence symétrique)',
            "unique": "ne conserver qu'une seule occurrence",
            "max": "conserver le plus grand nombre d'occurrences entre A et B",
            "min": "conserver le plus petit nombre d'occurrences entre A et B",
            'toutes': 'conserver toutes les occurrences de A et de B',
            "nb A": "conserver le nombre d'occurrences de A",
            "nb B": "conserver le nombre d'occurrences de B"
        };

    p = JSON.stringify([
        ['liste A'.bold() + ' : ' + headers[ca]],
        ['  séparateur'.bold() + ' : ' + sa],
        ['liste B'.bold() + ' : ' + headers[cb]],
        ['  séparateur'.bold() + ' : ' + sb],
        ['conserver'],
        ['  tous les éléments'.bold() + ' : ' + trad[o]],
        ['  pour les valeurs répétées'.bold() + ' : ' + trad[oc]],
        ['  en séparant les valeurs par'.bold() + ' : ' + sr]
    ]);

    bkp('déduire des sous-ensembles à partir de 2 listes', p);

    for (i = 0; i < dl; i++) {
        a = data[i][ca].split(sa);
        b = data[i][cb].split(sb);

        switch (o) {
        case 'A union B':
            lv[i] = a.concat(b);
            break;
        case 'A inter B':
            lv[i] = aInterB(a, b);
            break;
        case 'A moins B':
            lv[i] = aMoinsB(a, b);
            break;
        case 'B moins A':
            // ici, on inverse les opérandes pour obtenir "b moins a"
            lv[i] = aMoinsB(b, a);
            break;
        case 'A ou exclusif B':
            lv[i] = aMoinsB(a, b).concat(aMoinsB(b, a));
            break;
        }
    }

    switch (oc) {
    case 'unique':
        for (i = 0; i < dl; i++) {
            data[i].splice(0, 0, arrValUnique(lv[i]).join(sr));
        }
        break;
    case 'max':
        for (i = 0; i < dl; i++) {
            a = tab1hash(data[i][ca].split(sa));
            b = tab1hash(data[i][cb].split(sb));
            c = arrValUnique(lv[i]);
            r = [];
            for (j = 0; j < c.length; j++) {
                v = c[j];
                nb = minmax(a, b, v).max;
                r.push.apply(r, new Array(nb).fill(v));
            }
            data[i].splice(0, 0, r.join(sr));
        }
        break;
    case 'min':
        for (i = 0; i < dl; i++) {
            a = tab1hash(data[i][ca].split(sa));
            b = tab1hash(data[i][cb].split(sb));
            c = arrValUnique(lv[i]);
            r = [];
            for (j = 0; j < c.length; j++) {
                v = c[j];
                // pour certaines opérations ensemblistes (différences de ...)
                // le minmax.min renverra 0...
                if (a.hasOwnProperty(v) && b.hasOwnProperty(v)) {
                    nb = minmax(a, b, v).min;
                } else {
                    // pour s'en prémunir, on récupère bien le "réel" min (qui vaut d'ailleurs le max)
                    if (!a.hasOwnProperty(v)) { nb = b[v].length; }
                    if (!b.hasOwnProperty(v)) { nb = a[v].length; }
                }
                r.push.apply(r, new Array(nb).fill(v));
            }
            data[i].splice(0, 0, r.join(sr));
        }
        break;
    case 'toutes':
        for (i = 0; i < dl; i++) {
            a = tab1hash(data[i][ca].split(sa));
            b = tab1hash(data[i][cb].split(sb));
            c = arrValUnique(lv[i]);
            r = [];
            for (j = 0; j < c.length; j++) {
                v = c[j];
                if (a.hasOwnProperty(v)) { // si l'opération n'est pas l'intersection
                    nb = a[v].length; // on prend le nombre d'occurrence de A
                    r.push.apply(r, new Array(nb).fill(v));
                }
                if (b.hasOwnProperty(v)) { // si l'opération n'est pas l'intersection
                    nb = b[v].length; // on prend le nombre d'occurrence de B
                    r.push.apply(r, new Array(nb).fill(v));
                }
            }
            data[i].splice(0, 0, r.join(sr));
        }
        break;
    case 'nb A':
        for (i = 0; i < dl; i++) {
            a = tab1hash(data[i][ca].split(sa));
            c = arrValUnique(lv[i]);
            r = [];
            for (j = 0; j < c.length; j++) {
                v = c[j];
                if (a.hasOwnProperty(v)) { // si la valeur existe dans A
                    nb = a[v].length; // alors on prend le nombre d'occurrence de A
                    r.push.apply(r, new Array(nb).fill(v));
                } else { // si la valeur n'existe pas dans A (cas : A union B)
                    r.push(v);
                }
            }
            data[i].splice(0, 0, r.join(sr));
        }
        break;
    case 'nb B':
        for (i = 0; i < dl; i++) {
            b = tab1hash(data[i][cb].split(sb));
            c = arrValUnique(lv[i]);
            r = [];
            for (j = 0; j < c.length; j++) {
                v = c[j];
                if (b.hasOwnProperty(v)) { // si la valeur existe dans B
                    nb = b[v].length; // alors on prend le nombre d'occurrence de B
                    r.push.apply(r, new Array(nb).fill(v));
                } else { // si la valeur n'existe pas dans B (cas : A union B)
                    r.push(v);
                }
            }
            data[i].splice(0, 0, r.join(sr));
        }
        break;
    }

    h = o + ' (' + oc + ')';
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin sousEnsemble

/**
 * calcule toutes les permutations des valeurs des listes de la colonne c
 * @param   {number} c  - indice de colonne
 * @param   {string} sv - séparateur des valeurs dans les listes de c
 * @param   {string} sp - séparateur des permutations de l'ensemble résultat
 */
function permuterValeurs(c, sv, sp) {
    "use strict";
    var dl = datalen,
        i = 0,
        start = 0,
        a = [],
        t = [],
        t2 = [],
        p = [],
        r = [],
        h = '';

    p = JSON.stringify([
        ['liste'.bold() + ' : ' + headers[c]],
        ['  séparateur des valeurs'.bold() + ' : ' + sv],
        ['  séparateur des permutations'.bold() + ' : ' + sp]
    ]);

    bkp('déduire des sous-ensembles à partir de 2 listes', p);


    /* source : https://www.geeksforgeeks.org/print-all-permutation-of-array-using-javascript/ (recursion) */
    function approach1Fn(a, start) {
      if (start === a.length - 1) {
        r.push([...a]);
        return;
      }
      for (let i = start; i < a.length; i++) {
        [a[start], a[i]] = [a[i], a[start]];
        approach1Fn(a, start + 1);
        [a[start], a[i]] = [a[i], a[start]]; 
      }
    } // fin approach1Fn

    for (i = 0; i < dl; i++) { // pour chaque ligne
      a = data[i][c].split(sv);
      r = [];
      approach1Fn(a, 0);
      data[i].splice(0,0,r.map(x => x.join(sv)).join(sp));
    }

    h = "permutations ~ " + headers[c];
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin permuterValeurs

/**
 * mélange les valeurs des listes de la colonne c en se basant sur le mélange de Fisher–Yates
 * @param   {number} c - indice de colonne
 * @param   {string} s - séparateur des valeurs dans les listes de c
 */
function melangerValeurs(c, s) {
    "use strict";
    var dl = datalen,
        i = 0,
        p = '',
        h = '';

    p = JSON.stringify([
        ['liste'.bold() + ' : ' + headers[c]],
        ['  séparateur'.bold() + ' : ' + s]
    ]);

    bkp('mélanger les valeurs de la liste', p);

    for (i = 0; i < dl; i++) {
      data[i].splice(0,0,shuffle_array(data[i][c].split(s)).join(s));
    }

    h = "mélange ~ " + headers[c];
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin melangerValeurs


// ........................................................ traduire les listes


/**
 * normalisation 1FN selon Merise : traduit les listes de valeurs de la colonne c en autant de lignes
 * @param   {number} c - indice de colonne
 * @param   {string} s - séparateur des valeurs dans les listes
 */
function traduireEnLignes(c, s) {
    "use strict";
    var a = [],
        i = 0,
        v = '',
        dl = datalen,

        p = JSON.stringify([
            ['colonne'.bold() + ' : ' + headers[c]],
            ['  séparateur'.bold() + ' : ' + s]
        ]);
    bkp('traduire les listes en lignes', p);

    for (i = dl - 1; i >= 0; i--) { // on part de la fin pour éviter de recalculer l'index i
        a = data[i][c].split(s);
        while (a.length ) {
            v = a.pop();
            data.splice(i+1, 0, data[i].slice(0) ); // copie de data[i]
            data[i+1][c] = v;
        }
        data.splice(i, 1); // on supprime l'original
    }

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouveau nombre de lignes'.bold() + ' : ' + data.length]));
} // fin traduireEnLignes

/**
 * traduit les listes de valeurs séparées par s
 * de la colonne c en autant de colonnes et selon o : conserve ou non la colonne originale c
 * @param   {number} c   - indice de colonne
 * @param   {string} s   - séparateur des valeurs dans les listes
 * @param   {string} dir - option de direction : (de la gauche vers la droite), (de la droite vers la gauche)
 * @param   {string} o   - option sur la colonne c : (remplacer la colonne), (conserver la colonne)
 */
function traduireEnColonnes(c, s, dir, o) {
    "use strict";
    var aa = [],
        h = [],
        l = 0,
        i = 0,
        c2 = parseInt(c,10) + 1,
        dl = datalen,

        p = JSON.stringify([
            ['colonne'.bold() + ' : ' + headers[c]],
            ['  séparateur'.bold() + ' : ' + s],
            ['  direction'.bold() + ' : ' + dir],
            ['  option'.bold() + ' : ' + o]
        ]);
    bkp('traduire les listes en colonnes', p);

    // calcul nouveau nombre de colonnes
    for (i = 0; i < dl; i++) {
        aa[i] = data[i][c].split(s);
        l = l < aa[i].length ? aa[i].length : l;
    }

    // rallongement à la longueur l de tous les tableaux complémentaires (listes éclatées)
    if (dir === 'de la gauche vers la droite') {
        for (i = 0; i < dl; i++) { while (aa[i].length < l) { aa[i].push(''); } }
    } else {
        for (i = 0; i < dl; i++) { while (aa[i].length < l) { aa[i].unshift(''); } }
    }

    // concaténation data (données originales) + aa[i] (complément : listes éclatées)
    for (i = 0; i < dl; i++) {
        if (o === 'conserver la colonne') {
            data[i].splice(c2, 0, ...aa[i]);
        } else { // o === 'remplacer la colonne'
           data[i].splice(c,  1, ...aa[i]);
        }
    }

    for (i = 0; i < l; i++) { h[i] = headers[c] + "_" + (i+1); }
    if (dir === 'de la droite vers la gauche') { h.reverse(); }

    if (o === 'conserver la colonne') {
        headers.splice(c2, 0, ...h);
    } else { // o === 'remplacer la colonne'
        headers.splice(c,  1, ...h);
    }

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouveau nombre de colonnes'.bold() + ' : ' + headers.length]));
} // fin traduireEnColonnes

/**
 * @summary traduit les listes de valeurs séparées par s de la colonne c en une matrice
 * @description 1. si sv est vide,
 * traduit les listes de valeurs séparées par s
 * de la colonne c en autant de colonne avec pour en-tête
 * la valeur de la liste et à l'intersection ligne/colonne
 * le nombre de valeur (en-tête) dans la liste étudiée.
 * 2. si sv n'est pas vide,
 * traduit les listes de couples {clé sv valeur} séparés
 * par s de la colonne c en autant de colonne avec pour en-tête
 * la clé du couple clé => valeur et à l'intersection ligne/colonne
 * la valeur du couple clé => valeur.
 * Si sr n'est pas vide, lorsque dans une même liste, plusieurs clés
 * envoient plusieurs valeurs alors elles seront séparées en fonction
 * de l'option d'interprétation o
 * @param   {number} c  - indice de colonne
 * @param   {string} sl - séparateur des valeurs dans les listes
 * @param   {string} sv - opérateur pour couple clé => valeur
 * @param   {string} sr - séparateur des valeurs pour les résultats
 * @param   {string} o   - option d'interprétation de "a => b => c" : (a : b => c), (a : b + c), (a : b + a : c), (a : b + a : c + b : c)
 */
function traduireEnMatrice(c, sl, sv, sr, o) {
    "use strict";
    var h2 = [],
        vm = [],
        vs = [],
        h = [],
        v = '',
        dl = datalen,
        i = 0,
        j = 0,
        j2 = '',
        vt = [],
        vtt = 0,
        dt = [],
        trad = {
          'a : b => c': 'clé "a" et valeur "b => c"',
          'a : b + c': '(a => b) + (a => c)',
          'a : b + a : c': '(a => b) + (b => c)',
          'a : b + b : c + a : c': '(a => b) + (b => c) + (a => c)'
        },
        p = [
            ['colonne'.bold() + ' : ' + headers[c]],
            ['  séparateur'.bold() + ' : ' + sl],
            ];

    if (sv !== '') {
        p.push('  opérateur clé/valeur'.bold() + ' : ' + sv);
        p.push('  séparateur de retour'.bold() + ' : ' + sr);
        p.push('  "a => b => c" signifie'.bold() + ' : ' + trad[o]);
    }
    bkp('traduire les listes en matrice', JSON.stringify(p));

    // calcul
    if (sv === '') { // les valeurs de la liste sont des "valeurs simples"
        for (i = 0; i < dl; i++) {
            v = data[i][c];
            vs[i] = v.split(sl); // liste des "valeurs simples"
            vm[i] = [];
            for (j = 0; j < vs[i].length; j++) { // pour chaque "valeur simple"
                if (!h.hasOwnProperty(vs[i][j])) { h[vs[i][j]] = ''; } // maj de la liste des en-têtes
                if (!vm[i].hasOwnProperty(vs[i][j])) { vm[i][vs[i][j]] = 0; }
                vm[i][vs[i][j]]++;
            }
        }
    } else { // les valeurs de la liste sont des couples {clé/valeur} de la liste
        for (i = 0; i < dl; i++) {
            v = data[i][c];
            vs[i] = v.split(sl); // liste des couples {clé/valeur}
            vm[i] = [];
            for (j = 0; j < vs[i].length; j++) { // pour chaque couple {clé/valeur}
                vt = vs[i][j].split(sv);
                vtt = vt.shift(); // partie "clé"
                if (!h.hasOwnProperty(vtt)) { h[vtt] = ''; } // maj de la liste des en-têtes
                if (vt.length) { // s'il y a une partie "valeur"
                    // a => b => c devient ...

                    // clé "a", valeur "b => c"
                    switch(o){
                    case 'a : b => c' :
                    vm[i][vtt] = (!vm[i].hasOwnProperty(vtt)) ? vt.join(sv) : vm[i][vtt] + sr + vt.join(sv);
                    break;

                    // développement
                    // a : b + c
                    // équivalent à (a => b) + (a => c)
                    case 'a : b + c' :
                    vm[i][vtt] = (!vm[i].hasOwnProperty(vtt)) ? vt.join(sr) : vm[i][vtt] + sr + vt.join(sr);
                    break;

                    // chaîne
                    // a => b
                    // b => c
                    // équivalent à (a => b) + (b => c)
                    case 'a : b + a : c' :
                    do{
                      if (!h.hasOwnProperty(vtt)) { h[vtt] = ''; } // maj de la liste des en-têtes
                      vm[i][vtt] = (!vm[i].hasOwnProperty(vtt)) ? vt[0] : vm[i][vtt] + sr + vt[0];
                      vtt = vt.shift();
                    }while(vt.length);
                    break;

                    // transitivité
                    // a => b
                    // b => c
                    // a => c
                    // équivalent à (a => b) + (b => c) + (a => c)
                    case 'a : b + b : c + a : c' :
                    do{
                      if (!h.hasOwnProperty(vtt)) { h[vtt] = ''; } // maj de la liste des en-têtes
                      vm[i][vtt] = (!vm[i].hasOwnProperty(vtt)) ? vt.join(sr) : vm[i][vtt] + sr + vt.join(sr);
                      vtt = vt.shift();
                    }while(vt.length);
                    } // fin du switch

                } // fin du traitement s'il y a une partie "valeur"
            } // fin du traitement des couples clé/valeur
        } // fin de la boucle sur les lignes
    }

    // tableau associatif
    for (j2 in h) {
        if (h.hasOwnProperty(j2)) {
            h2[h2.length] = j2;
        }
    }

    // intégrer le résultat du calcul sous forme de nouvelles colonnes
    for (i = 0; i < dl; i++) {
        for (j = 0; j < h2.length; j++) {
            dt[j] = (!vm[i].hasOwnProperty(h2[j])) ? '' : vm[i][h2[j]].toString();
        }
        data[i].splice(data[i].length, 0, ...dt);
    }

    headers.splice(headers.length, 0, ...h2);

    resultat();
    message('terminé');
} // fin traduireEnMatrice


// .................................................................... en-tête


/**
 * @summary renomme jusqu'à 6 en-têtes de colonne
 * @description si une colonne est renommée plusieurs fois, alors la dernière valeur l'emporte
 * si un champ de saisie est laissé vide, alors la colonne n'est pas renommée
 * @param   {string} ci - préfixe des 6 sélecteurs de colonne
 * @param   {string} vi - préfixe des 6 champs de saisie des nouvelles valeurs
 */
function renommer(ci, vi) {
    "use strict";
    var v = '',
        p = [],
        r = [],
        i = 0,
        n = 6, // nombre de champs pouvant être renommés simultanément
        pa = '';

    for (i = 1; i <= n; i++) {
        v = valeur(vi + i.toString());
        if (v !== '') {
            p.push([headers[valeur(ci.toString() + '-' + i.toString())]]);
            r.push([v]);
        }
    }
    pa = JSON.stringify(p);
    bkp("renommer l'en-tête", pa);

    for (i = 1; i <= n; i++) {
        v = valeur(vi + i.toString());
        if (v !== '') { headers[valeur(ci.toString() + '-' + i.toString())] = v; }
    }

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(r));
} // fin renommer

/**
 * affiche l'en-tête selon o : horizontalement ou verticalement
 * @param   {string} o - option : horizontalement (h), verticalement (v)
 */
function extraireEnTete(o) {
    "use strict";
    var t = '';

    t  = `<table style="white-space:pre;">
          <tbody><tr>`;
    if (o === 'h') {
        t += '<td>' + headers.join('</td><td>') + '</td>';
    } else {
        t += '<td>' + headers.join('</td></tr><tr><td>') + '</td>';
    }
    t += '</tr></tbody>';

    messageEtBoutons('', t, headers.length, 'nocsv');
}

/**
 * ajoute le préfixe s aux colonnes sélectionnées dans cs
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} s  - préfixe à ajouter
 */
function ajouterPrefixe(cs, s) {
    "use strict";
    var p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  préfixe'.bold() + ' : <br> ' + s]
        ]);
    bkp('ajouter un préfixe', p);

    function f(x, i) {
        if (x.selected) { headers[i] = s + headers[i]; }
    }

    Array.prototype.forEach.call(cs, f);

    resultat();
    message('terminé');
}

/**
 * remplace le préfixe s par s2 aux colonnes sélectionnées dans cs
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} s  - préfixe à remplacer
 * @param   {string} s2 - préfixe de remplacement
 */
function remplacerPrefixe(cs, s, s2) {
    "use strict";
    var l = s.length,

        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  préfixe à remplacer'.bold() + ' : <br> ' + s],
            ['  par'.bold() + ' : <br> ' + s2]
        ]);
    bkp('remplacer un préfixe', p);

    function f(x, i) {
        if (x.selected) {
            if (headers[i].slice(0, l) === s) {
                headers[i] = s2 + headers[i].substring(l);
            }
        }
    }

    Array.prototype.forEach.call(cs, f);

    resultat();
    message('terminé');
} // fin remplacerPrefixe

/**
 * enlève le préfixe s aux colonnes sélectionnées dans cs
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} s  - préfixe à enlever
 */
function enleverPrefixe(cs, s) {
    "use strict";
    var l = s.length,

        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  préfixe à enlever'.bold() + ' : <br> ' + s]
        ]);
    bkp('enlever un préfixe', p);

    function f(x, i) {
        if (x.selected) {
            if (headers[i].slice(0, l) === s) {
                headers[i] = headers[i].substring(l);
            }
        }
    }

    Array.prototype.forEach.call(cs, f);

    resultat();
    message('terminé');
} // fin enleverPrefixe

/**
 * pour toutes les colonnes sélectionnées dans cs, supprime le caractère s
 * en début et fin de l'entête
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} s  - caractère délimiteur à supprimer
 */
function suppDelimEnTete(cs, s) {
    "use strict";
    var c = 0,
        v = '',
        pl = '',
        m = '',
        cpt = 0,

        p = JSON.stringify([
             ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
             ['  délimiteur'.bold() + ' : ' + s]
        ]);
    bkp("supprimer le délimiteur de l'en-tête", p);

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
            v = supprimerDelimiteurTexte(headers[c], s);
            if (headers[c] !== v) { cpt++; }
            headers[c] = v;
        }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    m = cpt === 0 ? 'aucune modification' : cpt + ' valeur' + pl + ' corrigée' + pl;
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin suppDelimEnTete

/**
 * pour toutes les en-têtes des colonnes sélectionnées dans cs,
 * remplace les lettres accentuées par leur équivalent sans accent
 * ainsi que la cédille par un simple 'c'.
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @see     {@link remplacerCaracteres}
 * @see     {@link A_Z__AVEC_ACCENT}
 * @see     {@link A_Z__SANS_ACCENT}
 */
function enTeteSansAccent(cs) {
    "use strict";
    var cpt = 0,
        c = 0,
        pl = '',
        m = '',
        v = '',

        p = JSON.stringify([ ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')] ]);
    bkp("enlever les accents et cédilles de l'en-tête", p);

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
			 v = remplacerCaracteres(headers[c], A_Z__AVEC_ACCENT, A_Z__SANS_ACCENT);
			 if (headers[c] !== v) { cpt++; }
			 headers[c] = v;
        }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    m = cpt === 0 ? 'aucune modification' : cpt + ' valeur' + pl + ' corrigée' + pl;
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin enTeteSansAccent


// ................................................................... colonnes


/**
 * selon l'option o, supprime ou conserver, les colonnes sélectionnées dans cs
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} o  - option : (supprimer), (conserver)
 */
function supprimerColonne(cs, o) {
    "use strict";
    var i = 0,
        c = 0,
        di = [],
        dl = datalen,
        trad = { 'supprimer' : false,
                 'conserver' : true },
        b = trad[o],

        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['    action'.bold() + ' : ' + o]
        ]);
    bkp('supprimer (colonne)', p);

    // construction tableau des indices des colonnes à supprimer
    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected !== b) { // colonnes à supprimer
            di[di.length] = c;
        }
    }

    // suppression des colonnes
    for (i = 0; i < dl; i++) {
        filtreArray(data[i], di);
    }
    filtreArray(headers, di);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouveau nombre de colonnes'.bold() + ' : ' + headers.length]));
} // fin supprimerColonne

/**
 * supprime toutes les colonnes entièrement vides
 */
function supprimerColonnesVides() {
    "use strict";
    var i = 0,
        c = 0,
        m = '',
        b = true,
        di = [],
        dl = datalen;

    bkp('supprimer les colonnes vides');

    // construction tableau des indices des colonnes à supprimer
    for (c = 0; c < headers.length; c++) {
        b = true;
        for(i = 0; i < dl; i++) {
            b = data[i][c].search(/^\s*$/) !== -1;
            if (b) {
                continue;
            }
            b = false;
            break;
        }
        if (b) { // colonne à supprimer
            di[di.length] = c;
        }
    }

    // on stocke les noms des colonnes avant de les supprimer
    m = di.map(x => headers[x]).join('<br />');

    // suppression des colonnes
    for (i = 0; i < dl; i++) {
        filtreArray(data[i], di);
    }
    filtreArray(headers, di);

    resultat();
    m = '' + di.length + ' colonne(s) supprimée(s) : <br />'
    + m + '<br />'
    + 'nouveau nombre de colonnes'.bold() + ' : ' + headers.length;
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin supprimerColonnesVides

/**
 * pour toutes les colonnes sélectionnées dans cs, remplace toutes les valeurs par une chaîne vide
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 */
function viderCol(cs) {
    "use strict";
    var dl = datalen,
        c = 0,
        i = 0,

        p = JSON.stringify([['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')]]);
    bkp('vider seulement les valeurs (colonne)', p);

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
            for (i = 0; i < dl; i++) { data[i][c] = ''; }
        }
    }

    resultat();
    message('terminé');
} // fin viderCol

/**
 * ajoute une nouvelle colonne intitulée h
 * selon p, avant ou après la colonne c,
 * en initialisation toutes les lignes avec la valeur v
 * @param   {string} c - indice de colonne
 * @param   {string} h - en-tête de la nouvelle colonne
 * @param   {string} p - position : (avant), (après)
 * @param   {string} v - valeur initiale de la nouvelle colonne
 */
function ajouterCol(c, h, p, v) {
    "use strict";
    var i = 0,
        dl = datalen,

        pa = JSON.stringify([
            ['colonne'.bold() + ' : ' + headers[c]],
            ['  position'.bold() + ' : ' + p],
            ['  initialisée à'.bold() + ' : ' + v]
        ]);
    bkp('ajouter une colonne', pa);

    if (p === "après") { c++; }
    headers.splice(c, 0, h);
    for (i = 0; i < dl; i++) { data[i].splice(c, 0, v); }

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin ajouterCol

/**
 * crée une nouvelle colonne intitulée h,
 * copie conforme de la la colonne c
 * selon p, avant ou après la colonne c
 * @param   {string} c - indice de colonne
 * @param   {string} h - en-tête de la nouvelle colonne
 * @param   {string} p - position : (avant), (après)
 * @see     {@link ajouterCol}
 */
function copierColonne(c, h, p) {
    "use strict";
    var i = 0,
        dl = datalen,
        // contrairement à ajouterCol, on ne modifie pas directement "c"
        ch = c,

        pa = JSON.stringify([
            ['colonne'.bold() + ' : ' + headers[c]],
            ['  position'.bold() + ' : ' + p]
        ]);
    bkp('copier (colonne)', pa);

    // principe : on insère le nouvel en-tête au bon endroit
    if (p === "après") { ch++; }
    headers.splice(ch, 0, h);
    for (i = 0; i < dl; i++) {
        data[i].splice(c, 0, data[i][c]);
        // "splice(ch...data[i][c]" et "splice(c...data[i][c]"
        // donneront au final le même résultat
    }

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne'.bold() + ' : ' + h]));
} // fin copierColonne

/**
 * copie chaque colonne sélectionnée dans cs
 * selon o, en début ou en fin du tableau
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} o  - option : (début), (fin)
 * @see     {@link ajouterCol}
 */
function copierPlusieursColonnes(cs, o) {
    "use strict";
    var i = 0,
        dl = datalen,
        l = headers.length,
        m = [],

        pa = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  ' + o]
        ]);

    bkp( "copier plusieurs colonnes", pa);

    function f1(x, i, a) { return (cs[i].selected); }
    function f2(x, i, a) { return "copie - " + x; }

    if (o === 'à la fin') {
        headers.splice(headers.length, 0, ...headers.filter(f1).map(f2));
        for (i = 0; i < dl; i++) {
            data[i].splice(data[i].length, 0, ...data[i].filter(f1));
        }
    } else { // o === 'au début'
        headers.splice(0, 0, ...headers.filter(f1).map(f2));
        for (i = 0; i < dl; i++) {
            data[i].splice(0, 0, ...data[i].filter(f1));
        }
    }

    resultat();
    message('terminé');
    m = ['nouvelle(s) colonne(s)'.bold()];
    for (i = 0; i < headers.length - l; i++) { m.push([headers[i]]); }
    rb.memr(JSON.stringify(m));
} // fin copierPlusieursColonnes

/**
 * crée une nouvelle colonne avec comme définition un en-tête
 * et la succession de colonnes/séparateur sous la forme
 * colonne + ensemble de {séparateur + colonne}
 * @param   {string}  p - préfixe
 * @param   {string}  s - suffixe
 * @param   {string}  h - en-tête
 */
function concatener(p, s, h) {
    "use strict";
    var l = taille(dgebi('tab_concat')),
        i = 0,
        j = 0,
        c = '',
        t = '',
        m = '',
        dl = datalen;

    bkp('concaténer / fusionner');

    for (i = 0; i < dl; i++) {
        c = data[i][valeur('col__colonnes_8-0')] + valeur('sepa_1') + data[i][valeur('col__colonnes_8-1')];
        t = headers[valeur('col__colonnes_8-0')] + valeur('sepa_1') + headers[valeur('col__colonnes_8-1')];
        for (j = 2; j < l; j++) {
            c += valeur('sepa_' + j) + data[i][valeur('col__colonnes_8-' + j)];
            t += valeur('sepa_' + j) + headers[valeur('col__colonnes_8-' + j)];
        }
        data[i].unshift(p + c + s);
    }

    headers.unshift(h);

    resultat();
    m = 'nouvelle colonne 1'.bold() + ' : ' + h + ' =<br>' + p + t + s;
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin concatener

/**
 * crée une nouvelle colonne avec le résultat de la concaténation des colonnes cs
 * et de la liste de séparateurs t
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string}  t  - liste de séparateurs séparés par un saut de ligne
 */
function concatenerEnBoucle(cs, t) {
    "use strict";
    var i = 0,
        c = 0,
        n = 0,
        s = t.split('\n'),
        a = [],
        v = '',
        m = '',
        dl = datalen,

        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  séparateurs'.bold() + ' : <br>' + t]
        ]);
    bkp('concaténer en boucle', p);

    function f(x, y) {
        var r = '';

        r = x + s[n] + y;
        n++;
        n %= s.length;

        return r;
    }

    for (i = 0; i < dl; i++) {
        a = null;
        a = [];
        n = 0;

        for (c = 0; c < cs.length; c++) {
            if (cs[c].selected) {
                a[a.length] = data[i][c];
            }
        }
        v = a.length > 0 ? a.reduce(f) : '';
        data[i].splice(0, 0, v);
    }

    headers.splice(0, 0, 'concat_boucle');

    resultat();
    m = 'nouvelle colonne 1'.bold() + ' : ' + headers[0];
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin concatenerEnBoucle

/**
 * crée une nouvelle colonne comportant pour une même valeur de la colonne cp (colonne pivot),
 * une liste de toutes les valeurs lues dans la colonne c1, c2...
 * (les valeurs de la liste seront séparées par s1, s2...)
 * @param   {number} cp - indice de colonne pivot
 */
function listeSelonPivot(cp) {
    "use strict";
    var t = dgebi('tab_pivot'),
        l = taille(t),
        i = 0,
        j = 0,
        cdata = [],
        v = '',
        h = '',
        c = 0,
        s = '',
        m = '',
        headers2 = [],
        dl = datalen,

        p = JSON.stringify([
            ['pivot'.bold() + ' : ' + headers[cp]]
        ]);

    bkp('pivot (plusieurs lignes vers une colonne)', p);

    // crée un tableau associatif par valeur et par colonne sélectionnée
    // valeur pivot (colonne cp) => liste de valeurs (colonne c1, c2...)
    for (j = l - 1; j >= 1; j--) {
        c = parseInt(valeur('col__colonnes_10-' + j), 10);

        for (i = 0; i < dl; i++) {
            v = data[i][cp];

            if (!cdata.hasOwnProperty(v)) { cdata[v] = []; }

            // quand l'utilisateur sélectionne plusieurs fois la même colonne
            // - la première fois, on stocke les valeurs
            // - les fois suivantes, on passe
            if ( i === 0 && cdata[v].hasOwnProperty(c)) { break; }

            // la toute première fois, on crée le tableau des valeurs
            if (!cdata[v].hasOwnProperty(c)) { cdata[v][c] = []; }

            // cas général, on stocke les valeurs
            cdata[v][c].push(data[i][c]);
        }
    }

    // préparation du nouvel en-tête
    for (j = l - 1; j >= 1; j--) {
        c = parseInt(valeur('col__colonnes_10-' + j), 10);
        s = valeur('pivot_delim-' + j);
        h = '{' + headers[c] + '}' + s;
        headers2.unshift(h); // ici, headers2 ne représente que les nouvelles colonnes
    }
    headers = headers2.concat(headers);

    // préparation des nouvelles colonnes
    for (i = 0; i < dl; i++) {
        v = data[i][cp];

        for (j = l - 1; j >= 1; j--) {
            c = parseInt(valeur('col__colonnes_10-' + j), 10);
            s = valeur('pivot_delim-' + j);
            data[i].unshift(cdata[v][c].join(s));
        }
    }

    resultat();
    message('terminé');

    m = ['nouvelle(s) colonne(s)'.bold()];
    for (i = 0; i < l - 1; i++) { m.push([headers[i]]); }
    rb.memr(JSON.stringify(m));
} // fin listeSelonPivot

/**
 * @summary réalise plusieurs pivots successifs
 * @description pour un groupe de colonnes cs et n groupes définis par
 * - 2ème groupe commence par c2,
 * - dernier commence par cn,
 * crée autant de lignes que de groupes
 * et pour chaque nouvelle ligne, rapporte dans les colonnes cs,
 * les valeurs lues dans les colonnes du groupe transposé
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {number} c2 - indice de colonne
 * @param   {number} cn - indice de colonne
 */
function pivotEnSerie(cs, c2, cn) {
    "use strict";
    var i = 0,
        j = 0,
        k = 0,
        c = 0,
        n = 0.0,
        offset = 0,
        a = [],
        ar = [],
        l = 0,
        vs = [],
        h = [],
        m = '',
        dl = datalen,

        p = JSON.stringify([
            ['groupe 1'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['début groupe 2'.bold() + ' : ' + headers[c2]],
            ['début dernier groupe'.bold() + ' : ' + headers[cn]]
        ]);

    function f(vv, ii, aa) { aa[ii] = vv + offset; }

    bkp("pivot en série", p);

    // lire le premier groupe
    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
            a[a.length] = c;
        }
    }

    l = a.length;
    // on détermine l'offset
    // = distance entre les 1ers éléments de 2 groupes successifs
    offset = c2 - a[0];

    // on détermine le nombre de groupe
    n = 1 + (cn - a[0]) / offset;

    // on met à jour des données
    for (i = dl - 1; i >= 0; i--) { // on part de la fin pour éviter de recalculer l'index i

        ar = a.slice(0); // copie

        for (j = 0; j < n; j++) { // pour chaque groupe

            vs = [];
            for (k = 0; k < l; k++) { // pour chaque colonne du groupe
// console.log('j / k / ar[k] = ' + j + '/' + k + '/' + ar[k]);
                vs[vs.length] = data[i][ar[k]];
            }
            data.splice(i + 1 + j, 0, vs.concat(data[i])); // on ajoute après la ligne originale i

            // ajout de l'offset pour l'itération suivante
            ar.forEach(f);
        }
        data.splice(i, 1); // on supprime la ligne originale i
    }

    // mise à jour de l'en-tête
    for (k = 0; k < l; k++) {
        h.push('pivot # ' + headers[a[k]]);
    }
    headers.splice(0, 0, ...h);

    resultat();
    m = colonnesLignes(headers.length, data.length);
    message('terminé');

    rb.memr(JSON.stringify([m]));
} // fin pivotEnSerie

/**
 * projette vers les colonnes vides de cs selon l'option o,
 * - soit la dernière valeur non-nulle rencontrée dans cs, vers la colonne la plus à droite de cs
 * - soit toutes les valeurs non-nulles, vers les autres colonnes voisines de droite (dans la sélection cs)
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} o  - option : projeter à droite (projeter), remplir les "trous" (remplir)
 */
function projeterColonnes(cs, o) {
    "use strict";
    var dl = datalen,
        c = 0,
        c2 = 0,
        v = '',
        i = 0,
        trad = {
          'projeter': 'projeter à droite',
          'remplir': 'remplir les "trous"'
        },

        p = JSON.stringify([
            ['projeter sur les colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  ' + trad[o]]
        ]);
    bkp('projeter des valeurs sur les colonnes', p);

    // détermine la colonne cible c2
    for (c = cs.length - 1; c >= 0  ; c--) {
        if (cs[c].selected) {
            c2 = c;
            break;
        }
    }

    switch(o) {
    case 'projeter':
        for (i = 0; i < dl; i++) {
            // les colonnes sont parcourues de droite à gauche
            for (c = cs.length - 1; c >= 0; c--) {
                if (cs[c].selected && data[i][c] !== '') {
                    data[i][c2] = data[i][c];
                    break;
                }
            }
        }
        break;
    case 'remplir':
        for (i = 0; i < dl; i++) {
            v = '';
            for (c = 0; c < cs.length; c++) {
                if (cs[c].selected) {
                    if (data[i][c] !== '') {
                        v = data[i][c]; // soit on mémorise (read)
                    } else {
                        data[i][c] = v; // soit on écrit (write)
                    }
                }
            }
        }
        break;
    }

    resultat();
    message('terminé');
} // fin projeterColonnes

/**
 * créer une liste contenant le chemin de tous les ancêtres ("parents" des "parents")
 * jusqu'à la valeur rencontrée dans la colonne "enfant"
 * @param   {number} c1 - indice de colonne parent
 * @param   {number} c2 - indice de colonne enfant
 * @param   {string} s  - séparateur des valeurs dans la liste des ancêtres
 * @param   {string} o  - option : dans l'ordre du fichier (projeter), remplir les "trous" (remplir)
 */
function listerAncetres(c1, c2, s, o) {
    "use strict";
    var dl = datalen,
        i  = 0,
        h  = '',
        d  = [],
        v  = '',
        a  = [],

        p = JSON.stringify([
            ['relation'.bold()],
            ['  parent'.bold() + ' : ' + headers[c1]],
            ['  enfant'.bold() + ' : ' + headers[c2]],
            ['filiation des ancêtres'.bold()],
            ['  séparateur'.bold() + ' : ' + s],
            ['  déterminée par'.bold() + ' : ' + o],
        ]);
    bkp('lister les ancêtres', p);

    // lecture des relations parent-enfant
    if (o === 'dernier parent connu') {
      for(i = 0; i < dl; i++) {
        if (data[i][c1] !== '' && data[i][c2] !== '') {
          d[data[i][c2]] = data[i][c1];
        }
      }
    } else { // o === 'premier parent connu'
      for(i = dl - 1; i >= 0; i--) {
        if (data[i][c1] !== '' && data[i][c2] !== '') {
          d[data[i][c2]] = data[i][c1];
        }
      }
    }

    // préparation du tableau résultat
    for (i = 0; i < dl; i++) {
        a = [];
        v = data[i][c2];

        while(d.hasOwnProperty(v)) {
           a[a.length] = d[v];
           v = d[v];
        }

        data[i].splice(0, 0, a.reverse().join(s));
    }

    h = 'ancêtres (' + headers[c1] + ') de (' + headers[c2] + ')';
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne'.bold() + ' : ' + h]));
} // fin listerAncetres


// ........................................................... colonnes : ordre


/**
 * réordonne les colonnes selon la double sélection :
 * - nouvel ordre des colonnes (pour le début du tableau)
 * - colonnes conservées dans l'ordre initial (pour la fin du tableau)
 */
function ordonner() {
    "use strict";
    var lo1 = dgebi('list1').options,
        lo2 = dgebi('list2').options,
        d1 = lo1.length,
        d2 = lo2.length,
        a = [],
        r = [],
        i = 0,
        m = '',
        dl = datalen;

    bkp('réordonner');

    if (dgebi('list2').options.length === 0) {
        m = '(aucune colonne sélectionnée)';
        message(m);
        rb.memr(JSON.stringify([m]));
        return;
    }

    for (i = 0; i < d2; i++) { a.push(lo2[i].value); }
    for (i = 0; i < d1; i++) { a.push(lo1[i].value); }

    headers = rangerSelonSelection(headers, a);
    for (i = 0; i < dl; i++) { data[i] = rangerSelonSelection(data[i], a); }

    r.push('colonnes réordonnées'.bold());
    for (i = 0; i < d2; i++) { r.push(headers[i]); }
    resultat();
    message('terminé');
    rb.memr(JSON.stringify(r));
} // fin ordonner

/**
 * permute les colonnes sélectionnées, selon o
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} o  - option : (1ère et 2ème, 3ème et 4ème, etc.) (1ère et dernière, 2ème et avant-dernière, etc.)
 */
function permuterColonnes(cs, o) {
    "use strict";
    var i = 0,
        c = 0,
        dl = datalen,
        a = [],
        l = 0,
        l2 = 0,

        pa = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  permuter'.bold() + ' : ' + o]
        ]);

    function swap(a, i1, i2) {
        var v = a[i1];
        a[i1] = a[i2];
        a[i2] = v;
    }

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
            a.push(c);
        }
    }

    if (a.length < 2){ 
        attention('sélectionnez au moins deux colonnes');
        return;
    }

    if (o === '1ère et 2ème, 3ème et 4ème, etc.' && (a.length % 2 === 1) ) {  
        attention('sélectionnez un nombre paire de colonnes');
        return;
    }

    bkp( "permuter les colonnes", pa);

    l = a.length;

    if ( o === '1ère et 2ème, 3ème et 4ème, etc.') {
        for (c = 0; c < l; c+=2) { swap(headers, a[c], a[1 + c]); }

        for (i = 0; i < dl; i++) {
            for (c = 0; c < l; c+=2) {
                swap(data[i], a[c], a[1 + c]);
            }
        }
    } else { // o === '1ère et dernière, 2ème et avant-dernière, etc.'
        l2 = Math.floor(l/2);
        for (c = 0; c < l2; c++) { swap(headers, a[c], a[l - 1 - c]); }

        for (i = 0; i < dl; i++) {
            for (c = 0; c < l2; c++) {
                swap(data[i], a[c], a[l - 1 - c]);
            }
        }
    }

    resultat();
    message('terminé');
} // fin permuterColonnes


// ..................................................... colonnes : comparaison


/**
 * compare les valeurs de 2 colonnes c et cc.
 * Les résultats des comparaisons peuvent être : (v. égales), (v. à droite), (v. à gauche), (v. différentes), (aucune valeur)
 * @param   {string} c1 - indice de colonne
 * @param   {string} c2 - indice de colonne
 * @see     {@link comparer}
 */
function comparerColonnes(c1, c2) {
    "use strict";
    var EQUA = "v. égales",
        SUPG = "v. à droite",
        SUPD = "v. à gauche",
        DIFF = "v. différentes",
        NULL = "aucune valeur",
        vc1 = '',
        vc2 = '',
        h = '',
        v = '',
        i = 0,
        dl = datalen;

    for (i = 0; i < dl; i++) {
        vc1 = data[i][c1];
        vc2 = data[i][c2];
        if (vc1 === '') {
            if (vc2 === '') {
                v = NULL;
            } else {
                v = SUPG;
            }
        } else {
            if (vc2 === '') {
                v = SUPD;
            } else {
                if (vc2 === vc1) {
                    v = EQUA;
                } else {
                    v = DIFF;
                }
            }
        }
        data[i].splice(0, 0, v);
    }

    h = '(' + headers[c1] + ') ?? (' + headers[c2] + ')';
    headers.splice(0, 0, h);
} // fin comparerColonnes

/**
 * compare des couples de colonnes et ajoute le résultat en première colonne
 * avec pour résultat des comparaisons : (v. égales), (v. à droite), (v. à gauche), (v. différentes), (aucune valeur)
 * @see     {@link comparerColonnes}
 */
function comparer() {
    "use strict";
    var t = dgebi('tab_comp'),
        l = taille(t),
        p = 0,
        g = 0,
        d = 0,
        i = 0,
        m = [];

    bkp('comparer des colonnes');

    for (i = l - 1; i >= 1; i--) {
        g = parseInt(valeur('col__col_compar_1-g' + i), 10) + p;
        d = parseInt(valeur('col__col_compar_1-d' + i), 10) + p;
        comparerColonnes(g, d);
        p++;
    }

    resultat();
    message('terminé');

    m = ['nouvelle(s) colonne(s)'.bold()];
    for (i = 0; i < l - 1; i++) { m.push([headers[i]]); }
    rb.memr(JSON.stringify(m));
} // fin comparer

/**
 * crée une nouvelle colonne avec le résultat du calcul de la distance d'édition
 * entre les valeurs de colonnes prises 2 par 2
 */
function distEditionCol() {
    "use strict";
    var t = dgebi('tab_dist_edit'),
        l = taille(t),
        p = 0,
        g = 0,
        d = 0,
        v = '',
        dist = [],
        h = [],
        m = [],
        dl = datalen,
        i = 0,
        j = 0;

    bkp("calculer la distance d'édition / colonnes");

    // calcul des distances d'édition pour chaque couple de colonne
    for (j = l - 1; j >= 1; j--) {
        g = parseInt(valeur('col__col_compar_2-g' + j), 10);
        d = parseInt(valeur('col__col_compar_2-d' + j), 10);

        for (i = 0; i < dl; i++) {
            if (p === 0) { dist[i] = []; }
            v = calculerDistanceEdition(data[i][g], data[i][d]).toString();
            dist[i][p] = v;
        }
        p++;

        h[h.length] = headers[valeur('col__col_compar_2-g' + j)] + '~' + headers[valeur('col__col_compar_2-d' + j)];
    }

    // préparation du tableau résultat
    for (i = 0; i < dl; i++) { data[i].splice(0, 0, ...dist[i]); }

    headers.splice(0, 0, ...h);

    resultat();
    message('terminé');
    m.push(['nouvelle(s) colonne(s)'.bold()]);
    m.push(...h);
    rb.memr(JSON.stringify(m));
} // fin distEditionCol

/**
 * crée une nouvelle colonne avec la liste des valeurs de la colonne cc séparées par sv
 * telles que ces valeurs aient une distance d'édition
 * strictement inférieure à s par rapport à la valeur de la colonne c
 * @param   {number} c  - indice de colonne
 * @param   {number} cc - indice de colonne
 * @param   {string} sv - séparateur des valeurs de retours
 */
function distEditionLig(c, s, cc, sv) {
    "use strict";
    var h = '',
        v = [],
        dl = datalen,
        dist = [],
        i = 0,
        j = 0,

        p = JSON.stringify([
            ['colonne'.bold() + ' : ' + headers[c]],
            ['  seuil'.bold() + ' : ' + s],
            ['  retour'.bold() + ' : ' + headers[cc]],
            ['  séparateur de résultat'.bold() + ' : ' + sv]
        ]);
    bkp("calculer la distance d'édition / lignes", p);

    // calcul des distances d'édition pour chaque ligne comparée à toutes les (autres) lignes
    for (i = 0; i < dl; i++) {
        v = [];
        for (j = 0; j < dl; j++) {
            if (calculerDistanceEdition(data[i][c], data[j][c]) < s) {
                if (i !== j) { v[v.length] = data[j][cc]; }
            }
        }
        if (v.length === 0) { v[0] = ''; }
        dist[i] = v;
    }

    // préparation du tableau résultat
    for (i = 0; i < dl; i++) {
        data[i].splice(0, 0, dist[i].join(sv));
    }

    h = '~(' + s + ') ' + headers[c];
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin distEditionLig

/**
 * crée une nouvelle colonne avec la valeur minimum ou maximum trouvée dans chaque ligne
 * parmi les valeurs des colonnes sélectionnées
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} vr - valeur recherchée : (minimum) ou (maximum)
 * @param   {string} o  - option : gestion des valeurs vides (ignorées) ou (traduites par zéro)
 */
function identifierMinMaxNumeriqueColonnes(cs, vr, o) {
    "use strict";
    var dl = datalen,
        a = [],
        ar = [],
        l = 0,
        c = 0,
        i = 0,
        j = 0,
        h = '',
        g = function (){},

        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['identifier le'.bold() + ' : ' + vr],
            ['les valeurs vides sont'.bold() + ' : ' + o]
        ]);
    bkp('identifier le min ou max / colonnes', p);

    function f(a, b) { return st2float(a) - st2float(b); }

    if (vr === 'minimum') {
      g = function(x) {
        if (!x.length) return 'aucune valeur';
        return x.sort(f).shift();
      };
    } else {
      g = function(x) {
        if (!x.length) return 'aucune valeur';
        return x.sort(f).pop();
      };
    }

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) { a[a.length] = c; }
    }

    l = a.length;

    if (o === 'ignorées') {
        for (i = 0; i < dl; i++) {
            ar = [];
            for (j = 0; j < l; j++) {
                c = a[j];
                if(data[i][c] === '') { continue; }
                ar[ar.length] = data[i][c];
            }
            data[i].unshift(g(ar));
        }

    } else { // o === 'traduites par zéro'

        for (i = 0; i < dl; i++) {
            ar = [];
            for (j = 0; j < l; j++) {
                c = a[j];
                ar[ar.length] = data[i][c];
              }
              data[i].unshift(g(ar));
          }

    }

    h = vr + '(' + a.length + ')' + (o === 'traduites par zéro' ? '/0' : '/_');
    headers.unshift(h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify([
        ['nouvelle colonne 1'.bold() + ' : ' + h]
    ]));
} // fin identifierMinMaxNumeriqueColonnes

/**
 * crée une nouvelle colonne avec la liste des valeurs ou des en-têtes des colonnes
 * qui satisfont, dans la sélection cs, la condition de comparaison avec la colonne c
 * @param   {number} cc - indice de colonne
 * @param   {string} o  - opérateur de comparaison numérique (=),(!=),(<),(<=),(>),(>=) ou comparaison de texte (identique), (différent)
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} r  - valeur de retour (la valeur), (l'intitulé de la colonne)
 * @param   {string} sv - séparateur des valeurs de retour
 */
function comparerValeurEtColonnes(cc, o, cs, r, sv) {
    "use strict";
    var dl = datalen,
        a = [],
        ar = [],
        c = '',
        v = '',
        h = '',
        l = 0,
        i = 0,
        j = 0,
        f = function(){},

        p = JSON.stringify([
            ['quand'.bold() + ' : ' + headers[cc]],
            ['est'.bold() + ' : ' + o],
            ['aux valeurs des colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['recopier'.bold() + ' : ' + r],
            ['séparer les résultats par'.bold() + ' : ' + sv]
        ]);
    bkp('comparer une valeur et plusieurs colonnes', p);

    switch(o) {
      /* comparaison numérique */
      case '<' : f = function(a,b) { return st2float(a) < st2float(b); };
        break;
      case '≤' : f = function(a,b) { return st2float(a) <= st2float(b); };
        break;
      case '>' : f = function(a,b) { return st2float(a) > st2float(b); };
        break;
      case '≥' : f = function(a,b) { return st2float(a) >= st2float(b); };
        break;
      case '=' : f = function(a,b) { return st2float(a) === st2float(b); };
        break;
      case '≠' : f = function(a,b) { return st2float(a) !== st2float(b); };
        break;
      /* comparaison stricte de texte */
      case 'identique' : f = function(a,b) { return a === b; };
        break;
      case 'différent' : f = function(a,b) { return a !== b; };
        break;
    }

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) { a[a.length] = c; }
    }

    l = a.length;

    if( r === 'la valeur'){
        for (i = 0; i < dl; i++) {
            ar = [];
            for (j = 0; j < l; j++) {
                if ( f(data[i][cc], data[i][a[j]]) ) {
                  ar[ar.length] = data[i][a[j]];
                }
            }
            v = ar.join(sv);
            data[i].unshift(v);
        }
    } else {
        for (i = 0; i < dl; i++) {
            ar = [];
            for (j = 0; j < l; j++) {
                if ( f(data[i][cc], data[i][a[j]]) ) {
                  ar[ar.length] = headers[a[j]];
                }
            }
            v = ar.join(sv);
            data[i].unshift(v);
        }
    }

    h = headers[cc] + ' ' + o + ' (' + a.length + '):' + (r === 'la valeur' ? 'val' : 'col'); //(o === 'traduites par zéro' ? '/0' : '/_');
    headers.unshift(h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify([
        ['nouvelle colonne 1'.bold() + ' : ' + h]
    ]));
} // fin comparerValeurEtColonnes


// ..................................................................... lignes


/**
 * supprime les lignes inconsistantes constituées de caractères espace, tabulation, saut de ligne, etc.
 */
function supprimerLignesVides() {
    "use strict";
    var cpt = 0,
        m = '',
        i = 0,
        pl = '',
        dl = datalen;

    bkp('supprimer les lignes vides');

    for (i = dl - 1; i >= 0; i--) { // on part de la fin pour éviter de recalculer l'index i
        if (data[i].join('').search(/^\s*$/) !== -1) {
            data.splice(i, 1);
        }
    }

    cpt = dl - data.length;

    resultat();
    pl = cpt > 1 ? 's' : '';
    m = cpt === 0 ? 'aucune modification' : cpt + ' ligne' + pl + ' vide' + pl + ' supprimée' + pl;
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin supprimerLignesVides

/**
 * remplace dans la colonne c toutes les valeurs inconsistantes
 * soit vide soit égale à v, par la plus proche valeur consistante
 * selon le sens de lecture dir
 * @param   {string} v   - si renseignée, valeur à remplacer
 * @param   {string} dir - direction : (avant), (après)
 * @param   {number} c   - indice de colonne
 * @param   {string} r   - règle d'application : (sans condition), (valeur commune)
 * @param   {number} id  - valeur identifiante commune (si r = "valeur commune")
 */
function remplacerValeurProche(v, dir, c, r, id) {
    "use strict";
    var v2 = v,
        dl = datalen,
        p = [],
        i = 0,
        iv = '',
        m = '',
        cpt = 0,
        pl = '';

    p.push(['colonne'.bold() + ' : ' + headers[c]]);
    if (v) { p.push(['  valeur à remplacer'.bold() + ' : ' + v]); }
    p.push(['  prendre la valeur juste'.bold() + ' : ' + dir]);

    if (r === 'sans condition') { p.push(['  appliquer'.bold() + ' : ' + r]); }
    else {
        p.push(['  appliquer'.bold() + ' : ' + r]);
        p.push(['    dans'.bold() + ' : ' + headers[id]]);
    }

    if (v) { bkp('remplacer une valeur par la plus proche valeur consistante', JSON.stringify(p)); }
    else   { bkp('compléter les vides par la plus proche valeur consistante',  JSON.stringify(p)); }

    if (dir === 'avant') {
        iv = data[i][id];
        for (i = 0; i < dl; i++) {
            if (data[i][c] === v) {
                if (r === 'sans condition' || data[i][id] === iv) {
                    data[i][c] = v2;
                    cpt++;
                }
            } else {
                v2 = data[i][c];
                iv = data[i][id];
            }
        }
    } else { // dir === 'après'
        iv = data[dl -1][id];
        for (i = dl - 1; i >= 0; i--) { // idem précédent, mais en partant de la fin
            if (data[i][c] === v) {
                if (r === 'sans condition' || data[i][id] === iv) {
                    data[i][c] = v2;
                    cpt++;
                }
            } else {
                v2 = data[i][c];
                iv = data[i][id];
            }
        }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    if (v) {
        m = cpt === 0 ? 'aucune modification' : cpt + ' valeur' + pl + ' complétée' + pl;
    } else {
        m = cpt === 0 ? 'aucune modification' : cpt + ' valeur' + pl + ' ' + v + ' remplacée' + pl;
    }
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin remplacerValeurProche

/**
 * pour toutes les colonnes sélectionnées dans cs ajoute avant/après ou remplace chaque valeur avec le libelle de l'en-tête
 * pour l'ajout avant/après, le séparateur s permet de distinguer la valeur original du libellé de l'en-tête
 * en neutralisant le préfixe indiqué dans s2
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} o  - position de l'en-tête par rapport à la valeur : préfixe (avant), suffixe (après), remplacement (à la place)
 * @param   {string} s  - séparateur entre la valeur et le préfixe/suffixe
 * @param   {string} s2 - préfixe à neutraliser
 */
function copierEnTete(cs, o, s, s2) {
    "use strict";
    var dl = datalen,
        i = 0,
        c = 0,
        l = s2.length,
        f = function () {},

        p = [
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  position'.bold() + ' : ' +  o],
            ['  séparateur'.bold() + ' : ' + s]
        ];
        if (s2) {
            p.push(['  préfixe neutralisé'.bold() + ' : ' + s2]);
        }

    bkp("recopier l'en-tête à chaque ligne", JSON.stringify(p));

    if (l) {
        f = function (x) {
            if (x.slice(0, l) === s2) { return x.substring(l); }
            return x; // else
        };
    } else {
        f = function (x) { return x; };
    }

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
            for (i = 0; i < dl; i++) {
                if (data[i][c] !== '') {
                    switch (o) {
                    case 'avant':
                        data[i][c] = f(headers[c]) + s + data[i][c];
                        break;
                    case 'après':
                        data[i][c] = data[i][c] + s + f(headers[c]);
                        break;
                    case 'à la place':
                        data[i][c] = f(headers[c]);
                        break;
                    }
                }
            }
        }
    }

    resultat();
    message('terminé');
} // fin copierEnTete

/**
 * compacte plusieurs lignes par agrégation (calculs ou listes)
 * des valeurs de certaines colonnes quand les lignes ont la même valeur de la colonne cp (colonne pivot).
 * (les valeurs de la liste seront séparées par s1, s2...)
 * Pour toutes les autres colonnes, seules les valeurs de la première occurrence (ligne) seront retenues.
 * @param   {number} cp - indice de colonne pivot
 */
function compacter(cp) {
    "use strict";
    var t = dgebi('tab_compact'),
        l = taille(t),
        i = 0,
        j = 0,
        cdata = [],
        v = '',
        c = 0,
        s = '',
        m = [],
        o = '',
        dl = datalen,

        p = JSON.stringify([
            ['compacter'.bold() + ' : ' + headers[cp]]
        ]);

    bkp('compacter plusieurs lignes', p);

    function fsum(x, y)  { return "" + (st2float(x) + st2float(y)); }
    function fprod(x, y) { return "" + (st2float(x) * st2float(y)); }
    function fmin(x, y)  { return st2float(x) > st2float(y) ? y : x; }
    function fmax(x, y)  { return st2float(x) < st2float(y) ? y : x; }

    function f(a, o) {
        if (a.length === 0) { return "";}
        switch(o) {
            case "somme":     return a.reduce(fsum,  "0");   //  break; // inutile
            case "produit":   return a.reduce(fprod, "1");   //  break; // inutile
            case "min":       return a.reduce(fmin, "" + (st2float(a[0]) + 1)); //  break; // inutile
            case "max":       return a.reduce(fmax, "" + (st2float(a[0]) - 1)); //  break; // inutile
            case "décompte":  return "" + a.length; //  break; // inutile
            case "décompte (valeurs distinctes)":  return "" + arrValUnique(a).length; //  break; // inutile
        }
    }

    // crée un tableau associatif par valeur et par colonne sélectionnée
    // valeur pivot (colonne cp) => liste de valeurs (colonne c1, c2...)
    for (j = l - 1; j >= 1; j--) {
        c = parseInt(valeur('col__lignes_8-' + j), 10);

        for (i = 0; i < dl; i++) {
            v = data[i][cp];

            if (!cdata.hasOwnProperty(v)) { cdata[v] = []; }

            // quand l'utilisateur sélectionne plusieurs fois la même colonne
            // - la première fois, on stocke les valeurs
            // - les fois suivantes, on passe
            if ( i === 0 && cdata[v].hasOwnProperty(c)) { break; }

            // la toute première fois, on crée le tableau des valeurs
            if (!cdata[v].hasOwnProperty(c)) { cdata[v][c] = []; }

            // cas général, on stocke les valeurs
            cdata[v][c].push(data[i][c]);
        }
    }

    // mise à jour des valeurs
    for (i = 0; i < dl; i++) {
        v = data[i][cp];

        for (j = l - 1; j >= 1; j--) {
            c = parseInt(valeur('col__lignes_8-' + j), 10);
            o = valeur('compact_ope-' + j);
            if (o === "liste") {
                s = valeur('compact_delim-' + j);
                data[i][c] = cdata[v][c].join(s);
            } else {
                data[i][c] = f(cdata[v][c], o);
            }
        }
    }
 /** FIXME : inverser petite et grande boucles => moins d'opération calcul parseInt(valeur...
  *  permet de créer le message historique ET la MAJ des valeurs en un même bloc */
    // création message historique
    for (j = l - 1; j >= 1; j--) {
        c = parseInt(valeur('col__lignes_8-' + j), 10);
        o = valeur('compact_ope-' + j);
        if (o === "liste") {
            s = valeur('compact_delim-' + j);
            m.push(headers[c] + ', ' + o + ', séparateur : ' + s);
        } else {
            m.push(headers[c] + ', ' + o);
        }
    }

    // suppression des lignes "compactées"
    for (i = dl - 1; i >= 0; i--) {
        v = data[i][cp];
        c = parseInt(valeur('col__lignes_8-' + 1), 10); // arbitraire, n'importe colonne "j" convient ici
        if (cdata[v][c].length > 1) { // (> 1) car on veut conserver une unique occurrence
            data.splice(i, 1);
            cdata[v][c].pop(); // on décremente le nombre de ligne
        }
    }

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(m));
} // fin compacter

/**
 * décale dans la direction dir (haut, bas) les valeurs de la colonne c
 * en appliquant la règle r
 * @param   {string} dir - direction : (vers le haut), (vers le bas)
 * @param   {number} c   - indice de colonne
 * @param   {string} r   - règle d'application : (sans condition), (pour chaque série), (pour chaque identifiant)
 * @param   {number} id  - valeur identifiante commune (si r <> "sans condition")
 */
function decaler(c, dir, r, id) {
    "use strict";
    var dl = datalen,
        i = 0,
        a = [],
        iv = '',
        v2 = '',
        v = '',
        p = [];

    p.push(['colonne'.bold() + ' : ' + headers[c]]);
    p.push(['  décaler'.bold() + ' : ' + dir]);

    if (r === 'sans condition') { p.push(['  appliquer'.bold() + ' : ' + r]); }
    else {
        p.push(['  appliquer'.bold() + ' : ' + r]);
        p.push(['    dans'.bold() + ' : ' + headers[id]]);
    }

    bkp('décaler vers le haut ou le bas', JSON.stringify(p));

    if (dir === 'vers le bas') {

      switch(r) {
        case 'sans condition':
          for (i = 0; i < dl; i++) {
            v = data[i][c];
            data[i][c] = v2;
            v2 = v;
          }
          break;

        case 'pour chaque série':
          iv = data[i][id];
          for (i = 0; i < dl; i++) {
            v = data[i][c];
            data[i][c] = iv === data[i][id] ? v2 : '';
            v2 = v;
            iv = data[i][id];
          }
          break;

        case 'pour chaque identifiant':
          for (i = 0; i < dl; i++) {
            v = data[i][c];
            data[i][c] = a.hasOwnProperty(data[i][id]) ? a[data[i][id]] : '';
            a[data[i][id]] = v;
          }
          break;
      } // fin du switch


    } else { // dir === 'vers le haut'

      switch(r) {
        case 'sans condition':
          for (i = dl -1; i >= 0; i--) {
            v = data[i][c];
            data[i][c] = v2;
            v2 = v;
          }
          break;

        case 'pour chaque série':
          iv = data[dl -1][id];
          for (i = dl -1; i >= 0; i--) {
            v = data[i][c];
            data[i][c] = iv === data[i][id] ? v2 : '';
            v2 = v;
            iv = data[i][id];
          }
          break;

        case 'pour chaque identifiant':
          for (i = dl -1; i >= 0; i--) {
            v = data[i][c];
            data[i][c] = a.hasOwnProperty(data[i][id]) ? a[data[i][id]] : '';
            a[data[i][id]] = v;
          }
          break;
      } // fin du switch

    } // fin du else (dir === 'vers le haut')

    resultat();
    message('terminé');
} // fin decaler


// ......................................................... lignes : numéroter


/**
 * crée une nouvelle colonne intitulée titre comportant un numéro de ligne
 * commençant par pr et incrémenté à chaque ligne par pa
 * @param   {string} h  - en-tête de colonne
 * @param   {number} pr - premier numéro (positif/négatif, avec ou sans partie décimale)
 * @param   {number} pa - incrément (positif/négatif, avec ou sans partie décimale)
 */
function numeroter(h, pr, pa) {
    "use strict";
    var dec = 0,
        n = 0.0,
        v = '',
        dl = datalen,
        i = 0,

        p = JSON.stringify([
            ['premier numéro'.bold() + ' : ' + pr],
            ['pas'.bold() + ' : ' + pa]
        ]);
    bkp('numéroter les lignes', p);

    dec = plusGrandePrecision(pr, pa);

    headers.unshift(h);
    for (i = 0; i < dl; i++) {
        n = i * st2float(pa) + st2float(pr);
        v = n.toFixed(dec);
        data[i].unshift(v);
    }

    resultat();
    message('dernier = ' + v);
    rb.memr(JSON.stringify([
        ['nouvelle colonne 1'.bold() + ' : ' + h],
        ['  dernier'.bold() + ' : ' + v]
    ]));
} // fin numeroter

/**
 * crée une nouvelle colonne en numérotant chaque série selon le sens s de la numérotation
 * (une série est une répétition sans interruption d'une même valeur)
 * @param   {number} c - indice de colonne
 * @param   {string} s - sens de la numérotation : (croissant), (décroissant)
 */
function numeroterSerie(c, s) {
    "use strict";
    var v = '',
        cpt = 0,
        h = '',
        dl = datalen,
        i = 0,

        p = JSON.stringify([
            ['colonne'.bold() + ' : ' + headers[c]],
            ['  ordre'.bold() + ' : ' + s]
        ]);
    bkp('numéroter chaque série de lignes', p);

    if (s === 'croissant') {
        v = data[0][c] + '1'; // initialisé avec une valeur factice différente de la première valeur évaluée
        for (i = 0; i < dl; i++) {
            cpt = v === data[i][c] ? cpt : cpt + 1;
            v = data[i][c]; // AVANT data[i].splice
            data[i].splice(0, 0, cpt.toString());
        }
    } else { // s === 'décroissant'
        v = data[dl - 1][c] + '1'; // initialisé avec une valeur factice différente de la première valeur évaluée
        for (i = dl - 1; i >= 0; i--) { // idem précédent, mais en partant de la fin
            cpt = v === data[i][c] ? cpt : cpt + 1;
            v = data[i][c]; // AVANT data[i].splice
            data[i].splice(0, 0, cpt.toString());
        }
    }

    h = 'num/série:' + headers[c];
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin numeroterSerie

/**
 * crée une nouvelle colonne en numérotant les éléments d'une série selon le sens s de la numérotation
 * (une série est une répétition sans interruption d'une même valeur)
 * @param   {number} c - indice de colonne
 * @param   {string} s - sens de la numérotation : (croissant), (décroissant)
 */
function numeroterDansSerie(c, s) {
    "use strict";
    var v = '',
        cpt = 0,
        h = '',
        dl = datalen,
        i = 0,

        p = JSON.stringify([
            ['colonne'.bold() + ' : ' + headers[c]],
            ['  ordre'.bold() + ' : ' + s]
        ]);
    bkp('numéroter les lignes de chaque série', p);

    if (s === 'croissant') {
        v = data[0][c] + '1'; // initialisé avec une valeur factice différente de la première valeur évaluée
        for (i = 0; i < dl; i++) {
            cpt = v === data[i][c] ? cpt + 1 : 1;
            v = data[i][c]; // AVANT data[i].splice
            data[i].splice(0, 0, cpt.toString());
        }
    } else { // s === 'décroissant'
        v = data[dl - 1][c] + '1'; // initialisé avec une valeur factice différente de la première valeur évaluée
        for (i = dl - 1; i >= 0; i--) { // idem précédent, mais en partant de la fin
            cpt = v === data[i][c] ? cpt + 1 : 1;
            v = data[i][c]; // AVANT data[i].splice
            data[i].splice(0, 0, cpt.toString());
        }
    }

    h = 'num/dansSérie:' + headers[c];
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin numeroterDansSerie


// ................................................................... agrégats


/**
 * fonction d'analyse qui affiche à l'utilisateur le domaine de valeur (nombre, proportion)
 * des valeurs de la colonne c
 * @param   {number} c - indice de colonne
 * @see     {@link majreferentiel}
 */
function referentiel(c) {
    "use strict";
    var t = '',
        i = 0,
        v = '',
        n = 0;

    majreferentiel(c);

    t  = '<table style="white-space:pre;">'
       + '<thead><tr>'
       + '<td>' + headers[c]
       + '<a class="sbd" onclick="' + "sortTable('tb_ref',0,true, 't');" + '">▼</a>'
       + '<a class="sbd" onclick="' + "sortTable('tb_ref',0,false,'t');" + '">▲</a>'
       + '</td>'
       + '<td>nombre</td>'
       + '<td>ratio'
       + '<a class="sbd" onclick="' + "sortTable('tb_ref',1,true, 'n');" + '">▼</a>'
       + '<a class="sbd" onclick="' + "sortTable('tb_ref',1,false,'n');" + '">▲</a>'
       + '</td></tr>'
       + '</thead>'
       + '<tbody id="tb_ref">';

    par = null;
    par = [];

    for (v in card) {
        if (card.hasOwnProperty(v)) {
            n += card[v];
        }
    }
    for (v in card) {
        if (card.hasOwnProperty(v)) {
            t += '<tr>'
               + '<td>' + v + '</td>'
               + '<td>' +  parseInt(card[v], 10) + '</td>'
               + '<td>' + (parseInt(card[v], 10) / n * 100).toFixed(2) + ' %</td>'
               + '</tr>';
            par[i] = [];
            par[i][0] = v;
            par[i][1] = card[v];
            i++;
        }
    }

    t += '</tbody>'
       + '<tfoot><tr>'
       + '<td>TOTAL</td>'
       + '<td>' + datalen + '</td>'
       + '<td>100.00 %</td>'
       + '</tr></tfoot>'
       + '</table>';
    messageEtBoutons(c, t, i);
} // fin referentiel

/**
 * fonction d'analyse qui affiche un dictionnaire (table de combinaisons)
 * des valeurs des colonnes sélectionnes dans cs
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 */
function dictionnaire(cs) {
    "use strict";
    var i = 0,
        j = 0,
        c = 0,
        ai = [], // tableau d'index de colonne sélectionnées
        ae = [], // tableau d'en-tête pour les colonnes indexées
        ad = [], // tableau de clés : pour chaque ligne de données, JSON des données des colonnes indexées
        t = '',
        dl = datalen;

    for (c = 0; c < cs.length; c++) {

        // on mémorise
        // - les numéros  des colonnes sélectionnées
        // - les en-têtes des colonnes sélectionnées
        if (cs[c].selected) {
            ai.push(c);
            ae.push(headers[c]);
        }
    }

    // pour chaque ligne de données, on calcule la clé (à partir de la sélection)
    for (i = 0; i < dl; i++) {
        ad[i] = [];
        for(j = 0; j < ai.length; j++) {
           c = ai[j];
           ad[i].push(data[i][c]);
        }
        ad[i] = JSON.stringify(ad[i]);
    }

    ad = tab1hash(ad);

    // on dessine le tableau résultat
    t  = '<table style="white-space:pre;">'
       + '<thead><tr>';

    // ... ligne d'en-tête
    for (i = 0; i < ae.length; i++) {
        t += '<td>' + ae[i]
          + '<a class="sbd" onclick="' + "sortTable('tb_dico'," + i + ",true, 't');" + '">▼</a>'
          + '<a class="sbd" onclick="' + "sortTable('tb_dico'," + i + ",false,'t');" + '">▲</a>'
          + '</td>';
    }

    t += '<td>nombre'
      + '<a class="sbd" onclick="' + "sortTable('tb_dico'," + i + ",true, 'n');" + '">▼</a>'
      + '<a class="sbd" onclick="' + "sortTable('tb_dico'," + i + ",false,'n');" + '">▲</a>'
      + '</td>';

    t += '</tr>'
      + '</thead>'
      + '<tbody id="tb_dico">';

    // ... données
    for (let v in ad) {
        if (ad.hasOwnProperty(v)) {
          t += '<tr><td>' + JSON.parse(v).join('</td><td>') + '</td>'
          +  '<td>' + ad[v].length + '</td></tr>';
        }
    }

    // fin du tableau
    t += '</tbody>'
      + '</table>';
    messageEtBoutons(c, t, i, 'nocsv');
} // fin dictionnaire

/**
 * crée une nouvelle colonne avec le nombre d'occurrences de la valeur
 * rencontrée sur la colonne c
 * @param   {number} c - indice de colonne
 * @see     {@link majreferentiel}
 */
function nbOccurCol(c) {
    "use strict";
    var h = '',
        i = 0,
        v = '',
        dl = datalen,

        p = JSON.stringify([ ['colonne'.bold() + ' : ' + headers[c]] ]);
    bkp("nombre d'occurrences", p);

    majreferentiel(c);

    for (i = 0; i < dl; i++) {
        v = card[data[i][c]].toString();
        data[i].splice(0, 0, v);
    }

    h = "nb:" + headers[c];
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin nbOccurCol

/**
 * crée une nouvelle colonne avec le numéro de l'occurrence de chaque valeur de la colonne c,
 * en numérotant selon la direction dir
 * @param   {number} c   - indice de colonne
 * @param   {string} dir - direction : (ascendant), (descendant)
 */
function referentielCol(c, dir) {
    "use strict";
    var v = '',
        a = [],
        dl = datalen,
        h = '',
        i = 0,

        p = JSON.stringify([
            ['colonne'.bold() + ' : ' + headers[c]],
            ['  tri'.bold() + ' : ' + dir]
        ]);
    bkp("numéroter les occurrences", p);

    h = "num:" + headers[c];
    headers.splice(0, 0, h);
    if (dir === 'ascendant') {
        for (i = 0; i < dl; i++) {
            v = data[i][c];
            if (!a.hasOwnProperty(v)) { a[v] = 0; }
            a[v]++;
            data[i].splice(0, 0, a[v].toString() );
        }
    } else { // dir === 'descendant'
        for (i = dl - 1; i >= 0; i--) { // idem précédent, mais en partant de la fin
            v = data[i][c];
            if (!a.hasOwnProperty(v)) { a[v] = 0; }
            a[v]++;
            data[i].splice(0, 0, a[v].toString() );
        }
    }

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin referentielCol

/**
 * fonction d'analyse qui affiche le résultat des agrégats :
 * nombre de valeurs (nombre de lignes), somme, moyenne, espérance mathématique
 * écart-type, min, max
 * @param   {number} c - indice de colonne
 */
function calcul(c) {
    "use strict";
    var t = '',
        max = 0.0, // valeur arbitraire pour typer la variable
        min = 0.0, // valeur arbitraire pour typer la variable
        tot = 0.0,
        moy = 0.0,
        sig = 0.0,
        nbLVides = 0,
        v = 0.0,
        i = 0,
        dl = datalen;

    // trouver un min et un max consistants
    for (i = 0; i< dl; i++) {
        if (data[i][c] !== '') {
            max = st2float(data[i][c]);
            min = st2float(data[i][c]);
            break;
        }
    }

    // si le min et max n'ont pas pu être déterminés, on quitte le traitement
    if(i === dl && data[i - 1][c] === '') {
      attention('la colonne ne comporte aucune valeur numérique');
      return;
    }

    for (i = 0; i< dl; i++) {
        if (data[i][c] !== '') {
            v = st2float(data[i][c]);
            tot += v;
            max = v > max ? v : max;
            min = v < min ? v : min;
        } else {
          nbLVides++;
        }
    }

    moy = tot / (dl - nbLVides);
    for (i = 0; i < dl; i++) {
      if (data[i][c] !== '') {
        v = st2float(data[i][c]);
        sig += ((v - moy) * (v - moy));
      }
    }
    sig = Math.sqrt(sig / (dl - nbLVides));

    t  = `<table style="white-space:pre;">
          <thead>
          <tr><td>X</td><td>${headers[c]}</td></tr>
          </thead>
          <tbody id="tb_cc">
          <tr><td>card(X)</td><td>${(dl - nbLVides)}</td></tr>
          <tr><td>Σ(X)</td><td>${tot.toFixed(2)}</td></tr>
          <tr><td>E(X)</td><td>${moy.toFixed(2)}</td></tr>
          <tr><td>σ(X)</td><td>${sig.toFixed(2)}</td></tr>
          <tr><td>min(X)</td><td>${min}</td></tr>
          <tr><td>max(X)</td><td>${max}</td></tr>
          </tbody>
          </table>`;

    messageEtBoutons(c, t, 7, 'nocsv');
} // fin calcul

/**
 * @summary fonction d'analyse, synthèse de la colonne c
 * @description fonction d'analyse qui affiche une synthèse de la colonne c
 * - nombre de valeurs vides
 * - nombre de valeurs différentes
 * - longueurs min et max
 * - vérification que toutes les valeurs de c sont un "nombre entier" ou vide
 * - vérification heure ou vide
 * - vérification mail ou vide
 * - vérification code postal France ou vide
 * - vérification date au format ISO8601 ou vide
 * - nombre de valeurs possèdant des espaces consécutifs
 * - nombre de valeurs possèdant des espaces en début ou fin de la valeur
 * - nombre de valeurs ne contenant que des espaces ou des tabulations
 * @param   {number} c - indice de colonne (première opérande)
 */
function format(c) {
    "use strict";
    var isUniq = true,
        isFormatIntNsig = true,
        isFormatIntNeg = true,
        isFormatIntPos = true,
        isFormatInt  = true,
        isFormatHour = true,
        isFormatMail = true,
        isCPFrance = true,
        isDtISO8601 = true,
        nbHasBoundSpace = 0,
        nbHasMultiSpace = 0,
        nbIsEmpty = 0,
        min = 0,
        max = 0,
        nNull = 0,
        t = '',
        v = '',
        l = 0,
        i = 0,
        regIntNsig  = new RegExp('^([0-9]{1,3} ?)*$', ''),
        // nombre entier positif au format français : triplets de chiffres séparés ou non par un espace
        regIntNeg = new RegExp('^- ?([0-9]{1,3} ?)*$', ''),
        regIntPos = new RegExp('^[+] ?([0-9]{1,3} ?)*$', ''),
        regInt = new RegExp('^[-+]? ?([0-9]{1,3} ?)*$', ''),
        
        regHour = new RegExp('^(([0-1][0-9])|(2[0-4])):[0-5][0-9]:[0-5][0-9]$', ''),
        //regMail = new RegExp('^[^ ][^ ]+@[^ ][^ ]+\.[^ ][^ ]+$', ''),
        regMail = new RegExp('^[^ ][^ ]+@[^ ][^ ]+[.][^ ][^ ]+$', ''),
        regCPFr = new RegExp('^([0-9]{5})$', ''),
        reg8601 = new RegExp('^[0-9]{4}-((0[1-9])|(1[0-2]))-((0[1-9])|([1-2][0-9])|(3[0-1]))$', ''),
        regMSpc = new RegExp('^.* {2,}.*$', ''),
        regBSpc = new RegExp('^(( +.*)|(.* +))$', ''),
        regIEpt = new RegExp('^\\s+$'),
        dl = datalen;

    card = [];
    for (i = 0; i < dl; i++) {
        v = data[i][c];

        if (v === '') {
            nNull++;
        } else {
            if (!card.hasOwnProperty(v)) {
                card[v] = 1;
            } else {
                isUniq = false;
            }
            isFormatIntNsig = isFormatIntNsig  && v.search(regIntNsig)  >= 0;
            isFormatIntNeg = isFormatIntNeg && v.search(regIntNeg)  >= 0;
            isFormatIntPos = isFormatIntPos && v.search(regIntPos)  >= 0;
            isFormatInt    = isFormatInt  && v.search(regInt)  >= 0;
            isFormatHour   = isFormatHour && v.search(regHour) >= 0;
            isFormatMail   = isFormatMail && v.search(regMail) >= 0;
            isCPFrance     = isCPFrance   && v.search(regCPFr) >= 0;
            isDtISO8601    = isDtISO8601  && v.search(reg8601) >= 0;
            if (v.search(regMSpc) >= 0) { nbHasMultiSpace++; }
            if (v.search(regBSpc) >= 0) { nbHasBoundSpace++; }
            if (v.search(regIEpt) >= 0) { nbIsEmpty++; }

            l = v.length;
            if (min === 0) { min = l; }
            min = min < l ? min : l;
            max = max > l ? max : l;
        }
    }

    function td(x, y) { return x + '<td>' + y + '</td>'; }
    function tr(x, y) { return x + '<tr>' + y.reduce(td, '') + '</tr>'; }

    t  = '<table style="white-space:pre;">'
       +  '<thead><tr>'
       +  ['&nbsp;', headers[c], 'remarque'].reduce(td, '')
       +  '</tr></thead><tbody>';

    if (nNull === dl) {
        t += '<tr>'
           +  ['nombre de valeurs absentes', dl + ' (sur ' + dl + ')', '&nbsp;'].reduce(td, '')
           +  '</tr>';
    } else {
        t += [
            ['est unique', isUniq ? "oui" : "non", '&nbsp;'],
            ['nombre de valeurs absentes', nNull, ' (' + nNull + ' sur ' + dl + ') soit ' + (nNull / dl * 100.00).toFixed(2) + '%'],
            ['nb de valeurs différentes', t2dlength(card), '&nbsp;'],
            ['longueur min-max', min + '-' + max, '&nbsp;'],
            ['est un nombre entier non-signé ?', isFormatIntNsig ? "oui" : "non", regIntNsig.toString()],
            ['est un nombre entier négatif (signé -) ?', isFormatIntNeg ? "oui" : "non", regIntNeg.toString()],
            ['est un nombre entier positif (signé +) ?', isFormatIntPos ? "oui" : "non", regIntPos.toString()],
            ['est un nombre entier négatif ou positif (signé ou non) ?', isFormatInt ? "oui" : "non", regInt.toString()],
            ['est une heure ?', isFormatHour ? "oui" : "non", regHour.toString()],
            ['est un mail ?', isFormatMail ? "oui" : "non", regMail.toString()],
            ['est un code postal France ?', isCPFrance ? "oui" : "non", regCPFr.toString()],
            ['date au format AAAA-MM-JJ (ISO 8601) ?', isDtISO8601 ? "oui" : "non", reg8601.toString()],
            ['contient espaces consécutifs ?', nbHasMultiSpace.toString(), regMSpc.toString()],
            ['commence/se termine par des espaces ?', nbHasBoundSpace.toString(), regBSpc.toString()],
            ['ne contient que des espaces ou tabulations', nbIsEmpty.toString(), regIEpt.toString()]
        ].reduce(tr, '');
    }
    t += '</tbody></table>';

    messageEtBoutons(c, t, 11, 'nocsv');
} // fin format

/** crée une nouvelle colonne avec la longueur de chaque valeur de la colonne c
 * @param   {number} c - indice de colonne (première opérande)
 */
function mesurerLongueur(c) {
    "use strict";
    var v = '',
        dl = datalen,
        h = '',
        i = 0,

        p = JSON.stringify([
            ['colonne'.bold() + ' : ' + headers[c]]
        ]);
    bkp("mesurer la longueur des valeurs", p);

    h = "longueur:" + headers[c];
    headers.splice(0, 0, h);

    for (i = 0; i < dl; i++) {
        v = data[i][c].length;
        data[i].splice(0, 0, '' + v);
    }
 
    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin mesurerLongueur

/*
  Function: Amd

    constructeur

  Parameters:

    ope - Select HTML Element, agrégation
    c   - Select HTML Element, sélecteur de colonne (axe)
    ci  - Select HTML Element, sélecteur de colonne (axe)
    p   - Select HTML Element, sélecteur de colonne (faits)
    no  - Integer, incrément de l'analyse d'origine (optionnel si p=1)
    nc  - Integer, incrément de l'analyse courante (optionnel si p=1)
    f   - valeur (optionnelle si p=1)

FIXME : voir implémentation debug.js
*/
function Amd(ope, c, ci, p, no, nc, f) {
    this.ope = ope;
    this.c = c;
    this.ci = ci;
    this.no = no;
    this.nc = nc;
    this.p = p;
    this.f = f;
    this.mat = [];
    this.mesures = [];
    this.r = '';
    this.prec = null;
    this.decrireXSelon = function () {
        var t = '<table style="white-space:pre;" class="mesures">';
        if (ope === 'co'){
            t += '<tbody><tr><td>distribution de</td><td>(nombre de lignes)</td></tr>';
        } else {
            t += '<tbody><tr><td>distribution de</td><td>' + headers[this.c] + '</td></tr>';
        }
        t += '<tr><td>selon (X)</td><td>' + headers[this.ci] + '</td></tr></tbody>';
        if (p !== 1) { t += '<tbody><tr><td>' + headers[this.prec.ci] + '</td><td>' + this.f + '</td></tr></tbody>'; }
        else         { t += '<tbody><tr><td>&nbsp;</td><td>&nbsp;</td></tr></tbody>'; } // aligne visuellement
        t += '</table>';
        return t;
    };

    this.setmatrice = function () {
        var v = '',
            dl = 0,
            i = 0,
            j = 0,
            d = [];

        if (this.p === 1) {
            dl = datalen;
            for (i = 0; i < dl; i++) {
                v = data[i][this.ci];
                if (!this.mat.hasOwnProperty(v)) { this.mat[v] = []; }
                this.mat[v][this.mat[v].length] = i;
            }
        } else {
            d = this.prec.mat[this.f];
            dl = d.length;
            for (j = 0, i = 0; j < dl; j++) {
                i = d[j];
                v = data[i][this.ci];
                if (!this.mat.hasOwnProperty(v)) { this.mat[v] = []; }
                this.mat[v][this.mat[v].length] = i;
            }
        }
    };

    this.mesurer = function () {
        var i = 0,
            l = 0,
            v = [],
            x = 0,
            k = '';

        switch (this.ope) {
        case 'co':
            for (k in this.mat) {
                if (this.mat.hasOwnProperty(k)) {
                    this.mesures[k] = this.mat[k].length;
                }
            }
            break;

        case 'co-uq':
            for (k in this.mat) {
                if (this.mat.hasOwnProperty(k)) {
                    l = this.mat[k].length;
                    v = [];
                    for (i = 0; i < l; i++) { v[v.length] = data[this.mat[k][i]][this.c]; }
                    this.mesures[k] = new Set(v).size;
                }
            }
            break;

        case 'so':
            for (k in this.mat) {
                if (this.mat.hasOwnProperty(k)) {
                    this.mesures[k] = 0.0;
                    l = this.mat[k].length;
                    for (i = 0; i < l; i++) { this.mesures[k] += st2float(data[this.mat[k][i]][this.c]); }
                }
            }
            break;

        case 'co+so':
            for (k in this.mat) {
                if (this.mat.hasOwnProperty(k)) {
                    this.mesures[k] = [0, 0.0];
                    this.mesures[k][0] = this.mat[k].length;
                    l = this.mat[k].length;
                    for (i = 0; i < l; i++) { this.mesures[k][1] += st2float(data[this.mat[k][i]][this.c]); }
                }
            }
            break;

        case 'ex':
            for (k in this.mat) {
                if (this.mat.hasOwnProperty(k)) {
                    this.mesures[k] = [0.0, 0.0];
                    l = this.mat[k].length;
                    for (i = 0; i < l; i++) {
                        x = st2float(data[this.mat[k][i]][this.c]);
                        if (i === 0) { this.mesures[k] = [x, x]; }
                        this.mesures[k][0] = this.mesures[k][0] < x ? this.mesures[k][0] : x;
                        this.mesures[k][1] = this.mesures[k][1] > x ? this.mesures[k][1] : x;
                    }
                }
            }
            break;
        }
    };

    this.lignes = function () { return this.mat; };

    this.tab = function () {
        var h = '',
            b = '',
            f = '',
            t = '',
            k = '',
            i = 0,
            r = 0.0,
            tot = [];

        switch (this.ope) {
        case 'co':
            h = '<tr><td>X';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',0,true,'t');" + '">▼</a>';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',0,false,'t');" + '">▲</a>';
            h += '</td><td>nb</td><td>ratio';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',1,true,'n');" + '">▼</a>';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',1,false,'n');" + '">▲</a>';
            h += '</td></tr>';
            tot[0] = 0;
            for (k in this.mesures) {
                if (this.mesures.hasOwnProperty(k)) {
                    tot[0] += this.mesures[k];
                }
            }
            f = '<tr><td>TOTAUX</td><td>' + tot[0] + '</td><td>100.00 %</td></tr>';

            for (k in this.mesures) {
                if (this.mesures.hasOwnProperty(k)) {
                    b += '<tr onClick="forer(' + "'" + this.ope + "'" + ',' + this.c + ',' + (this.p + 1) + ',' + this.nc + ',this.firstChild.innerHTML);">';
                    b += '<td>' + k + '</td>';
                    r = this.mesures[k];
                    b += '<td>' + r + '</td>';
                    r = r * 100 / tot[0];
                    r = r.toFixed(2);
                    b += '<td>' + r + ' %</td>';
                    b += '</tr>';
                }
            }
            break;

        case 'co-uq':
            h = '<tr><td>X';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',0,true,'t');" + '">▼</a>';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',0,false,'t');" + '">▲</a>';
            h += '</td><td>nb</td><td>ratio';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',1,true,'n');" + '">▼</a>';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',1,false,'n');" + '">▲</a>';
            h += '</td></tr>';
            tot[0] = 0;
            for (k in this.mesures) {
                if (this.mesures.hasOwnProperty(k)) {
                    tot[0] += this.mesures[k];
                }
            }
            f = '<tr><td>TOTAUX</td><td>' + tot[0] + '</td><td>100.00 %</td></tr>';

            for (k in this.mesures) {
                if (this.mesures.hasOwnProperty(k)) {
                    b += '<tr onClick="forer(' + "'" + this.ope + "'" + ',' + this.c + ',' + (this.p + 1) + ',' + this.nc + ',this.firstChild.innerHTML);">';
                    b += '<td>' + k + '</td>';
                    r = this.mesures[k];
                    b += '<td>' + r + '</td>';
                    r = r * 100 / tot[0];
                    r = r.toFixed(2);
                    b += '<td>' + r + ' %</td>';
                    b += '</tr>';
                }
            }
            break;

        case 'so':
            h = '<tr><td>X';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',0,true,'t');" + '">▼</a>';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',0,false,'t');" + '">▲</a>';
            h += '</td><td>Σ</td><td>ratio';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',1,true,'n');" + '">▼</a>';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',1,false,'n');" + '">▲</a>';
            h += '</td></tr>';
            tot[0] = 0.0;
            for (k in this.mesures) {
                if (this.mesures.hasOwnProperty(k)) {
                    tot[0] += this.mesures[k];
                }
            }
            f = '<tr><td>TOTAUX</td><td>' + tot[0].toFixed(2) + '</td><td>100.00 %</td></tr>';

            for (k in this.mesures) {
                if (this.mesures.hasOwnProperty(k)) {
                    b += '<tr onClick="forer(' + "'" + this.ope + "'" + ',' + this.c + ',' + (this.p + 1) + ',' + this.nc + ',this.firstChild.innerHTML);">';
                    b += '<td>' + k + '</td>';
                    r = this.mesures[k].toFixed(2);
                    b += '<td>' + r + '</td>';
                    r = r * 100 / tot[0];
                    r = r.toFixed(2);
                    b += '<td>' + r + ' %</td>';
                    b += '</tr>';
                }
            }
            break;

        case 'co+so':
            h = '<tr><td>X';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',0,true,'t');" + '">▼</a>';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',0,false,'t');" + '">▲</a>';
            h += '</td>';
            h += '<td>nb</td><td>ratio';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',1,true,'n');" + '">▼</a>';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',1,false,'n');" + '">▲</a>';
            h += '</td>';
            h += '<td>Σ</td><td>ratio';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',3,true,'n');" + '">▼</a>';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',3,false,'n');" + '">▲</a>';
            h += '</td></tr>';
            tot = [0, 0.0];
            for (k in this.mesures) {
                if (this.mesures.hasOwnProperty(k)) {
                    tot[0] += this.mesures[k][0];
                    tot[1] += this.mesures[k][1];
                }
            }
            f = '<tr><td>TOTAUX</td>';
            f += '<td>' + tot[0] + '</td><td>100.00 %</td>';
            f += '<td>' + tot[1].toFixed(2) + '</td><td>100.00 %</td></tr>';

            for (k in this.mesures) {
                if (this.mesures.hasOwnProperty(k)) {
                    b += '<tr onClick="forer(' + "'" + this.ope + "'" + ',' + this.c + ',' + (this.p + 1) + ',' + this.nc + ',this.firstChild.innerHTML);">';
                    b += '<td>' + k + '</td>';
                    r = this.mesures[k][0];
                    b += '<td>' + r + '</td>';
                    r = r * 100 / tot[0];
                    r = r.toFixed(2);
                    b += '<td>' + r + ' %</td>';
                    r = this.mesures[k][1].toFixed(2);
                    b += '<td>' + r + '</td>';
                    r = r * 100 / tot[1];
                    r = r.toFixed(2);
                    b += '<td>' + r + ' %</td>';
                    b += '</tr>';
                }
            }
            break;

        case 'ex':
            h = '<tr><td>X';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',0,true,'t');" + '">▼</a>';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',0,false,'t');" + '">▲</a>';
            h += '</td>';
            h += '<td>min';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',1,true,'n');" + '">▼</a>';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',1,false,'n');" + '">▲</a>';
            h += '</td>';
            h += '<td>max';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',2,true,'n');" + '">▼</a>';
            h += '<a class="sbd" onclick="' + "sortTable('tb_fd" + nc + "',2,false,'n');" + '">▲</a>';
            h += '</td></tr>';
            tot = [0.0, 0.0, 0.0, 0.0]; // min(min), max(min), min(max), max(max)

            for (k in this.mesures) {
                if (this.mesures.hasOwnProperty(k)) {
                    if (i === 0) {
                        var d = JSON.parse(JSON.stringify(this.mesures[k])); // = copie
                        tot = [d[0], d[0], d[1], d[1]];
                    }
                    i++;
                    tot[0] = this.mesures[k][0] < tot[0] ? this.mesures[k][0] : tot[0];
                    tot[1] = this.mesures[k][0] > tot[1] ? this.mesures[k][0] : tot[1];
                    tot[2] = this.mesures[k][1] < tot[2] ? this.mesures[k][1] : tot[2];
                    tot[3] = this.mesures[k][1] > tot[3] ? this.mesures[k][1] : tot[3];
                }
            }
            f = '<tr><td>SEUILS</td>';
            f += '<td>[' + tot[0] + ' ; ' + tot[1] + ']</td>';
            f += '<td>[' + tot[2] + ' ; ' + tot[3] + ']</td>';

            for (k in this.mesures) {
                if (this.mesures.hasOwnProperty(k)) {
                    b += '<tr onClick="forer(' + "'" + this.ope + "'" + ',' + this.c + ',' + (this.p + 1) + ',' + this.nc + ',this.firstChild.innerHTML);">';
                    b += '<td>' + k + '</td>';
                    r = this.mesures[k][0];
                    b += '<td>' + r + '</td>';
                    r = this.mesures[k][1];
                    b += '<td>' + r + '</td>';
                    b += '</tr>';
                }
            }

            break;

        }
        t = '<table style="white-space:pre;" class="hypercube">';
        t += '<thead>' + h + '</thead>';
        t += '<tbody id="tb_fd' + nc + '">' + b + '</tbody>';
        t += '<tfoot>' + f + '</tfoot>';
        t += '</table>';
        return t;
    };
}

/*
 amd builder

FIXME : voir implémentation debug.js
 * @see     {@link Date.prototype.timeNow}
 * @see     {@link horodatage}
 */
function Amdb(ope) {
    this.ope = ope;
    this.amds = [];
    this.clean = function () {
        while (this.amds.length > 0) {
             this.amds[this.amds.length - 1].prec = null;
             this.amds[this.amds.length - 1] = null;
             this.amds.pop();
        }
    };
    this.addNewAmd = function (ope, c, ci, p, no, f) {
        var aamd = new Amd(ope, c, ci, p, no, this.amds.length, f);
        if (p !== 1) { aamd.prec = this.amds[no]; }
        this.amds[this.amds.length] = aamd;
        return aamd;
    };

    this.cube2html = function (n) {
        var aamd = aamdB.amds[n],
            d = [],
            ch2 = '<table style="white-space:pre;"><tHead><tr>',
            i = 0,
            k = '',
            j = 0,
            url = window.location.href.split('/'),
            h = new Date().timeNow(),
            t = '',
            aBlob;

        h = h.split('');
        h = h[0] + h[1] + ':' + h[2] + h[3] + ':' + h[4] + h[5];

        for (i = 0; i < headers.length; i++) { ch2 += '<td>' + headers[i] + '</td>'; }
        ch2 += '</tr></tHead><tbody>';

        for (k in aamd.mat) {
            if (aamd.mat.hasOwnProperty(k)) {
                d = d.concat(aamd.mat[k]);
            }
        }
        d.sort(); // évolution : si les données sont numériques, ajouter une fonction f(...)
        for (i = 0; i < d.length; i++) {
            ch2 += '<tr>';
            for (j = 0; j < headers.length; j++) { ch2 += '<td>' + data[d[i]][j] + '</td>'; }
            ch2 += '</tr>';
        }
        ch2 += '</tbody></table>';

        url.pop();
        t = '<html lang="fr">'
          + '<head><meta charset="utf-8"/>'
          + '<title>Illico (explorer/HTML) ' + h + '</title>'
/* + '<link href="' + url.join('/') + '/css/style.css" rel="stylesheet" type="text/css" />' */
          + "<style>" + css_export + "</style>"
          + '</head>'
          + '<body class="export">'
/* + '<script src="' + url.join('/') + '/scripts/sort.js"></script>'; */
          + '<script>' + sortTable.toString() + '</script>';

    aBlob = new Blob([t, ch2, '</body></html>'], {type : 'text/html;charset=' + 'UTF-8', encoding : 'UTF-8'});
    window.open(URL.createObjectURL(aBlob, {oneTimeOnly : true}));
    };

    this.cube2csv = function (n) {
        var aamd = aamdB.amds[n],
            d = [],
            ch2 = '',
            k = '',
            i = 0,
            url = window.location.href.split('/'), //@deprecated : inutilisé ?
            conf = confGlobal, // @deprecated, passer en paramètre ?
            choixencode = 'UTF-8',
            aBlob;

        ch2 += headers.join(conf.sepa) + '\n';

        for (k in aamd.mat) {
            if (aamd.mat.hasOwnProperty(k)) {
                d = d.concat(aamd.mat[k]);
            }
        }
        d.sort(); // évolution : si les données sont numériques, ajouter une fonction f(...)
        for (i = 0; i < d.length; i++) { ch2 += data[d[i]].join(conf.sepa) + '\n'; }

        aBlob = new Blob(['\ufeff', ch2], {type: 'data:application/csv;charset=' + choixencode, encoding: choixencode});
        dgebi('dl_explorer').href = window.URL.createObjectURL(aBlob, {oneTimeOnly : true});
        dgebi('dl_explorer').download = 'illico_explorer__' + horodatage() + '.csv';
        dgebi('dl_explorer').click();

    };

    this.cube2tab = function (n) {
        var aamd = aamdB.amds[n],
            ch2 = aamd.decrireXSelon() + aamd.tab(),
/* url = window.location.href.split('/'), */
            t = '',
            h = new Date().timeNow(),
            aBlob;

        h = h.split('');
        h = h[0] + h[1] + ':' + h[2] + h[3] + ':' + h[4] + h[5];

/* url.pop(); */
        t = '<html lang="fr">'
          + '<head><meta charset="utf-8"/>'
          + '<title>Illico (explorer/TAB) ' + h + '</title>'
/* + '<link href="' + url.join('/') + '/css/style.css" rel="stylesheet" type="text/css" />' */
          + "<style>" + css_msg + "</style>"
          + '</head>'
          + '<body>'
/* + '<script src="' + url.join('/') + '/scripts/sort.js"></script>'; */
          + '<script>' + sortTable.toString() + '</script>';

    aBlob = new Blob([t, ch2, '</body></html>'], {type : 'text/html;charset=' + 'UTF-8', encoding : 'UTF-8'});
    window.open(URL.createObjectURL(aBlob, {oneTimeOnly : true}));
    };
}

/*
  Function: forer

    constuit le tableau de syntèse de l'opération ope
    sur la colonne p, rapportée à la colonne c
    en liant le nouveau de synthèse au précédent no

  Parameters:

    ope - Select HTML Element, agrégation
    c   - Select HTML Element, sélecteur de colonne (axe)
    p   - Select HTML Element, sélecteur de colonne (faits)
    no  - Integer, incrément de l'analyse d'origine (optionnel si p=1)
    v   - valeur (optionnelle si p=1)

FIXME : voir implémentation debug.js
*/
function forer(ope, c, p, no, v) {
    "use strict";
    var ci = 0,
        t = '',
        aamd = null,
        newart = document.createElement('article');

    if (v) { v = unescapeHTML(v); }

    try {
        ci = valeur('col__agregats_7-' + p);
        message("distribution des valeurs selon l'axe " + headers[ci].bold());
    } catch (e) {
        attention("sélectionnez d'abord un axe supplémentaire");
        return;
    }

    if (p === 1) {
        if (aamdB !== null) { aamdB.clean(); }
        aamdB = new Amdb(ope);
    }

    aamd = aamdB.addNewAmd(ope, c, ci, p, no, v);

    t += '<input type="button" class="dlx" onClick="this.parentNode.parentNode.removeChild(this.parentNode);" value="X" />';
    t += '<br />';
    t += aamd.decrireXSelon();
    aamd.setmatrice();
    aamd.mesurer();

    t += aamd.tab();
    t += '<br />';
    t += '<input type="button" class="dlx" onClick="aamdB.cube2html(' + aamd.nc + ');" value="⇒ HTML" />';
    t += '<input type="button" class="dlx" onClick="aamdB.cube2csv(' + aamd.nc + ');" value="⇒ CSV" />';
    t += '<input type="button" class="dlx" onClick="aamdB.cube2tab(' + aamd.nc + ');" value="⇒ TAB" />';
    //t += '<table><tr><td>&nbsp;</td></tr></table>'; // séparation pour export HTML
    t += '<br>';

    //masquer('maitre');
    console.warn("forer -> masquer('maitre'), désactivé");
    //afficher('cube');
    dgebi('cube').style.display = 'block'; // @deprecated : à gérer par CSS

    if (p === 1) {
        while (dgebi('axes').childNodes.length >= 1) { dgebi('axes').removeChild(dgebi('axes').firstChild); }
    }

    newart.innerHTML = t;
    dgebi('axes').appendChild(newart);
} // fin forer

/**
 * met à jour le sélecteur de colonnes e à partir des données
 * @param   {Object} conf - configuration
 * @param   {Object} e - Select HTML Element, sélecteur de colonne (du fichier maître)
 * @see     {@link genererListe}
 * @see     {@link genheaderslkp}
 * @TODO FIXME prendre l'implémentation genheaderslkp (slice) + conf.ign si conf.ign défini
 */
function genheaders(conf, e) {
    "use strict";
    genererListe(e, txt.split('\n')[conf.ign].split(conf.sepa), false);
}

/**
 * met à jour le sélecteur de colonnes e du fichier lookup à partir des données
 * @param   {Object} conf - configuration
 * @param   {Object} e - Select HTML Element, sélecteur de colonne (du fichier lookup)
 * @see     {@link genererListe}
 * @see     {@link genheaders}
 */
function genheaderslkp(conf, e) {
    "use strict";
    var lg = txtlkp.indexOf('\n', 0);

    headerslkp = txtlkp.slice(0, lg).split(conf.sepa);

    // on met à jour le sélecteur de colonne avec les en-têtes du fichier lookup
    genererListe(e, headerslkp, false);
}

/**
 * met à jour le sélecteur de colonnes e de la table des correspondances à partir des données
 * @param   {Object} conf - configuration
 * @param   {Object} e - Select HTML Element, sélecteur de colonne
 * @see     {@link genererListe}
 * @see     {@link genheaders}
 */
function genheadersListes(conf, e) {
    "use strict";
    var lg = valeur('val_recode_b').indexOf('\n', 0);

    genererListe(e, valeur('val_recode_b').slice(0, lg).split(conf.sepa), false);
}

/**
 * fonction d'analyse qui affiche un tableau croisé-dynamique des valeurs de c1 réparties selon celles de c2
 * - le nombre de valeurs
 * - la proportion
 * Les colonnes (valeurs de c2) sont triées selon fs et ft
 * @param   {number} c1 - indice de colonne
 * @param   {number} c2 - indice de colonne
 * @param   {string} fs - option, sens de tri : tri ascendant (asc), tri descendant (desc)
 * @param   {string} ft - option, type de tri : alphabétique (alpha), numérique (num)
 */
function adjacence(c1, c2, fs, ft) {
    "use strict";
    var d = [],
        da = [],
        h = [],
        v1 = '',
        v2 = '',
        t = '',
        i = 0,
        r = 0.0,
        to = 0,
        tot = 0,
        dl = datalen;

    function f(a, b) { return st2float(a) - st2float(b); }

    // construction des tableaux associatifs
    for (i = 0; i < dl; i++) {
        v1 = data[i][c1];
        v2 = data[i][c2];

        // pour chaque valeur v1 (de c1)...
        if (!d.hasOwnProperty(v1)) { d[v1] = []; }
        if (!d[v1].hasOwnProperty(v2)) { d[v1][v2] = 0; }
        // ... on compte le nombre de valeurs v2 (de c2) rencontrées
        d[v1][v2]++;

        // pour chaque valeur v2 (de c2)...
        if (!da.hasOwnProperty(v2)) { da[v2] = 0; }
        // ... on compte le nombre total d'occurrence
        da[v2]++;

        // on compte le nombre total de valeur v2 (toute valeur confondue)
        tot++;
    }

    // avec chaque valeur v2 (de c2), on crée un en-tête de la matrice d'adjacence
    // remarque : cela permet aussi de fixer un ordre des valeurs
    // car selon les implémentations des moteurs JS,
    // l'ordre du parcours des clés d'un tableau associatif
    // n'est pas forcément le même d'un navigateur à un autre
    for (v2 in da) {
        if (da.hasOwnProperty(v2)) {
             h[h.length] = v2;
        }
    }

    // on applique le tri demandé par l'utilisateur
    if (ft === 'alpha') {
      if (fs === 'asc') {
        console.info('h trié ? : ' + h.sort());
      } else {
        h.sort().reverse();
      }
    } else {
      if (fs === 'asc') {
        h.sort(f);
      } else {
        h.sort(f).reverse();
      }
    }

    t  = '<table style="white-space:pre;">'
    // on dessine l'en-tête de la matrice d'adjacence
       + '<thead><tr>'
    // la première colonne va contenir les valeurs v1 (de c1)
       + '<td style="border-right: 1px solid #dddddd;">' + headers[c1]
       + '<a class="sbd" onclick="' + "sortTable('tb_adj',0,true, 't');" + '">▼</a>'
       + '<a class="sbd" onclick="' + "sortTable('tb_adj',0,false,'t');" + '">▲</a>'
       + '</td>';

    // les autres colonnes représentent les valeurs de v2
    for (i = 1; i < h.length +1 ; i++) { // on commence à 1 car la colonne '0' est définie juste au-dessus
        t += '<td>'
           + h[i-1] // (i-1) permet de commencer à l'item '0'
           + '<a class="sbd" onclick="' + "sortTable('tb_adj'," + i + ",true, 'n');" + '">▼</a>'
           + '<a class="sbd" onclick="' + "sortTable('tb_adj'," + i + ",false,'n');" + '">▲</a>'
           + '</td>';
    }
    t += '<td style="border-left: 1px solid #dddddd;">TOTAUX</td>'
       + '<td>ratio'
       + '<a class="sbd" onclick="' + "sortTable('tb_adj'," + i + ",true, 'n');" + '">▼</a>'
       + '<a class="sbd" onclick="' + "sortTable('tb_adj'," + i + ",false,'n');" + '">▲</a>'
       + '</td>'
       + '</tr></thead>'
       + '<tbody id="tb_adj">';

    // on compose chaque ligne de la matrice d'adjacence
    for (v1 in d) {
        // 1 ligne pour chaque valeur de v1 (de c1)
        if (d.hasOwnProperty(v1)) {
             t += '<tr>'
               +  '<td>' + v1 + '</td>';
             to = 0;
             for (i = 0; i < h.length; i++) {
                 if (!d[v1].hasOwnProperty(h[i])) {
                     //t += '<td style="visibility:hidden;">0</td>';
                     t += '<td>&nbsp;</td>';
                 } else {
                     t += '<td>' + d[v1][h[i]] + '</td>';
                     to += d[v1][h[i]];
                 }
             }
             r = parseInt(to, 10) * 100 / tot;
             r = r.toFixed(2);
             t += '<td>' + to + '</td>'
                + '<td>' + r + ' %</td>'
                + '</tr>';
        }
    }

    t += '</tbody>'

    // on dessine la ligne des totaux
       + '<tfoot><tr>'
       + '<td>TOTAUX</td>';
    for (i = 0; i < h.length; i++) { t += '<td>' + da[h[i]] + '</td>'; }
    t += '<td>' + tot + '</td>'
       + '<td>&nbsp;</td>'
       + '</tr>'
       + '<tr>'
       + '<td>ratio</td>';
    for (i = 0; i < h.length; i++) {
        r = parseInt(da[h[i]], 10) * 100 / tot;
        r = r.toFixed(2);
        t += '<td>' + r + ' %</td>';
    }
    t += '<td>&nbsp;</td><td>100.00 %</td>'
       + '</tr></tfoot>'
       + '</table>';

    messageEtBoutons(c1, t, t2dlength(d), 'nocsv');
} // fin adjacence

/**
 * applique pour chaque valeur de la colonne c les couples facteur/unité
 * pour obtenir autant de colonnes que de sous-unités
 * @param   {number} c - indice de colonne
 */
function discretiser(c) {
    "use strict";
    var dl = datalen,
        a = [],
        r = [],
        v = '',
        u = '',
        n = 0,
        l = 0,
        i = 0,
        j = 0,

        p = JSON.stringify([ ['colonne'.bold() + ' : ' + headers[c]] ]);
    bkp('convertir dans un système de comptage mixte', p);

    // construction de la grille des facteurs
    a[0] = { 'groupe': valeur('unite0'),
             'facteur': 1,
             'unite': valeur('unite0'),
             'nbU0': 1
            };
    for (i = 1; i < 7; i++) {
        v = valeur('unite' + i);
        u = valeur('unite' + (i - 1));
        n = parseInt(valeur('facteur' + i), 10);

        if (!v) { break; }

        a[i] = { 'groupe': v,
                 'facteur': n,
                 'unite': u,
                 'nbU0': n * a[i-1].nbU0
               };
    }

    l = a.length;
    a = a.reverse();

    // MAJ en-tête
    headers.splice(0, 0, ...(a.map(x => x.groupe)));

    // calcul et MAJ données
    for (i = 0; i < dl; i++) {
        r = [];
        if (data[i][c] === '') {
          for (j = 0; j < l; j++) {
            r.push('');
          }
          data[i].splice(0, 0, ...r);
          continue;
        }

        // else
        v = parseInt(data[i][c], 10);
        n = 0;
        for (j = 0; j < l; j++) {
            n = Math.trunc(v / a[j].nbU0);
            r.push('' + n);
            v = v - (n * a[j].nbU0);
        }
        data[i].splice(0, 0, ...r);
    }

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(
            a.map(
              x => x.groupe.bold()
              + ' : '
              + x.facteur
              + ' '
              + (x.facteur > 1 ? pluriel(x.unite) : x.unite)
              )
            )
          );
} // fin discretiser

/**
 * produit une liste de méta-données pour les colonnes cs sélectionnées
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 */
 function metadonnees(cs) {
   "use strict";
   var dl = datalen,
        i = 0,
        c = 0,
        a = [],
        t = '',

       vs = new Set(),
       vv = 0,
       uq = true,
        v = '',
      cpt = 0;

  for (c = 0; c < cs.length; c++) {
    if (cs[c].selected) {

      vs = new Set();
      uq = true;
      cpt = 0;
      vv = 0;

      for (i = 0; i < dl; i++) {

        v = data[i][c];

        if (v === '') {
          vv++;
        } else {
          cpt++;
          if(uq && vs.has(v) ) { uq = false; }
          vs.add(v);
        }

      } // fin toutes les lignes

      a[a.length] = {
        'uniq': uq,
        'nb_val': cpt,
        'nb_val_unique': vs.size,
        'nb_val_vide': vv
      };
    }
  } // fin pour toutes les colonnes sélectionnées

  t = `<table class="table-center"><thead><tr>
       <td>colonne</td>
       <td>val. uniques ?</td>
       <td>nb lig. sans valeur</td>
       <td>nb lig. renseignées</td>
       <td>nb val. différentes</td>
       </tr></thead>
       <tbody>`;

  i = 0;
  for (c = 0; c < cs.length; c++) {
    if (cs[c].selected) {
      t+= `<tr>
           <td>${headers[c]}</td>
           <td>${a[i].uniq ? 'oui' : '-'}</td>
           <td>${a[i].nb_val_vide}</td>
           <td>${a[i].nb_val}</td>
           <td>${a[i].nb_val_unique}</td>
           </tr>`;
      i++;
    }
  }

   t += '</tbody>'
     + '</table>';
   messageEtBoutons(0, t, 10, 'nocsv');
 } // fin metadonnees


// ...................................................................... temps


/**
 * conversion d'un temps exprimé en hh:mm ou mm:ss en fraction de 100
 * (changement de base 60 vers base 100)
 * @param   {number} c - indice de colonne
 * @see     {@link conv100_60}
 */
function conv60_100(c) {
    "use strict";
    var dl = datalen,
        i = 0,

        p = JSON.stringify([ ['colonne'.bold() + ' : ' + headers[c]] ]);
    bkp('convertir x/60 en y/100', p);

    for (i = 0; i < dl; i++) {
        data[i][c] = convertirDecimales(data[i][c], 100, 60);
    }
    headers[c] += ' / 60->100';

    resultat();
    message('terminé');
} // fin conv60_100

/**
 * conversion d'un temps exprimé en fraction de 100 en un temps exprimé en hh:mm ou mm:ss
 * (changement de base 100 vers base 60)
 * @param   {number} c - indice de colonne
 * @see     {@link conv60_100}
 */
function conv100_60(c) {
    "use strict";
    var dl = datalen,
        i = 0,

        p = JSON.stringify([ ['colonne'.bold() + ' : ' + headers[c]] ]);
    bkp('convertir x/100 en y/60', p);

    for (i = 0; i < dl; i++) {
        data[i][c] = convertirDecimales(data[i][c], 60, 100);
    }
    headers[c] += ' / 100->60';

    resultat();
    message('terminé');
} // fin conv100_60

/**
 * ajoute une nouvelle colonne avec le cumul des temps des 2 colonnes c1 et c2
 * compte-tenu du format de temps sélectionné
 * (une durée non-renseignée est équivalent à 00:00:00)
 * @param   {string} f  - format de la durée : (hh:mm:ss), (hh:mm), (mm:ss)
 * @param   {number} c1 - indice de colonne
 * @param   {number} c2 - indice de colonne
 */
function cumulerTemps(f, c1, c2) {
    "use strict";
    var dl = datalen,
        h = '',
        t1 = [],
        t2 = [],
        s = [],
        v = '',
        i = 0,

        p = JSON.stringify([
             ['format'.bold() + ' : ' + f],
             ['  colonne 1'.bold() + ' : ' + headers[c1]],
             ['  colonne 2'.bold() + ' : ' + headers[c2]]
        ]);
    bkp('cumuler 2 temps', p);

    switch (f) {
    case 'hh:mm:ss':
        for (i = 0; i < dl; i++) {
             if (data[i][c1] === '' || data[i][c2] === '') {
                 v = data[i][c1] + data[i][c2];
             } else {
                 t1 = data[i][c1].split(':');
                 t2 = data[i][c2].split(':');
                 s = [0, 0, 0];

                 // secondes
                 s[2] = parseInt(t1[2], 10) + parseInt(t2[2], 10);
                 if (s[2] >= 60) {
                     s[2] -= 60;
                     s[1] += 1;
                 }

                 // minutes
                 s[1] += parseInt(t1[1], 10) + parseInt(t2[1], 10);
                 if (s[1] >= 60) {
                     s[1] -= 60;
                     s[0] += 1;
                 }

                 // heures
                 s[0] += parseInt(t1[0], 10) + parseInt(t2[0], 10);

                 // on rectifie l'affichage des nombres inférieurs à 10
                 // en ajoutant un 0 au début
                 v = (s[0] < 10 ? '0' + s[0] : s[0]) + ':';
                 v = v + (s[1] < 10 ? '0' + s[1] : s[1]) + ':';
                 v = v + (s[2] < 10 ? '0' + s[2] : s[2]);
             }

             // mise à jour du tableau résultat
             data[i].splice(0, 0, v);
        }
        break;
    case 'hh:mm':
        /* falls through */
    case 'mm:ss':
        for (i = 0; i < dl; i++) {
             if (data[i][c1] === '' || data[i][c2] === '') {
                 v = data[i][c1] + data[i][c2];
             } else {
                 t1 = data[i][c1].split(':');
                 t2 = data[i][c2].split(':');
                 s = [0, 0, 0];

                 // minutes (hh:mm) ou secondes (mm:ss)
                 s[1] += parseInt(t1[1], 10) + parseInt(t2[1], 10);
                 if (s[1] >= 60) {
                     s[1] -= 60;
                     s[0] += 1;
                 }

                 // heures (hh:mm) ou minutes (mm:ss)
                 s[0] += parseInt(t1[0], 10) + parseInt(t2[0], 10);

                 // on rectifie l'affichage des nombres inférieurs à 10
                 // en ajoutant un 0 au début
                 v = (s[0] < 10 ? '0' + s[0] : s[0]) + ':';
                 v = v + (s[1] < 10 ? '0' + s[1] : s[1]);
             }

             // mise à jour du tableau résultat
             data[i].splice(0, 0, v);
        }
        break;
    }

    h = '(' + headers[c1] + ') + (' + headers[c2] + ')';
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin cumulerTemps

/**
 * ajoute une nouvelle colonne avec la différence des temps des 2 colonnes c1 et c2
 * compte-tenu du format de temps sélectionné
 * FIXME : à vérifier : (une durée non-renseignée est équivalente à 00:00:00)
 * @param   {string} f  - format de la durée : (hh:mm:ss), (hh:mm), (mm:ss)
 * @param   {number} c1 - indice de colonne
 * @param   {number} c2 - indice de colonne
 */
function soustraireTemps(f, c1, c2) {
    "use strict";
    var dl = datalen,
        h = '',
        t1 = [],
        t2 = [],
        v1 = '',
        v2 = '',
        z = '',
        v = 0,
        s = [],
        i = 0,

        p = JSON.stringify([
             ['format'.bold() + ' : ' + f],
             ['  début'.bold() + ' : ' + headers[c1]],
             ['  fin'.bold() + ' : ' + headers[c2]]
        ]);
    bkp('calculer une durée (temps) entre 2 instants', p);

    switch (f) {
    case 'hh:mm:ss':
        s = [0, 0, 0];
        for (i = 0; i < dl; i++) {
             if (data[i][c1] === '' || data[i][c2] === '') {
                 z = 'donnée manquante';
             } else {
                 t1 = data[i][c1].split(':');
                 t2 = data[i][c2].split(':');

                 // conversion en secondes
                 v1 = parseInt(t1[2], 10) + 60 * (parseInt(t1[1], 10) + 60 * parseInt(t1[0], 10));
                 v2 = parseInt(t2[2], 10) + 60 * (parseInt(t2[1], 10) + 60 * parseInt(t2[0], 10));
                 if (v2 < v1) {
                     v2 += 24 * 60 * 60;
                 }

                 // calcul
                 v = v2 - v1;

                 // conversion en hh:mm:ss
                 s[0] = Math.floor(v / 3600);
                 s[1] = Math.floor((v - (s[0] * 3600)) / 60);
                 s[2] = v - (s[0] * 3600) - (s[1] * 60);
                 if (s[0] >= 24) {
                     s[0] -= 24;
                 }

                 // on rectifie l'affichage des nombres inférieurs à 10
                 // en ajoutant un 0 au début
                 z = (s[0] < 10 ? '0' + s[0] : s[0]) + ':';
                 z = z + (s[1] < 10 ? '0' + s[1] : s[1]) + ':';
                 z = z + (s[2] < 10 ? '0' + s[2] : s[2]);
             }
             data[i].splice(0, 0, z);
        }
        break;
    case 'hh:mm':
        s = [0, 0];
        for (i = 0; i < dl; i++) {
             if (data[i][c1] === '' || data[i][c2] === '') {
                 z = 'donnée manquante';
             } else {
                 t1 = data[i][c1].split(':');
                 t2 = data[i][c2].split(':');

                 // conversion en minutes
                 v1 = parseInt(t1[1], 10) + 60 * parseInt(t1[0], 10);
                 v2 = parseInt(t2[1], 10) + 60 * parseInt(t2[0], 10);
                 if (v2 < v1) {
                     v2 += 24 * 60;
                 }

                 // calcul
                 v = v2 - v1;

                 // conversion en hh:mm
                 s[0] = Math.floor(v / 60);
                 s[1] = v - (s[0] * 60);
                 if (s[0] >= 24) {
                     s[0] -= 24;
                 }

                 // on rectifie l'affichage des nombres inférieurs à 10
                 // en ajoutant un 0 au début
                 z = (s[0] < 10 ? '0' + s[0] : s[0]) + ':';
                 z = z + (s[1] < 10 ? '0' + s[1] : s[1]);
             }
             data[i].splice(0, 0, z);
        }
        break;
    case 'mm:ss':
        s = [0, 0];
        for (i = 0; i < dl; i++) {
             if (data[i][c1] === '' || data[i][c2] === '') {
                 z = 'donnée manquante';
             } else {
                 t1 = data[i][c1].split(':');
                 t2 = data[i][c2].split(':');

                 // conversion en secondes
                 v1 = parseInt(t1[1], 10) + 60 * parseInt(t1[0], 10);
                 v2 = parseInt(t2[1], 10) + 60 * parseInt(t2[0], 10);
                 if (v2 < v1) {
                     v2 += 60 * 60;
                 }

                 // calcul
                 v = v2 - v1;

                 // conversion en mm:ss
                 s[0] = Math.floor(v / 60);
                 s[1] = v - (s[0] * 60);
                 if (s[0] >= 60) {
                     s[0] -= 60;
                 }

                 // on rectifie l'affichage des nombres inférieurs à 10
                 // en ajoutant un 0 au début
                 z = (s[0] < 10 ? '0' + s[0] : s[0]) + ':';
                 z = z + (s[1] < 10 ? '0' + s[1] : s[1]);
             }
             data[i].splice(0, 0, z);
        }
        break;
    }

    h = 'temps écoulé [' + headers[c1] + ',' + headers[c2] + ']';
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin soustraireTemps

/**
 * ajoute une nouvelle colonne avec la différence des dates des 2 colonnes c1 et c2
 * compte-tenu du format de date sélectionné
 * @param   {string} f  - format de la date : (jj mm ssaa), (mm jj ssaa), (ssaa mm jj)
 * @param   {string} s  - séparateur : (.), (-), (/), ( )
 * @param   {number} c1 - indice de colonne
 * @param   {number} c2 - indice de colonne
 * @param   {string} o  - dernier jour de l'intervalle (est exclu), (est inclus)
 * @param   {string} cj - compter les jours (calendaires), (ouvrés)
 * @see     {@link nbJoursIntervalleDernierJourExclu}
 * @see     {@link nbJoursIntervalleDernierJourInclus}
 * @see     {@link UN_JOUR_EN_MS}
 */
function differenceDates(f, s, c1, c2, o, cj) {
    "use strict";
    var dl = datalen,
        i = 0,
        v1 = '',
        v2 = '',
        r1 = '',
        r2 = '',
        a = [],
        reg8601    = new RegExp('^[0-9]{4}-((0[1-9])|(1[0-2]))-((0[1-9])|([1-2][0-9])|(3[0-1]))$', ''),
        v = '',
        h = '',
        formater   = function(){},
        diff2dates = function(){},

        p = JSON.stringify([
             ['format'.bold() + ' : ' + f],
             ['séparateur'.bold() + ' : ' + (s === ' ' ? '(espace)' : s)],
             ['  début'.bold() + ' : ' + headers[c1]],
             ['  fin'.bold() + ' : ' + headers[c2]],
             ["dernier jour de l'intervalle".bold() + ' : ' + o],
             ['compter les jours'.bold() + ' : ' + o]
        ]);
    bkp('calculer une durée (jours) entre 2 dates', p);

    // adaptation de la fonction formater() en fonction du format de la date
    formater = convertirDate(f);

    // adaptation de la formation diff2dates() en fonction du traitement des bornes
    if (cj === 'calendaires') {
      if (o === 'est exclu') {
        diff2dates = function(d1, d2) { return nbJoursIntervalleDernierJourExclu(d1, d2); };
      }
      if (o === 'est inclus') {
        diff2dates = function(d1, d2) { return nbJoursIntervalleDernierJourInclus(d1, d2); };
      }
    } // fin cj === 'calendaires'

    if (cj === 'ouvrés') {

      diff2dates = function(d1, d2) {

        // pas de calcul pour les intervalles inversés
        if(d2.getTime() < d1.getTime() ) {
          return 'intervalle inversé';
        }
        if (o === 'est exclu' && d2.getTime() === d1.getTime() ) { return 'intervalle trop court'; }

        let deb  =  8 - (d1.getUTCDay() || 7 );  // entre 7 et 1
        let fin  =      (d2.getUTCDay() || 7 );  // entre 1 et 7
        let per  = Math.ceil( (d2.getTime() - d1.getTime() ) / UN_JOUR_EN_MS) + 1; // nombre de jours calendaires (borne fin incluse)
        let ouv  = (per - deb - fin); // nombre de jours calendaires moins les 2 semaines début/fin, conv
            ouv /=  7;                // converti en nombre de semaines
            ouv *=  5;                // converti en nombre de jours ouvrés
        let pl   =  6 - (d1.getUTCDay() || 6 );     // nombre de jours ouvrés avant le premier lundi
        let dl   = (d2.getUTCDay() %  6 ) || 5;     // nombre de jours ouvrés après le dernier lundi
/*
        console.info('- - - - - - - - - - - - - - - - - - - -');
        console.log('d1.getUTCDay()                  : ' + d1.getUTCDay()        );
        console.log('d1.getUTCDay() || 7             : ' + ( d1.getUTCDay() || 7) );
        console.log('deb = 8 - (d1.getUTCDay() || 7) : ' + deb                );
        console.log('d2.getUTCDay()                  : ' + d2.getUTCDay()        );
        console.log('fin = d2.getUTCDay() || 7       : ' + fin                );
        console.log('per                             : ' + per                );
        console.log('per - deb - fin                 : ' +  (per - deb - fin) );
        console.warn('(per - deb - fin) / 7 * 5)     : ' + ( (per - deb - fin) / 7 * 5 ) );
        console.warn(' = ouv                         : ' + ouv                           );
        console.log('nb jrs ouvrés 1er lundi         : ' + pl                 );
        console.log('nb jrs ouvrés dernier lundi     : ' + dl                 );
        console.log('dernier jour est jour ouvré ?     ' + (d2.getUTCDay() % 6 ? 'oui' : 'non')   );
*/

	    console.log( {
			  col:data[i][1],
			  info: {
				  deb: deb,
				  fin: fin,
				  per:per,
				  ouv:ouv,
				  pl:pl,
				  dl:dl
				  }
			  }
	    );

        // cas particulier : la période ne couvre pas une semaine complète
        if (ouv < 0) {
          let v = (d2.getUTCDay() % 6) - (d1.getUTCDay() % 6 );
          if (v < 0) { v += 6; }
          return v + (o === 'est exclu' ? 0 : (d2.getUTCDay() % 6 ? 1 : 0) );
        }

        // cas général : un nombre de semaines pleines + 1 semaine partielle ou complète début + 1 semaine partielle ou complète fin
        return  ouv + pl + dl - (o === 'est inclus' ? 0 : (d2.getUTCDay() % 6 ? 1 : 0) );
      }; // fin diff2dates
    } // fin cj === 'ouvrés'

    // pour chaque ligne de données
    for (i = 0; i < dl; i++){

        // réinitialisation
        r1 = '';
        r2 = '';
        a = [];

        // conversion des formats vers ISO 8601 : AAAA-MM-JJ
        v1 = formater(data[i][c1]);
        v2 = formater(data[i][c2]);

        // si une des dates n'est pas renseignée (message partiel de retour si une des bornes n'est pas définie)
        if (v1 === '') { a.push('début'); }
        if (v2 === '') { a.push('fin');   }

        // si une des dates est manquante, on écrit tout le message de retour
        // et on quitte l'itération pour passer à la ligne suivante de données
        if (a.length) {
            v = 'donnée manquante (' + a.join('/') + ')';
            data[i].splice(0, 0, v);
            continue;
        }

        // si les dates sont incorrectes (message partiel de retour si une des bornes n'est pas au format attendu)
        if (v1.search(reg8601) !== 0 ) { a.push('début'); }
        if (v2.search(reg8601) !== 0 ) { a.push('fin');   }

        // si une des dates est incorrecte, on écrit tout le message de retour
        // et on quitte l'itération pour passer à la ligne suivante de données
        if (a.length) {
            v = 'format incorrect (' + a.join('/') +')';
            data[i].splice(0, 0, v);
            continue;
        }

        // si les deux dates sont au bon format, on fait le calcul
        // conversion en date
        try {
            v = '' + diff2dates(new Date(v1), new Date(v2) );
        } catch (e) {
            // la conversion en date peut échouer pour des dates impossibles (32 décembre, etc.)
            v = 'dates impossibles';
        }

        data[i].splice(0, 0, v);
    } // fin for

    h = 'nb jours [' + headers[c1] + ',' + headers[c2] + ' (' + (o === "est exclu" ? "exclu" : "inclus") + ')]/' + cj;
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin differenceDates

/**
 * ajoute une nouvelle colonne avec la nouvelle date calculée
 * compte-tenu du format de date sélectionné et de la constante n
 * @param   {string} f  - format de la date : (jj mm ssaa), (mm jj ssaa), (ssaa mm jj)
 * @param   {string} s  - séparateur : (.), (-), (/), ( )
 * @param   {number} c  - indice de colonne
 * @param   {string} n  - nombre de périodes à ajouter ou soustraire
 * @param   {string} u  - unité (jours), (semaines), (mois), (ans)
 */
function decalerDatesConstante(f, s, c, n, u) {
    "use strict";
    var dl = datalen,
        i = 0,
        d = new Date(),
        v = '',
        r = new Date(),
        h = '',
        reg8601  = new RegExp('^[0-9]{4}-((0[1-9])|(1[0-2]))-((0[1-9])|([1-2][0-9])|(3[0-1]))$', ''),
        formater = function(){},

        p = JSON.stringify([
           ['format'.bold() + ' : ' + f],
           ['séparateur'.bold() + ' : ' + (s === ' ' ? '(espace)' : s)],
           ['  dates'.bold() + ' : ' + headers[c]],
           ['  décaler de'.bold() + ' : ' + n],
           ['  unité'.bold() + ' : ' + u]
       ]);

    bkp('décaler des dates avec 1 constante', p);

    // adaptation de la fonction formater() en fonction du format de la date
    formater = convertirDate(f);

    // pour chaque ligne de données
    for (i = 0; i < dl; i++){
        // conversion des formats vers ISO 8601 : AAAA-MM-JJ
        v = formater(data[i][c]);

        // si la date n'est pas renseignée
        if (v === '') {
          data[i].splice(0, 0, 'donnée manquante');
          continue;
        }

        // si la date est incorrecte
        if (v.search(reg8601) !== 0 ) {
            data[i].splice(0, 0, 'format incorrect (' + v +')');
            continue;
        }

        n = parseInt(n, 10);
        d = new Date(v);
        r = new Date(d);

        switch(u) {
          case 'jours':    r.setDate(d.getDate()   +    n     ); break;
          case 'semaines': r.setDate(d.getDate()   + (n * 7)  ); break;
          case 'mois':     r.setMonth(d.getMonth() + n        ); break;
          case 'ans':      r.setFullYear(d.getFullYear() + n  ); break;
        }

        v = '' + r.toISOString().substring(0,10);
        data[i].splice(0, 0, v);
    } // fin for

    h = '[' + headers[c] + ']' + (n >= 0 ? '+' : '') + n + ' ' + u;
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin decalerDatesConstante

/**
 * ajoute une nouvelle colonne avec la nouvelle date calculée
 * compte-tenu du format de date sélectionné et de la valeur lue sur la même ligne et colonne cn
 * @param   {string} f  - format de la date : (jj mm ssaa), (mm jj ssaa), (ssaa mm jj)
 * @param   {string} s  - séparateur : (.), (-), (/), ( )
 * @param   {number} c  - indice de colonne
 * @param   {number} cn - indice de colonne, nombre de périodes à ajouter ou soustraire
 * @param   {string} u  - unité (jours), (semaines), (mois), (ans)
 */
function decalerDatesSelonColonne(f, s, c, cn, u) {
    "use strict";
    var dl = datalen,
        i = 0,
        d = new Date(),
        v = '',
        n = 0,
        r = new Date(),
        h = '',
        reg8601  = new RegExp('^[0-9]{4}-((0[1-9])|(1[0-2]))-((0[1-9])|([1-2][0-9])|(3[0-1]))$', ''),
        formater = function(){},

        p = JSON.stringify([
           ['format'.bold() + ' : ' + f],
           ['séparateur'.bold() + ' : ' + (s === ' ' ? '(espace)' : s)],
           ['  dates'.bold() + ' : ' + headers[c]],
           ['  décaler selon'.bold() + ' : ' + headers[cn]],
           ['  unité'.bold() + ' : ' + u]
       ]);

    bkp('décaler des dates selon 1 autre colonne', p);

    // adaptation de la fonction formater() en fonction du format de la date
    formater = convertirDate(f);

    // pour chaque ligne de données
    for (i = 0; i < dl; i++){
        // conversion des formats vers ISO 8601 : AAAA-MM-JJ
        v = formater(data[i][c]);

        // si la date n'est pas renseignée
        if (v === '') {
          data[i].splice(0, 0, 'donnée manquante');
          continue;
        }

        // si la date est incorrecte
        if (v.search(reg8601) !== 0 ) {
            data[i].splice(0, 0, 'format incorrect (' + v +')');
            continue;
        }

        n = parseInt(data[i][cn], 10);
        if (Number.isNaN(n)) {
			data[i].splice(0, 0, 'décalage manquant');
            continue;
		}

        d = new Date(v);
        r = new Date(d);

        switch(u) {
          case 'jours':    r.setDate(d.getDate()   +    n     ); break;
          case 'semaines': r.setDate(d.getDate()   + (n * 7)  ); break;
          case 'mois':     r.setMonth(d.getMonth() + n        ); break;
          case 'ans':      r.setFullYear(d.getFullYear() + n  ); break;
        }

        v = '' + r.toISOString().substring(0,10);
        data[i].splice(0, 0, v);
    } // fin for

    h = '[' + headers[c] + '] + [' + headers[cn] + ']' + u;
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin decalerDatesSelonColonne

/**
 * ajoute une nouvelle colonne avec le nom du jour 
 * compte-tenu du format de date sélectionné
 * @param   {string} f  - format de la date : (jj mm ssaa), (mm jj ssaa), (ssaa mm jj)
 * @param   {string} s  - séparateur : (.), (-), (/), ( )
 * @param   {number} c  - indice de colonne
 */
function jourSemaineDates(f, s, c) {
    "use strict";
    var dl = datalen,
        i = 0,
        d = new Date(),
        v = '',
        h = '',
        reg8601  = new RegExp('^[0-9]{4}-((0[1-9])|(1[0-2]))-((0[1-9])|([1-2][0-9])|(3[0-1]))$', ''),
        formater = function(){},
        dayNames = ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],

        p = JSON.stringify([
           ['format'.bold() + ' : ' + f],
           ['séparateur'.bold() + ' : ' + (s === ' ' ? '(espace)' : s)],
           ['  dates'.bold() + ' : ' + headers[c]]
       ]);

    bkp('donner le jour de la semaine', p);

    // adaptation de la fonction formater() en fonction du format de la date
    formater = convertirDate(f);

    // pour chaque ligne de données
    for (i = 0; i < dl; i++){
        // conversion des formats vers ISO 8601 : AAAA-MM-JJ
        v = formater(data[i][c]);

        // si la date n'est pas renseignée
        if (v === '') {
          data[i].splice(0, 0, 'donnée manquante');
          continue;
        }

        // si la date est incorrecte
        if (v.search(reg8601) !== 0 ) {
            data[i].splice(0, 0, 'format incorrect (' + v +')');
            continue;
        }

        d = new Date(v);
        v = isNaN(d.getUTCDay()) ? 'date incorrecte (' + v + ')' : dayNames[d.getUTCDay()];
        data[i].splice(0, 0, v);
    } // fin for

    h = '[' + headers[c] + ']/jour';
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin jourSemaineDates

/**
 * ajoute une nouvelle colonne avec la fréquence de chaque jour de la semaine
 * compte-tenu du format de date sélectionné
 * @param   {string} f  - format de la date : (jj mm ssaa), (mm jj ssaa), (ssaa mm jj)
 * @param   {string} s  - séparateur : (.), (-), (/), ( )
 * @param   {number} c1 - indice de colonne (début de la période)
 * @param   {number} c2 - indice de colonne (fin de la période)
 * @param   {string} o  - dernier jour de l'intervalle (est exclu), (est inclus)
 * @see     {@link nbJoursIntervalleDernierJourExclu}
 * @see     {@link nbJoursIntervalleDernierJourInclus}
 */
function chaqueJourSemainePeriodes(f, s, c1, c2, o) {
    "use strict";
    var dl = datalen,
        i = 0,
        j = 0,
        n = 0,
        nbS = 0,
        v1 = '',
        v2 = '',
        r1 = '',
        r2 = '',
        a = [],
        d1 = new Date(),
        d2 = new Date(),
        v = '',
        h = '',
        reg8601  = new RegExp('^[0-9]{4}-((0[1-9])|(1[0-2]))-((0[1-9])|([1-2][0-9])|(3[0-1]))$', ''),
        formater = function(){},
        diff2dates = function(){},
        dayNames = ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
        semaine = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"],
        nbJours = {},

        p = JSON.stringify([
           ['format'.bold() + ' : ' + f],
           ['séparateur'.bold() + ' : ' + (s === ' ' ? '(espace)' : s)],
           ['  du'.bold() + ' : ' + headers[c1]],
           ['  au'.bold() + ' : ' + headers[c2]],
           ["dernier jour de l'intervalle".bold() + " : " + o]
       ]);

    bkp('compter chacun des jours de la semaine', p);

    // adaptation de la fonction formater() en fonction du format de la date
    formater = convertirDate(f);

    // adaptation de la formation diff2dates() en fonction du traitement des bornes
    if (o === 'est exclu') {
      diff2dates = function(d1, d2) { return nbJoursIntervalleDernierJourExclu(d1, d2); };
    }
    if (o === 'est inclus') {
      diff2dates = function(d1, d2) { return nbJoursIntervalleDernierJourInclus(d1, d2); };
    }

    // pour chaque ligne de données
    for (i = 0; i < dl; i++){

        // réinitialisation
        r1 = '';
        r2 = '';
        a = [];

        // conversion des formats vers ISO 8601 : AAAA-MM-JJ
        v1 = formater(data[i][c1]);
        v2 = formater(data[i][c2]);

        // si une des dates n'est pas renseignée (message partiel de retour si une des bornes n'est pas définie)
        if (v1 === '') { a.push('début'); }
        if (v2 === '') { a.push('fin');   }

        // si une des dates est manquante, on écrit tout le message de retour
        // et on quitte l'itération pour passer à la ligne suivante de données
        if (a.length) {
            v = 'donnée manquante (' + a.join('/') + ')';
            data[i].splice(0, 0, v);
            continue;
        }

        // si les dates sont incorrectes (message partiel de retour si une des bornes n'est pas au format attendu)
        if (v1.search(reg8601) !== 0 ) { a.push('début'); }
        if (v2.search(reg8601) !== 0 ) { a.push('fin');   }

        // si une des dates est incorrecte, on écrit tout le message de retour
        // et on quitte l'itération pour passer à la ligne suivante de données
        if (a.length) {
            v = 'format incorrect (' + a.join('/') +')';
            data[i].splice(0, 0, v);
            continue;
        }

        // si les deux dates sont au bon format, on fait le calcul
        // conversion en date
        try {
            nbJours = { lundi: 0, mardi: 0, mercredi: 0, jeudi: 0, vendredi: 0, samedi: 0, dimanche: 0 };
            d1 = new Date(v1);
            d2 = new Date(v2);
            n = diff2dates(d1, d2);
            // soit il y a une incohérence
            if (n === 'intervalle inversé' || n === 'intervalle trop court') {
                v = n;
            } else { // pour les semaines pleines
                if (n > 7) {
                    nbS = parseInt(n/7);
                    nbJours = { lundi: nbS, mardi: nbS, mercredi: nbS, jeudi: nbS, vendredi: nbS, samedi: nbS, dimanche:nbS };
                    n = n % 7;
                }
                // si moins d'une semaine, qui peut aussi être le reliquat de (n%7)
                if (n > 0) {
                    j = d1.getUTCDay();
                    while(n > 0) {
                      nbJours[dayNames[j]]++;
                      j++;
                      j = j % 7;
                      n--;
                    }
                }
                v = semaine.map(x => x + ':' + nbJours[x]).join(',');
            } // fin else
            
        } catch (e) {
            // la conversion en date peut échouer pour des dates impossibles (32 décembre, etc.)
            v = 'dates impossibles';
        }

        data[i].splice(0, 0, v);
    } // fin for

    h = '[' + headers[c1] + ' à ' + headers[c2] + ' (' + (o === "est exclu" ? "exclu" : "inclus") + ')]/jourSemaine';
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin chaqueJourSemainePeriodes

/**
 * ajoute une nouvelle colonne avec le numéro du jour dans l'année
 * compte-tenu du format de date sélectionné
 * @param   {string} f  - format de la date : (jj mm ssaa), (mm jj ssaa), (ssaa mm jj)
 * @param   {string} s  - séparateur : (.), (-), (/), ( )
 * @param   {number} c  - indice de colonne
 */
function jourAnneeDates(f, s, c) {
    "use strict";
    var dl = datalen,
        i = 0,
        d = new Date(),
        v = '',
        h = '',
        reg8601  = new RegExp('^[0-9]{4}-((0[1-9])|(1[0-2]))-((0[1-9])|([1-2][0-9])|(3[0-1]))$', ''),
        formater = function(){},

        p = JSON.stringify([
           ['format'.bold() + ' : ' + f],
           ['séparateur'.bold() + ' : ' + (s === ' ' ? '(espace)' : s)],
           ['  dates'.bold() + ' : ' + headers[c]]
       ]);

    bkp("obtenir le numéro du jour dans l'année", p);

    // adaptation de la solution https://stackoverflow.com/questions/8619879/javascript-calculate-the-day-of-the-year-1-366#8619946
    // pour une prise en compte des timezones négatifs par rapport à UTC (utilisation de Date.UTC())
    function daysIntoYear(date){
      return (Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate()) - Date.UTC(date.getUTCFullYear(), 0, 0)) / 24 / 60 / 60 / 1000;
    }

    // adaptation de la fonction formater() en fonction du format de la date
    formater = convertirDate(f);

    // pour chaque ligne de données
    for (i = 0; i < dl; i++){
        // conversion des formats vers ISO 8601 : AAAA-MM-JJ
        v = formater(data[i][c]);

        // si la date n'est pas renseignée
        if (v === '') {
          data[i].splice(0, 0, 'donnée manquante');
          continue;
        }

        // si la date est incorrecte
        if (v.search(reg8601) !== 0 ) {
            data[i].splice(0, 0, 'format incorrect (' + v +')');
            continue;
        }

        d = new Date(v);
        v = isNaN(d.getUTCDay()) ? 'date incorrecte (' + v + ')' : '' + daysIntoYear(d);
        data[i].splice(0, 0, v);
    } // fin for

    h = '[' + headers[c] + ']/numJourAnnée';
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin jourAnneeDates


// ......................................................... temps : intervalle


/** @summary complète un intervalle de dates par les dates intermédiaires
 * @description ajoute une colonne au début du tableau
 * en complétant un intervalle sous la forme a-b (a et b dates)
 * - par toutes les dates comprises entre a et b
 * - séparées par une virgule (,)
 * @param   {string} f   - format de la date : (jj mm ssaa), (mm jj ssaa), (ssaa mm jj)
 * @param   {string} s   - séparateur : (.), (-), (/), ( )
 * @param   {number} c1  - indice de colonne (début de la période)
 * @param   {number} c2  - indice de colonne (fin de la période)
 * @param   {string} o   - dernier jour de l'intervalle (est exclu), (est inclus)
 * @see     {@link nbJoursIntervalleDernierJourExclu}
 * @see     {@link nbJoursIntervalleDernierJourInclus}
 * @see     {@link UN_JOUR_EN_MS}
 */
function completerIntervalleDates(f, s, c1, c2, o) {
    "use strict";
    var dl = datalen,
        i = 0,
        j = 0,
        n = 0,
        v1 = '',
        v2 = '',
        r1 = '',
        r2 = '',
        a = [],
        d1 = new Date(),
        d2 = new Date(),
        dv = new Date(),
        v = '',
        h = '',
        reg8601  = new RegExp('^[0-9]{4}-((0[1-9])|(1[0-2]))-((0[1-9])|([1-2][0-9])|(3[0-1]))$', ''),
        formater = function(){},
        diff2dates = function(){},
        periode = [],

        p = JSON.stringify([
           ['format'.bold() + ' : ' + f],
           ['séparateur'.bold() + ' : ' + (s === ' ' ? '(espace)' : s)],
           ['  du'.bold() + ' : ' + headers[c1]],
           ['  au'.bold() + ' : ' + headers[c2]],
           ["dernier jour de l'intervalle".bold() + " : " + o]
       ]);

    bkp('compléter un intervalle de date', p);

    // adaptation de la fonction formater() en fonction du format de la date
    formater = convertirDate(f);

    // adaptation de la formation diff2dates() en fonction du traitement des bornes
    if (o === 'est exclu') {
      diff2dates = function(d1, d2) { return nbJoursIntervalleDernierJourExclu(d1, d2); };
    }
    if (o === 'est inclus') {
      diff2dates = function(d1, d2) { return nbJoursIntervalleDernierJourInclus(d1, d2); };
    }

    // pour chaque ligne de données
    for (i = 0; i < dl; i++){

        // réinitialisation
        r1 = '';
        r2 = '';
        a = [];

        // conversion des formats vers ISO 8601 : AAAA-MM-JJ
        v1 = formater(data[i][c1]);
        v2 = formater(data[i][c2]);

        // si une des dates n'est pas renseignée (message partiel de retour si une des bornes n'est pas définie)
        if (v1 === '') { a.push('début'); }
        if (v2 === '') { a.push('fin');   }

        // si une des dates est manquante, on écrit tout le message de retour
        // et on quitte l'itération pour passer à la ligne suivante de données
        if (a.length) {
            v = 'donnée manquante (' + a.join('/') + ')';
            data[i].splice(0, 0, v);
            continue;
        }

        // si les dates sont incorrectes (message partiel de retour si une des bornes n'est pas au format attendu)
        if (v1.search(reg8601) !== 0 ) { a.push('début'); }
        if (v2.search(reg8601) !== 0 ) { a.push('fin');   }

        // si une des dates est incorrecte, on écrit tout le message de retour
        // et on quitte l'itération pour passer à la ligne suivante de données
        if (a.length) {
            v = 'format incorrect (' + a.join('/') +')';
            data[i].splice(0, 0, v);
            continue;
        }

        // si les deux bornes sont bien deux dates, on fait le calcul
        // conversion en date
        try {
            d1 = new Date(v1);
            dv = new Date(v1);
            d2 = new Date(v2);
            n = diff2dates(d1, d2);
            // s'il y a une incohérence
            if (n === 'intervalle inversé' || n === 'intervalle trop court') {
                v = n;
            } else { // si l'intervalle est non-nul entre les 2 dates (donc un intervalle à compléter)
                periode = [];
                j = 0;
                while (n >= 1) {
                    periode[periode.length] = dv.toISOString().substring(0,10);
                    j++;
                    dv.setTime(d1.getTime() + j * UN_JOUR_EN_MS);
                    n--;
                }
                v = periode.join(',');
            }
        } catch (e) {
            // la conversion en date peut échouer pour des dates impossibles (32 décembre, etc.)
            v = 'dates impossibles';
        }

        data[i].splice(0, 0, v);
    } // fin for

    h = '[' + headers[c1] + ' à ' + headers[c2] + ' (' + (o === "est exclu" ? "exclu" : "inclus") + ')]/intervalle';
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin completerIntervalleDates

/** recherche une date dans un intervalle de dates
 * @param   {string} f   - format de la date : (jj mm ssaa), (mm jj ssaa), (ssaa mm jj)
 * @param   {string} s   - séparateur : (.), (-), (/), ( )
 * @param   {number} c1  - indice de colonne (début de la période)
 * @param   {number} c2  - indice de colonne (fin de la période)
 * @param   {number} c3  - indice de colonne (date recherchée)
 * @param   {string} o   - dernier jour de l'intervalle (est exclu), (est inclus)
 * @see     {@link nbJoursIntervalleDernierJourExclu}
 * @see     {@link nbJoursIntervalleDernierJourInclus}
 */
function rechercherDansIntervalleDates(f, s, c1, c2, c3, o) {
  "use strict";
   var dl = datalen,
        i = 0,
        n = 0,
        v1 = '',
        v2 = '',
        v3 = '',
        r1 = '',
        r2 = '',
        r3 = '',
        a = [],
        d1 = new Date(),
        d2 = new Date(),
        d3 = new Date(),
        v = '',
        h = '',
        reg8601  = new RegExp('^[0-9]{4}-((0[1-9])|(1[0-2]))-((0[1-9])|([1-2][0-9])|(3[0-1]))$', ''),
        formater = function(){},
        diff2dates = function(){},

        p = JSON.stringify([
           ['format'.bold() + ' : ' + f],
           ['séparateur'.bold() + ' : ' + (s === ' ' ? '(espace)' : s)],
           ['période'.bold() + ' : '],
           ['  du'.bold() + ' : ' + headers[c1]],
           ['  au'.bold() + ' : ' + headers[c2]],
           ["dernier jour de l'intervalle".bold() + " : " + o],
           ['date recherchée'.bold() + ' : ' + headers[c3]]
       ]);

    bkp('rechercher une date dans un intervalle de date', p);

    // adaptation de la fonction formater() en fonction du format de la date
    formater = convertirDate(f);

    // adaptation de la formation diff2dates() en fonction du traitement des bornes
    if (o === 'est exclu') {
      diff2dates = function(d1, d2) { return nbJoursIntervalleDernierJourExclu(d1, d2); };
    }
    if (o === 'est inclus') {
      diff2dates = function(d1, d2) { return nbJoursIntervalleDernierJourInclus(d1, d2); };
    }

    // pour chaque ligne de données
    for (i = 0; i < dl; i++){

        // réinitialisation
        r1 = '';
        r2 = '';
        r3 = '';
        a = [];

        // conversion des formats vers ISO 8601 : AAAA-MM-JJ
        v1 = formater(data[i][c1]);
        v2 = formater(data[i][c2]);
        v3 = formater(data[i][c3]);

        // si une des dates n'est pas renseignée (message partiel de retour si une des bornes ou la date recherchée n'est pas définie)
        if (v1 === '') { a.push('début'); }
        if (v2 === '') { a.push('fin');   }
        if (v3 === '') { a.push('date recherchée');   }

        // si une des dates est manquante, on écrit tout le message de retour
        // et on quitte l'itération pour passer à la ligne suivante de données
        if (a.length) {
            v = 'donnée manquante (' + a.join('/') + ')';
            data[i].splice(0, 0, v);
            continue;
        }

        // si les dates sont incorrectes (message partiel de retour si une des bornes ou la date recherchée n'est pas au format attendu)
        if (v1.search(reg8601) !== 0 ) { a.push('début'); }
        if (v2.search(reg8601) !== 0 ) { a.push('fin');   }
        if (v3.search(reg8601) !== 0 ) { a.push('date recherchée'); }

        // si une des dates est incorrecte, on écrit tout le message de retour
        // et on quitte l'itération pour passer à la ligne suivante de données
        if (a.length) {
            v = 'format incorrect (' + a.join('/') +')';
            data[i].splice(0, 0, v);
            continue;
        }

        // si les deux bornes et la date recherchée sont bien des dates, on fait le calcul
        // conversion en date
        try {
            d1 = new Date(v1);
            d2 = new Date(v2);
            d3 = new Date(v3); // vérifie la conversion en date (try/catch)
            n = diff2dates(d1, d2);
            // s'il y a une incohérence
            if (n === 'intervalle inversé' || n === 'intervalle trop court') {
                v = n;
            } else { // si l'intervalle est non-nul entre les 2 dates (donc une période correctement définie)
                if(v3 < v1) {
                    v = 'antérieure';
                } else {
                    if (
                         (o === 'est exclu'  && v3 >= v2)
                             ||
                         (o === 'est inclus' && v3 > v2)
                       ) {
                        v = 'postérieure';
                    } else {
                        v = 'incluse';
                    }
                }
            }
        } catch (e) {
            // la conversion en date peut échouer pour des dates impossibles (32 décembre, etc.)
            v = 'dates impossibles';
        }

        data[i].splice(0, 0, v);
    } // fin for

    h = '[ ' + headers[c1] + ' ; ' + headers[c2] + ' ](' + (o === "est exclu" ? "exclu" : "inclus") + ')]' + ' ? ' + headers[c3];
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin rechercherDansIntervalleDates

/**
 * @summary combiner deux intervalles de dates (deux périodes)
 * @description ajoute une colonne au début du tableau
 * en indiquant l'intervalle résultat de la fusion, l'union, ou de l'intersection
 * @param   {string} f   - format de la date : (jj mm ssaa), (mm jj ssaa), (ssaa mm jj)
 * @param   {string} s   - séparateur : (.), (-), (/), ( )
 * @param   {number} c1  - indice de colonne (début de la première période)
 * @param   {number} c2  - indice de colonne (fin de la première période)
 * @param   {number} c3  - indice de colonne (début de la seconde période)
 * @param   {number} c4  - indice de colonne (fin de la seconde période)
 * @param   {string} o  - option : (fusion), (union), (intersection)
 */
function combinerDeuxPeriodes(f, s, c1, c2, c3, c4, o) {
  "use strict";
    var dl = datalen,
        h = '',
        i  = 0,
        v1 = '',
        v2 = '',
        v3 = '',
        v4 = '',
        r  = '',
        a = [],
        d1 = new Date(),
        d2 = new Date(),
        d3 = new Date(),
        d4 = new Date(),
        reg8601  = new RegExp('^[0-9]{4}-((0[1-9])|(1[0-2]))-((0[1-9])|([1-2][0-9])|(3[0-1]))$', ''),
        formater = function(){},

        p = JSON.stringify([
             ['format'.bold() + ' : ' + f],
             ['séparateur'.bold() + ' : ' + (s === ' ' ? '(espace)' : s)],
             ['période 1 définie par'.bold()],
             ['  début'.bold() + ' : ' + headers[c1]],
             ['  fin'.bold() + ' : ' + headers[c2]],
             ['période 2 définie par'.bold()],
             ['  début'.bold() + ' : ' + headers[c3]],
             ['  fin'.bold() + ' : ' + headers[c4]],
             ['action'.bold() + ' : ' + o]
        ]);

    bkp('combiner deux périodes', p);

    // adaptation de la fonction formater() en fonction du format de la date
    formater = convertirDate(f);

   // pour chaque ligne de données
    for (i = 0; i < dl; i++){

        // réinitialisation
        a = [];

        // conversion des formats vers ISO 8601 : AAAA-MM-JJ
        v1 = formater(data[i][c1]);
        v2 = formater(data[i][c2]);
        v3 = formater(data[i][c3]);
        v4 = formater(data[i][c4]);

        // si une des dates n'est pas renseignée (message partiel de retour si une des bornes ou la date recherchée n'est pas définie)
        if (v1 === '') { a.push('début 1'); }
        if (v2 === '') { a.push('fin 1');   }
        if (v3 === '') { a.push('début 2'); }
        if (v4 === '') { a.push('fin 2');   }


        // si une des dates est manquante, on écrit tout le message de retour
        // et on quitte l'itération pour passer à la ligne suivante de données
        if (a.length) {
            r = 'donnée manquante (' + a.join('/') + ')';
            data[i].splice(0, 0, r);
            continue;
        }

        // si les dates sont incorrectes (message partiel de retour si une des bornes ou la date recherchée n'est pas au format attendu)
        if (v1.search(reg8601) !== 0 ) { a.push('début 1'); }
        if (v2.search(reg8601) !== 0 ) { a.push('fin 1');   }
        if (v3.search(reg8601) !== 0 ) { a.push('début 2'); }
        if (v4.search(reg8601) !== 0 ) { a.push('fin 2');   }

        // si une des dates est incorrecte, on écrit tout le message de retour
        // et on quitte l'itération pour passer à la ligne suivante de données
        if (a.length) {
            r = 'format incorrect (' + a.join('/') +')';
            data[i].splice(0, 0, r);
            continue;
        }

        // si les quatre bornes sont bien des dates, on fait le calcul
        // conversion en date
        try {
            d1 = new Date(v1);
            d2 = new Date(v2);
            d3 = new Date(v3);
            d4 = new Date(v4); // vérifie la conversion en date (try/catch)

		    // si les quatre bornes bien des dates,
            // alors on compare directement les chaînes de texte, sans convertir en date
            if (v1 > v2) {
              if (v3 > v4) {
                  r = 'périodes 1 et 2 incorrectes';
              } else {
                r = 'période 1 incorrecte';
                data[i].splice(0, 0, r);
                continue;
              }
            } // else

            if (v3 > v4) {
                r = 'période 2 incorrecte';
                data[i].splice(0, 0, r);
                continue;
            }

            if (o !== 'fusion' && ((v2 < v3) || v1 > v4) ) {
                r = 'périodes disjointes';
                data[i].splice(0, 0, r);
                continue;
            }

            if (o === 'intersection') {
                r = '' + ((v1 > v3) ? data[i][c1] : data[i][c3]) + ' à ' + ((v2 < v4) ? data[i][c2] : data[i][c4]);
            } else { // union ou fusion
                r = '' + ((v1 < v3) ? data[i][c1] : data[i][c3]) + ' à ' + ((v2 < v4) ? data[i][c4] : data[i][c2]);
            }
            data[i].splice(0, 0, r);

        } catch (e) {
            // la conversion en date peut échouer pour des dates impossibles (32 décembre, etc.)
            r = 'dates impossibles';
           data[i].splice(0, 0, r);
        }
    }

    h  = '[ ' + headers[c1] + ' ; ' + headers[c2] + ' ]';
    h += ' ?';
    h += o === 'fusion' ? 'f ' : '';
    h += o === 'union' ? 'u ' : '';
    h += o === 'intersection' ? 'i ' : '';
    h += '[ ' + headers[c3] + ' ; ' + headers[c4] + ' ]';
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin combinerDeuxPeriodes

/**
 * ajoute une colonne en début de tableau
 * avec le résultat de la comparaison des seuils
 * @param   {string} f   - format de la date : (jj mm ssaa), (mm jj ssaa), (ssaa mm jj)
 * @param   {string} sep - séparateur : (.), (-), (/), ( )
 * @param   {number} c   - indice de colonne
 * @param   {string} o   - opérateur (<), (≤), (>), (≥)
 * @param   {string} t   - liste de seuils séparés par un saut de ligne
 */
function comparerSeuilsDates(f, s, c, o, t) {
    "use strict";
    var dl = datalen,
        a = t.split('\n'),
        h = '',
        r = '',
        i = 0,
        v = '',
        reg8601  = new RegExp('^[0-9]{4}-((0[1-9])|(1[0-2]))-((0[1-9])|([1-2][0-9])|(3[0-1]))$', ''),
        formater = function(){},
        g = function (){},

        p = JSON.stringify([
            ['format'.bold() + ' : ' + f],
            ['séparateur'.bold() + ' : ' + (s === ' ' ? '(espace)' : s)],
            ['colonne'.bold() + ' : ' + headers[c]],
            ['  comparaison'.bold() + ' : ' + o],
            ['  avec les seuils'.bold() + ' : </br>' + t]
        ]);
    bkp('comparer les dates et une liste de seuils', p);

    // adaptation de la fonction formater() en fonction du format de la date
    formater = convertirDate(f);

    function fs(a, b) { return a > b; }

    // formatage en date ISO8601
    a = a.filter(e => e !== '');
    a = a.map(e => formater(e));
    if (a.filter(e => e.search(reg8601) !== 0).length) {
        attention('un ou plusieurs seuils sont incorrects :</br>' + a.filter(e => e.search(reg8601) !== 0).join('<\br>'));
        return;
    }

    try {
        a.map(e => new Date(e));
    } catch (e) {
        attention(e.message);
        return;
    }

    // pré-traitement pour éviter des boucles infinies si les seuils n'étaient pas triés initialement
    a.sort(fs);
    if (o === '>' || o === '≥') { a.reverse(); }

    // teste l'opérateur 1 seule fois (et non 1 fois par ligne)
    switch(o) {
      case '<' : g = function(v, a) { return a.find(e => v <  e); }; break;
      case '≤' : g = function(v, a) { return a.find(e => v <= e); }; break;
      case '>' : g = function(v, a) { return a.find(e => v >  e); }; break;
      case '≥' : g = function(v, a) { return a.find(e => v >= e); }; break;
    }

    for (i = 0; i < dl; i++) {

      // conversion des formats vers ISO 8601 : AAAA-MM-JJ
      v = formater(data[i][c]);

      // si la date à comparer n'est pas renseignée
      // et on quitte l'itération pour passer à la ligne suivante de données
      if (v === '') {
        data[i].splice(0, 0, 'donnée manquante');
        continue;
      }

      // si une des dates est incorrecte, on écrit tout le message de retour
      // et on quitte l'itération pour passer à la ligne suivante de données
      if (v.search(reg8601) !== 0) {
        r = 'format incorrect (' + v +')';
        data[i].splice(0, 0, '' + r);
        continue;
      }

      r = g(v, a);
      r = typeof r  === 'undefined' ? '' : r;
      data[i].splice(0, 0, '' + r);
    }

    h = headers[c] + ' ' + o + ' {seuils}';
    headers.unshift(h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin comparerSeuilsDates

/**
 * ajoute une colonne en début de tableau
 * avec le résultat de la recherche de collision entre les périodes
 * en appliquant la règle r
 * @param   {string} f   - format de la date : (jj mm ssaa), (mm jj ssaa), (ssaa mm jj)
 * @param   {string} sep - séparateur : (.), (-), (/), ( )
 * @param   {number} c1  - indice de colonne (début des périodes comparées)
 * @param   {number} c2  - indice de colonne (fin des périodes comparées)
 * @param   {string} o   - dernier jour de l'intervalle (est exclu), (est inclus)
 * @param   {string} r   - règle d'application : (sans condition), (pour chaque série), (pour chaque identifiant)
 * @param   {number} id  - valeur identifiante commune (si r <> "sans condition")
 * @param   {string} vr  - valeur de retour (la valeur), (l'intitulé de la colonne)
 * @param   {string} sv  - séparateur des valeurs de retour
 * @see     {@link nbJoursIntervalleDernierJourExclu}
 * @see     {@link nbJoursIntervalleDernierJourInclus}
 */
function detecterCollisionsPeriodes(f, sep, c1, c2, o, r, id, vr, sv) {
 "use strict";
   var dl = datalen,
        i = 0,
        j = 0,
        k  = '',
        at = [], // temporaire
        aa = [],
        ar = [], // résultat
        h = '',
        reg8601  = new RegExp('^[0-9]{4}-((0[1-9])|(1[0-2]))-((0[1-9])|([1-2][0-9])|(3[0-1]))$', ''),
        formater = function(){},
        diff2dates = function(){},
        trad = {'est exclu': '[',
              'est inclus': ']',
              'sans condition' : 'tout',
              'pour chaque série'      : 'série',
              'pour chaque identifiant': 'id'},

        p = [
           ['format'.bold() + ' : ' + f],
           ['séparateur'.bold() + ' : ' + (sep === ' ' ? '(espace)' : sep)],
           ['période'.bold() + ' : '],
           ['  du'.bold() + ' : ' + headers[c1]],
           ['  au'.bold() + ' : ' + headers[c2]],
           ["dernier jour de l'intervalle".bold() + " : " + o]
       ];

    if (r === 'sans condition') { p.push(['  appliquer'.bold() + ' : ' + r]); }
    else {
        p.push(['  appliquer'.bold() + ' : ' + r]);
        p.push(['    dans'.bold() + ' : ' + headers[id]]);
    }

    p.push(['recopier'.bold() + ' : ' + headers[vr]]);
    p.push(['séparer les résultats par'.bold() + ' : ' + sv]);

    bkp('détecter des collisions de périodes', JSON.stringify(p));

    // adaptation de la fonction formater() en fonction du format de la date
    formater = convertirDate(f);

    // adaptation de la formation diff2dates() en fonction du traitement des bornes
    if (o === 'est exclu') {
      diff2dates = function(d1, d2) { return nbJoursIntervalleDernierJourExclu(d1, d2); };
    }
    if (o === 'est inclus') {
      diff2dates = function(d1, d2) { return nbJoursIntervalleDernierJourInclus(d1, d2); };
    }

    switch(r) {
      case 'sans condition':
        aa[0] = [];
        for (i=0; i < dl; i++) { aa[0].push([i]); } 
        break;
      case 'pour chaque série':
        k = data[i][id] + 'x';
        for (i = 0; i < dl; i++) {
          if(data[i][id] !== k) {
            aa[aa.length] = [];
          }
          aa[aa.length-1].push(i);
          k = data[i][id];
        }
        break;
      case 'pour chaque identifiant':
        at = tab2hash(data, id);
        for (k in at){
          if (at.hasOwnProperty(k)){ aa[aa.length] = at[k].slice(); }
        } // fin for
        break;
    } // fin switch

    // vérification si les périodes sont correctement précisées
    at = aa.map(a => a.filter(e => data[e][c1] === '' || data[e][c2] === ''));
    ar = at.filter(a => a.length); // réduction de at
    if (ar.length){
        attention('une ou plusieurs périodes sont imprécises :</br>' + ar);
        return;
    }

    // modification structure de données (pour améliorer la lisibilité de l'algorithme)
    aa = aa.map(a => a.map(e => ({ col: e, debut: formater(data[e][c1]), fin: formater(data[e][c2]) }) ));

    // formatage en date ISO8601
    at = aa.map(a => a.filter(e => e.debut.search(reg8601) !==0 || e.fin.search(reg8601) !== 0));
    ar = at.filter(a => a.length); // réduction de at
    if (ar.length) {
        attention('un ou plusieurs seuils sont incorrects :</br>' + ar);
        return;
    }

    try {
        ar.forEach(a => a.map(e => [new Date(e.debut), new Date(e.fin)]));
    } catch (e) {
        attention(e.message);
        return;
    }

// pour chaque élément du tableau d'indice
// on détecte une anomalie
// si 1. a[i].debut === a[i+1].debut
// ou si le dernier jour est inclus et (b[i].debut <= a[i].fin)
// ou si le dernier jour est exclu  et (b[i].debut <  a[i].fin)

/**
 * pour chaque période lue pl1 => [série ou identifiant][début-fin]
 * pour chaque période lue pl2 différent de pl1 (mais dans la même série ou identifiant) : si collision => on enrichit le tableau de collision par rapport à cette période (même série ou identifiant)
 */

    at = [];
    for (i = 0; i < dl; i++) { at[i] = []; } // évite d'avoir à tester systématiquement si le tableau existe (x2), gain de temps vs charge mémoire (x tableaux vides)

// pour réduire la complexité du traitement, on ne vérifie qu'une seule fois la condition o
    if (o === 'est inclus') {
        for (i = 0; i < aa.length; i++) { // pour chaque regroupement (tout, par série, par identifiant)
            for (j = 0; j < aa[i].length -1; j++) {
                for (k = j+1; k < aa[i].length; k++) {
                    if (aa[i][j].fin < aa[i][k].debut || aa[i][j].debut > aa[i][k].fin) { // pas de collision
                        // ne rien faire
                    } else { // sinon collision
                        at[aa[i][k].col].push(aa[i][j].col);
                        at[aa[i][j].col].push(aa[i][k].col);
                    }
                }
            }
        }
    } else { // o === 'est exclu'
        for (i = 0; i < aa.length; i++) {
            for (j = 0; j < aa[i].length -1; j++) {
                for (k = j+1; k < aa[i].length; k++) {
                    if (aa[i][j].fin <= aa[i][k].debut || aa[i][j].debut >= aa[i][k].fin) { // pas de collision
                        // ne rien faire
                    } else { // sinon collision
                        at[aa[i][k].col].push(aa[i][j].col);
                        at[aa[i][j].col].push(aa[i][k].col);
                    }
                }
            }
        }
    }

    // initialisation à vide
    for (i = 0; i < dl; i++) {
      data[i].unshift(''); // data[i].splice(0, 0, '' + r); ??
    }
    vr++; // du fait de la colonne 1 ajoutée
    for (i = 0; i < dl; i++) {
      data[i][0] = at[i].map(e => data[e][vr]).join(sv);
    }

    h = ( r === 'sans condition' ? '' : (headers[id] + '/' + trad[r]) + ' ') + '{collisions}[' + headers[c1] + ',' + headers[c2] + trad[o];
    headers.unshift(h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin detecterCollisionsPeriodes


// .................................................................... calculs


/**
 * pour chaque ligne, inscrit le total de l'opération o sur les données de la colonne c, à partir de la liste des lots aa
 * @param   {string} o   - opération : (minimum), (maximum), (moyenne), (somme)
 * @param   {number} c   - indice de colonne (opérandes)
 * @param   {number} aa  - tableau des lots (tableau de tableaux d'indices)
 */
function prepareTotal(o, c, aa) {
  "use strict";
  var l = aa.length, // nombre de lots
      i = 0, // indice de l'item dans un lot
      j = 0, // indice du lot
      v = '',
      r = 0.0;

  switch (o) {
    case 'minimum' :
      for (i = 0; i < l; i++) { // pour chaque lot
        r = st2float(data[aa[i][0]][c]);
        for (j = 0; j < aa[i].length; j++) { // pour chaque item dans chaque lot
          v = st2float(data[aa[i][j]][c]);
          r = v < r ? v : r;
        }

        // changement de type + troncature des décimales + écriture
        r = r.toFixed(2);
        aa[i].forEach( x => data[x].unshift(r) );

      } // fin for i
    break;

    case 'maximum' :
      for (i = 0; i < l; i++) { // pour chaque lot
        r = st2float(data[aa[i][0]][c]);
        for (j = 0; j < aa[i].length; j++) { // pour chaque item dans chaque lot
          v = st2float(data[aa[i][j]][c]);
          r = v > r ? v : r;
        }

        // changement de type + troncature des décimales + écriture
        r = r.toFixed(2);
        aa[i].forEach( x => data[x].unshift(r) );

      } // fin for i
    break;

    case 'somme' :
      for (i = 0; i < l; i++) { // pour chaque lot
        r = 0.0;
        for (j = 0; j < aa[i].length; j++) { // pour chaque item dans chaque lot
          r += st2float(data[aa[i][j]][c]);
        }

        // changement de type + troncature des décimales + écriture
        r = r.toFixed(2);
        aa[i].forEach( x => data[x].unshift(r) );

      } // fin for i
    break;

    case 'moyenne' :
      for (i = 0; i < l; i++) { // pour chaque lot
        r = 0.0;
        for (j = 0; j < aa[i].length; j++) { // pour chaque item dans chaque lot
          r += st2float(data[aa[i][j]][c]);
        }

        r = r / aa[i].length;

        // changement de type + troncature des décimales + écriture
        r = r.toFixed(2);
        aa[i].forEach( x => data[x].unshift(r) );

      } // fin for i
    break;

  } // fin switch

} // fin prepareTotal

/**
 * pour chaque ligne, inscrit le cumul de l'opération o sur les données de la colonne c, à partir de la liste des lots aa
 * @param   {string} o   - opération : (minimum), (maximum), (moyenne), (somme)
 * @param   {number} c   - indice de colonne (opérandes)
 * @param   {number} aa  - tableau des lots (tableau de tableaux d'indices)
 */
function prepareCumul(o, c, aa) {
  "use strict";
  var l = aa.length, // nombre de lots
      i = 0, // indice de l'item dans un lot
      j = 0, // indice du lot
      v = '',
      r = 0.0;

  switch (o) {
    case 'minimum' :
      for (i = 0; i < l; i++) { // pour chaque lot
        r = st2float(data[aa[i][0]][c]);
        for (j = 0; j < aa[i].length; j++) { // pour chaque item dans chaque lot
          v = data[aa[i][j]][c];
          r = st2float(v) < r ? st2float(v) : r;
          data[aa[i][j]].unshift(r.toFixed(2));
        }
      } // fin for i
    break;

    case 'maximum' :
      for (i = 0; i < l; i++) { // pour chaque lot
        r = st2float(data[aa[i][0]][c]);
        for (j = 0; j < aa[i].length; j++) { // pour chaque item dans chaque lot
          v = data[aa[i][j]][c];
          r = st2float(v) > r ? st2float(v) : r;
          data[aa[i][j]].unshift(r.toFixed(2));
        }
      } // fin for i
    break;

    case 'somme' :
      for (i = 0; i < l; i++) { // pour chaque lot
        r = 0.0;
        for (j = 0; j < aa[i].length; j++) { // pour chaque item dans chaque lot
          v = data[aa[i][j]][c];
          r += st2float(v);
          data[aa[i][j]].unshift(r.toFixed(2));
        }
      } // fin for i
    break;

    case 'moyenne' :
      for (i = 0; i < l; i++) { // pour chaque lot
        r = 0.0;
        for (j = 0; j < aa[i].length; j++) { // pour chaque item dans chaque lot
          r += st2float(data[aa[i][j]][c]);
          v = r / (j + 1);
          v = v.toFixed(2);
          data[aa[i][j]].unshift(v);
        }
      } // fin for i
    break;

  } // fin switch

} // fin prepareCumul

/**
 * crée une nouvelle colonne intitulée h avec pour chaque ligne le résultat de l'opération o sur c
 * @param   {string} o   - opération : (minimum), (maximum), (moyenne), (somme)
 * @param   {string} ov  - option : gestion des valeurs vides, (si o <> "somme") : (ignorées) ou (traduites par zéro)
 * @param   {number} c   - indice de colonne (opérandes)
 * @param   {string} ot  - option : type de calcul : (total), (cumul)
 * @param   {string} dir - option : direction, (si ot = cumul) : en partant (de la première ligne), (de la dernière ligne)
 * @param   {string} op  - option : périmètre : (global), (local)
 * @param   {string} r   - règle d'application, (si op = local) : (pour chaque série), (pour chaque identifiant)
 * @param   {number} id  - valeur identifiante commune, (si op = local)
 * @see     {@link prepareTotal}
 * @see     {@link prepareCumul}
 */
function operation1colonne(o, ov, c, ot, dir, op, r, id) {
  "use strict";
  var dl = datalen,
      i  =  0,
      h  = '',
      at = [],
      k  = '',
      aa = [], // 2 dimensions (valeurs non-vides)
      av = [], // 1 dimension (valeurs vides)
      trad = {'de la première ligne' : '0-9',
              'de la dernière ligne' : '9-0',
              'pour chaque série'      : 'série',
              'pour chaque identifiant': 'id'   ,
              'ignorées'             : '[_]',
              'traduites par zéro'   : '[0]'},

      p = [
          ['calculer'.bold() + ' : ' + o],
          ['  les valeurs vides sont'.bold() + ' : ' + ov],
          ['  de'.bold() + ' : ' + headers[c]],
          ['inscrire'.bold() + ' : ' + ot]
          ];

      if (ot === 'cumul') { p.push(['  en partant de'.bold() + ' : ' + dir]); }

      p.push(['résultat'.bold() + ' : ' + op]);

      if (op === 'local') {
        p.push(['  appliquer'.bold() + ' : ' + r]);
        p.push(['  de'.bold() + ' : ' + headers[id]]);
      }

  bkp("calculer une opération sur 1 colonne", JSON.stringify(p));

  // préparation des sous-ensembles de données (tableau de tableaux d'indices) ...

  if (ov === 'traduites par zéro') {
    // si global = 1 tableau de tableaux (1) avec tous les indices ...
    if (op === 'global'){
      aa[0] = [];
      for (i = 0; i < dl; i++) { aa[0][i] = i; }
    } // fin op === 'global'

    // si local = 1 tableau de tableaux avec les indices (par série ou identifiant) ...
    if (op === 'local'){

      if (r === 'pour chaque identifiant'){
        at = tab2hash(data, id);
        for (k in at){
          if (at.hasOwnProperty(k)){ aa[aa.length] = at[k].slice(); }
        } // fin for
      } // fin r === 'pour chaque identifiant'

      if (r === 'pour chaque série'){
        k = data[i][id] + 'x';
        for (i = 0; i < dl; i++) {
          if(data[i][id] !== k) {
            aa[aa.length] = [];
          }
          aa[aa.length-1].push(i);
          k = data[i][id];
        }
      } // fin r === 'pour chaque série'

    } // fin op === 'local'

  } // fin ov === 'traduites par zéro'


  // else / si valeurs vides ignorées
  if (ov === 'ignorées') {

    // au moment de leur identification, les valeurs vides sont isolées des tableaux de tableaux d'indices (de valeurs non-vides)
    for (i = 0; i < dl; i++) {
      if (data[i][c] === '') { av[av.length] = i; }
    }

    // si global = 1 tableau de tableaux (1) avec tous les indices des valeurs non-vides ...
    if (op === 'global'){
      aa[0] = [];
      for (i = 0; i < dl; i++) {
        if (data[i][c] !== '') { aa[0].push(i); }
      }
    }

    // si local = 1 tableau de tableaux avec les indices (par série ou identifiant) ...
    if (op === 'local'){

      if (r === 'pour chaque identifiant'){
        at = tab2hash(data,id);
        for (k in at){
          if (at.hasOwnProperty(k)){
            aa[aa.length] = at[k].filter(x => data[x][c] !== ''); // on exclut les valeurs vides
          }
        } // fin for
      } // fin r === 'pour chaque identifiant'

      if (r === 'pour chaque série'){
        k = data[0][id] + 'x';
        for (i = 0; i < dl; i++) {
          if(data[i][c] === '') { continue; } // on exclut les valeurs vides
          if(data[i][id] !== k) {
            aa[aa.length] = [];
          }
          aa[aa.length-1].push(i);
          k = data[i][id];
        }
      } // fin r === 'pour chaque série'

    } // fin op === 'local'

  } // fin ov === 'ignorées'

  // traitement commun
  // ... trié selon l'ordre (dir)
  if (ot === 'cumul' && dir === 'de la dernière ligne') {
    for (i = 0; i < aa.length; i++) { aa[i].reverse(); }
  }

  // résultat "vide" pour les valeurs vides
  for (i = 0; i < av.length; i++) {
    data[av[i]].unshift('');
  }

  // traitements spécifiques
  if (ot === 'total') {
    prepareTotal(o, c, aa);
  } else { // ot === 'cumul'
    prepareCumul(o, c, aa);
  }

  // préparation de l'en-tête
  h = o.substring(0,3) + '(' + ot + '/' + op + '){' + headers[c] + '}' + trad[ov];

  if (op === 'local') {
    h += ':' + trad[r] + '(' + headers[id] + ')';
  }

  if (ot === 'cumul'){
    h += '/' + trad[dir];
  }

  headers.unshift(h);

  resultat();
  message('terminé');
  rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin operation1colonne

/**
 * crée une nouvelle colonne intitulée h avec pour chaque ligne le résultat de l'opération { c - o - p }
 * @param   {number} c - indice de colonne (première opérande)
 * @param   {string} o - opération : somme (+), soustraction (-), multiplication (*), division (/), division entière, modulo, pourcentage (%)
 * @param   {number} p - indice de colonne (seconde opérande)
 * @param   {string} h - en-tête de la colonne résultat
 */
function operation2colonnes(c, o, p, h) {
    "use strict";
    var f = 0.0,
        v = '',
        dec = 0,
        i = 0,
        dl = datalen,
        t = JSON.stringify([
            ['colonne'.bold() + ' : ' + headers[c]],
            ['opérateur'.bold() + ' : ' + o],
            ['colonne'.bold() + ' : ' + headers[p]]
        ]);

    bkp("calculer une opération sur 2 colonnes", t);

    headers.unshift(h);

    switch (o) {
    case '+':
        for (i = 0; i < dl; i++) {
            if (data[i][c] !== '' && data[i][p] !== '') {
                f = st2float(data[i][c]) + st2float(data[i][p]);
                dec = plusGrandePrecision(data[i][c], data[i][p]);
                v = f.toFixed(dec);
                data[i].unshift(v);
            } else {
                data[i].unshift('');
            }
        }
        break;
    case '-':
        for (i = 0; i < dl; i++) {
            if (data[i][c] !== '' && data[i][p] !== '') {
                f = st2float(data[i][c]) - st2float(data[i][p]);
                dec = plusGrandePrecision(data[i][c], data[i][p]);
                v = f.toFixed(dec);
                data[i].unshift(v) ;
            } else {
                data[i].unshift('');
            }
        }
        break;
    case '*':
        for (i = 0; i < dl; i++) {
            if (data[i][c] !== '' && data[i][p] !== '') {
                f = st2float(data[i][c]) * st2float(data[i][p]);
                dec = nombreDecimales(data[i][c]) + nombreDecimales(data[i][p]);
                v = f.toFixed(dec);
                data[i].unshift(v);
            } else {
                data[i].unshift('');
            }
        }
        break;
    case '/':
        for (i = 0; i < dl; i++) {
            if (data[i][c] !== '' && data[i][p] !== '') {
                f = st2float(data[i][c]) / st2float(data[i][p]);
                dec = nombreDecimales(data[i][c]) + longueurPartieEntiere(data[i][p]);
                v = f.toFixed(dec);
                data[i].unshift(v);
            } else {
                data[i].unshift('');
            }
        }
        break;
    case 'division entière':
        for (i = 0; i < dl; i++) {
            if (data[i][c] !== '' && data[i][p] !== '') {
                f = st2float(data[i][c]) / st2float(data[i][p]);
                v = Math.floor(f).toString();
                data[i].unshift(v);
            } else {
                data[i].unshift('');
            }
        }
        break;
    case 'modulo':
        for (i = 0; i < dl; i++) {
            if (data[i][c] !== '' && data[i][p] !== '') {
                f = Math.ceil(st2float(data[i][c])) % Math.ceil(st2float(data[i][p]));
                v = f.toString();
                data[i].unshift(v);
            } else {
                data[i].unshift('');
            }
        }
        break;
    case '%':
        for (i = 0; i < dl; i++) {
            if (data[i][c] !== '' && data[i][p] !== '') {
                f = 100 * (st2float(data[i][p]) - st2float(data[i][c])) / st2float(data[i][c]);
                v = f.toFixed(2);
                data[i].unshift(v);
            } else {
                data[i].unshift('');
            }
        }
        break;
    }

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin operation2colonnes

/**
 * crée une nouvelle colonne intitulée h avec pour chaque ligne le résultat de l'opération { c - o - vc }
 * @param   {number} c  - indice de colonne (opérande)
 * @param   {number} vc - valeur constante
 * @param   {string} o  - opération : (opérande + constante),(opérande - constante),(constante - opérande),(opérande * constante),(opérande / constante),(constante / opérande)
 * @param   {string} h  - en-tête de la colonne résultat
 */
function operation1constante(c, vc, o, h) {
    "use strict";
    var f = 0.0,
        v = '',
        dec = 0,
        i = 0,
        dl = datalen,
        t = JSON.stringify([
            ['colonne opérande'.bold() + ' : ' + headers[c]],
            ['constante'.bold() + ' : ' + vc],
            ['opération'.bold() + ' : ' + o]
            
        ]);

    if (vc === '') {
      attention("la constante n'est pas renseignée");
      return;
    }

    bkp("calculer une opération avec 1 constante", t);

    headers.unshift(h);

    switch (o) {
    case 'opérande + constante':
        for (i = 0; i < dl; i++) {
            if (data[i][c] !== '') {
                f = st2float(data[i][c]) + st2float(vc);
                dec = plusGrandePrecision(data[i][c], vc);
                v = f.toFixed(dec);
                data[i].unshift(v);
            } else {
                data[i].unshift('');
            }
        }
        break;
    case 'opérande - constante':
        for (i = 0; i < dl; i++) {
            if (data[i][c] !== '') {
                f = st2float(data[i][c]) - st2float(vc);
                dec = plusGrandePrecision(data[i][c], vc);
                v = f.toFixed(dec);
                data[i].unshift(v);
            } else {
                data[i].unshift('');
            }
        }
        break;
     case 'constante - opérande':
        for (i = 0; i < dl; i++) {
            if (data[i][c] !== '') {
                f = st2float(vc) - st2float(data[i][c]);
                dec = plusGrandePrecision(data[i][c], vc);
                v = f.toFixed(dec);
                data[i].unshift(v);
            } else {
                data[i].unshift('');
            }
        }
        break;
     case 'opérande * constante':
        for (i = 0; i < dl; i++) {
            if (data[i][c] !== '') {
                f = st2float(data[i][c]) * st2float(vc);
                dec = nombreDecimales(data[i][c]) + nombreDecimales(vc);
                v = f.toFixed(dec);
                data[i].unshift(v);
            } else {
                data[i].unshift('');
            }
        }
        break;
    case 'opérande / constante':
        for (i = 0; i < dl; i++) {
            if (data[i][c] !== '') {
                f = st2float(data[i][c]) / st2float(vc);
                dec = nombreDecimales(data[i][c]) + longueurPartieEntiere(vc);
                v = f.toFixed(dec);
                data[i].unshift(v);
            } else {
                data[i].unshift('');
            }
        }
        break;
    case 'constante / opérande':
        for (i = 0; i < dl; i++) {
            if (data[i][c] !== '') {
                f = st2float(vc) / st2float(data[i][c]);
                dec = nombreDecimales(vc) + longueurPartieEntiere(data[i][c]);
                v = f.toFixed(dec);
                data[i].unshift(v);
            } else {
                data[i].unshift('');
            }
        }
        break;
    }

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin operation1constante

/**
 * crée une nouvelle colonne avec la somme ou la moyenne des valeurs de chaque ligne
 * pour les colonnes sélectionnées
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} vr - valeur calculée : (somme) ou (moyenne)
 * @param   {string} o  - option : gestion des valeurs vides (ignorées) ou (traduites par zéro)
 */
function calculerSommeMoyenneColonnes(cs, vr, o) {
    "use strict";
    var dl = datalen,
        a = [],
        ar = [],
        l = 0,
        c = 0,
        i = 0,
        j = 0,
        h = '',
        g = function (){},

        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['calculer la'.bold() + ' : ' + vr],
            ['les valeurs vides sont'.bold() + ' : ' + o]
        ]);
    bkp('calculer la somme ou la moyenne / colonnes', p);

    function f(a, b) { return st2float(a) + st2float(b); }

    if (vr === 'somme') {
      g = function(x) {
        if (!x.length) return 'aucune valeur';
        return '' + x.reduce(f);
      };
    } else { // vr === 'moyenne'
      g = function(x) {
        var r = 0.0;
        if (!x.length) return 'aucune valeur';
        r = x.reduce(f) / x.length;
        return '' + r.toFixed(2);
      };
    }

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) { a[a.length] = c; }
    }

    l = a.length;

    if (o === 'ignorées') {
        for (i = 0; i < dl; i++) {
            ar = [];
            for (j = 0; j < l; j++) {
                c = a[j];
                if(data[i][c] === '') { continue; }
                ar[ar.length] = data[i][c];
            }
            data[i].unshift(g(ar));
        }

    } else { // o === 'traduites par zéro'

        for (i = 0; i < dl; i++) {
            ar = [];
            for (j = 0; j < l; j++) {
                c = a[j];
                ar[ar.length] = data[i][c];
              }
              data[i].unshift(g(ar));
          }

    }

    h = vr + '(' + a.length + ')' + (o === 'traduites par zéro' ? '/0' : '/_');
    headers.unshift(h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify([
        ['nouvelle colonne 1'.bold() + ' : ' + h]
    ]));
} // fin calculerSommeMoyenneColonnes


// ................................................................ intervalles


/**
 * @summary complète un intervalle de valeurs par des valeurs intermédiaires
 * @description ajoute une colonne au début du tableau
 * en complétant un intervalle sous la forme a-b (a et b valeurs entières)
 * - par toutes les valeurs comprises entre a et b
 * - incrémentées de la valeur entière "pas"
 * - séparées par s
 * @param   {number} c1  - indice de colonne
 * @param   {number} c2  - indice de colonne
 * @param   {number} pas - pas (par défaut = 1)
 * @param   {string} s   - caractère séparateur des valeurs de l'intervalle
 */
function completerIntervalle(c1, c2, pas, s) {
    "use strict";
    var dl = datalen,
        v1 = 0.0,
        v2 = 0.0,
        inc = pas === '' ? 1 : st2float(pas),
        l = [],
        h = '',
        i = 0,
        j = 0.0,
        j2 = 0.0,

        p = JSON.stringify([
             ['intervalles définis par'.bold()],
             ['  début'.bold() + ' : ' + headers[c1]],
             ['  fin'.bold() + ' : ' + headers[c2]],
             ['valeurs générées'.bold()],
             ['  pas'.bold() + ' : ' + inc],
             ['  séparées par'.bold() + ' : ' + s]
        ]);

    if (inc < 0.0) {
        attention("le pas doit être positif");
        return;
    }

    bkp('compléter un intervalle', p);

    for (i = 0; i < dl; i++) {
        l = [];

        if ( data[i][c1] === '' || data[i][c2] === '' ) {
             l[0] = data[i][c1] + data[i][c2];
        } else {
            v1 = st2float(data[i][c1]);
            v2 = st2float(data[i][c2]);

            if (v1 > v2) {
                l.push('intervalle incorrect');
            } else {
                for (j = v1; j <= v2; j = j + inc) {
                    j2 = j;
                    l.push(j2.toFixed(2));
                }
           }
        }
        data[i].splice(0, 0, l.join(s));
    }

    h = '[ ' + headers[c1] + ' ; ' + headers[c2] + ' ]' + ' (complet)';
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin completerIntervalle

/**
 * @summary recherche une valeur dans un intervalle de valeurs
 * @description ajoute une colonne au début du tableau
 * en indiquant si la valeur recherche se trouve (inférieure), (incluse), (supérieure) à l'intervalle de référence
 * @param   {number} c1 - indice de colonne
 * @param   {number} c2 - indice de colonne
 * @param   {number} v  - valeur recherchée
 */
function rechercherDansIntervalle(c1, c2, v) {
    "use strict";
    var dl = datalen,
        h = '',
        i  = 0,
        v1 = 0,
        v2 = 0,
        v0 = 0,
        r  = '',

        p = JSON.stringify([
             ['intervalles définis par'.bold()],
             ['  début'.bold() + ' : ' + headers[c1]],
             ['  fin'.bold() + ' : ' + headers[c2]],
             ['valeur recherchée'.bold() + ' : ' + headers[v]]
        ]);

    bkp('rechercher une valeur dans un intervalle', p);

    for (i = 0; i < dl; i++) {
        if ( data[i][c1] === '' || data[i][c2] === '' ) {
            r = 'intervalle non-borné';
        } else {
            v0 = st2float(data[i][v]);
            v1 = st2float(data[i][c1]);
            v2 = st2float(data[i][c2]);

            if (v1 > v2) {
                r = 'intervalle incorrect';
                data[i].splice(0, 0, r);
                continue;
            }

            // si l'intervalle est "cohérent"
            if(v0 < v1) {
                r = 'inférieure';
            } else {
                if (v0 > v2) {
                    r = 'supérieure';
                } else {
                    r = 'incluse';
                }
            }
        }

        data[i].splice(0, 0, r);
    }

    h = '[ ' + headers[c1] + ' ; ' + headers[c2] + ' ]' + ' ? ' + headers[v];
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin rechercherDansIntervalle

/**
 * @summary combiner deux intervalles
 * @description ajoute une colonne au début du tableau
 * en indiquant l'intervalle résultat de la fusion, l'union, ou de l'intersection
 * @param   {number} c1 - indice de colonne
 * @param   {number} c2 - indice de colonne
 * @param   {number} c3 - indice de colonne
 * @param   {number} c4 - indice de colonne
 * @param   {string} o  - option : (fusion), (union), (intersection)
 */
function combinerDeuxIntervalles(c1, c2, c3, c4, o) {
    "use strict";
    var dl = datalen,
        h = '',
        i  = 0,
        v1 = 0.0,
        v2 = 0.0,
        v3 = 0.0,
        v4 = 0.0,
        r  = '',

        p = JSON.stringify([
             ['intervalle 1 défini par'.bold()],
             ['  début'.bold() + ' : ' + headers[c1]],
             ['  fin'.bold() + ' : ' + headers[c2]],
             ['intervalle 2 défini par'.bold()],
             ['  début'.bold() + ' : ' + headers[c3]],
             ['  fin'.bold() + ' : ' + headers[c4]],
             ['action'.bold() + ' : ' + o]
        ]);

    bkp('combiner deux intervalles', p);

    for (i = 0; i < dl; i++) {
        if ( data[i][c1] === '' || data[i][c2] === '' ) {
          if ( data[i][c3] === '' || data[i][c4] === '' ) {
              r = 'interv. 1 et 2 non-bornés';
          } else {
              r = 'interv. 1 non-borné';
          }
          data[i].splice(0, 0, r);
          continue;
        } // else
        if ( data[i][c3] === '' || data[i][c4] === '' ) {
            r = 'interv. 2 non-borné';
            data[i].splice(0, 0, r);
            continue;
        }

        v1 = st2float(data[i][c1]);
        v2 = st2float(data[i][c2]);
        v3 = st2float(data[i][c3]);
        v4 = st2float(data[i][c4]);

        if (v1 > v2) {
          if (v3 > v4) {
              r = 'interv. 1 et 2 incorrects';
          } else {
            r = 'interv. 1 incorrect';
            data[i].splice(0, 0, r);
            continue;
          }
        } // else

        if (v3 > v4) {
            r = 'interv. 2 incorrect';
            data[i].splice(0, 0, r);
            continue;
        }

        if (o !== 'fusion' && ((v2 < v3) || v1 > v4) ) {
            r = 'interv. disjoints';
            data[i].splice(0, 0, r);
            continue;
        }

        if (o === 'intersection') {
            r = '' + ((v1 > v3) ? data[i][c1] : data[i][c3]) + ' à ' + ((v2 < v4) ? data[i][c2] : data[i][c4]);
        } else { // union ou fusion
            r = '' + ((v1 < v3) ? data[i][c1] : data[i][c3]) + ' à ' + ((v2 < v4) ? data[i][c4] : data[i][c2]);
        }
        data[i].splice(0, 0, r);
    }

    h  = '[ ' + headers[c1] + ' ; ' + headers[c2] + ' ]';
    h += ' ?';
    h += o === 'fusion' ? 'f ' : '';
    h += o === 'union' ? 'u ' : '';
    h += o === 'intersection' ? 'i ' : '';
    h += '[ ' + headers[c3] + ' ; ' + headers[c4] + ' ]';
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin combinerDeuxIntervalles

/**
 * ajoute une colonne en début de tableau
 * avec le résultat de l'opération "centrer et réduire" des valeurs d'une colonne
 * dans l'intervalle o parmi [0, 1], [-1, 1]
 * @param   {number} c - indice de colonne
 * @param   {string} o  - option : ([0, 1]),([-1, 1])
 */
function centrerReduire(c, o) {
    "use strict";
    var dl = datalen,
        h = '',
        min = 0.0, // valeur arbitraire pour typer la variable
        max = 0.0, // valeur arbitraire pour typer la variable
        v = 0.0,
        i = 0,

        p = JSON.stringify([
             ['sélection'.bold() + ' : ' + headers[c]],
             ['entre'.bold() + ' : ' + o]
        ]);
    bkp('centrer et réduire', p);

    function centrer01(v){
        var r = 0.0;
        r = (v - min) / (max - min);
        return r;
    }

    function centrer_11(v) {
        var r = 0.0;
        r = (v - min) / (max - min);
        return (2 * r) - 1;
    }

    function isMin(x)  { return min > st2float(x) ? st2float(x) : min; }
    function isMax(x)  { return max < st2float(x) ? st2float(x) : max; }

    // lever les valeurs arbitraires
    for (i = 0; i < dl; i++) {
      if (data[i][c] !== '') {
        min = st2float(data[i][c]);
        max = st2float(data[i][c]);
        break;
      }
    }

    // si le min et max n'ont pas pu être déterminés, on quitte le traitement
    if(i === dl && data[i - 1][c] === '') {
      attention('la colonne ne comporte aucune valeur numérique');
      rb.memr(JSON.stringify(['attention'.bold() + ' : ' + 'aucune valeur numérique ?']));
      return;
    }

    // traitement général
    for (i = 0; i < dl; i++) {
      if (data[i][c] !== '') {
          min = isMin(data[i][c]);
          max = isMax(data[i][c]);
      }
    }

    switch(o) {
        case 'entre 0 et 1' :
            for (i = 0; i < dl; i++) {
              if (data[i][c] !== '') {
                v = st2float(data[i][c]);
                data[i].unshift('' + centrer01(v).toFixed(2));
              } else {
                data[i].unshift('');
              }
            }
            h = headers[c] + ' ~ 0 1';
            break;
        case 'entre -1 et 1' :
            for (i = 0; i < dl; i++) {
              if (data[i][c] !== '') {
                v = st2float(data[i][c]);
                data[i].unshift('' + centrer_11(v).toFixed(2));
              } else {
                data[i].unshift('');
              }
            }
            h = headers[c] + ' ~ -1 1';
            break;
    }

    headers.unshift(h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin centrerReduire

/**
 * ajoute une colonne en début de tableau
 * avec le résultat de la comparaison des seuils
 * @param   {number} c  - indice de colonne
 * @param   {string} o  - opérateur (<), (≤), (>), (≥)
 * @param   {string} t  - liste de seuils séparés par un saut de ligne
 */
function comparerSeuils(c, o, t) {
    "use strict";
    var dl = datalen,
        s = t.split('\n'),
        h = '',
        r = '',
        i = 0,
        g = function (){},

        p = JSON.stringify([
            ['colonne'.bold() + ' : ' + headers[c]],
            ['  comparaison'.bold() + ' : ' + o],
            ['  avec les seuils'.bold() + ' : </br>' + t]
        ]);
    bkp('comparer seuils', p);

    function f(a, b) { return a - b; }

    // conversion numérique
    s = s.map(e => parseFloat(e) ); /*@FIXME comparerSeuilsDates*/

    // pré-traitement pour éviter des boucles infinies si les seuils n'étaient pas triés initialement
    s.sort(f);
    if (o === '>' || o === '≥') { s.reverse(); }

    // teste l'opérateur 1 seule fois (et non 1 fois par ligne)
    switch(o) {
      case '<' : g = function(v, s) { return s.find(e => v <  e); }; break;
      case '≤' : g = function(v, s) { return s.find(e => v <= e); }; break;
      case '>' : g = function(v, s) { return s.find(e => v >  e); }; break;
      case '≥' : g = function(v, s) { return s.find(e => v >= e); }; break;
    }

    for (i = 0; i < dl; i++) {
      if (data[i][c] === '') {
        data[i].splice(0, 0, '');
      } else {
        r = g(parseFloat(data[i][c]), s);
        r = typeof r  === 'undefined' ? '' : r;
        data[i].splice(0, 0, '' + r);
      }
    }

    h = headers[c] + ' ' + o + ' {seuils}';
    headers.unshift(h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin comparerSeuils


// .................................................................. structure


/**
 * crée un index (clé unique) pour chaque valeur de la colonne c
 * et remplace les valeurs de c avec cet index (l'index commence à 1)
 * @param   {number} c - indice de colonne
 * @param   {number} p - premier numéro
 * @see     {@link normalise}
 * @see     {@link card}
 * @see     {@link drac}
 */
function majnormalise(c, p) {
    "use strict";
    var v = '',
        dl = datalen,
        cpt = p,
        i = 0;

    card = null;
    card = [];
    drac = null;
    drac = [];
    for (i = 0; i < dl; i++) {
        v = data[i][c];
        if (!card.hasOwnProperty(v)) {
            drac[drac.length] = v;
            card[v] = cpt;
            cpt++;
        }
        data[i][c] = card[v].toString();
    }
} // fin majnormalise

/**
 * remplace chaque valeur de la colonne c par un identifiant unique
 * et affiche une table de correspondance entre ces identifiants
 * et les anciennes valeurs de la colonne c
 * (i l s'agit de la 2ème forme normale selon Merise)
 * @param   {number} c  - indice de colonne
 * @param   {number} pn - premier numéro (si vide, initialisé à 1)
 * @see     {@link majnormalise}
 * @see     {@link card}
 * @see     {@link drac}
 */
function normalise(c, pn) {
    "use strict";
    var t = '',
        n = 0,
        i = 0,
        v = '',

        p = JSON.stringify([ ['colonne'.bold() + ' : ' + headers[c]] ]);
    bkp('créer des identifiants', p);

    pn = pn ? pn : 1;
    majnormalise(c, pn);

    t  = '<table style="white-space:pre;">'
       + '<thead><tr><thead><tr><td>' + headers[c] + '</td>'
       + '<td>clé</td></tr></thead>'
       + '<tbody>';

    par = null;
    par = [];

    // dans l'ordre croissant des index
    for (i = 0; i < drac.length; i++) {
        v = drac[i];
        if (card.hasOwnProperty(v)) {
             t += '<tr><td>' + v + '</td><td>' + card[v] + '</td></tr>';
             par[n] = [];
             par[n][0] = v;
             par[n][1] = card[v];
             n++;
        }
    }
    t += '</tbody>'
       + '</table>';

    resultat();
    messageEtBoutons(c, t, n);
} // fin normalise

/**
 * pour toutes les colonnes sélectionnées dans cs,
 * remplace les lettres accentuées par leur équivalent sans accent
 * ainsi que la cédille par un simple 'c'.
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @see     {@link remplacerCaracteres}
 * @see     {@link A_Z__AVEC_ACCENT}
 * @see     {@link A_Z__SANS_ACCENT}
 */
function sansAccent(cs) {
    "use strict";
    var cpt = 0,
        i = 0,
        c = 0,
        pl = '',
        m = '',
        v = '',
        dl = datalen,

        p = JSON.stringify([ ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')] ]);
    bkp('enlever les accents et cédilles', p);

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
             for (i = 0; i < dl; i++) {
                 v = remplacerCaracteres(data[i][c], A_Z__AVEC_ACCENT, A_Z__SANS_ACCENT);
                 if (data[i][c] !== v) { cpt++; }
                 data[i][c] = v;
             }
        }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    m = cpt === 0 ? 'aucune modification' : cpt + ' valeur' + pl + ' corrigée' + pl;
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin sansAccent

/**
 * pour toutes les colonnes sélectionnées dans cs
 * remplace les caractères de ponctuation par le caractère r
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} r  - caractère de remplacement
 * @see     {@link remplacerCaracteres}
 * @see     {@link PONCTUATION_COMP}
 */
function remplacerPonctuation(cs, r) {
    "use strict";
    var avc = PONCTUATION_COMP, /* FIXME : pourquoi l'utilisation d'une variable locale ? */
        l = avc.length,
        sns = [],
        i = 0,
        c = 0,
        cpt = 0,
        dl = datalen,
        v = '',
        pl = '',
        m = '',

        p = JSON.stringify([
             ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
             ['  remplacer par'.bold() + ' : ' + r]
        ]);
    bkp('remplacer les caractères non-alphanumériques', p);

    if (r !== '') { r = r.charAt(0); }
    for (i = 0; i < l; i++) { sns[i] = r; }

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
             for (i = 0; i < dl; i++) {
                 v = remplacerCaracteres(data[i][c], avc, sns);
                 if (data[i][c] !== v) { cpt++; }
                 data[i][c] = v;
             }
        }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    m = cpt === 0 ? 'aucune modification' : cpt + ' valeur' + pl + ' corrigée' + pl;
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin remplacerPonctuation

/**
 * affiche toutes les valeurs de la colonne c de longueur strictement supérieure à s
 * @param   {number} c - indice de colonne
 * @param   {number} s - seuil inférieur
 * @see     {@link compteCarMax}
 */
function compteCarMin(c, s) {
    "use strict";
    var t = '',
        a = [],
        ob = {},
        l = 0,
        i = 0,
        j = 0,
        n = 0,
        v = '',
        dl = datalen;

    if (typeof s === 'undefined') { s = 0; }

    for (i = 0; i < dl; i++) {
        v = data[i][c];
        l = v.length;
        if (l > s) {
             if (! ob.hasOwnProperty(l)) { ob[l] = []; }
             ob[l].push(v);
        }
    }

    a = Object.keys(ob).reverse(); // Object.keys renvoie dans l'ordre croissant

    t  = '<table style="white-space:pre;">'
       + '<thead><tr>'
       + '<td>' + headers[c]
       + '<a class="sbd" onclick="' + "sortTable('tb_valmn',0,true, 't');" + '">▼</a>'
       + '<a class="sbd" onclick="' + "sortTable('tb_valmn',0,false,'t');" + '">▲</a>'
       + '</td>'
       + '<td>longueur(car.)'
       + '<a class="sbd" onclick="' + "sortTable('tb_valmn',1,true, 'n');" + '">▼</a>'
       + '<a class="sbd" onclick="' + "sortTable('tb_valmn',1,false,'n');" + '">▲</a>'
       + '</td></tr></thead>'
       + '<tbody id="tb_valmn">';

    par = null;
    par = [];

    for (i = 0; i < a.length; i++) {
        l = a[i];
        ob[l] = arrValUnique(ob[l]);
        for (j = 0; j < ob[l].length; j++) {
             t += '<tr><td>' + ob[l][j] + '</td><td>' + l + '</td></tr>';
// @deprecated (car pas d'export CSV) ?
             par[n] = [];
             par[n][0] = l;
             par[n][1] = ob[l][j];
// utile pour le total
             n++;
        }
    }

    t += '</tbody>'
       + '<tfoot><tr>'
       + '<td>TOTAL</td>'
       + '<td>' + n + '</td>'
       + '</tr></tfoot>'
       + '</table>';

    messageEtBoutons(c, t, n, 'nocsv');
} // fin compteCarMin

/**
 * affiche toutes les valeurs de la colonne c de longueur strictement inférieure à s
 * @param   {number} c - indice de colonne
 * @param   {number} s - seuil supérieur
 * @see     {@link compteCarMin}
 */
function compteCarMax(c, s) {
    "use strict";
    var t = '',
        a = [],
        ob = {},
        l = 0,
        i = 0,
        j = 0,
        n = 0,
        v = '',
        dl = datalen;

    if (typeof s === 'undefined') { s = 0; }

    for (i = 0; i < dl; i++) {
        v = data[i][c];
        l = v.length;
        if (l < s) {
             if (! ob.hasOwnProperty(l)) { ob[l] = []; }
             ob[l].push(v);
        }
    }

    a = Object.keys(ob).reverse(); // Object.keys renvoie dans l'ordre croissant

    t  = '<table style="white-space:pre;">'
       + '<thead><tr>'
       + '<td>' + headers[c]
       + '<a class="sbd" class="sbd" onclick="' + "sortTable('tb_valmx',0,true, 't');" + '">▼</a>'
       + '<a class="sbd" class="sbd" onclick="' + "sortTable('tb_valmx',0,false,'t');" + '">▲</a>'
       + '</td>'
       + '<td>longueur(car.)'
       + '<a class="sbd" class="sbd" class="sbd" onclick="' + "sortTable('tb_valmx',1,true, 'n');" + '">▼</a>'
       + '<a class="sbd" class="sbd" class="sbd" onclick="' + "sortTable('tb_valmx',1,false,'n');" + '">▲</a>'
       + '</td></tr></thead>'
       + '<tbody id="tb_valmx">';

    par = null;
    par = [];

    for (i = 0; i < a.length; i++) {
        l = a[i];
        ob[l] = arrValUnique(ob[l]);
        for (j = 0; j < ob[l].length; j++) {
          t += '<tr><td>' + ob[l][j] + '</td><td>' + l + '</td></tr>';
// @deprecated (car pas d'export CSV) ?
          par[n] = [];
          par[n][0] = l;
          par[n][1] = ob[l][j];
// utile pour le total
          n++;
        }
    }

    t += '</tbody>'
      + '<tfoot><tr>'
      + '<td>TOTAL</td>'
      + '<td>' + n + '</td>'
      + '</tr></tfoot>'
      + '</table>';

    messageEtBoutons(c, t, n, 'nocsv');
} // fin compteCarMax

/**
 * crée une nouvelle colonne intitulée h qui contient
 * le nombre de caractères des valeurs de la colonne c
 * @param   {number} c - indice de colonne
 * @param   {string} h - en-tête de la nouvelle colonne
 */
function colNbCar(c, h) {
    "use strict";
    var v = '',
        dl = datalen,
        i = 0,

        p = JSON.stringify([ ['colonne'.bold() + ' : ' + headers[c]] ]);
    bkp('connaître le nombre de caractères', p);

    headers.unshift(h);

    for (i = 0; i < dl; i++) {
        v = data[i][c].length.toString();
        data[i].unshift(v);
    }

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin colNbCar

/**
 * pour toutes les colonnes sélectionnées dans cs, supprime le caractère s en début et fin
 * de toutes les valeurs des colonnes sélectionnées
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} s  - caractère délimiteur à supprimer
 */
function supprimerDelimiteur(cs, s) {
    "use strict";
    var c = 0,
        i = 0,
        v = '',
        pl = '',
        m = '',
        cpt = 0,
        dl = datalen,

        p = JSON.stringify([
             ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
             ['  délimiteur'.bold() + ' : ' + s]
        ]);
    bkp('supprimer le délimiteur', p);

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
             for (i = 0; i < dl; i++) {
                 v = supprimerDelimiteurTexte(data[i][c], s);
                 if (data[i][c] !== v) { cpt++; }
                 data[i][c] = v;
             }
        }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    m = cpt === 0 ? 'aucune modification' : cpt + ' valeur' + pl + ' corrigée' + pl;
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin suppDelim

/**
 * pour toutes les colonnes sélectionnées dans cs, supprime les espaces en début et fin
 * pour toutes les valeurs des colonnes sélectionnées
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 */
function supprimerEspacesColonne(cs) {
    "use strict";
    var c = 0,
        i = 0,
        v = '',
        pl = '',
        m = '',
        cpt = 0,
        dl = datalen,

        p = JSON.stringify([ ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')] ]);
    bkp('enlever les espaces en début/fin', p);

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
             for (i = 0; i < dl; i++) {
                 v = trim(data[i][c]);
                 if (data[i][c] !== v) { cpt++; }
                 data[i][c] = v;
             }
        }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    m = cpt === 0 ? 'aucune modification' : cpt + ' valeur' + pl + ' corrigée' + pl;
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin supprimerEspacesColonne

/**
 * pour toutes les colonnes sélectionnées dans cs, réduit à une seule occurrence les espaces consécutifs
 * pour toutes les valeurs des colonnes sélectionnées
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 */
function compacterEspaces(cs) {
    "use strict";
    var v = '',
        cpt = 0,
        dl = datalen,
        c = 0,
        i = 0,
        pl = '',
        m = '',

        p = JSON.stringify([['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')]]);
    bkp('compacter les espaces consécutifs', p);

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
             for (i = 0; i < dl; i++) {
                 v = data[i][c].replace(/ +/g, " ");
                 if (data[i][c] !== v) { cpt++; }
                 data[i][c] = v;
             }
        }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    m = cpt === 0 ? 'aucune modification' : cpt + ' valeur' + pl + ' corrigée' + pl;
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin compacterEspaces

/**
 * pour toutes les colonnes sélectionnées dans cs, supprime tous les articles
 * qui apparaissent dans les valeurs
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @see     {@link similariteEntreSelon}
 */
function sansArticle(cs) {
    "use strict";
    var cpt = 0,
        pl = '',
        m = '',
        i = 0,
        c = 0,
        v = '',
        dl = datalen,

        p = JSON.stringify([['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')]]);
    bkp('enlever les articles', p);

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
             for (i = 0; i < dl; i++) {
                 v = supprimerArticles(data[i][c]);
                 if (v !== data[i][c]) { cpt++; }
                 data[i][c] = v;
             }
        }
    }

    resultat();
    pl = cpt > 1 ? 's' : '';
    m = cpt === 0 ? 'aucune modification' : cpt + ' valeur' + pl + ' modifiée' + pl;
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin sansArticle


// .................................................................... emphase


/**
 * @summary applique la classe .bold pour toutes les colonnes sélectionnées dans cs
 * @description selon le réglage, l'emphase peut
 * - soit être appliquée à la valeur (apparaît en gras)
 * - soit faire apparaître les balises HTML
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @see     {@link normal}
 */
function gras(cs) {
    "use strict";
    var dl = datalen,
        c = 0,
        i = 0,

        p = JSON.stringify([ ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')] ]);
    bkp('gras', p);

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
             for (i = 0; i < dl; i++) { data[i][c] = '<span class="bold">' + data[i][c] + '</span class="bold">'; }
        }
    }

    resultat();
    message('terminé');
} // fin gras

/**
 * @summary applique la classe .italic pour toutes les colonnes sélectionnées dans cs
 * @description selon le réglage, l'emphase peut
 * - soit être appliquée à la valeur (apparaît en italique)
 * - soit faire apparaître les balises HTML
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @see     {@link normal}
 */
function italique(cs) {
    "use strict";
    var dl = datalen,
        c = 0,
        i = 0,

        p = JSON.stringify([ ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')] ]);
    bkp('italique', p);

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
             for (i = 0; i < dl; i++) { data[i][c] = '<span class="italic">' + data[i][c] + '</span class="italic">'; }
        }
    }

    resultat();
    message('terminé');
} // fin italique

/**
 * @summary applique la class .strike pour toutes les colonnes sélectionnées dans cs
 * @description selon le réglage, l'emphase peut
 * - soit être appliquée à la valeur (apparaît barré)
 * - soit faire apparaître les balises HTML
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @see     {@link normal}
 */
function barre(cs) {
    "use strict";
    var dl = datalen,
        c = 0,
        i = 0,

        p = JSON.stringify([ ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')] ]);
    bkp('barré', p);

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
             for (i = 0; i < dl; i++) { data[i][c] = '<span class="strike">' + data[i][c] + '</span class="strike">'; }
        }
    }

    resultat();
    message('terminé');
} // fin barre

/**
 * @summary applique la classe css .big pour toutes les colonnes sélectionnées dans cs
 * @description selon le réglage, l'emphase peut
 * - soit être appliquée à la valeur (police plus grande)
 * - soit faire apparaître les balises HTML
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @see     {@link normal}
 */
function augmenter(cs) {
    "use strict";
    var dl = datalen,
        c = 0,
        i = 0,

        p = JSON.stringify([ ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')] ]);
    bkp('augmenter la taille', p);

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
             for (i = 0; i < dl; i++) { data[i][c] = '<span class="big">' + data[i][c] + '</span class="big">'; }
        }
    }

    resultat();
    message('terminé');
} // fin augmenter

/**
 * @summary applique la classe css .small pour toutes les colonnes sélectionnées dans cs
 * @description selon le réglage, l'emphase peut
 * - soit être appliquée à la valeur (police plus petite)
 * - soit faire apparaître les balises HTML
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @see     {@link normal}
 */
function diminuer(cs) {
    "use strict";
    var dl = datalen,
        c = 0,
        i = 0,

        p = JSON.stringify([ ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')] ]);
    bkp('diminuer la taille', p);

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
             for (i = 0; i < dl; i++) { data[i][c] = '<span class="small">' + data[i][c] + '</span class="small">'; }
        }
    }

    resultat();
    message('terminé');
} // fin diminuer

/**
 * @summary pour toutes les colonnes sélectionnées dans cs, applique au texte la couleur fc (balise html <span class="color" style="color:..."><\/span>
 * @description selon le réglage, l'emphase peut
 * - soit être appliquée à la valeur (couleur)
 * - soit faire apparaître les balises HTML
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} s  - code couleur (exemple : red, #078464)
 * @see     {@link normal}
 * @see     {@link sansCouleur}
 */
function colorier(cs, s) {
    "use strict";
    var dl = datalen,
        c = 0,
        i = 0,
        v = '',

        p = JSON.stringify([
             ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
             ['  couleur'.bold() + ' : ' + s]
        ]);
    bkp('couleur', p);

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
             for (i = 0; i < dl; i++) {
                 v = sansCouleur(data[i][c]);
                 data[i][c] = '<span class="color" style="color:' + s + ';">' + v + '</span class="color">';
             }
        }
    }

    resultat();
    message('terminé');
} // fin colorier

/**
 * pour toutes les colonnes sélectionnées dans cs, réinitialise le style html (supprime l'emphase)
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @see     {@link gras}
 * @see     {@link italique}
 * @see     {@link barre}
 * @see     {@link augmenter}
 * @see     {@link diminuer}
 * @see     {@link colorier}
 * @see     {@link sansMiseEnForme}
 */
function normal(cs) {
    "use strict";
    var dl = datalen,
        c = 0,
        i = 0,

        p = JSON.stringify([ ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')] ]);
    bkp('normal', p);

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) {
             for (i = 0; i < dl; i++) {
                 data[i][c] = sansMiseEnForme(data[i][c]);
             }
        }
    }

    resultat();
    message('terminé');
} // fin normal


// .................................................................... filtres


/**
 * appelle la fonction de filtre attendu
 * @param   {number} c - indice de colonne
 * @param   {string} v - valeur recherchée
 * @param   {string} o - opération de filtre
 * @see     {@link apresValeur}
 * @see     {@link avantValeur}
 * @see     {@link egalValeur}
 * @see     {@link diffValeur}
 */
function filtrer(c, v, o) {
    "use strict";
    var trad = {
         'apresValeur' : 'situées juste après la valeur témoin',
         'avantValeur' : 'situées juste avant la valeur témoin',
         'egalValeur' : 'qui contiennent cette valeur',
         'diffValeur' : 'qui ne contiennent pas cette valeur'
        },
        p = JSON.stringify([
          ['colonne'.bold() + ' : ' + headers[c]],
          ['  retenir les lignes'.bold() + ' : ' + trad[o]],
          ['  valeur témoin'.bold() + ' : ' + v]
        ]);

    bkp('retenir les lignes...', p);

    switch(o) {
        case 'apresValeur': apresValeur(c, v); break;
        case 'avantValeur': avantValeur(c, v); break;
        case 'egalValeur' : egalValeur(c, v);  break;
        case 'diffValeur' : diffValeur(c, v);  break;
    }
}

/**
 * conserve les lignes situées juste avant celles qui possèdent
 * la valeur v (dans la colonne c)
 * @param   {number} c - indice de colonne
 * @param   {string} v - valeur recherchée
 * @see     {@link apresValeur}
 */
function avantValeur(c, v) {
    "use strict";
    var i = 0,
        b = false,
        pl = '',
        m = '',
        cpt = 0,
        dl = datalen;

    for (i = dl - 1; i >= 0; i--) { // on part de la fin pour éviter de recalculer l'index i
        if (data[i][c] === v) {
             data.splice(i, 1);
             b = true;
        } else {
             if (b) { /* on conserve la ligne */ }
             else   { data.splice(i, 1); }
             b = false;
        }
    }

    cpt = data.length;

    resultat();
    pl = cpt > 1 ? 's' : '';
    if (cpt === 0) {
        m = 'aucune modification';
    } else {
        m = cpt + ' ligne' + pl + ' conservée' + pl
          + '<br>'
          + (dl - cpt) + ' supprimée' + ((dl - cpt) > 1 ? 's' : '');
    }
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin avantValeur

/**
 * conserve les lignes situées juste après celles qui possèdent
 * la valeur v (dans la colonne c)
 * @param   {number} c - indice de colonne
 * @param   {string} v - valeur recherchée
 * @see     {@link avantValeur}
 */
function apresValeur(c, v) {
    "use strict";
    var cpt = 0,
        i = 0,
        pl = '',
        m = '',
        b = false,
        dl = datalen;

    /** @see avantValeur (reverse + strictement identique + reverse) */
    data.reverse();
    for (i = dl - 1; i >= 0; i--) { // on part de la fin pour éviter de recalculer l'index i
        if (data[i][c] === v) {
             data.splice(i, 1);
             b = true;
        } else {
             if (b) { /* on conserve la ligne */ }
             else   { data.splice(i, 1); }
             b = false;
        }
    }
    data.reverse();

    cpt = data.length;

    resultat();
    pl = cpt > 1 ? 's' : '';
    if (cpt === 0) {
        m = 'aucune modification';
    } else {
        m = cpt + ' ligne' + pl + ' conservée' + pl
          + '<br>'
          + (dl - cpt) + ' supprimée' + ((dl - cpt) > 1 ? 's' : '');
    }
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin apresValeur

/**
 * conserve les lignes situées qui possèdent la valeur v (dans la colonne c)
 * @param   {number} c - indice de colonne
 * @param   {string} v - valeur recherchée
 * @see     {@link diffValeur}
 */
function egalValeur(c, v) {
    "use strict";
    var cpt = 0,
        pl = '',
        m = '',
        i = 0,
        dl = datalen;

    for (i = dl - 1; i >= 0; i--) { // on part de la fin pour éviter de recalculer l'index i
        if (data[i][c] !== v) { data.splice(i, 1); }
    }

    cpt = data.length;

    resultat();
    pl = cpt > 1 ? 's' : '';
    if (cpt === dl) {
        m = 'aucune modification';
    } else {
        m = cpt + ' ligne' + pl + ' conservée' + pl
          + '<br>'
          + (dl - cpt) + ' supprimée' + ((dl - cpt) > 1 ? 's' : '');
    }
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin egalValeur

/**
 * exclut les lignes situées qui possèdent la valeur v (dans la colonne c)
 * @param   {number} c - indice de colonne
 * @param   {string} v - valeur recherchée
 * @see     {@link egalValeur}
 */
function diffValeur(c, v) {
    "use strict";
    var cpt = 0,
        pl = '',
        m = '',
        i = 0,
        dl = datalen;

    for (i = dl - 1; i >= 0; i--) { // on part de la fin pour éviter de recalculer l'index i
        if (data[i][c] === v) { data.splice(i, 1); }
    }

    cpt = data.length;

    resultat();
    pl = cpt > 1 ? 's' : '';
    if (cpt === dl) {
        m = 'aucune modification';
    } else {
        m = cpt + ' ligne' + pl + ' conservée' + pl
          + '<br>'
          + (dl - cpt) + ' supprimée' + ((dl - cpt) > 1 ? 's' : '');
    }
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin diffValeur

/**
 * appelle la fonction de filtre attendu
 * @param   {number} c - indice de colonne
 * @param   {string} o - opération de filtre
 * @see     {@link retenirLesPremieresOccurrences}
 * @see     {@link retenirLesDernieresOccurrences}
 */
function conserverOccurrences(c, o) {
    "use strict";

    switch(o) {
        case 'premières occurrences': retenirLesPremieresOccurrences(c); break;
        case 'dernières occurrences': retenirLesDernieresOccurrences(c); break;
    }
}

/**
 * filtre la colonne c en retenant les premières occurrences
 * en partant du haut, des valeurs pour chaque série de valeurs consécutives
 * @param   {number} c - indice de colonne
 * @see     {@link retenirLesDernieresOccurrences}
 */
function retenirLesPremieresOccurrences(c) {
    "use strict";
    var dl = datalen,
        i = 0,
        cpt = 0,
        pl = '',
        m = '',

        p = JSON.stringify([
             ['première occurrence'.bold()],
             ['colonne'.bold() + ' : ' + headers[c]]
        ]);
    bkp("première ou dernière occurrence de chaque série", p);

    /** @see retenirLesDernieresOccurrences (reverse + strictement identique + reverse) */
    data.reverse();
    for (i = dl - 2; i >= 0; i--) { // on part de la fin pour éviter de recalculer l'index i
        if (data[i][c] === data[i + 1][c]) {
            data.splice(i, 1);
        }
    }
    data.reverse();

    cpt = data.length;

    resultat();
    pl = cpt > 1 ? 's' : '';
    if (cpt === dl) {
        m = 'aucune modification';
    } else {
        m = cpt + ' ligne' + pl + ' conservée' + pl
          + '<br>'
          + (dl - cpt) + ' supprimée' + ((dl - cpt) > 1 ? 's' : '');
    }
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin retenirLesPremieresOccurrences

/**
 * filtre la colonne c en retenant les premières occurrences
 * en partant du bas (donc les dernières occurrences)
 * des valeurs pour chaque série de valeurs consécutives
 * @param   {number} c - indice de colonne
 * @see     {@link retenirLesPremieresOccurrences}
 */
function retenirLesDernieresOccurrences(c) {
    "use strict";
    var dl = datalen,
        i = 0,
        cpt = 0,
        pl = '',
        m = '',

        p = JSON.stringify([
             ['dernière occurrence'.bold()],
             ['colonne'.bold() + ' : ' + headers[c]]
        ]);
    bkp("première ou dernière occurrence de chaque série", p);

    for (i = dl - 2; i >= 0; i--) { // on part de la fin pour éviter de recalculer l'index i
        if (data[i][c] === data[i + 1][c]) {
            data.splice(i, 1);
        }
    }

    cpt = data.length;

    resultat();
    pl = cpt > 1 ? 's' : '';
    if (cpt === dl) {
        m = 'aucune modification';
    } else {
        m = cpt + ' ligne' + pl + ' conservée' + pl
          + '<br>'
          + (dl - cpt) + ' supprimée' + ((dl - cpt) > 1 ? 's' : '');
    }
    message(m);
    rb.memr(JSON.stringify([m]));
} // fin retenirLesDernieresOccurrences

/**
 * @summary identifie, exclut ou isole les lignes vides (aussi fonction d'analyse)
 * @description identifie les lignes pour lesquelles la sélection cs contient
 * - selon o, soit une valeur vide, soit que des valeurs vides
 * - selon l'action a, ces lignes sont filtrées, exclues ou marquées
 * Pour toutes les opérations, une synthèse de l'analyse est présentée à l'utilisateur.
 * Si l'opération a est "analyser", alors le jeu de données n'est pas
 * modifié et l'historique ne trace pas ce traitement.
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} o  - condition : (au moins une valeur vide), (uniquement des valeurs vides)
 * @param   {string} a  - action : (filtrer), (exclure), (marquer), (analyser)
 */
function lignesValeursVides(cs, o, a) {
    "use strict";
    var dl = datalen,
        i = 0,
        cpt = 0,
        ar = [],
        v = '',
        pl = '',
        m = '',

        p = JSON.stringify([
            ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
            ['  condition'.bold() + ' : ' +  o],
            ['  action'.bold() + ' : ' +  a]
        ]);

    if (a !== 'analyser') {
      bkp("lignes contenant des valeurs vides", p);
    }

    // identification des lignes à supprimer/conserver
    function fs(x, c){ return cs[c].selected && x === ''; }
    function fe(x, c){ return cs[c].selected && x !== ''; }

    // identification des lignes à supprimer/conserver
    if (o === 'au moins une valeur vide') {
        for (i = 0; i < dl; i++) {
            // vrai si la ligne a au moins une valeur vide pour la sélection de colonnes
            ar[i] = data[i].some(fs);
            if (ar[i]) { cpt++; }
        }
    } else { // o === 'uniquement des valeurs vides'
        for (i = 0; i < dl; i++) {
            // vrai si la ligne n'a que des valeurs vides pour la sélection de colonnes
            // équivalent à : vrai si la ligne a au moins une valeur non-vide pour la sélection de colonnes
            ar[i] = ! data[i].some(fe);
            if (ar[i]) { cpt++; }
        }
    }

    // action à réaliser
    switch(a){
    case 'filtrer':
      for (i = dl - 1; i >= 0; i--) { // on part de la fin pour éviter de recalculer l'index i
          if( !ar[i] ) { data.splice(i, 1); }
      }
      break;
    case 'exclure':
      for (i = dl - 1; i >= 0; i--) { // on part de la fin pour éviter de recalculer l'index i
          if( ar[i] ) { data.splice(i, 1); }
      }
      break;
    case 'marquer':
      for (i = 0; i < dl; i++) {
          v = ar[i] ? 'oui' : '';
          data[i].splice(0, 0, v);
      }
      headers.splice(0, 0, o);
      break;
    case 'analyser': // ne rien faire
      break;
    }

    resultat();

    pl = cpt > 1 ? 's' : '';
    m = cpt === 0 ? 'aucune modification' : cpt + ' ligne' + pl + ' identifiée' + pl + ' (sur ' + dl + ')';
    message(m);
    if (a !== 'analyser') {
         if (a !== 'marquer') {
            rb.memr(JSON.stringify(['nouveau nombre de lignes'.bold() + ' : ' + data.length]));
        } else {
            rb.memr(JSON.stringify([m]));
        }
    }
} // fin lignesValeursVides

/**
 * @summary identifier, exclure ou isoler une cohorte
 * @description quand la colonne mq possède la valeur mvac - caractère discriminant -
 * - on lit sur la ligne la valeur présente dans la colonne c
 * - on applique l'opération o sur toutes les lignes qui possèdent cette même valeur
 * Le caractère discriminant permet de marquer une population (un ensemble d'individus)
 * ensuite tous les évènements de ces individus sont
 * - soit filtrés (conservés)
 * - soit exclus (supprimés)
 * - soit marqués (nouvelle colonne)
 * @param   {string}  mq   - indice de colonne "marqueur" (caractère observé)
 * @param   {number}  c    - indice de colonne "identifiant"
 * @param   {string}  mavc - valeur recherchée
 * @param   {boolean} scon - option sensible à la casse (oui), (non)
 * @param   {string}  o    - opération : (filtrer), (exclure), (marquer)
 */
function marquerSelon(mq, c, mavc, sc, o) {
    "use strict";
    var i = 0,
        reg = '',
        cdata = [],
        data2 = [],
        v = '',
        s = '',
        h = '',
        dl = datalen,
        m = '',

        trad = {
             'i': 'non',
             '' : 'oui'
        },

        p = JSON.stringify([
             ['si (colonne)'.bold() + ' : ' + headers[mq]],
             ['  motif'.bold() + ' : ' + mavc],
             ['  sensible à la casse'.bold() + ' : ' + trad[sc]],
             ['alors ' + o.bold() + ' : ' + headers[c]]
        ]);
    bkp('suivre une cohorte', p);

    // programmation défensive + aide utilisateur
    try {
        reg = new RegExp(mavc, sc + "gm");
    }
    catch (e) {
        m = e.message;
        message(m);
        rb.memr(JSON.stringify([m]));
        return;
    }

    // quand on trouve dans la colonne mq le caractère discriminant (/reg/)
    // on mémorise alors la valeur v rencontrée sur la même ligne et dans la colonne c
    for (i = 0; i < dl; i++) {
        s = data[i][mq];
        v = data[i][c];
        if (s.search(reg) >= 0) {
             if ( ! cdata.hasOwnProperty(v)) { cdata[v] = []; }
             cdata[v].push(i);
        }
    }

    // construction du tableau résultat
    switch (o) {
    case 'exclure':
        for (i = 0; i < dl; i++) {
             v = data[i][c];
             if ( ! cdata.hasOwnProperty(v)) { data2[data2.length] = data[i]; }
        }
        break;
    case 'filtrer':
        for (i = 0; i < dl; i++) {
             v = data[i][c];
             if ( cdata.hasOwnProperty(v)) { data2[data2.length] = data[i]; }
        }
        break;
    case 'marquer': // on ajoute une nouvelle colonne qui contient '', 'trouvé' ou 'suivi'
        for (i = 0; i < dl; i++) {
             v = data[i][c];
             if ( ! cdata.hasOwnProperty(v)) { data2.push([''].concat(data[i])); }
             else {
                 if (cdata[v].length > 0 && cdata[v][0] === i) {
                     data2.push(['trouvé'].concat(data[i]));
                     cdata[v].shift(); // cdata[v] est un tableau d'entiers ordonnés
                 } else { data2.push(['suivi'].concat(data[i])); }
             }
        }
        h = headers[mq] + '[=' + mavc + ']/' + headers[c];
        headers.unshift(h);
        break;
    }

    data = data2;

    resultat();
    message('terminé');
    if (o === 'marquer') {
        rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
    }
} // fin marquerSelon

/**
 * @summary identifier, exclure ou isoler les lignes en doublons (aussi fonction d'analyse)
 * @description effectue l'opération o sur le jeu de données
 * en recherchant des doublons sur les colonnes sélectionnées dans cs
 * Pour toutes les opérations, une synthèse de l'analyse est présentée à l'utilisateur.
 * Si l'opération ac est "analyser", alors le jeu de données n'est pas
 * modifié et l'historique ne trace pas ce traitement.
 * 1. les valeurs des colonnes sélectionnées sont stockées dans un JSON
 * 2. la recherche de doublon revient à une simple comparaison de
 * eux chaînes de caractères contenant les JSON des tableaux partiels
 * d'une ligne ne contenant que les valeurs des colonnes sélectionnées
 * @param   {Object} cs - Select HTML Element, sélecteur multiple de colonne
 * @param   {string} o  - opération : (filtrer), (exclure), (marquer), (marquer et filtrer), (analyser)
 */
function marquerDoublons(cs, o) {
    "use strict";
    var a = [],
        d = [],
        cd = [],
        cptR = 0,
        cptS = 0,
        cptL = 0,
        v = '',
        t = '',
        data2 = [],
        headers2 = [],
        p = '',
        i = 0,
        c = 0,
        diff = 0,
        dl = datalen;

    if (o !== 'analyser') {
        p = JSON.stringify([
             ['colonnes'.bold() + ' : <br>  ' + listeColSelection(cs).join('<br>  ')],
             ['  action'.bold() + ' : ' + o]
        ]);
        bkp('singletons, doublons', p);
    }

    for (c = 0; c < cs.length; c++) {
        if (cs[c].selected) { cptS++; }
    }

    // construction d'un tableau associatif "index => valeur"
    // où l'index est la clé (composée des valeurs des colonnes sélectionnées)
    // et la valeur est un tableau contenant les numéros des lignes concernées
    for (i = 0; i < dl; i++) { // pour chaque ligne de données
        a = [];
        for (c = 0; c < cs.length; c++) {
             if (cs[c].selected) { a[a.length] = data[i][c]; }
        }
        v = JSON.stringify(a);
        if ( ! cd.hasOwnProperty(v)) { cd[v] = []; }
        cd[v][cd[v].length] = i;
    }

    for (i = 0; i < dl; i++) { d[i] = 0; }
    for (v in cd) { // pour chaque "clé" identifiante
        if (cd.hasOwnProperty(v)) {
            // on marque chaque groupe de valeurs répétées (au moins 2 répétitions)
            // par un numéro identifié (incrément)
            if (cd[v].length >= 2) {
                cptR++;
                for (i = 0; i < cd[v].length; i++) { d[cd[v][i]] = cptR; }
                cptL += cd[v].length;
            }
        }
    }

    switch (o) {
    case 'exclure':
        for (i = 0; i < dl; i++) {
             if (d[i] === 0) { data2[data2.length] = data[i]; }
        }
        data = data2;
        break;
    case 'filtrer':
        for (i = 0; i < dl; i++) {
             if (d[i] !== 0) { data2[data2.length] = data[i]; }
        }
        data = data2;
        break;
    case 'marquer':
        headers2 = ['! doublons(' + cptS + ')'].concat(headers);
        for (i = 0; i < dl; i++) {
             v = (d[i] !== 0) ? d[i].toString() : '';
             data2[data2.length] = [v].concat(data[i]);
        }
        headers = headers2;
        data = data2;
        break;
    case 'marquer et filtrer':
        headers2 = ['! doublons(' + cptS + ')'].concat(headers);
        for (i = 0; i < dl; i++) {
             if (d[i] !== 0) { data2[data2.length] = [d[i].toString()].concat(data[i]); }
        }
        headers = headers2;
        data = data2;
        break;
    case 'analyser': // ne rien faire
        break;
    }

    t  = `<table>
          <thead><tr>
          <td>&nbsp;</td>
          <td>nb de lignes</td><td>nb valeurs différentes</td>
          </tr></thead>
          <tbody>`;

    diff = dl - cptL + cptR;

    t += `<tr><td>situation initiale</td>
          <td>${dl}</td><td>${diff}</td></tr>
          <tr><td>↳ dont lignes singletons</td>`;

    diff = dl - cptL;

    t += `<td>${diff}</td><td>${diff}</td></tr>
          <tr><td>↳ dont lignes doublons, triplons…</td>
          <td>${cptL}</td><td>${cptR}</td></tr>
          </tbody>
          </table>`;

    resultat();
    messageEtBoutons('', t, 10, 'nocsv');
    if (o !== 'analyser' && o !== 'marquer') {
        rb.memr(JSON.stringify(['nouveau nombre de lignes'.bold() + ' : ' + data.length]));
    }
} // fin marquerDoublons

/**
 * réduit le jeu de données à un échantillon de taille n en se basant sur le mélange de Fisher–Yates
 * @param   {number} n - nombre d'éléments dans l'échantillon à conserver (taille de l'échantillon)
 * @see     {@link shuffle_array}
 */
function echantillon(n) {
    "use strict";
    var dl = datalen,
        ai = [],
        m = '',

        p = JSON.stringify([ ["taille de l'échantillon".bold() + ' : ' + n] ]);
    bkp('échantillonner', p);

    // programmation défensive + feedback utilisateur
    if (n > dl) {
        m = "l'échantillon ne peut pas contenir plus de " + dl + " lignes de données";
        message(m);
        rb.memr(JSON.stringify([m]));
        return;
    }

    // création d'une liste des numéros de ligne
    ai = data.map(function(x, i, a) { return i; } );

    // échantillonnage
    ai = shuffle_array(ai); // mélange         (échantillon aléatoire...)
    ai = ai.splice(0, dl - n); // on déduit la partie "à supprimer" (échantillon de taille n)

    filtreArray(data, ai); // on supprime la partie "à supprimer"

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouveau nombre de lignes'.bold() + ' : ' + data.length]));
} // fin echantillon

/**
 * réduit le jeu de données à un échantillon constitué de segments
 * définis dans la colonne sg de taille définies dans la colonne t.
 * Les sous-échantillons sont générés par le mélange de Fisher–Yates.
 * @param   {string} cg - indice de colonne décrivant les segments
 * @param   {string} ct - indice de colonne décrivant la taille des segments
 * @see     {@link shuffle_array}
 * @see     {@link echantillon}
 */
function echantillonPoids(cg, ct) {
    "use strict";
    var ets = [], // liste des indices d'éléments à tirer par segment
        sg = '', // un segment
        n = 0,
        re = [], // regroupement des lignes à supprimer

        p = JSON.stringify([
            ['segments'.bold() + ' : ' + headers[cg]],
            ['taille des segments'.bold() + ' : ' + headers[ct]]
        ]);
    bkp('échantillonner avec pondérations', p);

    // création des segments
    ets = tab2hash(data,cg);

    // échantillonnage
    for (sg in ets) {
        if (ets.hasOwnProperty(sg)) {
            /** @see echantillon (strictement identique) */
            ets[sg] = shuffle_array(ets[sg]); // mélange (échantillon aléatoire...)
            n = parseInt(data[ets[sg][0]][ct], 10);
            // on déduit la partie "à supprimer" (échantillon de taille n)
            ets[sg] = ets[sg].splice(0, ets[sg].length - n);
            re = re.concat(ets[sg]);
        }
    }

    // suppression des lignes "à supprimer"
    filtreArray(data, re);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouveau nombre de lignes'.bold() + ' : ' + data.length]));
} // fin echantillonPoids


// ............................................................. enrichissement


/**
 * fonction d'analyse, analyse les correspondances (jointure) entre deux jeux de données
 * @param   {Array} cm - associations {valeur du fichier maître -> [indices de ligne]}
 * @param   {Array} cl - associations {valeur du fichier lookup -> [indices de ligne]}
 * @see     {@link croiser}
 * FIXME : JSHint (octobre 2018) :
 * Largest function has 41 statements in it, while the median is 7.
 * Cyclomatic complexity number for this function is 18, while the median is 3.
 */
function analyserCroisement(cm, cl){
    "use strict";
    var cc = [], // cardinalités
        cv = [], // vides
        ccl = [], // libellés cardinalités
        cvl = [], // libellés vides
        s = '',
        t = '';

    cc = {
        '1-0': 0,
        '1-1': 0,
        '1-n': 0,
        '0-1': 0,
        'n-1': 0,
        'n-m': 0,
        'n-0': 0,
        '0-n': 0
    };

    ccl = {
        '1-0': 'clés uniques du fichier maître, absentes du fichier lookup',
        '1-1': 'clés uniques, apparaissant 1 seule fois dans chaque fichier',
        '1-n': 'clés uniques du fichier maître, avec plusieurs correspondances dans le fichier lookup',
        '0-1': 'clés uniques du fichier lookup, absentes du fichier maître',
        'n-1': 'clés uniques du fichier lookup, avec plusieurs correspondances dans le fichier maître',
        'n-m': 'clés doublons apparaissant plusieurs fois dans chaque fichier',
        'n-0': 'clés doublons dans le fichier maître, sans correspondance dans le fichier lookup',
        '0-n': 'clés doublons dans le fichier lookup, sans correspondance dans le fichier maître'
    };

    cv = {
        'master': 0,
        'lookup': 0
    };

    cvl = {
        'master': 'lignes du fichier master sans clé',
        'lookup': 'lignes du fichier lookup sans clé'
    };

    // mémo : cm = tab2hash(data, c);
    if (cm.hasOwnProperty('')) { cv.master = cm[''].length; }

    // mémo : cl = tab2hash(datalkp, clkp);
    if (cl.hasOwnProperty('')) { cv.lookup = cl[''].length; }

    for (s in cm) {
        if (cm.hasOwnProperty(s)) {
             if (! cl.hasOwnProperty(s) ) {
                 if (cm[s].length > 1) {
                     cc['n-0']++;
                 } else {
                     cc['1-0']++;
                 }
             } else {
                 if (cm[s].length === 1) {
                     if (cl[s].length === 1) {
                         cc['1-1']++;
                     } else { cc['1-n']++; }
                 } else {
                     if (cl[s].length === 1) {
                         cc['n-1']++;
                     } else {
                         cc['n-m']++;
                     }
                 }
             }
        }
    }
    for (s in cl) {
        if (cl.hasOwnProperty(s)) {
             if (! cm.hasOwnProperty(s) ) {
                 if (cl[s].length > 1) {
                     cc['0-n']++;
                 } else {
                     cc['0-1']++;
                 }
             }
        }
    }

    t = `nombre de correspondances fichiers maître-lookup<br />
         <table>
         <thead><tr><td>cardinalités</td><td>nb</td></tr></thead>
         <tbody>`;
    for (s in cc) {
        if (cc.hasOwnProperty(s)) {
             t += '<tr><td>' + s + ' : ' + ccl[s] + '</td><td>' + cc[s] + '</td></tr>';
        }
    }
    for (s in cv) {
        if (cv.hasOwnProperty(s)) {
             t += '<tr><td>' + cvl[s] + '</td><td>' + cv[s] + '</td></tr>';
        }
    }
    t += `</tbody>
          </table>`;

    messageEtBoutons('', t, 7, 'nocsv');
} // fin analyserCroisement

/**
 * @summary croise deux sources de données (aussi fonction d'analyse)
 * @description Applique la transformation jt aux 2 sources de données en utilisant
 * pour clés les colonnes c (fichier maître) et clkp (fichier lookup)
 * et en préfixant les en-têtes de colonnes du fichier résultat par
 * pmaitre (fichier maître) et plkp (fichier lookup).
 * Dans le cas de la transformation "analyser les correspondances",
 * aucune modification n'est reportée sur le jeu de données
 * et l'historique ne garde pas de trace de cette fonction d'analyse
 * @param   {number}  clkp   - indice de colonne, clé du fichier lookup
 * @param   {number}  c      - indice de colonne, clé du fichier maître
 * @param   {string}  jt     - traitement :
 * - uniquement les correspondances exactes (1)
 * - les lignes du fichier maître absentes du lookup (2)
 * - les lignes du lookup absentes du fichier maître (3)
 * - les lignes du fichier maître enrichies par le lookup (4)
 * - les lignes du lookup enrichies par le fichier maître (5)
 * - toutes les lignes des deux fichiers enrichies (6)
 * - uniquement les lignes sans correspondance (7)
 * - analyser les correspondances (simuler) (8)
 * @param   {string} pmaitre - préfixe des en-têtes du fichier maître
 * @param   {string} plkp    - préfixe des en-têtes du fichier lookup
 * @see     {@link analyserCroisement}
 * @see     {@link texte2data}
 * FIXME : JSHint (juillet 2019) :
 * Largest function has 75 statements in it, while the median is 7.
 * The most complex function has a cyclomatic complexity value of 36 while the median is 3.
 */
function croiser(clkp, c, jt, pmaitre, plkp) {
    "use strict";
    var cdata = [],
        cdatalkp = [],
        data2 = [],
        headers2 = [],
        factice = [],
        s = '',
        i = 0,
        j = 0,
        n = 0, // numéro ligne
        p = [],
        m = '',
        dl = datalen,
        trad = {
            '1': 'uniquement les correspondances exactes',
            '2': 'les lignes du fichier maître absentes du lookup',
            '3': 'les lignes du lookup absentes du fichier maître',
            '4': 'les lignes du fichier maître enrichies par le lookup',
            '5': 'les lignes du lookup enrichies par le fichier maître',
            '6': 'toutes les lignes des deux fichiers enrichies',
            '7': 'uniquement les lignes sans correspondance',
            '8': 'analyser les correspondances (simuler)',
            '|': '|',
            ';': ';',
            ',': ',',
            '.': '.',
            ':': ':',
            '\\': '\\',
            '/': '/',
            ' ': '(espace)',
            '	': '(tabulation)'
        };

    // réinitialisation
    headerslkp = [];
    datalkp = [];

    texte2data(txtlkp.split('\n'), headerslkp, datalkp, confChargeLkp);

    // création des index
    cdata = tab2hash(data, c);
    cdatalkp = tab2hash(datalkp, clkp);

    // analyser les correspondances (simuler)
    if (jt === '8'){
        analyserCroisement(cdata, cdatalkp);
        return;
    }

    p.push([derChgLkp[0] + ' (' + colonnesLignes(headerslkp.length, datalkp.length) + ')']);
    p.push(['encodage'.bold() + ' : ' + confChargeLkp.encod]);
    p.push(['séparateur'.bold() + ' : ' + trad[confChargeLkp.sepa]]);
    p.push(['conserver'.bold() + ' : ' + trad[jt]]);
    p.push(['clé de jointure fic. lookup'.bold() + ' : ' + headerslkp[clkp]]);
    if (plkp) { p.push(['  préfixée par'.bold() + ' : ' + plkp]); }
    p.push(['clé de jointure fic. maître'.bold() + ' : ' + headers[c]]);
    if (pmaitre) { p.push(['  préfixée par'.bold() + ' : ' + pmaitre]); }

    bkp('fusionner, compléter ou exclure par jointure', JSON.stringify(p));

    headers.forEach(function (x, i, a) { a[i] = pmaitre + x; });
    headerslkp.forEach(function (x, i, a) { a[i] = plkp + x; });

    // selon le type de jointure
    switch (jt) {
    case '1': // uniquement les correspondances exactes
        // pour tous les éléments (dans l'ordre du fichier de référence)
        for (i = 0; i < dl; i++) {
             s = data[i][c];
             // s'il existe dans tel ou tel autre index, concaténer les données
             if (cdatalkp.hasOwnProperty(s)) {
                 for (j = 0; j < cdatalkp[s].length; j++) {
                     n = cdatalkp[s][j];
                     data2.push(data[i].concat(datalkp[n]));
                 }
             }
        }
        headers2 = headers.concat(headerslkp);
        break;
    case '2': // les lignes du fichier maître absentes du lookup
        for (i = 0; i < dl; i++) {
             s = data[i][c];
             if (! cdatalkp.hasOwnProperty(s)) { data2.push(data[i]); }
        }
        headers2 = headers;
        break;
    case '3': // les lignes du lookup absentes du fichier maître
        for (i = 0; i < datalkp.length; i++) {
             s = datalkp[i][clkp];
             if (! cdata.hasOwnProperty(s)) { data2.push(datalkp[i]); }
        }
        headers2 = headerslkp;
        break;
    case '4': // les lignes du fichier maître enrichies par le lookup
        factice = new Array(headerslkp.length).fill('');
        for (i = 0; i < dl; i++) {
             s = data[i][c];
             if (cdatalkp.hasOwnProperty(s)) {
                 for (j = 0; j < cdatalkp[s].length; j++) {
                     n = cdatalkp[s][j];
                     data2.push(data[i].concat(datalkp[n]));
                 }
             } else {
                 data2.push(data[i].concat(factice));
             }
        }
        headers2 = headers.concat(headerslkp);
        break;
    case '5': // les lignes du lookup enrichies par le fichier maître
        factice = new Array(headers.length).fill('');
        for (i = 0; i < datalkp.length; i++) {
             s = datalkp[i][clkp];
             if (cdata.hasOwnProperty(s)) {
                 for (j = 0; j < cdata[s].length; j++) {
                     n = cdata[s][j];
                     data2.push(datalkp[i].concat(data[n]));
                 }
             } else { data2.push(datalkp[i].concat(factice)); }
        }
        headers2 = headerslkp.concat(headers);
        break;
    case '6': // toutes les lignes des deux fichiers enrichies
        factice = new Array(headerslkp.length).fill('');
        for (i = 0; i < dl; i++) {
             s = data[i][c];
             if (cdatalkp.hasOwnProperty(s)) {
                 for (j = 0; j < cdatalkp[s].length; j++) {
                     n = cdatalkp[s][j];
                     data2.push(data[i].concat(datalkp[n]));
                 }
             } else { data2.push(data[i].concat(factice)); }
        }
        factice = [];
        factice = new Array(headers.length).fill('');
        for (i = 0; i < datalkp.length; i++) {
             s = datalkp[i][clkp];
             if (! cdata.hasOwnProperty(s)) {
                 data2.push(factice.concat(datalkp[i]));
             }
        }
        headers2 = headers.concat(headerslkp);
        break;
    case '7': // uniquement les lignes sans correspondance
        factice = new Array(headerslkp.length).fill('');
        for (i = 0; i < dl; i++) {
             s = data[i][c];
             if (! cdatalkp.hasOwnProperty(s)) {
                 data2.push(data[i].concat(factice));
             }
        }
        factice = [];
        factice = new Array(headers.length).fill('');
        for (i = 0; i < datalkp.length; i++) {
             s = datalkp[i][clkp];
             if (! cdata.hasOwnProperty(s)) {
                 data2.push(factice.concat(datalkp[i]));
             }
        }
        headers2 = headers.concat(headerslkp);
        break;
    }

    data = data2;
    headers = headers2;

    resultat();
    m = colonnesLignes(headers.length, data.length);
    message(m);

    rb.memr(JSON.stringify([m]));
} // fin croiser

/**
 * @summary chiffre ou déchiffre les valeurs d'une colonne
 * @description selon b, chiffre ou déchiffre les valeurs de la colonne c
 * avec la clé générée ou saisie par l'utilisateur
 * remarque : historiquement, permet de chiffrer des adresses mails,
 * la clé est une série de caractères codée sur 7 bits AVAILABLE_CHAR_LIST
 * (ne gère pas les accents)
 * @param   {number}  c - indice de colonne
 * @param   {string}  o - action : (chiffrer), (déchiffrer)
 * @see     {@link AVAILABLE_CHAR_LIST}
 * @see     {@link nsc}
 * @see     {@link nsd}
 */
function chiffrer(c, o) {
    "use strict";
    var v = '',
        dl = datalen,
        i = 0,
        h = '',
        p = '',
        kc = valeur('kc'),
        trad = {
          'chiffrer': 'chiffré',
          'déchiffrer': 'déchiffré'
        };

    p = JSON.stringify([
        ['colonne'.bold() + ' : ' + headers[c]],
        ['  clé'.bold() + ' : ' + kc],
        ['  action'.bold() + ' : ' + o]
    ]);

    bkp('chiffrer/déchiffrer une donnée', p);

    h = headers[c] + ' - ' + trad[o];
    headers.splice(0, 0, h);

    if (o === 'chiffrer') {
        for (i = 0; i < dl; i++) {
            v = nsc(data[i][c], kc);
            data[i].splice(0, 0, v);
        }
    } else { // o === 'déchiffrer'
        for (i = 0; i < dl; i++) {
            v = nsd(data[i][c], kc);
            data[i].splice(0, 0, v);
        }
    }

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin chiffrer

/**
 * ajoute une nouvelle colonne avec le hash des valeurs d'une colonne
 * @param   {number}  c - indice de colonne
 * @see     {@link https://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript}
 */
function hasher(c) {
    "use strict";
    var dl = datalen,
        h = '',
        i  = 0,

        p = JSON.stringify([
             ['colonne'.bold() + ' : ' + headers[c]]
        ]);

    bkp('créer une empreinte pour chaque donnée', p);

    // source : https://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript
    function hashString(s) {
        var hash = 0, i, chr;
        for (i = 0; i < s.length; i++) {
            chr   = s.charCodeAt(i);
            hash  = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }
        return hash;
    } // fin hashstring

    for (i = 0; i < dl; i++) {
        data[i].splice(0, 0, '' + hashString(data[i][c]));
    }

    h = 'hash ~ ' + headers[c];
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));
} // fin hasher


/**
 * ajoute une nouvelle colonne avec le recodage d'un système de numération à un autre des valeurs d'une colonne
 * @param   {number}  c - indice de colonne
 * @param   {number}  a - base du système de numération de la colonne c
 * @param   {number}  b - base du système de numération du résultat
 */
function changerBase(c, a, b) {
    "use strict";
    var dl = datalen,
        h = '',
        i  = 0,

        p = JSON.stringify([
             ['colonne'.bold() + ' : ' + headers[c]],
             ['  depuis la base '.bold() + ' : ' + a],
             ['  vers la base '.bold() + ' : ' + b]
        ]);

    bkp("convertir un nombre d'un système de numération à un autre", p);

    for (i = 0; i < dl; i++) {
        if (data[i][c] == '') {
            data[i].splice(0,0,'');
            continue;
        } // else
        data[i].splice(0, 0, '' + parseInt(data[i][c], a).toString(b));
    }

    h = headers[c] + ' ~ base(' + a + '->' + b + ')';
    headers.splice(0, 0, h);

    resultat();
    message('terminé');
    rb.memr(JSON.stringify(['nouvelle colonne 1'.bold() + ' : ' + h]));

} // fin changerBase


// .................................................................... matrice


/**
 * transpose le jeu de données, en-tête compris
 */
function transposer() {
    "use strict";
    var dl = datalen,
        dc = headers.length,
         i = 0,
         j = 0,
         min = 0,
         v = '',
         m = '';

  bkp('transposer');

  // dans le tableau de données, on intègre la ligne d'en-tête
  data.unshift(headers.slice());
  dl++;
  min = dl < dc ? dl : dc;

  // pour le "carré" : on pivote les valeurs par symétrie (diagonale)
  for (i = 0; i < min; i++) {
    for (j = i; j < min; j++) {
      v = data[i][j];
      data[i][j] = data[j][i];
      data[j][i] = v;
    }
  }

  // cas n°1 : tableau "en hauteur"
  if (dl > dc) {
    for (i = dc; i < dl; i++) {
      for (j = 0; j < dc; j++) {
        data[j][i] = data[i][j];
      }
    }

    // on supprime les lignes de l'original, désormais en colonne
    data.splice(dc);
  }

  // cas n°2 : tableau "en largeur"
  if (dl < dc) {
    // on crée les nouvelles lignes
    for (j = dl; j < dc; j++) { data[j] = []; }

    // on transpose
    for (j = dl; j < dc; j++) {
      for (i = 0; i < dl; i++) {
        data[j][i] = data[i][j];
      }
    }

    // on supprime le "bout" de l'original de chaque ligne
    for (i = 0; i < dc; i++) { // ici, dc = data.length
      data[i].splice(dl);
    }
  }

  // on récupère la nouvelle ligne d'en-tête
  headers = data.shift();

  resultat();
  m = colonnesLignes(headers.length, data.length);
  message(m);

  rb.memr(JSON.stringify([m]));
} // fin transposer

/**
 * inverse l'ordre des lignes du jeu de données
 */
function inverserLignes() {

  bkp("inverser l'ordre des lignes");

  data.reverse();

  resultat();
  message('terminé');

} // fin inverserLignes

/**
 * trie les lignes par ordre alphabétique, selon les valeurs de la colonne c
 * l'ordre or et selon la position des valeurs vides selon ov
 * @param   {number} c  - indice de colonne
 * @param   {string} or - ordre  :  (A...Z...a...z...É...é),(A...É...Z...a...é...z)
 * @param   {string} ov - valeurs vides  :  (au début),(à la fin)
 **/
function trierAlpha(c, or, ov) {
  "use strict";
  var f = function(){},
      p = JSON.stringify([
            ['colonne'.bold() + ' : ' + headers[c]],
            ['  ordre'.bold() + ' : ' + or],
            ['  valeurs vides placés'.bold() + ' : ' + ov]
        ]);
  bkp('trier par ordre alphabétique', p);

  if (or === 'A...Z...a...z...É...é' && ov === 'au début') {
    f = function(a, b) {
        if (a[c] === b[c]) return 0;
        //else
        return a[c] > b[c];
        };
  }

  if (or === 'A...Z...a...z...É...é' && ov === 'à la fin') {
    f = function(a, b) {
          if (a[c] === b[c]) return 0;
          if (a[c] === '') return  1;
          if (b[c] === '') return -1;
          return a[c] > b[c];
    };
  }

  if (or === 'A...É...Z...a...é...z' && ov === 'au début') {
    f = function(a, b) {
        if (remplacerCaracteres(a[c], A_Z__AVEC_ACCENT, A_Z__SANS_ACCENT) === remplacerCaracteres(b[c], A_Z__AVEC_ACCENT, A_Z__SANS_ACCENT) ) return 0;
        // else
        return remplacerCaracteres(a[c], A_Z__AVEC_ACCENT, A_Z__SANS_ACCENT) > remplacerCaracteres(b[c], A_Z__AVEC_ACCENT, A_Z__SANS_ACCENT);
    };
  };

  if (or === 'A...É...Z...a...é...z' && ov === 'à la fin') {
    f = function(a, b) {
          if (a[c] === b[c]) return 0;
          if (a[c] === '') return  1;
          if (b[c] === '') return -1;
         return remplacerCaracteres(a[c], A_Z__AVEC_ACCENT, A_Z__SANS_ACCENT) > remplacerCaracteres(b[c], A_Z__AVEC_ACCENT, A_Z__SANS_ACCENT);
    };
  }

  data.sort(f);

  resultat();
  message('terminé');

} // fin trierAlpha

/**
 * trie les lignes par ordre numérique, selon les valeurs de la colonne c
 * et considérer les valeurs vides selon o
 * @param   {number} c  - indice de colonne
 * @param   {string} o  - valeurs vides  : (les plus petites),(les plus grandes),(égales à zéro)
 **/
function trierNum(c, o) {
  "use strict";
  var f = function(){},
      p = JSON.stringify([
            ['colonne'.bold() + ' : ' + headers[c]],
            ['  les valeurs vides sont'.bold() + ' : ' + o]
        ]);
  bkp('trier par ordre numérique', p);

  // o === 'égales à zéro'
  f = function f(a, b) { return st2float(a[c]) - st2float(b[c]); };

  if (o === 'les plus petites') {
    f = function(a, b) {
          if (a[c] === b[c]) return 0;
          if (a[c] === '') return -1;
          if (b[c] === '') return  1;
          return st2float(a[c]) - st2float(b[c]);
    };
  }
  if (o === 'les plus grandes') {
    f = function(a, b) {
          if (a[c] === b[c]) return 0;
          if (a[c] === '') return  1;
          if (b[c] === '') return -1;
          return st2float(a[c]) - st2float(b[c]);
    };
  }

  data.sort(f);

  resultat();
  message('terminé');

} // fin trierNum

/**
 * trie les lignes par ordre chronologique, selon les valeurs de la colonne c
 * et considérer les valeurs vides selon o
 * compte-tenu du format de date sélectionné
 * @param   {string} f  - format de la date : (jj mm ssaa), (mm jj ssaa), (ssaa mm jj)
 * @param   {string} s  - séparateur : (.), (-), (/), ( )
 * @param   {number} c  - indice de colonne
 * @param   {string} o  - valeurs vides  : (dans le passé lointain), (dans un futur lointain), (égales à la date du jour), (égales à une date précise)
 * @param   {date}   dp - paramètre optionnel : date si o = "égales à une date précise"
 **/
function trierDate(f, s, c, o, dp) {
  "use strict";
  var d = new Date(),
      auj = d.UTCtoday(),
      formater = function(){},
      g = function(){},

      p = [
            ['format'.bold() + ' : ' + f],
            ['séparateur'.bold() + ' : ' + (s === ' ' ? '(espace)' : s)],
            ['colonne'.bold() + ' : ' + headers[c]],
            ['  les valeurs vides sont'.bold() + ' : ' + o + (o === 'égales à la date du jour' ? ' (' + auj + ')' : '')]
        ];

  if (o === 'égales à une date précise') {
      p[3][0] = p[3][0] + " : " + dp;
  }

  bkp('trier par ordre chronologique', JSON.stringify(p));

  // adaptation de la fonction formater() en fonction du format de la date
  formater = convertirDate(f);

  if (o === 'dans le passé lointain') {
    g = function(a, b) {
        if (a[c] === b[c]) return 0;
        //else
        if (a[c] === '') return -1;
        //else
        if (b[c] === '') return 1;
        //else
        return formater(a[c]) > formater(b[c]);
        };
  }
  if (o === 'dans un futur lointain') {
    g = function(a, b) {
        if (a[c] === b[c]) return 0;
        //else
        if (a[c] === '') return 1;
        //else
        if (b[c] === '') return -1;
        //else
        return formater(a[c]) > formater(b[c]);
        };
  }
  if (o === 'égales à la date du jour') {
    g = function(a, b) {
        if (a[c] === b[c]) return 0;
        //else
        if (a[c] === '') return auj > formater(b[c]);
        //else
        if (b[c] === '') return formater(a[c]) > auj;
        //else
        return formater(a[c]) > formater(b[c]);
        };
  }
  if (o === 'égales à une date précise') {
    g = function(a, b) {
        if (a[c] === b[c]) return 0;
        //else
        if (a[c] === '') return dp > formater(b[c]);
        //else
        if (b[c] === '') return formater(a[c]) > dp;
        //else
        return formater(a[c]) > formater(b[c]);
        };
  }
  data.sort(g);

  resultat();
  message('terminé');

} // fin trierDate


// .................................................... préférences utilisateur


/**
 * vérifie que le séparateur s sélectionné par l'utilisateur
 * n'apparaît pas déjà dans l'en-tête ou dans les données
 * @param   {string} s - caractère séparateur à tester
 * @see     {@link changerSeparateur}
 */
function testerSeparateur(s) {
    "use strict";
    var reg = new RegExp(s, 'g'),
        d = [],
        t = '',
        cptd = [],
        cpth = [],
        tot = 0,
        match = '',
        dl = datalen,
        hl = headers.length,
        i = 0,
        c = 0,
        trad = {
          '|': '|',
          ';': ';',
          ',': ',',
          '.': '.',
          ':': ':',
          '/': '/',
          '\\': '\\',
          '\t': '(tabulation)',
          ' ': '(espace)'
        };


    if (s === '|') { reg = /\|/g; }
    if (s === '.') { reg = /\//g; }
    if (s === '/') { reg = /\./g; }

    // pour toutes les colonnes
    for (c = 0; c < hl; c++) {
        d = [];
        cptd[c] = 0;
        cpth[c] = 0;
        for (i = 0; i < dl; i++) { d[i] = data[i][c]; }
        t = d.join('');
        while (match = reg.exec(t)) { // il ne s'agit pas d'un test d'égalité, ne pas mettre "=="
             cptd[c] += match.length;
             tot += match.length;
        }
        while (match = reg.exec(headers[c])) { // il ne s'agit pas d'un test d'égalité, ne pas mettre "=="
             cpth[c] += match.length;
             tot += match.length;
        }
    }

    // dans le cas le plus simple
    if (tot === 0) {
        message(`test OK pour le séparateur "${trad[s]}"<br />Vous pouvez changer le séparateur sans risque.`);
        return;
    }

    // dans le cas où certaines valeurs contiennent déjà le caractère séparateur s
    t  = `<p>Le caractère ${trad[s]} a été détecté ${tot} fois.<br />
          Il est préférable d'essayer un autre caractère séparateur.</p>
          <table style="white-space:pre;">
          <thead><tr><td>colonne concernée</td><td>nombre de détection</td></tr></thead>
          <tbody>`;

    // construction du tableau de résultat
    tot = 3;
    for (i = 0; i < hl; i++) {
        if (cpth[i] !== 0) {
             t += '<tr><td>en-tête : ' + headers[i] + '</td><td>' + cpth[i] + '</td></tr>';
             tot++;
        }
        if (cptd[i] !== 0) {
             t += '<tr><td>colonne : ' + headers[i] + '</td><td>' + cptd[i] + '</td></tr>';
             tot++;
        }
    }

    t += `</tbody>
          </table>`;

    messageEtBoutons('', t, tot, 'nocsv');
} // fin testerSeparateur

/**
 * remplace le séparateur actuel par le séparateur s sélectionné par l'utilisateur
 * @param   {string} s - nouveau caractère séparateur
 * @see     {@link testerSeparateur}
 */
function changerSeparateur(s) {
    "use strict";
    var trad = {
      '|': '|',
      ';': ';',
      ',': ',',
      '.': '.',
      ':': ':',
      '/': '/',
      '\\': '\\',
      '\t': '(tabulation)',
      ' ': '(espace)'
    },

        p = JSON.stringify([ ['nouveau séparateur'.bold() + ' : ' + trad[s]] ]);
    bkp('modifier le séparateur de colonne', p);

    confGlobal.oldsepa = confGlobal.sepa;
    confGlobal.sepa = s;
    dgebi('oldsepa').value = confGlobal.sepa; // attention, ici il s'agit d'indiquer la nouvelle valeur

    message('terminé');
} // fin changerSeparateur

// ..... fin section

///////////////////////////////////////////////////////////////////////////////
//                               INSTANCIATION                               //
///////////////////////////////////////////////////////////////////////////////


// ................................................. gestion des des événements


/**
 * sélection "générique" d'une liste de fichiers
 * @param   {Object} e - événement evt.target ou evt.dataTransfer
 * @see     {@link handleFileSelect}
 * @see     {@link handleFileSelectDnD}
 */
function handleFile(e) {
    "use strict";
    var files = e.files,
        l = e.files.length,
        lf = dgebi('listeFic'),
        i = 0;

    lf.options.length = 0;
//    dgebi('nbfic').innerHTML = files.length === 1 ? '1 fichier' : files.length + ' fichiers'; // @deprecated ?
    for (i = 0; i < l; i++) {
        lf.options[lf.options.length] = new Option(files[i].name, files[i].name);
    }
}

/**
 * sélection d'une liste de fichiers
 * @param   {Object} evt - événement
 * @see     {@link handleFile}
 * @see     {@link handleFileSelectDnD}
 */
function handleFileSelect(evt) {
    "use strict";
    handleFile(evt.target);
}

/**
 * glisser-déposer d'une liste de fichiers
 * @param   {Object} evt - événement
 * @see     {@link handleFile}
 * @see     {@link handleFileSelect}
 */
function handleFileSelectDnD(evt) {
    "use strict";
    handleFile(evt.dataTransfer);
}

/**
 * élément HTML lié au dépôt d'une liste de fichiers
 */

var df = dgebi('action__charge_2');
df.ondragenter = function () {  "use strict"; return false; };
df.ondragover  = function () {  "use strict"; return false; };
df.ondragleave = function () {  "use strict"; return false; };
df.ondrop = function (event) {
    "use strict";
    handleFileSelectDnD(event);
    return false;
};

var df = dgebi('dfiles');
df.addEventListener('change', handleFileSelect, false);

/**
 * sélection d'une nouvelle source comme fichier maître
 */
dgebi('file').onchange = function () {
    "use strict";
    resetmsg();
    orig = this.files[0];
    confGlobal.detectsepa = 'auto';
    displayFile(orig, confChargeCSV, 'previ_csv');
    //    genheaders(conf, 'previ_csv'); // @deprecated, dans displayFile
};

/*
* sélection d'une nouvelle source comme version à comparer
* @see     {@link displayFile}
* @see     {@link genhealders}
*/
dgebi('rapFileB').onchange = function () {
    "use strict";
    resetmsg();
    orig = this.files[0];
    displayFile(orig, confCompVersion, 'cles_rapport');
};

/**
 * sélection d'une nouvelle source comme fichier lookup
 * @see     {@link displayFileLkp}
 * @see     {@link genhealderslkp}
 */
dgebi('filelkp').onchange = function () {
    "use strict";
    origlkp = this.files[0];
    displayFileLkp(origlkp, confChargeLkp);
};

/**
 * inhibe l'événement dragenter du fichier maître
 */
content.ondragenter = function () { "use strict"; return false; };

/**
 * inhibe l'événement dragover du fichier maître
 */
content.ondragover  = function () { "use strict"; return false; };

/**
 * inhibe l'événement dragleave du fichier maître
 */
content.ondragleave = function () { "use strict"; return false; };

/**
 * glisser-déposer d'une nouvelle source comme fichier maître
 */
content.ondrop      = function (event) {
    "use strict";
    resetmsg();
    dgebi('file').value = ""; // réinitialise le nom affiché à côté du champ
    orig = event.dataTransfer.files[0];
    confGlobal.detectsepa = 'auto';
    displayFile(orig, confChargeCSV, 'previ_csv');
    //    genheaders(conf, 'previ_csv'); // @deprecated, dans displayFile
    return false;
};

/**
 * inhibe l'événement dragenter du fichier lookup
 */
contentlkp.ondragenter = function () {  "use strict"; return false; };

/**
 * inhibe l'événement dragover du fichier lookup
 */
contentlkp.ondragover  = function () {  "use strict"; return false; };

/**
 * inhibe événement dragleave du fichier lookup
 */
contentlkp.ondragleave = function () {  "use strict"; return false; };

/**
 * glisser-déposer d'une nouvelle source comme fichier lookup
 * @see     {@link displayFileLkp}
 * @see     {@link genheaderslkp}
 */
contentlkp.ondrop      = function (event) {
    "use strict";
    dgebi('filelkp').value = ""; // réinitialise le nom affiché à côté du champ
    origlkp = event.dataTransfer.files[0];
    displayFileLkp(origlkp, confChargeLkp);
    return false;
};

/**
 * inhibe l'événement dragenter de la version à comparer
 */
contentcomp.ondragenter = function () { "use strict"; return false; };

/**
 * inhibe l'événement dragover de la version à comparer
 */
contentcomp.ondragover  = function () { "use strict"; return false; };

/**
 * inhibe l'événement dragleave de la version à comparer
 */
contentcomp.ondragleave = function () { "use strict"; return false; };

/**
 * glisser-déposer d'une nouvelle source comme ficde la version à comparer
 */
contentcomp.ondrop      = function (event) {
    "use strict";
    resetmsg();
    dgebi('rapFileB').value = ""; // réinitialise le nom affiché à côté du champ
    orig = event.dataTransfer.files[0];
    displayFile(orig, confCompVersion, 'cles_rapport');
    return false;
};

/**
 * demande une confirmation utilisateur pour les événements :
 * - recharger la page (F5)
 * - revenir à la page précédente
 * - fermer l'onglet
 * - quitter le navigateur
 */
window.onbeforeunload = function (e) {
    "use strict";
    return "onbeforeunload";
};


// ................................................................. mode debug


/**
 * affiche dans la console
 *  - l'objet passé en paramètre
 *  - la configuration (objet conf)
 * @param   {Object} o    - objet ou chaîne de caractère pour nommer l'étape
 * @param   {object} conf - configuration
 */
function testConf(o, conf) {
    "use strict";
    console.info(o);
    console.log(conf);
    console.warn(confGlobal);
}


// ............................................................. initialisation


/*
  instancie la "mémoire" de l'historique
*/
rb = new Rollback();


/*
  masque tous les champs de saisie de toutes les fonctionnalités
*/
masquerTout();


confChargeCSV = {
    nom:      "confChargeCSV",
    sepa:     valeur('sepa_chg_csv'),
    sepa_l:   valeur('csv_sepa_l'),
    ign:      (st2float( valeur('csv_ign') ) || 1) -1,
    delim:    valeur('csv_s_delim'),
    fit:      valeur('csv_fitnbcol'),
    encod:    valeur('encod')
};

confChargeListe = {
    nom:      "confChargeListe",
    sepa:     valeur('sepa_chg_liste'),
    sepa_l:   null,
    ign:      0,
    delim:    null,
    fit:      null,
    encod:    null
};

confChargeTexte = {
    nom:      "confChargeTexte",
    sepa:     valeur('sepa_chg_tableau'),
    sepa_l:   valeur('txt_sepa_l'),
    ign:      (st2float( valeur('txt_ign') ) || 1) -1,
    delim:    valeur('txt_s_delim'),
    fit:      valeur('txt_fitnbcol'),
    encod:    null
};

confChargeIdent = {
    nom:      "confChargeIdent",
    sepa:     valeur('sepa_chg_identifiants'),
    sepa_l:   null,
    ign:      0,
    delim:    null,
    fit:      null,
    encod:    null
 };

 confChargeCalendrier = {
     nom:      "confChargeCalendrier",
     sepa:     valeur('sepa_chg_calendrier'),
     sepa_l:   null,
     ign:      0,
     delim:    null,
     fit:      null,
     encod:    null
  };

confChargeLkp = {
    nom:      "confChargeLkp",
    sepa:     valeur('sepalkp'),
    sepa_l:   null,
    ign:      0,
    delim:    0,
    fit:      null,
    encod:    valeur('encodlkp')
};

confCompVersion = {
    nom:      "confCompVersion",
    sepa:     valeur('sepa_rap_diff'),
    sepa_l:   null,
    ign:      0,
    delim:    0,
    fit:      null,
    encod:    valeur('rapEncod')
};

confRecodeListes = {
    nom:      "confRecodeListes",
    sepa:     valeur('val_recode_b_sepa'),
    sepa_l:   null,
    ign:      0,
    delim:    0,
    fit:      null,
    encod:    null
};


/*
  initialise avec les constantes certains textes de l'interface
*/
remplacerInnerHTML('entete_accents_a_o', A_O__AVEC_ACCENT);
remplacerInnerHTML('entete_accents_e_z', E_Z__AVEC_ACCENT);
remplacerInnerHTML('accents_a_o', A_O__AVEC_ACCENT);
remplacerInnerHTML('accents_e_z', E_Z__AVEC_ACCENT);
remplacerInnerHTML('ponctuation', PONCTUATION);
remplacerInnerHTML('availableCharList', AVAILABLE_CHAR_LIST);


/*
   initialise les aperçus des combinaisons d'input {format} et {séparateur} (équivalent à <output>)
*/
                 dgebi('diff_combi').innerHTML =                  valeur('diff_dateformat').split(' ').join(valeur('diff_dateformat_sep'));
              dgebi('decaler_combi').innerHTML =               valeur('decaler_dateformat').split(' ').join(valeur('decaler_dateformat_sep'));
          dgebi('decaler_col_combi').innerHTML =           valeur('decaler_col_dateformat').split(' ').join(valeur('decaler_col_dateformat_sep'));
          dgebi('joursemaine_combi').innerHTML =           valeur('joursemaine_dateformat').split(' ').join(valeur('joursemaine_dateformat_sep'));
           dgebi('chaquejour_combi').innerHTML =            valeur('chaquejour_dateformat').split(' ').join(valeur('chaquejour_dateformat_sep'));
 dgebi('intervalle_completer_combi').innerHTML =  valeur('intervalle_completer_dateformat').split(' ').join(valeur('intervalle_completer_dateformat_sep'));
dgebi('intervalle_rechercher_combi').innerHTML = valeur('intervalle_rechercher_dateformat').split(' ').join(valeur('intervalle_rechercher_dateformat_sep'));
  dgebi('intervalle_combiner_combi').innerHTML =   valeur('intervalle_combiner_dateformat').split(' ').join(valeur('intervalle_combiner_dateformat_sep'));
     dgebi('intervalle_seuil_combi').innerHTML =      valeur('intervalle_seuil_dateformat').split(' ').join(valeur('intervalle_seuil_dateformat_sep'));
dgebi('intervalle_collisions_combi').innerHTML = valeur('intervalle_collisions_dateformat').split(' ').join(valeur('intervalle_collisions_dateformat_sep'));
           dgebi('trier_date_combi').innerHTML =            valeur('trier_date_dateformat').split(' ').join(valeur('trier_date_dateformat_sep'));

// .....  F   I   N
