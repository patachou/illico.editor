#!/usr/bin/env bash

#    Illico Editor - data quality Swiss Army knife
#    Copyright (C) 2025 Arnaud COCHE < i l l i c o . e d i t o r /// n e t - c . f r >
#
#    This program is free software: you can redistribute it
#    and/or modify it under the terms of the GNU General Public
#    License as published by the Free Software Foundation,
#    either version 3 of the License, or (at your option)
#    any later version.
#
#    This program is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied
#    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.
#    See the GNU General Public License for more details.

## variables locales
mainhtml="$ILLICO_HTDOCS/illicoEditor.html"
mainjs="$ILLICO_HTDOCS/scripts/main.js"
maincss="$ILLICO_HTDOCS/css/*.css"


## interface utilisateur

nbTrf=`cat "$mainhtml"  | grep "div id=\"action__"               | wc -l`
nbPar=`cat "$mainhtml"  | grep "<input "  | grep -v "button"    | wc -l`
nbPar2=`cat "$mainhtml" | grep "<select " | grep -v "col__" | wc -l`

echo "INTERFACE UTILISATEUR
------------------------------------------------
nb transformations de données           $nbTrf
nb paramètres (nombre, texte, fichier)  $((nbPar + nbPar2))

"

## nombre de fonctions JS
nbFct=`cat "$ILLICO_HTDOCS"/scripts/*     | grep -E "^function " | wc -l`
nbFcP=`cat "$mainjs"                      | grep -E "^function " | wc -l`

echo "ORGANISATION DU CODE SOURCE
------------------------------------------------
nb fonctions (JS) de 1er niveau         $nbFct
   dont script principal                $nbFcP

"


## longueur du code HTML

nbLig=`cat "$mainhtml"  | wc -l`
nbLgV=`cat "$mainhtml"  | grep -e "^$" | wc -l`
nbLOC=$((nbLig-nbLgV))

nbLig=`cat "$mainhtml"  |                      wc -l`
nbLgV=`cat "$mainhtml"  | grep -e "^$"       | wc -l`
nbLgC=`cat "$mainhtml"  | awk '/<!--/,/-->/' | wc -l`
nbLgVC=`cat "$mainhtml" | awk '/!--/,/-->/'  | grep -e "^$" | wc -l`
nbLOC=$((nbLig-nbLgC+nbLgVC-nbLgV))
nbLgVS=$((nbLgV-nbLgVC))

echo "CODE PRINCIPAL HTML
------------------------------------------------
longueur totale                        $nbLig
-  commentaires                         $nbLgC
-  lignes vides                         $nbLgVS
=  LOC strictes                        $nbLOC

"


## longueur du code JS

nbLig=`cat "$mainjs"    | wc -l`
nbLgV=`cat "$mainjs"    | grep -e "^$"        | wc -l`
nbLgC=`cat "$mainjs"    | awk '/\/\*/,/\*\//' | wc -l`
nbLgC1L=`cat "$mainjs"  | grep -E '^\s*//'    | wc -l`
nbLgVC=`cat "$mainjs"   | awk '/\/\*/,/\*\//' | grep -e "^$" | wc -l`
nbLgVC1L=`cat "$mainjs" | awk '/\/\*/,/\*\//' | grep -E '^\s*//' | wc -l`
nbLgC2=$((nbLgC+nbLgC1L-nbLgVC1L))
nbLgVS=$((nbLgV-nbLgVC))
nbLOC=$((nbLig-nbLgC-nbLgC1L+nbLgVC1L+nbLgVC-nbLgV))

#echo "NB total de lignes	$nbLig"
#echo "NB lig. vides	    	$nbLgV"
#echo "longueur commentaires multi-lignes $nbLgC"
#echo "longueur commentaires 1 ligne $nbLgC1L"
#echo "NB lig. comm. vides	$nbLgVC"
#echo "NB LOC strictes		$nbLOC"

echo "CODE PRINCIPAL JAVASCRIPT
------------------------------------------------
longueur totale                       $nbLig
-  commentaires                        $nbLgC2
-  lignes vides                        $nbLgVS
=  LOC strictes                        $nbLOC

"


nbCSS=`ls -1 $maincss | wc -l`
nbLig=`cat $maincss  | wc -l`
nbLgV=`cat $maincss  | grep -e "^$"        | wc -l`
nbLgC=`cat $maincss  | awk '/\/\*/,/\*\//' | wc -l`
nbLgVC=`cat $maincss | awk '/\/\*/,/\*\//' | grep -e "^$" | wc -l`
nbLOC=$((nbLig-nbLgC+nbLgVC-nbLgV))
nbLgVS=$((nbLgV-nbLgVC))

#echo -e "\n*********** css/*css ***********"
#echo "NB total de lignes	$nbLig"
#echo "NB lig. vides	    	$nbLgV"
#echo "longueur commentaires $nbLgC"
#echo "NB lig. comm. vides	$nbLgVC"
#echo "NB LOC strictes		$nbLOC"

echo "CODE CSS
------------------------------------------------
nb de fichiers CSS                        $nbCSS
longueur totale                         $nbLig
-  commentaires                         $nbLgC
-  lignes vides                         $nbLgVS
=  LOC strictes                         $nbLOC"
