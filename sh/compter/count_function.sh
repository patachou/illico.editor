#!/usr/bin/env bash

#    Illico Editor - data quality Swiss Army knife
#    Copyright (C) 2025 Arnaud COCHE < i l l i c o . e d i t o r /// n e t - c . f r >
#
#    This program is free software: you can redistribute it
#    and/or modify it under the terms of the GNU General Public
#    License as published by the Free Software Foundation,
#    either version 3 of the License, or (at your option)
#    any later version.
#
#    This program is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied
#    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.
#    See the GNU General Public License for more details.


# fichiers temporaires
buf_rep=`mktemp -d /tmp/illico.XXXXXXXX`
buf_lf=`mktemp $buf_rep/liste_fonctions.XXXXXXXX`
buf_nblpf=`mktemp $buf_rep/nbLignes_parFonction.XXXXXXXX`


## récupère le corps de toutes les fonctions de premier niveau
## du script main.js
cat "$ILLICO_HTDOCS/scripts/main.js" | awk /^function/,/^}.*$/ > $buf_lf


## récupère chaque fonction dans un fichier texte séparé
cd $buf_rep
csplit -z -f 'f_' -s -b '%02d' $buf_lf /^function/ {*}


## pour chaque fonction/fichier, compte le nombre de lignes
wc -l f_*         | \
  sort -n         | \
  grep -v "total" | \
  sed 's/^ *//'    > $buf_nblpf


## tableau de correspondance
## fonction -> nb de lignes

echo "     #  fonction                      nb_lignes"
echo "   ...  ............................. ........."

while read ligne
do

  nb=`echo "$ligne"     | cut -f1 -d ' '`
  fic=`echo "$ligne"    | cut -f2 -d ' '`
  fct=`head -n 1 "$fic" | cut -f1 -d '(' | cut -f2 -d ' '`

  #echo "$fct ($fic) = $nb"
  echo $fct    $nb

done < $buf_nblpf | column -t | nl -b a
