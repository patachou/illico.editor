#!/usr/bin/env bash

#    Illico Editor - data quality Swiss Army knife
#    Copyright (C) 2025 Arnaud COCHE < i l l i c o . e d i t o r /// n e t - c . f r >
#
#    This program is free software: you can redistribute it
#    and/or modify it under the terms of the GNU General Public
#    License as published by the Free Software Foundation,
#    either version 3 of the License, or (at your option)
#    any later version.
#
#    This program is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied
#    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.
#    See the GNU General Public License for more details.

## remarque
## --------
##
##     se lance depuis le répertoire ./illicoEditor
##
##

prefixe="25.03"
version="25.03-2"

echo "version recherchée : $version"

cd "$ILLICO_ROOT" >/dev/null

echo -e "\n  --[conf.py]--  \n"
find .                                  -iname conf.py  -exec grep -H -F "$prefixe" {} \;  | grep -v "prefixe"

echo -e "\n  --[.html]--  \n"
find ./htdocs             -maxdepth 1   -iname "*.html" -exec grep -H -F "$prefixe" {} \;  | grep -v "prefixe"

echo -e "\n  --[.js]--  \n"
find ./htdocs/scripts     -maxdepth 1   -iname "*.js"   -exec grep -H -F "$prefixe" {} \;  | grep -v "prefixe"

echo -e "\n  --[zim]--  \n"
find ./zim-illico/                      -iname "*.txt"  -exec grep -H -F "$prefixe" {} \;  | grep -v "prefixe"

echo -e "\n  --[.sh]--  \n"
find ./sh                               -iname "*.sh"   -exec grep -H -F "$prefixe" {} \;  | grep -v "prefixe"

cd - >/dev/null
