#!/usr/bin/env bash

#    Illico Editor - data quality Swiss Army knife
#    Copyright (C) 2025 Arnaud COCHE < i l l i c o . e d i t o r /// n e t - c . f r >
#
#    This program is free software: you can redistribute it
#    and/or modify it under the terms of the GNU General Public
#    License as published by the Free Software Foundation,
#    either version 3 of the License, or (at your option)
#    any later version.
#
#    This program is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied
#    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.
#    See the GNU General Public License for more details.

## dépendances
## ------------
##
##   - pandoc
##   - lmodern
##


echo -e "% Illico Editor\n% Catalogue des transformations\n%\n\n- - - " > "$ILLICO_TEMP/illico_catalogue.md"

cat "$ILLICO_HTDOCS/illicoEditor.html"                                         | \
    sed -n '/<optgroup label="créer un jeu de données"/,/<\/select>/p' | \
    grep -E "(value|label)"                  | \
    tr -s ' '                                | \
    sed 's/, Σ/, somme/g'                    | \
    sed 's/, σ/, écart-type/g'               | \
    sed 's/chiffres clés.*$/chiffres clés/g' | \
    sed 's/.*label="/\n# /g'                 | \
    sed 's/.* value=".*">/  * /g'            | \
    sed 's/<\/option>.*//g'                  | \
    sed 's/".*>/\n/g'                        | \
    sed 's/ \[colonne 1\]$//g'               | \
    sed 's/⇒/=>/g'                           >> "$ILLICO_TEMP/illico_catalogue.md"

pandoc "$ILLICO_TEMP/illico_catalogue.md" \
       --toc \
       -M lang=fr \
       -V lang=fr \
       --number-sections \
       -o "$ILLICO_TEMP/illico_catalogue.pdf"

echo "Le catalogue est enregistré dans $ILLICO_TEMP"
