#!/usr/bin/env bash

#    Illico Editor - data quality Swiss Army knife
#    Copyright (C) 2025 Arnaud COCHE < i l l i c o . e d i t o r /// n e t - c . f r >
#
#    This program is free software: you can redistribute it
#    and/or modify it under the terms of the GNU General Public
#    License as published by the Free Software Foundation,
#    either version 3 of the License, or (at your option)
#    any later version.
#
#    This program is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied
#    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.
#    See the GNU General Public License for more details.

clear

echo "************************"
echo "*    Illico  Wizard    *"
echo "************************"
echo ""

# (.../illicoEditor)
set -a
ILLICO_ROOT=`dirname $PWD`
ILLICO_HTDOCS="$ILLICO_ROOT/htdocs"
ILLICO_JS="$ILLICO_ROOT/htdocs/scripts"
ILLICO_DOC="$ILLICO_ROOT/htdocs/doc"
ILLICO_TUTO="$ILLICO_ROOT/htdocs/tuto"
ILLICO_SH="$ILLICO_ROOT/sh"
ILLICO_TEMP="$ILLICO_ROOT/_TEMP"
set +a

#sh (illicoEditor/sh)
find . -iname "*sh" -exec chmod -R u=rwx {} \;

PS3='
Que voulez-vous faire ?
(aide : touche Enter) '
select choix in \
 "[DOC ] lister les transformations disponibles" \
 "[CTRL] lister les messages intégrés dans le journal de bord" \
 "[CTRL] générer les feuilles CSS" \
 "[CTRL]   (idem 3)   + MAJ compteurs + MAJ la doc HTML" \
 "[Ver.]   (idem 4)   + regénérer doc HTML/LaTeX + packager nouvelle version" \
 "[Ver.] modifier le numéro de version dans tous les scripts" \
 "[EXIT] quitter ce menu/script"

do
  case $REPLY in
    1 ) echo "-> $choix"
        mkdir -p "$ILLICO_TEMP" 2>/dev/null
        bash "$ILLICO_SH/lister/liste_fonctions.sh"
        ;;
    2 ) echo "-> $choix"
        bash "$ILLICO_SH/lister/message_historique.sh"
        ;;
    3 ) echo "-> $choix"
        bash "$ILLICO_SH/MAJ/css/make_css_blob.sh"
        ;;
    4 ) echo "-> $choix"
        bash "$ILLICO_SH/MAJ/doc/MAJ_compteurs.sh"
        bash "$ILLICO_SH/MAJ/doc/makedoc.sh"
        bash "$ILLICO_SH/MAJ/css/make_css_blob.sh"
        ;;
    5 ) echo "-> $choix"
        bash "$ILLICO_SH/MAJ/doc/MAJ_compteurs.sh"
        bash "$ILLICO_SH/MAJ/css/make_css_blob.sh"
        bash "$ILLICO_SH/lister/search.sh"
        echo "----------------------------------------------------------"
        bash "$ILLICO_SH/MAJ/version/release.sh"
        ;;
    6 ) echo "-> $choix"
        bash "$ILLICO_SH/MAJ/version/nextVersion.sh"
        ;;
    7 ) echo "-> $choix"
        echo "Bye."
        exit
        ;;
    * ) echo "choix invalide"
        ;;
  esac
done

## fin du script
