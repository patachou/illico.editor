#!/usr/bin/env bash

#    Illico Editor - data quality Swiss Army knife
#    Copyright (C) 2025 Arnaud COCHE < i l l i c o . e d i t o r /// n e t - c . f r >
#
#    This program is free software: you can redistribute it
#    and/or modify it under the terms of the GNU General Public
#    License as published by the Free Software Foundation,
#    either version 3 of the License, or (at your option)
#    any later version.
#
#    This program is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied
#    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.
#    See the GNU General Public License for more details.


auto_ver=`date +%y.%m`

last_ver=`git tag | tac | head -n 1 | cut -f2 -d 'v' | cut -f1 -d '-'`
last_inc=`git tag | tac | head -n 1 | cut -f2 -d 'v' | cut -f2 -d '-'`


if [ $auto_ver = $last_ver ]
then
  # nouvelle version dans le même mois, alors prendre last_ver + next_inc = 1
  next_ver="$last_ver"
  next_inc=$((last_inc + 1))
else
  # changement de mois (ou d'année)
  next_ver="$auto_ver"
  next_inc=1
fi


# versions
old_version="'$last_ver'"
new_version="'$next_ver'"
#echo "old_version : $old_version"
#echo "new_version : $new_version"

old_version2="$last_ver"
new_version2="$next_ver"
#echo "old_version2 : $old_version2"
#echo "new_version2 : $new_version2"

old_release="$last_ver-$last_inc"
new_release="$next_ver-$next_inc"
#echo "old_release : $old_release"
#echo "new_release : $new_release"


# mise à jour des documentations Sphinx (doc, tuto)
for i in "$ILLICO_HTDOCS/doc/source/conf.py $ILLICO_HTDOCS/tuto/source/conf.py"
do
  sed -i "s/$old_release/$new_release/g"   $i
  sed -i "s/$old_version/$new_version/g"   $i
done


# mise à jour des fichiers html
for i in "$ILLICO_HTDOCS/illicoEditor.html $ILLICO_HTDOCS/index.html"
do
  sed -i "s/$old_release/$new_release/g"   $i
done


# mise à jour des fichiers JS
for i in "$ILLICO_HTDOCS/scripts/version.js"
do
  sed -i "s/$old_release/$new_release/g"   $i
done


# mise à jour des scripts Bash
for i in "$ILLICO_ROOT/sh/MAJ/version/release.sh $ILLICO_ROOT/sh/lister/search.sh"
do
  sed -i "s/$old_release/$new_release/g"   $i
  sed -i "s/$old_version2/$new_version2/g" $i
done
