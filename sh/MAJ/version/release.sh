#!/usr/bin/env bash

#    Illico Editor - data quality Swiss Army knife
#    Copyright (C) 2025 Arnaud COCHE < i l l i c o . e d i t o r /// n e t - c . f r >
#
#    This program is free software: you can redistribute it
#    and/or modify it under the terms of the GNU General Public
#    License as published by the Free Software Foundation,
#    either version 3 of the License, or (at your option)
#    any later version.
#
#    This program is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied
#    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.
#    See the GNU General Public License for more details.

## actions
## -------
##
##   - met à jour la documentation utilisateur (HTML/PDF via LaTeX)
##   - met à jour les tutoriels (HTML/PDF via LaTeX)
##   - construit le package pour la dernière version à télécharger
##   - prépare le package pour mettre à jour le site Web officiel
##
##
## dépendances
## -----------
##
##   - sphinx (-> pandoc ?)
##   - zip
##   - latex
##

# variables locales
ver="illicoEditor_v25.03-2"
dest="${ILLICO_TEMP}/$ver"
web="${ILLICO_TEMP}/${ver}_web"
latest="illicoEditor_latest"

# donne le temps d'arrêter le script si le numéro de version est incorrect
echo    "nouvelle version          $dest"
echo    "(ctrl-C pour annuler)"

echo -n  "10.."
sleep 1
for i in {9..1}
do
  echo -ne "\r$i..."
  sleep 1
done
echo ""

# corrige les droits sur les fichiers
echo -n "`date +%T`  1/10 correction des droits sur les fichiers..."

#illicoEditor (root)
chmod -R u=rwX,g=r,o=r "$ILLICO_HTDOCS"              # pour les compes utilisateurs "classiques"
find "$ILLICO_HTDOCS" -type d -exec chmod og+x {} \; # pour le compte utilisateur Web (www-data/apache)
echo "OK"


# supprime les fichiers index Mac/Apple
echo -n "`date +%T`  2/10 suppression des fichiers index Mac......."
find "$ILLICO_ROOT"  -iname   .DS_Store  -delete
find "$ILLICO_ROOT"  -iname ._.DS_Store  -delete
echo "OK"


# crée les répertoires de travail temporaires
echo  -n "`date +%T`  3/10 création du répertoire de travail........"
rm   -fr "$dest" "$web" 2>/dev/null
mkdir -p "$dest"
mkdir -p "$web"
echo "OK"


# suppression anciennes documentations générées
echo  -n "`date +%T`  4/10 suppression des anciens répertoires......"
rm   -fr "$ILLICO_DOC/build" "$ILLICO_TUTO/build"
echo "OK"


# génération Sphinx HTML
echo -n "`date +%T`  5/10 mise à jour HTML (doc, tuto)............."

dohtml() {
  cd "$1"
  make html > /dev/null 2> /dev/null
}
export -f dohtml
(dohtml $ILLICO_DOC)  &
(dohtml $ILLICO_TUTO) &
wait

echo "OK"


# génération Sphinx LaTeX
echo -n "`date +%T`  6/10 mise à jour LaTeX (doc, tuto)............"

dolatex() {
  cd "$1"
  make latexpdf > /dev/null 2> /dev/null
  find build/latex -not -iname "*pdf" -delete 2> /dev/null
}
export -f dolatex
(dolatex $ILLICO_DOC)  &
(dolatex $ILLICO_TUTO) &
wait

echo "OK"

# prépare les archives des versions
echo -n "`date +%T`  7/10 préparation des archives................."
cp -r "$ILLICO_HTDOCS/css" \
      "$ILLICO_HTDOCS/doc" \
      "$ILLICO_HTDOCS/illicoEditor.html" \
      "$ILLICO_HTDOCS/index.html" \
      "$ILLICO_HTDOCS/scripts"    \
      "$ILLICO_HTDOCS/tuto"       \
      "$dest"
cd "$ILLICO_TEMP"
tar -zcf ${ver}.tar.gz ${ver}/
zip -qr  ${ver}.zip    ${ver}
cd - >/dev/null
echo "OK"


# déplace et renomme les archives
echo -n "`date +%T`  8/10 bon à archiver..........................."
rm -fr "$ILLICO_HTDOCS/releases"
mkdir  "$ILLICO_HTDOCS/releases"
mv ${dest}.tar.gz   "${ILLICO_HTDOCS}/releases/${latest}.tar.gz"
mv ${dest}.zip      "${ILLICO_HTDOCS}/releases/${latest}.zip"
md5sum "${ILLICO_HTDOCS}"/releases/* > "${ILLICO_HTDOCS}/releases/MD5SUM.txt"
echo "OK"


# prépare l'archive pour le site Web
echo -n "`date +%T`  9/10 préparation site Web....................."
cp -r "$ILLICO_ROOT/captures"     \
      "$ILLICO_HTDOCS/css"        \
      "$ILLICO_HTDOCS/doc"        \
      "$ILLICO_HTDOCS/illicoEditor.html" \
      "$ILLICO_HTDOCS/index.html"        \
      "$ILLICO_HTDOCS/releases"   \
      "$ILLICO_HTDOCS/robots.txt" \
      "$ILLICO_HTDOCS/scripts"    \
      "$ILLICO_HTDOCS/tuto"       \
      "$ILLICO_HTDOCS/.htaccess"  \
      "$web"
cd "$ILLICO_TEMP"
tar -zcf ${ver}_web.tar.gz ${ver}_web/
cd - >/dev/null
echo "OK"


# supprime les répertoires de travail temporaires
echo -n "`date +%T` 10/10 nettoyage................................"
rm   -fr "$dest" "$web" 2>/dev/null
echo "OK"


# FIN
