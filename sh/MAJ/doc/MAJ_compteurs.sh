#!/usr/bin/env bash

#    Illico Editor - data quality Swiss Army knife
#    Copyright (C) 2025 Arnaud COCHE < i l l i c o . e d i t o r /// n e t - c . f r >
#
#    This program is free software: you can redistribute it
#    and/or modify it under the terms of the GNU General Public
#    License as published by the Free Software Foundation,
#    either version 3 of the License, or (at your option)
#    any later version.
#
#    This program is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied
#    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.
#    See the GNU General Public License for more details.

bash "$ILLICO_SH/lister/toc_main.sh"          > "$ILLICO_DOC/source/dev/toc_script_principal.txt"
bash "$ILLICO_SH/compter/count.sh"            > "$ILLICO_DOC/source/dev/count.txt"
bash "$ILLICO_SH/compter/count_function.sh"   > "$ILLICO_DOC/source/dev/count_function.txt"
