#!/usr/bin/env bash

#    Illico Editor - data quality Swiss Army knife
#    Copyright (C) 2025 Arnaud COCHE < i l l i c o . e d i t o r /// n e t - c . f r >
#
#    This program is free software: you can redistribute it
#    and/or modify it under the terms of the GNU General Public
#    License as published by the Free Software Foundation,
#    either version 3 of the License, or (at your option)
#    any later version.
#
#    This program is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied
#    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.
#    See the GNU General Public License for more details.

# variables locales
src="$ILLICO_HTDOCS/css/style.css"
dest="$ILLICO_HTDOCS/scripts/css_msg.js"

echo    'var css_msg = `'            > "$dest"

cat "$src" | \
awk     '/DEBUT MSG/,/FIN MSG/'     >> "$dest"

echo    '`;'                        >> "$dest"


# MAJ css_histo
dest="$ILLICO_HTDOCS/scripts/css_histo.js"

echo    'var css_histo = `'          > "$dest"

cat "$src" | \
awk     '/DEBUT HISTO/,/FIN HISTO/' >> "$dest"

echo    '`;'                        >> "$dest"


# MAJ css_histo
dest="$ILLICO_HTDOCS/scripts/css_export.js"

echo    'var css_export = `'         > "$dest"
cat "$src" | \
awk '/DEBUT EXPORT/,/FIN EXPORT/'   >> "$dest"

echo    '`;'                        >> "$dest"
